/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.valueformatter;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.stream.Collectors.joining;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

/**
 * Value formatter for {@link Iterable}.
 */
public class IterableValueFormatter implements ValueFormatter<Iterable<?>> {

   /**
    * Maximum number of iterable elements to display. Set to 0 to display all elements.
    */
   private static final int DEFAULT_MAX_ITERABLE_ITEMS = 20;

   /**
    * Max items to display. Set to 0 to disable the limitation.
    */
   private final int maxIterableItems;
   private final ValueFormatter<Object> itemMapper;

   public IterableValueFormatter(int maxIterableItems, ValueFormatter<Object> itemMapper) {
      this.maxIterableItems = maxIterableItems;
      this.itemMapper = itemMapper;
   }

   public IterableValueFormatter(int maxIterableItems) {
      this(maxIterableItems, defaultValueFormatter());
   }

   public IterableValueFormatter(ValueFormatter<Object> itemMapper) {
      this(DEFAULT_MAX_ITERABLE_ITEMS, itemMapper);
   }

   public IterableValueFormatter() {
      this(DEFAULT_MAX_ITERABLE_ITEMS, defaultValueFormatter());
   }

   @Override
   public String apply(Iterable<?> iterable) {
      if (iterable == null) {
         return null;
      }
      AtomicInteger i = new AtomicInteger();
      return StreamSupport
                   .stream(iterable.spliterator(), false)
                   .limit(maxIterableItems == 0 ? Long.MAX_VALUE : maxIterableItems)
                   .map(o -> nullable(o)
                         .flatMap(oo -> nullable(itemMapper.apply(oo)))
                         .map(Object::toString)
                         .orElse(null))
                   .peek(o -> i.getAndIncrement())
                   .collect(joining(",", "[", "")) + (maxIterableItems > 0 && i.get() >= maxIterableItems
                                                      ? ",...]"
                                                      : "]");
   }

   @SuppressWarnings("unchecked")
   private static <T> ValueFormatter<T> defaultValueFormatter() {
      return (ValueFormatter<T>) new StringValueFormatter();
   }

}
