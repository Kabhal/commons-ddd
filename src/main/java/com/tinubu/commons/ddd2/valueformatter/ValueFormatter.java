/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.valueformatter;

import java.util.function.Function;

import com.tinubu.commons.ddd2.invariant.MessageValue;

/**
 * Formatter for message values.
 * These formatters are suitable for {@link MessageValue#validatingObjectValue(Function)}
 * mapping function.
 * <p>
 * You must override {@link #apply(Object)} method in implementation classes.
 *
 * @see MessageValue
 */
@FunctionalInterface
public interface ValueFormatter<T> extends Function<T, Object> {

   /**
    * Formats value.
    *
    * @param value value
    *
    * @return formatted value
    *
    * @apiNote If value is {@code null}, the formatter implementation must return {@code null}.
    */
   default Object format(T value) {
      return apply(value);
   }

}
