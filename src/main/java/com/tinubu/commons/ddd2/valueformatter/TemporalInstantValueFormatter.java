/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.valueformatter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;

/**
 * Representation string of an instant in time formatter for {@link Temporal}.
 */
public class TemporalInstantValueFormatter implements ValueFormatter<Temporal> {

   @Override
   public String apply(Temporal temporal) {
      String result;
      if (temporal == null) {
         result = null;
      } else if (temporal instanceof OffsetDateTime) {
         result = DateTimeFormatter.ISO_INSTANT.format(temporal);
      } else if (temporal instanceof ZonedDateTime) {
         result = DateTimeFormatter.ISO_INSTANT.format(temporal);
      } else if (temporal instanceof LocalDateTime) {
         result = DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(temporal);
      } else if (temporal instanceof LocalDate) {
         result = DateTimeFormatter.ISO_LOCAL_DATE.format(temporal);
      } else if (temporal instanceof Instant) {
         result = DateTimeFormatter.ISO_INSTANT.format(temporal);
      } else {
         throw new IllegalArgumentException(
               String.format("Unsupported '%s' temporal type", temporal.getClass().getName()));
      }
      return result;
   }

}
