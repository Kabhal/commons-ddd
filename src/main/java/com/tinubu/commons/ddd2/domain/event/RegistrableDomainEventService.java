/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.event;

/**
 * Registrable {@link DomainEventService}. Any event service with ability to register/unregister events is
 * registrable.
 *
 * @apiNote No "unregister all" operation is available as this considered an "administration" operation
 *       and no all implementations will be able to provide it.
 */
public interface RegistrableDomainEventService extends DomainEventService {

   /**
    * Register a new {@link DomainEventListener} into the registry.
    *
    * @param eventClass supported event class
    * @param eventListener event listener
    * @param <E> supported event type
    */
   <E extends DomainEvent> void registerEventListener(Class<E> eventClass,
                                                      DomainEventListener<? super E> eventListener);

   /**
    * Unregisters all event listeners.
    */
   void unregisterEventListeners();

}
