package com.tinubu.commons.ddd2.domain.ids;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.string;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.fileName;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.bytesLength;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.tinubu.commons.ddd2.domain.type.AbstractSimpleId;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.rules.BaseRules;
import com.tinubu.commons.ddd2.invariant.rules.ComparableRules;

/**
 * A DDD {@link Id} represented as a {@link Path}.
 * Valid paths (when set) :
 * <ul>
 *    <li>must have at least one name component</li>
 *    <li>can be relative or absolute (e.g.: "path" or "/path")</li>
 *    <li>names cannot be blank</li>
 *    <li>value cannot have more than {@value #MAX_VALUE_LENGTH} bytes total (caution {@link String#length()} is in chars)</li>
 *    <li>each value path element, including last element, cannot have more than {@value #MAX_PATH_ELEMENT_LENGTH} bytes (caution {@link String#length()} is in chars)</li>
 * </ul>
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class PathId extends AbstractSimpleId<Path> {
   protected static final int MAX_VALUE_LENGTH = 1024;
   protected static final int MAX_PATH_ELEMENT_LENGTH = 255;
   protected static final String URN_ID_TYPE = "path";

   protected final boolean relativePath;

   /**
    * Builds {@link PathId} from specified path.
    *
    * @param value optional path value
    * @param relativePath enforce relative path. Path can be absolute or relative if set
    *       to {@link false}
    * @param newObject new object status
    */
   protected PathId(Path value, boolean relativePath, boolean newObject) {
      super(normalize(value), newObject);
      this.relativePath = relativePath;
   }

   /**
    * Builds {@link PathId} from specified path.
    *
    * @param value optional path value
    * @param relativePath enforce relative path
    */
   protected PathId(Path value, boolean relativePath) {
      super(normalize(value));
      this.relativePath = relativePath;
   }

   /** Builds {@link PathId} with no value (auto-generated id). */
   protected PathId(boolean relativePath) {
      this.relativePath = relativePath;
   }

   /**
    * Creates a new {@link PathId} for a reconstituting object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static PathId of(Path value, boolean relativePath) {
      return checkInvariants(new PathId(value, relativePath));
   }

   /**
    * Creates a new {@link PathId} for a reconstituting object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static PathId of(URI urn, boolean relativePath) {
      return of(nullable(urn).map(u -> Paths.get(idUrnValue(u, URN_ID_TYPE))).orElse(null), relativePath);
   }

   /**
    * Creates a new {@link PathId} for a new object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static PathId ofNewObject(Path value, boolean relativePath) {
      return checkInvariants(new PathId(value, relativePath, true));
   }

   /**
    * Creates a new {@link PathId} for a new object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static PathId ofNewObject(URI urn, boolean relativePath) {
      return ofNewObject(nullable(urn).map(u -> Paths.get(idUrnValue(u, URN_ID_TYPE))).orElse(null),
                         relativePath);
   }

   /**
    * Creates a new {@link PathId} for a new object with uninitialized value.
    *
    * @return new id
    */
   public static PathId ofNewObject(boolean relativePath) {
      return checkInvariants(new PathId(relativePath));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends PathId> defineDomainFields() {
      return Fields
            .<PathId>builder()
            .superFields((Fields<PathId>) super.defineDomainFields())
            .field("value",
                   AbstractSimpleId::value,
                   isNull().orValue(isNotEmpty()
                                          .andValue(fileName(string(isNotBlank())))
                                          .andValue(isNotAbsolute().ifIsSatisfied(() -> relativePath))
                                          .andValue(hasNoTraversal())
                                          .andValue(hasCorrectLength())))
            .field("relativePath", v -> v.relativePath)
            .build();
   }

   private InvariantRule<Path> hasCorrectLength() {
      ParameterValue<Integer> maxPathElementLength =
            ParameterValue.value(MAX_PATH_ELEMENT_LENGTH, "MAX_PATH_ELEMENT_LENGTH");
      ParameterValue<Integer> maxValueLength = ParameterValue.value(MAX_VALUE_LENGTH, "MAX_VALUE_LENGTH");

      return BaseRules
            .<Path>isNotNull()
            .andValue(allSatisfies(string(bytesLength(ComparableRules.isLessThanOrEqualTo(maxPathElementLength)))))
            .andValue(string(bytesLength(ComparableRules.isLessThanOrEqualTo(maxValueLength))));
   }

   @Override
   public PathId value(Path value) {
      return checkInvariants(new PathId(value, relativePath, newObject));
   }

   public PathId value(String first, String... more) {
      return value(Paths.get(first, more));
   }

   @Override
   public URI urnValue() {
      return idUrn("path");
   }

   protected static Path normalize(Path value) {
      return value == null ? null : value.normalize();
   }

}
