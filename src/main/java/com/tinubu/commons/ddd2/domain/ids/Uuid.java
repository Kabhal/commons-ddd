package com.tinubu.commons.ddd2.domain.ids;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.ValidatingObject.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.UrnRules.isUrn;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.UrnRules.nidIsEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.isUuid;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

import com.tinubu.commons.ddd2.domain.type.AbstractSimpleId;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.ParameterValue;

/**
 * A DDD {@link Id} represented as a valid RFC4122 UUID string.
 * Uppercase Uuid format is supported, but will be lower-cased internally.
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class Uuid extends AbstractSimpleId<String> {

   protected static final Uuid NIL_UUID = ofNewObject(new UUID(0, 0));
   protected static final String URN_ID_TYPE = "uuid";

   protected Uuid(String value, boolean newObject) {
      super(normalize(value), newObject);
   }

   protected Uuid(String value) {
      super(normalize(value));
   }

   protected Uuid() {
      super();
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends Uuid> defineDomainFields() {
      return Fields
            .<Uuid>builder()
            .superFields((Fields<Uuid>) super.defineDomainFields())
            .field("value", v -> v.value, isNull().orValue(isUuid()))
            .build();
   }

   /**
    * Creates a new {@link Uuid} for a reconstituting object with specified UUID string.
    * Supported UUID versions 1, 2, 3, 4, 5.
    *
    * @param uuid UUID string representation
    *
    * @return new id
    */
   public static Uuid of(String uuid) {
      return checkInvariants(new Uuid(uuid));
   }

   /**
    * Creates a new {@link Uuid} for a reconstituting object with specified URN.
    * Supports both {@code urn:id:uuid:<id-value>} and official {@code urn:uuid:<id-value>} formats.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static Uuid of(URI urn) {
      if (urn == null) {
         return of((String) null);
      }

      if (validatingObject(urn).satisfies(isUrn().and(nidIsEqualTo(ParameterValue.value("id"))))) {
         return of(idUrnValue(urn, URN_ID_TYPE));
      } else {
         return of(urnValue(urn));
      }
   }

   /**
    * Creates a new {@link Uuid} for a new object with specified UUID string.
    * Supported UUID versions 1, 2, 3, 4, 5.
    *
    * @param uuid UUID string representation
    *
    * @return new id
    */
   public static Uuid ofNewObject(String uuid) {
      return checkInvariants(new Uuid(uuid, true));
   }

   /**
    * Creates a new {@link Uuid} for a new object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static Uuid ofNewObject(URI urn) {
      if (urn == null) {
         return ofNewObject((String) null);
      }

      if (validatingObject(urn).satisfies(isUrn().and(nidIsEqualTo(ParameterValue.value("id"))))) {
         return ofNewObject(idUrnValue(urn, URN_ID_TYPE));
      } else {
         return ofNewObject(urnValue(urn));
      }
   }

   /**
    * Creates a new {@link Uuid} for a new object with specified {@link UUID}.
    *
    * @param uuid UUID string representation
    *
    * @return new id
    */
   public static Uuid ofNewObject(UUID uuid) {
      notNull(uuid, "uuid");

      return ofNewObject(uuid.toString());
   }

   /**
    * Creates a new {@link Uuid} for a new object with uninitialized value.
    *
    * @return new id
    */
   public static Uuid ofNewObject() {
      return checkInvariants(new Uuid());
   }

   /**
    * Returns "nil" {@link Uuid}.
    *
    * @return nil UUID instance
    */
   public static Uuid nilUuid() {
      return NIL_UUID;
   }

   /**
    * Creates a new {@link Uuid} for a new object with a time-based generated UUID (version 1).
    *
    * @return new id
    */
   public static Uuid newUuidV1() {
      return ofNewObject(version1Uuid());
   }

   /**
    * Alias for {@link #newUuidV1()}
    *
    * @return new id
    */
   public static Uuid newTimeBasedUuid() {
      return newUuidV1();
   }

   /**
    * Creates a new {@link Uuid} for a new object with a name-based UUID (version 3) using MD5.
    * Created id will always be the same for the same input {@code name}.
    *
    * @param name UUID base name to hash
    *
    * @return new id
    */
   public static Uuid newUuidV3(byte[] name) {
      notNull(name, "name");

      return ofNewObject(UUID.nameUUIDFromBytes(name));
   }

   /**
    * Creates a new {@link Uuid} for a new object with a name-based UUID (version 3) using MD5.
    * Created id will always be the same for the same input {@code name}.
    *
    * @param name UUID base name to hash
    *
    * @return new id
    */
   public static Uuid newUuidV3(String name) {
      notBlank(name, "name");

      return newUuidV3(name.getBytes(StandardCharsets.UTF_8));
   }

   /**
    * Alias for {@link #newUuidV3(byte[])}
    *
    * @return new id
    */
   public static Uuid newNameBasedMd5Uuid(byte[] name) {
      return newUuidV3(name);
   }

   /**
    * Alias for {@link #newUuidV3(String)}
    *
    * @return new id
    */
   public static Uuid newNameBasedMd5Uuid(String name) {
      return newUuidV3(name);
   }

   /**
    * Creates a new {@link Uuid} for a new object with a randomly generated UUID (version 4).
    *
    * @return new id
    */
   public static Uuid newUuidV4() {
      return ofNewObject(UUID.randomUUID());
   }

   /**
    * Alias for {@link #newUuidV4()}
    *
    * @return new id
    */
   public static Uuid newRandomUuid() {
      return newUuidV4();
   }

   /**
    * Creates a new {@link Uuid} for a new object with a name-based UUID (version 5) using SHA-1.
    * Created id will always be the same for the same input {@code name}.
    *
    * @param name UUID base name to hash
    *
    * @return new id
    */
   public static Uuid newUuidV5(byte[] name) {
      notNull(name, "name");

      return ofNewObject(version5Uuid(name));
   }

   /**
    * Creates a new {@link Uuid} for a new object with a name-based UUID (version 5) using SHA-1.
    * Created id will always be the same for the same input {@code name}.
    *
    * @param name UUID base name to hash
    *
    * @return new id
    */
   public static Uuid newUuidV5(String name) {
      notBlank(name, "name");

      return newUuidV5(name.getBytes(StandardCharsets.UTF_8));
   }

   /**
    * Alias for {@link #newUuidV5(byte[])}
    *
    * @return new id
    */
   public static Uuid newNameBasedSha1Uuid(byte[] name) {
      return newUuidV5(name);
   }

   /**
    * Alias for {@link #newUuidV5(String)}
    *
    * @return new id
    */
   public static Uuid newNameBasedSha1Uuid(String name) {
      return newUuidV5(name);
   }

   @Override
   public Uuid value(String value) {
      return checkInvariants(new Uuid(value, newObject));
   }

   protected static String normalize(String uuid) {
      return uuid != null ? uuid.toLowerCase() : uuid;
   }

   /**
    * {@inheritDoc}
    *
    * @see <a href="https://tools.ietf.org/html/rfc4122">RFC 4122 : A UUID URN namespace</a>
    */
   @Override
   public URI urnValue() {
      try {
         return new URI("urn", URN_ID_TYPE + ":" + value, null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Parses UUID URN with format {@code urn:uuid:<id-value>}.
    *
    * @param urn UUID URN
    *
    * @return UUID value
    *
    * @throws IllegalArgumentException if URN syntax is invalid
    * @see <a href="https://tools.ietf.org/html/rfc4122">RFC 4122 : A UUID URN namespace</a>
    */
   protected static String urnValue(URI urn) {
      validatingObject(urn, "urn").validate(isValidUrnSyntax()).orThrow();

      return urn.getSchemeSpecificPart().split(":", 2)[1];
   }

   protected static InvariantRule<URI> isValidUrnSyntax() {
      return isUrn().andValue(nidIsEqualTo(ParameterValue.value(URN_ID_TYPE)));
   }

   /**
    * @see <a
    *       href="https://github.com/eugenp/tutorials/tree/master/core-java-modules/core-java-uuid/src/main/java/com/baeldung/uuid">Baeldung
    *       repository</a>
    */
   private static UUID version1Uuid() {
      Random random = new Random();
      long random63BitLong = random.nextLong() & 0x3FFFFFFFFFFFFFFFL;
      long variant3BitFlag = 0x8000000000000000L;
      long version1LeastSigBits = random63BitLong + variant3BitFlag;

      LocalDateTime start = LocalDateTime.of(1582, 10, 15, 0, 0, 0);
      Duration duration = Duration.between(start, LocalDateTime.now());
      long seconds = duration.getSeconds();
      long nanos = duration.getNano();
      long timeForUuidIn100Nanos = seconds * 10000000 + nanos * 100;
      long least12SignificantBitOfTime = (timeForUuidIn100Nanos & 0x000000000000FFFFL) >> 4;
      long version = 1 << 12;
      long version1MostSigBits =
            (timeForUuidIn100Nanos & 0xFFFFFFFFFFFF0000L) + version + least12SignificantBitOfTime;

      return new UUID(version1MostSigBits, version1LeastSigBits);
   }

   /**
    * @see <a
    *       href="https://github.com/eugenp/tutorials/tree/master/core-java-modules/core-java-uuid/src/main/java/com/baeldung/uuid">Baeldung
    *       repository</a>
    */
   private static UUID version5Uuid(byte[] name) {
      notNull(name, "name");

      final MessageDigest md;
      try {
         md = MessageDigest.getInstance("SHA-1");
      } catch (NoSuchAlgorithmException exception) {
         throw new IllegalStateException("SHA-1 not supported", exception);
      }
      final byte[] bytes = Arrays.copyOfRange(md.digest(name), 0, 16);
      bytes[6] &= 0x0f; /* clear version        */
      bytes[6] |= 0x50; /* set to version 5     */
      bytes[8] &= 0x3f; /* clear variant        */
      bytes[8] |= 0x80; /* set to IETF variant  */
      long msb = 0;
      long lsb = 0;
      assert bytes.length == 16 : "data must be 16 bytes in length";

      for (int i = 0; i < 8; i++) {
         msb = (msb << 8) | (bytes[i] & 0xff);
      }
      for (int i = 8; i < 16; i++) {
         lsb = (lsb << 8) | (bytes[i] & 0xff);
      }
      return new UUID(msb, lsb);
   }

}
