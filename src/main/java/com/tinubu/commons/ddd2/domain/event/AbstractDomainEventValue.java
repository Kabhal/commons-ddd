/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.event;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.time.Instant;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.lang.datetime.ApplicationClock;

/**
 * Abstract {@link Value}-backed {@link DomainEvent} implementation.
 */
public abstract class AbstractDomainEventValue extends AbstractValue implements DomainEvent {

   protected final Object source;
   protected final Instant eventDate;

   /**
    * Constructs a prototypical event.
    *
    * @param source object on which the event initially occurred
    * @param eventDate event creation/publish date
    */
   protected AbstractDomainEventValue(Object source, Instant eventDate) {
      this.source = source;
      this.eventDate = eventDate;
   }

   /**
    * Constructs a prototypical event.
    *
    * @param source object on which the event initially occurred
    */
   protected AbstractDomainEventValue(Object source) {
      this(source, ApplicationClock.nowAsInstant());
   }

   @Override
   protected Fields<? extends AbstractDomainEventValue> defineDomainFields() {
      return Fields
            .<AbstractDomainEventValue>builder()
            .field("source", v -> v.source, isNotNull())
            .field("eventDate", v -> v.eventDate, isNotNull())
            .build();
   }

   @Override
   public Object source() {
      return source;
   }

   /**
    * Event creation/publish date.
    *
    * @return event creation/publish date
    */
   public Instant eventDate() {
      return eventDate;
   }
}
