package com.tinubu.commons.ddd2.domain.type.collection;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.Collections.emptyList;

import java.util.AbstractList;
import java.util.List;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.domain.type.support.CachedFields;
import com.tinubu.commons.ddd2.valueformatter.IterableValueFormatter;

/**
 * Generic immutable values collection.
 * <p>
 * The collection itself is a {@link Value}.
 * {@code null} elements in list are not supported.
 *
 * @param <T> value type
 *
 * @implSpec Immutable class implementation.
 */
public class ImmutableValueList<T extends Value> extends AbstractList<T> implements Value {

   protected final List<T> elements;

   protected ImmutableValueList(List<T> values) {
      this.elements = immutable(list(values));
   }

   protected Fields<? extends Value> defineDomainFields() {
      return Fields
            .<ImmutableValueList<T>>builder()
            .field("elements", v -> v.elements, new IterableValueFormatter(), hasNoNullElements())
            .build();
   }

   protected ImmutableValueList() {
      this(emptyList());
   }

   public static <T extends Value> ImmutableValueList<T> ofValues(List<T> values) {
      return checkInvariants(new ImmutableValueList<>(values));
   }

   @SafeVarargs
   public static <T extends Value> ImmutableValueList<T> ofValues(T... values) {
      return checkInvariants(new ImmutableValueList<>(list(values)));
   }

   public static <T extends Value> ImmutableValueList<T> empty() {
      return checkInvariants(new ImmutableValueList<>());
   }

   @Override
   public T get(int index) {
      return elements.get(index);
   }

   @Override
   public int size() {
      return elements.size();
   }

   protected final CachedFields<? extends Value> domainFields = new CachedFields<>(this::defineDomainFields);

   @Override
   public Fields<? extends Value> domainFields() {
      return domainFields.get();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

}
