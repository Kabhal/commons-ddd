package com.tinubu.commons.ddd2.domain.specification;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Specification interface.
 *
 * @param <T> domain object type
 *
 * @apiNote Specification implements {@link Predicate} for better Java integration. Predicate logic
 *       operations (and, or, negate) only apply to current specification evaluation, use {@link
 *       CompositeSpecification} to compose several specifications.
 */
@FunctionalInterface
public interface Specification<T> extends Predicate<T> {

   /**
    * Checks if {@code object} is satisfied by the specification.
    *
    * @param object domain object to check
    *
    * @return {@code true} if {@code object} satisfies the specification
    */
   boolean satisfiedBy(T object);

   /**
    * Checks if any {@code object} is satisfied by the specification.
    *
    * @param objects domain objects to check
    *
    * @return {@code true} if any {@code object} satisfies the specification
    */
   default boolean satisfiedByAny(final Stream<T> objects) {
      notNull(objects, "objects");

      return objects.anyMatch(this::satisfiedBy);
   }

   /**
    * Checks if all {@code object} is satisfied by the specification.
    *
    * @param objects domain objects to check
    *
    * @return {@code true} if all {@code object} satisfies the specification
    */
   default boolean satisfiedByAll(final Stream<T> objects) {
      notNull(objects, "objects");

      return objects.allMatch(this::satisfiedBy);
   }

   /**
    * Checks if none {@code object} is satisfied by the specification.
    *
    * @param objects domain objects to check
    *
    * @return {@code true} if none {@code object} satisfies the specification
    */
   default boolean satisfiedByNone(final Stream<T> objects) {
      notNull(objects, "objects");

      return objects.noneMatch(this::satisfiedBy);
   }

   /**
    * Links Predicate implementation to Specification's {@link #satisfiedBy(Object)}.
    *
    * @param object object
    *
    * @return predicate evaluation result
    */
   @Override
   default boolean test(T object) {
      return satisfiedBy(object);
   }

   /**
    * Predicate's {@link Predicate#negate()} covariant override.
    *
    * @return this specification
    */
   @Override
   default Specification<T> negate() {
      return object -> !test(object);
   }

   /**
    * Predicate's {@link Predicate#and(Predicate)} covariant override.
    *
    * @param other predicate to compose
    *
    * @return this specification
    */
   @Override
   default Specification<T> and(final Predicate<? super T> other) {
      notNull(other, "other");

      return object -> test(object) && other.test(object);
   }

   /**
    * Predicate's {@link Predicate#or(Predicate)} covariant override.
    *
    * @param other predicate to compose
    *
    * @return this specification
    */
   @Override
   default Specification<T> or(final Predicate<? super T> other) {
      notNull(other, "other");

      return object -> test(object) || other.test(object);
   }

   /**
    * Returns a specification that is the negation of the supplied specification.
    * This is accomplished by returning result of the calling
    * {@code target.negate()}.
    *
    * @param <T> the type of arguments to the specified specification
    * @param target specification to negate
    *
    * @return a specification that negates the results of the supplied
    *       specification
    *
    * @throws NullPointerException if target is null
    */
   @SuppressWarnings("unchecked")
   static <T> Specification<T> not(final Predicate<? super T> target) {
      notNull(target, "target");

      return (Specification<T>) target.negate();
   }

   /**
    * Cast current specification to inferred {@code U} type. Type is not checked at compilation time.
    *
    * @param <U> inferred cast target type
    *
    * @return casted specification
    *
    * @throws ClassCastException if specification cannot be cast to {@code T}
    */
   @SuppressWarnings("unchecked")
   default <U> Specification<U> cast() {
      return object -> satisfiedBy((T) object);
   }

   /**
    * {@link CompositeSpecificationVisitor} visitor entry point.
    *
    * @param visitor visitor
    * @param <V> Visitor return type
    *
    * @return visitor result for visited node
    */
   default <V> V accept(final CompositeSpecificationVisitor<T, V> visitor) {
      return visitor.visit(this);
   }

}
