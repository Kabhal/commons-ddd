package com.tinubu.commons.ddd2.domain.type;

import java.util.Comparator;

import com.tinubu.commons.ddd2.domain.type.Fields.Field;
import com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport;

/**
 * A DDD entity representation. You should use {@link RootEntity} if this entity is a root entity.
 *
 * @param <ID> entity id implementation type
 *
 * @see Value
 * @see RootEntity
 */
public interface Entity<ID extends Id> extends DomainObject, Comparable<Entity<ID>> {

   /**
    * Entity id accessor.
    *
    * @return entity id
    */
   ID id();

   /**
    * Entities compared by identity, not by attributes.
    * Compared entity classes must be equals.
    *
    * @param entity the entity to compare to
    *
    * @return {@code true} if the identities are the same, regardless of other attributes.
    *
    * @implSpec this comparison method should not depend on {@code equals}, but the contrary is
    *       possible.
    */
   default boolean sameIdentityAs(Entity<ID> entity) {
      if (this == entity) {
         return true;
      }
      return entity != null && this.getClass().equals(entity.getClass()) && id() != null && id().sameValueAs(
            entity.id());
   }

   /**
    * Compares entities by the values of their attributes, when needed. {@link
    * Field#technical() technical} fields are excluded from the comparison.
    * Compared entity classes must be equals.
    *
    * @param entity the entity object to compare by value to
    *
    * @return {@code true} if the given entity object's and this entity object's attributes are the same.
    */
   @SuppressWarnings("unchecked")
   default boolean sameValueAs(Entity<ID> entity) {
      if (this == entity) {
         return true;
      }
      if (entity != null && this.getClass().equals(entity.getClass())) {
         SameValueBuilder sameValueBuilder = new SameValueBuilder(this, entity);
         ((Fields<DomainObject>) domainFields())
               .values()
               .stream()
               .filter(f -> !f.technical())
               .forEach(field -> sameValueBuilder.append(field.value().apply(this),
                                                         field.value().apply(entity)));
         return sameValueBuilder.build();
      } else {
         return false;
      }
   }

   default boolean entityEquals(Object object) {
      return DomainObjectSupport.entityEquals(this, object);
   }

   default int entityHashCode() {
      return DomainObjectSupport.entityHashCode(this);
   }

   default String entityToString() {
      return DomainObjectSupport.entityToString(this);
   }

   default String entityToString(boolean identityOnly) {
      return DomainObjectSupport.entityToString(this, identityOnly);
   }

   @SuppressWarnings("unchecked")
   default String entityToString(Fields<? extends Entity<?>> extraFields) {
      return DomainObjectSupport.entityToString(this, (Fields<Entity<?>>) extraFields);
   }

   @SuppressWarnings("unchecked")
   default String entityToString(boolean identityOnly, Fields<? extends Entity<?>> extraFields) {
      return DomainObjectSupport.entityToString(this, identityOnly, (Fields<Entity<?>>) extraFields);
   }

   default boolean valueEquals(Object object) {
      return DomainObjectSupport.entityEqualsByValue(this, object);
   }

   default int valueHashCode() {
      return DomainObjectSupport.entityHashCodeByValue(this);
   }

   @Override
   default int compareTo(Entity<ID> entity) {
      if (entity == this) {
         return 0;
      }
      if (!(this.getClass().equals(entity.getClass()))) {
         throw new ClassCastException(String.format("'%s' can't be compared with '%s'",
                                                    this.getClass().getName(),
                                                    entity.getClass().getName()));
      }
      return id().compareTo(entity.id());
   }

   static <T extends Entity<ID>, ID extends Id> Comparator<? super T> comparator() {
      return Entity::compareTo;
   }

   static <T extends Entity<ID>, ID extends Id> Comparator<? super T> comparatorByValue() {
      return (e1, e2) -> e1.sameValueAs(e2) ? 0 : (e1.valueHashCode() < e2.valueHashCode() ? -1 : 1);
   }

}
