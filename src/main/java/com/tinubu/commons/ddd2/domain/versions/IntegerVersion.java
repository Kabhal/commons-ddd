/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.domain.type.AbstractVersion;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Version;

/**
 * A DDD {@link Version} represented as a {@link Integer}
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class IntegerVersion extends AbstractVersion<Integer> {

   protected static final int INITIAL_VALUE = 0;

   protected IntegerVersion(Integer value) {
      super(value);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends IntegerVersion> defineDomainFields() {
      return Fields
            .<IntegerVersion>builder()
            .superFields((Fields<IntegerVersion>) super.defineDomainFields())
            .field("value", v -> v.value, isPositive())
            .build();
   }

   /**
    * Creates a new {@link IntegerVersion} with specified value.
    *
    * @param value version value
    *
    * @return new version
    */
   public static IntegerVersion of(Integer value) {
      return checkInvariants(new IntegerVersion(value));
   }

   /**
    * Creates a new {@link IntegerVersion} with initial value.
    *
    * @return new version
    */
   public static IntegerVersion initialVersion() {
      return checkInvariants(new IntegerVersion(INITIAL_VALUE));
   }

   @Override
   public IntegerVersion nextVersion(DomainObject domainObject) {
      notNull(domainObject, "domainObject");

      if (value >= Integer.MAX_VALUE) {
         throw new IllegalStateException("Version overflow : " + value);
      }
      return of(value + 1);
   }

}
