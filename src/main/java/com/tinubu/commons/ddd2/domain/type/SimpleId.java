package com.tinubu.commons.ddd2.domain.type;

import com.tinubu.commons.lang.beans.Getter;

/**
 * Represents a simple id with one value.
 * Value can be of any {@link Comparable} type.
 *
 * @param <T> value type
 *
 * @see Id
 * @see BiCompositeId
 */
public interface SimpleId<T extends Comparable<? super T>> extends Id {

   /**
    * Raw id value.
    *
    * @return raw id value, can be {@code null} if id is uninitialized
    */
   @Getter
   T value();

   /**
    * Raw string representation of the id value.
    * This is NOT the internal representation of the class which is returned using {@link Object#toString()}.
    * This is a default implementation using {@link Object#toString()}, you can override it as requested.
    *
    * @return raw string representation, can be {@code null} if id is uninitialized
    */
   default String stringValue() {
      return value() == null ? null : value().toString();
   }
}
