package com.tinubu.commons.ddd2.domain.type;

import java.util.Comparator;

import com.tinubu.commons.ddd2.domain.type.Fields.Field;
import com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport;

/**
 * A DDD value representation.
 *
 * @see Entity
 */
public interface Value extends DomainObject {

   /**
    * Value objects compare by the values of their attributes, they don't have an identity. {@link
    * Field#technical() technical} fields are excluded from the comparison.
    * Compared entity classes must be equals.
    *
    * @param value The value object to compare to
    *
    * @return {@code true} if the given value object's and this value object's attributes are the same.
    *
    * @implSpec This comparison method should not depend on {@code equals)}, but the contrary
    *       is possible.
    * @see SameValueBuilder You should use this builder to implement this function in each value
    *       object.
    */
   @SuppressWarnings("unchecked")
   default boolean sameValueAs(Value value) {
      if (this == value) {
         return true;
      }
      if (value != null && this.getClass().equals(value.getClass())) {
         SameValueBuilder sameValueBuilder = new SameValueBuilder(this, value);
         ((Fields<DomainObject>) domainFields())
               .values()
               .stream()
               .filter(f -> !f.technical())
               .forEach(field -> sameValueBuilder.append(field.value().apply(this),
                                                         field.value().apply(value)));
         return sameValueBuilder.build();
      } else {
         return false;
      }
   }

   default boolean valueEquals(Object object) {
      return DomainObjectSupport.valueEquals(this, object);
   }

   default int valueHashCode() {
      return DomainObjectSupport.valueHashCode(this);
   }

   default String valueToString() {
      return DomainObjectSupport.valueToString(this);
   }

   @SuppressWarnings("unchecked")
   default String valueToString(Fields<? extends Value> extraFields) {
      return DomainObjectSupport.valueToString(this, (Fields<Value>) extraFields);
   }

   static <T extends Value> Comparator<? super T> comparator() {
      return (v1, v2) -> v1.sameValueAs(v2) ? 0 : (v1.valueHashCode() < v2.valueHashCode() ? -1 : 1);
   }

}
