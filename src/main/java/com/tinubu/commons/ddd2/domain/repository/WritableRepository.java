/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.repository;

import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * DDD writable repository interface with create and delete operations.
 *
 * @param <T> entity type
 * @param <ID> entity id type
 *
 * @see ReadableRepository
 * @see IterableRepository
 * @see QueryableRepository
 * @see WritableRepository
 */
public interface WritableRepository<T extends Entity<ID>, ID extends Id> extends Repository<T, ID> {

   /**
    * Saves specified entity.
    * <p>
    * Returned entity has always the same {@link Entity#id()}.
    *
    * @param entity entity to save
    *
    * @return saved entity, potentially different from original entity, if entity has been saved or
    *       {@link Optional#empty} otherwise.
    *
    * @throws RepositoryException if unexpected repository error occurs
    * @throws RepositoryIOException if I/O error occurs
    */
   @CheckReturnValue
   Optional<T> save(T entity);

   /**
    * Deletes specified entity.
    * <p>
    * Returned entity has always the same {@link Entity#id()}.
    *
    * @param entityId entity to delete
    *
    * @return deleted entity, if entity has been deleted or {@link Optional#empty} otherwise
    *
    * @throws RepositoryException if unexpected repository error occurs
    * @throws RepositoryIOException if I/O error occurs
    */
   @CheckReturnValue
   Optional<T> delete(ID entityId);

}
