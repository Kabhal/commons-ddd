/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.repository;

import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;

/**
 * CRUD repository interface if repository supports the complete base set of operations.
 *
 * @param <T> entity type
 * @param <ID> entity id type
 *
 * @see ReadableRepository
 * @see IterableRepository
 * @see QueryableRepository
 * @see WritableRepository
 */
public interface CrudRepository<T extends Entity<ID>, ID extends Id> extends ReadableRepository<T, ID>,
                                                                             IterableRepository<T, ID>,
                                                                             QueryableRepository<T, ID>,
                                                                             WritableRepository<T, ID> {

}
