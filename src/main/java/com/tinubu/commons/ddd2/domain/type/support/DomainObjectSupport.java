package com.tinubu.commons.ddd2.domain.type.support;

import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Fields.Field;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.invariant.InvariantResults;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Failure;
import com.tinubu.commons.lang.util.Try.Success;

/**
 * Operations supporting {@link DomainObject}.
 */
public final class DomainObjectSupport {

   /**
    * Public global static flag to change {@link DomainObjectSupport#entityToString(Entity)} behavior.
    * Display only the entity id if set to {@code true}.
    */
   public static boolean ENTITY_TO_STRING_IDENTITY_ONLY = false;

   /**
    * Special object reference to return by {@link Field#displayValue(Function)} function in order to hide the
    * fields from {@link #valueToString(Value)}-like and {@link #entityToString(Entity)}-like operations..
    */
   private static final String HIDE_FIELD_TO_STRING_REF_VALUE = "<hidden>";
   private static final String ENTITY_DEFAULT_ID_FIELD_NAME = "id";

   private DomainObjectSupport() {
   }

   /**
    * General equality function for {@link Value value}.
    * Specified value and object are equals if one or both are {@code null} or value and object have
    * the {@link Value#sameValueAs(Value)}.
    * <p>
    * If {@link Object} is not a value, the function returns {@code false} without {@link ClassCastException}.
    * <p>
    * You should use this function if you use {@link #valueHashCode(Value)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    * <p>
    * It is not mandatory for {@link Value values} to implement {@link Object#equals(Object)} like this, it's
    * the user choice, but it is a good default.
    *
    * @param value value
    * @param object second object
    * @param <T> value type
    *
    * @return {@code true} if one or both of value and object are {@code null}, or have the same values.
    *
    * @implSpec Technical fields are not taken into account for sameValueAs/equals/hashCode to be
    *       consistent
    */
   @SuppressWarnings("unchecked")
   public static <T extends Value> boolean valueEquals(T value, Object object) {
      if (value == object) return true;
      if (value == null || object == null || value.getClass() != object.getClass()) return false;
      return value.sameValueAs((T) object);
   }

   /**
    * General hashcode function for {@link Value value} using {@link DomainObject#domainFields()}.
    * <p>
    * You should use this function if you use {@link #valueEquals(Value, Object)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    *
    * @param value value to hash
    * @param <T> value type
    *
    * @return value hashcode
    *
    * @implSpec Technical fields are not taken into account for sameValueAs/equals/hashCode to be
    *       consistent
    */
   @SuppressWarnings("unchecked")
   public static <T extends Value> int valueHashCode(T value) {
      notNull(value, "value");

      return Objects.hash(((Fields<DomainObject>) value.domainFields())
                                .values()
                                .stream()
                                .filter(f -> !f.technical())
                                .map(f -> f.value().apply(value))
                                .toArray());
   }

   public static <T extends Value> String valueToString(T value) {
      notNull(value, "value");

      return valueToString(value, Fields.<T>builder().empty());
   }

   @SuppressWarnings("unchecked")
   public static <T extends Value> String valueToString(T value, Fields<T> extraFields) {
      notNull(value, "value");
      notNull(extraFields, "extraFields");

      StringJoiner toStringBuilder = new StringJoiner(",", value.getClass().getSimpleName() + "[", "]");
      ((Fields<DomainObject>) value.domainFields()).values().forEach(field -> {
         var displayValue = field.displayValue().apply(value);
         if (displayValue != HIDE_FIELD_TO_STRING_REF_VALUE) {
            toStringBuilder.add(field.name() + "=" + formatToStringValue(displayValue));
         }
      });
      extraFields.values().forEach(field -> {
         var displayValue = field.displayValue().apply(value);
         if (displayValue != HIDE_FIELD_TO_STRING_REF_VALUE) {
            toStringBuilder.add(field.name() + "=" + formatToStringValue(displayValue));
         }
      });

      return toStringBuilder.toString();
   }

   /**
    * General equality function for {@link Entity entity}.
    * Specified entity and object are equals if one or both are {@code null} or entity and object have
    * the {@link Entity#sameIdentityAs(Entity)}.
    * <p>
    * You should use this function if you use {@link #entityHashCode(Entity)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    * <p>
    * If {@link Object} is not a value, the function returns {@code false} without {@link ClassCastException}.
    * <p>
    * It is not mandatory for {@link Entity entities} to implement {@link Object#equals(Object)} like this,
    * alternatively the user will prefer to compare all fields with a classic implementation.
    *
    * @param entity entity
    * @param object second object
    * @param <T> entity type
    *
    * @return {@code true} if one or both of entity and object are {@code null}, or have the same identities.
    */
   @SuppressWarnings("unchecked")
   public static <T extends Entity<ID>, ID extends Id> boolean entityEquals(T entity, Object object) {
      if (entity == object) return true;
      if (entity == null || object == null || entity.getClass() != object.getClass()) return false;
      return entity.sameIdentityAs((T) object);
   }

   /**
    * Special equality function for {@link Value value} considering its values (including
    * {@link Entity#id()}).
    *
    * @param entity entity
    * @param object second object
    * @param <T> value type
    *
    * @return {@code true} if one or both of entity and object are {@code null}, or have the same values.
    *
    * @implSpec Technical fields are not taken into account for sameValueAs/equals/hashCode to be
    *       consistent
    */
   @SuppressWarnings("unchecked")
   public static <T extends Entity> boolean entityEqualsByValue(T entity, Object object) {
      if (entity == object) return true;
      if (entity == null || object == null || entity.getClass() != object.getClass()) return false;
      return entity.sameValueAs((T) object);
   }

   /**
    * General hashcode function for {@link Entity entity}.
    * <p>
    * You should use this function if you use {@link #entityEquals(Entity, Object)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    * <p>
    * It is not mandatory for {@link Entity entities} to implement {@link Object#hashCode()} like this,
    * alternatively the user will prefer to hash all fields with a classic implementation.
    *
    * @param <T> entity type
    * @param entity entity to hash
    *
    * @return entity hashcode
    */
   public static <T extends Entity<?>> int entityHashCode(T entity) {
      notNull(entity, "entity");

      return Objects.hash(entity.id());
   }

   /**
    * Special hashcode function for {@link Entity entity} when considering its values (including its
    * {@link Entity#id()}).
    *
    * @param <T> entity type
    * @param entity entity to hash
    *
    * @return entity hashcode considering its values
    */
   @SuppressWarnings("unchecked")
   public static <T extends Entity<?>> int entityHashCodeByValue(T entity) {
      notNull(entity, "entity");

      return Objects.hash(((Fields<DomainObject>) entity.domainFields())
                                .values()
                                .stream()
                                .filter(f -> !f.technical())
                                .map(f -> f.value().apply(entity))
                                .toArray());
   }

   @SuppressWarnings("unchecked")
   public static <T extends Entity<?>> String entityToString(T entity,
                                                             boolean identityOnly,
                                                             Fields<T> extraFields) {
      notNull(entity, "entity");
      notNull(extraFields, "extraFields");

      StringJoiner toStringBuilder = new StringJoiner(",", entity.getClass().getSimpleName() + "[", "]");

      if (identityOnly || entity.domainFields().isEmpty()) {
         toStringBuilder.add(ENTITY_DEFAULT_ID_FIELD_NAME + "=" + entity.id());
      } else {
         for (Field<DomainObject, ?> field : ((Fields<DomainObject>) entity.domainFields()).values()) {
            var displayValue = field.displayValue().apply(entity);
            if (displayValue != HIDE_FIELD_TO_STRING_REF_VALUE) {
               toStringBuilder.add(field.name() + "=" + formatToStringValue(displayValue));
            }
         }
      }
      for (Field<T, ?> field : extraFields.values()) {
         var displayValue = field.displayValue().apply(entity);
         if (displayValue != HIDE_FIELD_TO_STRING_REF_VALUE) {
            toStringBuilder.add(field.name() + "=" + formatToStringValue(displayValue));
         }
      }
      return toStringBuilder.toString();
   }

   public static <T extends Entity<?>> String entityToString(T entity, boolean identityOnly) {
      notNull(entity, "entity");

      return entityToString(entity, identityOnly, Fields.<T>builder().empty());
   }

   public static <T extends Entity<?>> String entityToString(T entity) {
      notNull(entity, "entity");

      return entityToString(entity, ENTITY_TO_STRING_IDENTITY_ONLY);
   }

   public static <T extends Entity<?>> String entityToString(T entity, Fields<T> extraFields) {
      notNull(entity, "entity");
      notNull(extraFields, "extraFields");

      return entityToString(entity, ENTITY_TO_STRING_IDENTITY_ONLY, extraFields);
   }

   private static String formatToStringValue(Object value) {
      if (value == null) {
         return "<null>";
      } else {
         return value.toString();
      }
   }

   /**
    * Returns {@code true} if this value is the same that any listed {@code values}.
    *
    * @param value value to compare
    * @param inValues list of values to compare value to
    * @param <T> values type
    *
    * @return {@code true} if this value is the same that any listed values
    */
   @SafeVarargs
   public static <T extends Value> boolean valueIn(T value, T... inValues) {
      notNull(value, "value");
      noNullElements(inValues, "inValues");

      return stream(inValues).anyMatch(value::sameValueAs);
   }

   /**
    * Returns {@code true} if this value is the same that any listed {@code values}.
    *
    * @param value value to compare
    * @param inValues list of values to compare value to
    * @param <T> values type
    *
    * @return {@code true} if this value is the same that any listed values
    */
   public static <T extends Value> boolean valueIn(T value, Collection<T> inValues) {
      notNull(value, "value");
      noNullElements(inValues, "inValues");

      return stream(inValues).anyMatch(value::sameValueAs);
   }

   /**
    * Returns {@code true} if this entity has the same identity that any listed {@code inEntities}.
    *
    * @param entity entity to compare
    * @param inEntities list of entities to compare entity to
    * @param <T> entities type
    * @param <ID> Identity type
    *
    * @return {@code true} if this entity has the same identity that any listed identities
    */
   @SafeVarargs
   public static <T extends Entity<ID>, ID extends Id> boolean identityIn(T entity, T... inEntities) {
      notNull(entity, "entity");
      noNullElements(inEntities, "inEntities");

      return stream(inEntities).anyMatch(entity::sameIdentityAs);
   }

   /**
    * Returns {@code true} if this entity has the same identity that any listed {@code inEntities}.
    *
    * @param entity entity to compare
    * @param inEntities list of entities to compare entity to
    * @param <T> entities type
    * @param <ID> Identity type
    *
    * @return {@code true} if this entity has the same identity that any listed identities
    */
   public static <T extends Entity<ID>, ID extends Id> boolean identityIn(T entity,
                                                                          Collection<T> inEntities) {
      notNull(entity, "entity");
      noNullElements(inEntities, "inEntities");

      return stream(inEntities).anyMatch(entity::sameIdentityAs);
   }

   /**
    * Validates domain object invariants.
    *
    * @param domainObject domainObject to check
    * @param groups validation groups
    * @param <T> domain object type
    *
    * @return domain object, or invariant validation results in case of failure
    */
   public static <T extends DomainObject> Try<T, InvariantResults> tryValidateInvariants(T domainObject,
                                                                                         List<String> groups) {
      notNull(domainObject, "domainObject");
      noNullElements(groups, "groups");

      var results = domainObject.validateInvariants(groups);

      if (results.success()) {
         return Success.of(domainObject);
      } else {
         return Failure.of(results);
      }
   }

   /**
    * Validates domain object invariants.
    *
    * @param domainObject domainObject to check
    * @param groups validation groups
    * @param <T> domain object type
    *
    * @return domain object, or invariant validation results in case of failure
    */
   public static <T extends DomainObject> Try<T, InvariantResults> tryValidateInvariants(T domainObject,
                                                                                         String... groups) {
      notNull(domainObject, "domainObject");
      noNullElements(groups, "groups");

      var results = domainObject.validateInvariants(groups);

      if (results.success()) {
         return Success.of(domainObject);
      } else {
         return Failure.of(results);
      }
   }

   /**
    * Checks domain object invariants and throw exception in case of failure.
    *
    * @param domainObject domainObject to check
    * @param groups validation groups
    * @param <T> domain object type
    *
    * @return domain object
    *
    * @throws InvariantValidationException if invariant validation fails
    */
   public static <T extends DomainObject> T checkInvariants(T domainObject, List<String> groups) {
      return tryValidateInvariants(domainObject, groups).orElseThrow(InvariantValidationException::new);
   }

   /**
    * Checks domain object invariants and throw exception in case of failure.
    *
    * @param domainObject domainObject to check
    * @param groups optional validation groups
    * @param <T> domain object type
    *
    * @return domain object
    *
    * @throws InvariantValidationException if invariant validation fails
    */
   public static <T extends DomainObject> T checkInvariants(T domainObject, String... groups) {
      return tryValidateInvariants(domainObject, groups).orElseThrow(InvariantValidationException::new);
   }

   /**
    * Builds a domain object applying all post operations in order :
    * <ul>
    *    <li>{@link DomainObject#postConstruct()}</li>
    *    <li>{@link DomainObject#validateInvariants(String...)}</li>
    *    <li>{@link DomainObject#postValidate()}</li>
    *    <li>{@link DomainObject#onBuildFailure(Throwable)} ()}</li>
    * </ul>
    *
    * @param domainObject raw domain object instance builder
    * @param groups optional validation groups
    * @param <T> domain object type
    *
    * @return fully built domain object
    *
    * @throws RuntimeException if object fails to build for any reason
    */
   // FIXME to refactor : why useing Supplier here ? why not an XCheckedSupplier to rethrow X (int his case how to throw InvariantExceptions ?) ?
   @SuppressWarnings("unchecked")
   public static <T extends DomainObject> T build(Supplier<T> domainObject, String... groups) {
      T domainObjectValue = domainObject.get();

      return checkedSupplier(() -> (T) checkInvariants(domainObjectValue.postConstruct(),
                                                       groups).postValidate())
            .tryCatch(failure -> domainObjectValue.onBuildFailure(failure))
            .getChecked();
   }

   /**
    * Builds a domain object applying all post operations in order :
    * <ul>
    *    <li>{@link DomainObject#postConstruct()}</li>
    *    <li>{@link DomainObject#validateInvariants(String...)}</li>
    *    <li>{@link DomainObject#postValidate()}</li>
    *    <li>{@link DomainObject#onBuildFailure(Throwable)} ()}</li>
    * </ul>
    *
    * @param domainObject raw domain object instance builder
    * @param groups optional validation groups
    * @param <T> domain object type
    *
    * @return fully built domain object, or invariant validation results in case of invariant failure
    *
    * @throws RuntimeException if object fails to build for any reason that is not an invariant failure
    */
   // FIXME review usage
   @SuppressWarnings("unchecked")
   public static <T extends DomainObject> Try<T, InvariantResults> tryBuild(Supplier<T> domainObject,
                                                                            String... groups) {
      T domainObjectValue = domainObject.get();

      return checkedSupplier(() -> tryValidateInvariants(domainObjectValue.postConstruct(), groups).map(
            DomainObject::<T>postValidate))
            .tryCatch(failure -> domainObjectValue.onBuildFailure(failure))
            .getChecked();
   }

   /**
    * Special {@link Field#displayValue(Function)} function to hide a fields from
    * {@link #valueToString(Value)}-like and {@link #entityToString(Entity)}-like operations.
    */
   public static <T> Function<T, Object> hideField() {
      return hideField(alwaysTrue());
   }

   /**
    * Special {@link Field#displayValue(Function)} function to hide a fields from
    * {@link #valueToString(Value)}-like and {@link #entityToString(Entity)}-like operations.
    *
    * @param hideCondition object value predicate to hide field only if condition is satisfied
    */
   public static <T> Function<T, Object> hideField(Predicate<? super T> hideCondition) {
      notNull(hideCondition, "hideCondition");

      return object -> hideCondition.test(object) ? HIDE_FIELD_TO_STRING_REF_VALUE : object;
   }
}
