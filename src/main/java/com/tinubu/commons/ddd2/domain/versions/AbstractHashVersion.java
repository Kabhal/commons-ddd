/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toList;

import java.util.List;

import com.tinubu.commons.ddd2.domain.type.AbstractVersion;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Version;

/**
 * A DDD {@link Version} represented as a hash generated from domain object fields. The hash
 * is deterministic for the same set of domain object fields.
 *
 * @param <H> Hash representation type
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public abstract class AbstractHashVersion<H extends Comparable<? super H>> extends AbstractVersion<H> {

   /**
    * Generates version with explicit value.
    *
    * @param value version value
    */
   public AbstractHashVersion(H value) {
      super(value);
   }

   @SuppressWarnings("unchecked")
   protected static <D extends DomainObject> List<Object> domainObjectHashValues(D domainObject) {
      return ((Fields<D>) domainObject.domainFields())
            .values()
            .stream()
            .map(field -> field.value().apply(domainObject))
            .collect(toList());
   }

   protected abstract Version<H> hashVersion(List<Object> hashValues);

   @Override
   public Version<H> nextVersion(DomainObject domainObject) {
      notNull(domainObject, "domainObject");

      return hashVersion(domainObjectHashValues(domainObject));
   }

}
