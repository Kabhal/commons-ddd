package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;

import com.tinubu.commons.ddd2.invariant.InvariantResult;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.PredicateInvariantRule;
import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * A sensitive {@link Value} representation.
 * <p>
 * This value contain one or more sensitive fields.
 */
public interface SensitiveValue extends Value {

   /**
    * Wipes out sensitive value from memory when possible.
    */
   void clearSensitiveValues();

   /**
    * Whether sensitive values have been cleared.
    */
   @CheckReturnValue
   boolean clearedSensitiveValues();

   class Validate {

      public static <T extends SensitiveValue> InvariantRule<T> isCleared() {
         return PredicateInvariantRule
               .satisfiesValue(SensitiveValue::clearedSensitiveValues,
                               "'%s' sensitive value must be cleared",
                               validatingObject())
               .ruleContext("SensitiveValue.isCleared");
      }

      public static <T extends SensitiveValue> InvariantRule<T> isNotCleared() {
         return PredicateInvariantRule
               .satisfiesValue((SensitiveValue sensitiveValue) -> !sensitiveValue.clearedSensitiveValues(),
                               "'%s' sensitive value must not be cleared",
                               validatingObject())
               .ruleContext("SensitiveValue.isNotCleared");
      }

      public static <T extends SensitiveValue> InvariantResult<T> cleared(final T value, final String name) {
         return com.tinubu.commons.ddd2.invariant.Validate.validate(value, name, isCleared());
      }

      public static <T extends SensitiveValue> InvariantResult<T> cleared(final T value) {
         return com.tinubu.commons.ddd2.invariant.Validate.validate(value, isCleared());
      }

      public static <T extends SensitiveValue> InvariantResult<T> notCleared(final T value,
                                                                             final String name) {
         return com.tinubu.commons.ddd2.invariant.Validate.validate(value, name, isNotCleared());
      }

      public static <T extends SensitiveValue> InvariantResult<T> notCleared(final T value) {
         return com.tinubu.commons.ddd2.invariant.Validate.validate(value, isNotCleared());
      }

      public static class Check {

         public static <T extends SensitiveValue> T cleared(final T value, final String name) {
            return com.tinubu.commons.ddd2.invariant.Validate.Check.validate(value, name, isCleared());
         }

         public static <T extends SensitiveValue> T cleared(final T value) {
            return com.tinubu.commons.ddd2.invariant.Validate.Check.validate(value, isCleared());
         }

         public static <T extends SensitiveValue> T notCleared(final T value, final String name) {
            return com.tinubu.commons.ddd2.invariant.Validate.Check.validate(value, name, isNotCleared());
         }

         public static <T extends SensitiveValue> T notCleared(final T value) {
            return com.tinubu.commons.ddd2.invariant.Validate.Check.validate(value, isNotCleared());
         }

      }
   }

}
