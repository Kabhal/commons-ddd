/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.event;

import java.util.EventListener;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Domain event listener interface.
 * Each listener is associated with a single event type, but this can be an event super interface.
 *
 * @param <E> event type to listen
 */
@FunctionalInterface
public interface DomainEventListener<E extends DomainEvent> extends EventListener, Consumer<E> {

   /**
    * Accept the listened event.
    *
    * @param event listened event, never {@code null}
    */
   @Override
   void accept(E event);

   /**
    * Returns a new instance of this listener that filters events based on specified predicate.
    *
    * @param filter predicate filter to apply to accepted events. Events that does not satisfy the
    *       predicate are not accepted
    *
    * @return new listener instance filtering events
    */
   default DomainEventListener<E> filteredListener(Predicate<DomainEvent> filter) {
      return event -> {
         if (filter.test(event)) {
            DomainEventListener.this.accept(event);
         }
      };
   }

}
