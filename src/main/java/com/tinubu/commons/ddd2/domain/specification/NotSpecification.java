package com.tinubu.commons.ddd2.domain.specification;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 'not' decorator, used to create a new specification that is the inverse of the given specification.
 */
public class NotSpecification<T> implements CompositeSpecification<T> {

   /**
    * Negated specification.
    */
   private Specification<T> specification;

   /**
    * Create a new 'not' specification based on another specification.
    *
    * @param specification specification instance to negate
    */
   public NotSpecification(final Specification<T> specification) {
      this.specification = notNull(specification, "specification");
   }

   public boolean satisfiedBy(final T object) {
      notNull(object, "object");

      return !specification.satisfiedBy(object);
   }

   public Specification<T> specification() {
      return specification;
   }

   @Override
   public <V> V accept(CompositeSpecificationVisitor<T, V> visitor) {
      notNull(visitor, "visitor");

      return visitor.visit(this);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      NotSpecification<?> that = (NotSpecification<?>) o;
      return Objects.equals(specification, that.specification);
   }

   @Override
   public int hashCode() {
      return Objects.hash(specification);
   }

   @Override
   public String toString() {
      return new StringJoiner(",", NotSpecification.class.getSimpleName() + "[", "]")
            .add("specification=" + specification)
            .toString();
   }

}
