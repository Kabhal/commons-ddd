/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.hideField;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isTrue;
import static java.util.Comparator.comparing;
import static java.util.Objects.isNull;

import java.util.Comparator;

/**
 * A default {@link TriCompositeId} implementation.
 * The implementation support both persistence style for ids :
 * <ul>
 *    <li>{@link #AbstractTriCompositeId(T1, T2, T3)} : initialized (requesting next id from ORM)</li>
 *    <li>{@link #AbstractTriCompositeId()} : uninitialized (after ORM's save). In this case many operations are restricted</li>
 * </ul>
 *
 * @param <T1> first value type
 * @param <T2> second value type
 * @param <T3> third value type
 *
 * @implNote Implementation is immutable.
 * @see AbstractSimpleId for simple ids.
 */
public abstract class AbstractTriCompositeId<T1 extends Comparable<? super T1>, T2 extends Comparable<? super T2>, T3 extends Comparable<? super T3>>
      extends AbstractValue implements TriCompositeId<T1, T2, T3> {

   protected T1 value1;
   protected T2 value2;
   protected T3 value3;
   protected final boolean newObject;

   /**
    * Builds {@link AbstractSimpleId} from specified value.
    *
    * @param value1 optional first raw id value
    * @param value2 optional second raw id value
    * @param value3 optional third raw id value
    * @param newObject informs whether the entity object is a new entity. If one value is uninitialized
    *       ({@code null}), newObject must be set to {@code true}
    */
   protected AbstractTriCompositeId(T1 value1, T2 value2, T3 value3, boolean newObject) {
      this.value1 = value1;
      this.value2 = value2;
      this.value3 = value3;
      this.newObject = newObject;
   }

   /**
    * Builds {@link AbstractSimpleId} from specified value for object reconstitution.
    * {@code newObject} is defaulted to {@code false}.
    *
    * @param value1 optional first raw id value
    * @param value2 optional second raw id value
    * @param value3 optional third raw id value
    */
   protected AbstractTriCompositeId(T1 value1, T2 value2, T3 value3) {
      this(value1, value2, value3, false);
   }

   /**
    * Builds {@link AbstractSimpleId} with uninitialized values.
    * {@code newObject} is defaulted to {@code true}.
    */
   protected AbstractTriCompositeId() {
      this(null, null, null, true);
   }

   /**
    * Overridable domain fields definitions.
    *
    * @return domain fields
    */
   @Override
   protected Fields<? extends TriCompositeId<T1, T2, T3>> defineDomainFields() {
      return Fields
            .<AbstractTriCompositeId<T1, T2, T3>>builder()
            .field("value1", v -> v.value1)
            .field("value2", v -> v.value2)
            .field("value3", v -> v.value3)
            .field("newObject",
                   v -> v.newObject,
                   hideField(v -> !v),
                   isTrue("'%1$s' must be true if at least one value is uninitialized",
                          validatingObjectName()).ifIsSatisfied(() -> isNull(value1)
                                                                      || isNull(value2)
                                                                      || isNull(value3)))
            .build();
   }

   @Override
   public boolean newObject() {
      return newObject;
   }

   public T1 value1() {
      return value1;
   }

   public T2 value2() {
      return value2;
   }

   public T3 value3() {
      return value3;
   }

   /**
    * Compares {@link Id} by value.
    * Only comparison of exactly the same id classes is supported.
    * Only comparison of initialized ids is supported.
    *
    * @param value the id to be compared.
    */
   @Override
   @SuppressWarnings("unchecked")
   public int compareTo(Id value) {
      if (value == this) {
         return 0;
      }
      if (!(this.getClass().equals(value.getClass()))) {
         throw new ClassCastException(String.format("'%s' can't be compared with '%s'",
                                                       this.getClass().getName(),
                                                       value.getClass().getName()));
      }
      if (this.value1 == null
          || this.value2 == null
          || this.value3 == null
          || ((AbstractTriCompositeId<?, ?, ?>) value).value1 == null
          || ((AbstractTriCompositeId<?, ?, ?>) value).value2 == null
          || ((AbstractTriCompositeId<?, ?, ?>) value).value3 == null) {
         throw new IllegalStateException("Can't compare uninitialized ids");
      }

      Comparator<AbstractTriCompositeId<T1, T2, T3>> value1Comparator = comparing(id -> id.value1);
      Comparator<AbstractTriCompositeId<T1, T2, T3>> value2Comparator = comparing(id -> id.value2);
      Comparator<AbstractTriCompositeId<T1, T2, T3>> value3Comparator = comparing(id -> id.value3);

      return value1Comparator
            .thenComparing(value2Comparator)
            .thenComparing(value3Comparator)
            .compare(this, (AbstractTriCompositeId<T1, T2, T3>) value);
   }

   /**
    * {@inheritDoc}
    * Always return {@code false} if any id is uninitialized.
    */
   @Override
   public boolean sameValueAs(Value value) {
      if (this.value1 == null || this.value2 == null || this.value3 == null) {
         return false;
      } else {
         return super.sameValueAs(value);
      }
   }
}
