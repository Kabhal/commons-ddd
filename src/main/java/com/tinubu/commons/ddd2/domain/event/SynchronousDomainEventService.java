/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.event;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;

import com.tinubu.commons.lang.util.Pair;

/**
 * Synchronous {@link DomainEventService} managing a configurable registry of listeners.
 */
public class SynchronousDomainEventService implements RegistrableDomainEventService {
   @SuppressWarnings("rawtypes")
   protected final List<Pair<Class<? extends DomainEvent>, DomainEventListener>> listeners = list();

   /**
    * Register a new {@link DomainEventListener} into the registry.
    *
    * @param eventClass supported event class
    * @param eventListener event listener
    * @param <E> supported event type
    */
   public <E extends DomainEvent> void registerEventListener(Class<E> eventClass,
                                                             DomainEventListener<? super E> eventListener) {
      notNull(eventClass, "eventClass");
      notNull(eventListener, "eventListener");

      listeners.add(Pair.of(eventClass, eventListener));
   }

   /**
    * Unregisters all event listeners.
    */
   public void unregisterEventListeners() {
      listeners.clear();
   }

   @Override
   @SuppressWarnings("unchecked")
   public void publishEvent(DomainEvent event) {
      notNull(event, "event");

      listeners.forEach(listener -> {
         if (listener.getKey().isAssignableFrom(event.getClass())) {
            listener.getValue().accept(event);
         }
      });
   }
}
