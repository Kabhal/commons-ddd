package com.tinubu.commons.ddd2.domain.ids;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.AbstractSimpleId;
import com.tinubu.commons.ddd2.domain.type.Id;

/**
 * A DDD {@link Id} represented as an {@link Integer}.
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class IntegerId extends AbstractSimpleId<Integer> {

   protected static final String URN_ID_TYPE = "int";

   protected IntegerId(Integer value, boolean newObject) {
      super(value, newObject);
   }

   protected IntegerId(Integer value) {
      super(value);
   }

   protected IntegerId() {
      super();
   }

   /**
    * Creates a new {@link IntegerId} for a reconstituting object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static IntegerId of(Integer value) {
      return checkInvariants(new IntegerId(value));
   }

   /**
    * Creates a new {@link IntegerId} for a reconstituting object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static IntegerId of(URI urn) {
      return of(nullable(urn).map(u -> Integer.parseInt(idUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   /**
    * Creates a new {@link IntegerId} for a new object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static IntegerId ofNewObject(Integer value) {
      return checkInvariants(new IntegerId(value, true));
   }

   /**
    * Creates a new {@link IntegerId} for a new object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static IntegerId ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> Integer.parseInt(idUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   /**
    * Creates a new {@link IntegerId} for a new object with uninitialized value.
    *
    * @return new id
    */
   public static IntegerId ofNewObject() {
      return checkInvariants(new IntegerId());
   }

   @Override
   public IntegerId value(Integer value) {
      return checkInvariants(new IntegerId(value, newObject));
   }

   @Override
   public URI urnValue() {
      return idUrn(URN_ID_TYPE);
   }

}
