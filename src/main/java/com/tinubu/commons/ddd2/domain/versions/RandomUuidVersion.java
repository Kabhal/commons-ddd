/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.isVersion4;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.uuid;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.UUID;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Version;

/**
 * A DDD {@link Version} represented as a RFC4122 UUID version 4 (random) string.
 * Uppercase UUID format is supported, but will be lower-cased internally.
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class RandomUuidVersion extends StringVersion {

   protected RandomUuidVersion(String value) {
      super(normalize(value));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends RandomUuidVersion> defineDomainFields() {
      return Fields
            .<RandomUuidVersion>builder()
            .superFields((Fields<RandomUuidVersion>) super.defineDomainFields())
            .field("value", v -> v.value, uuid(isVersion4()))
            .build();
   }

   /**
    * Creates a new {@link RandomUuidVersion} with specified value.
    *
    * @param value version value
    *
    * @return new version
    */
   public static RandomUuidVersion of(String value) {
      return checkInvariants(new RandomUuidVersion(value));
   }

   /**
    * Creates a new {@link RandomUuidVersion} with specified value.
    *
    * @param value version value
    *
    * @return new version
    */
   public static RandomUuidVersion of(UUID value) {
      notNull(value, "value");

      return of(value.toString());
   }

   /**
    * Creates a new {@link RandomUuidVersion} from a randomly generated UUID (version 4).
    *
    * @return new version
    */
   public static RandomUuidVersion initialVersion() {
      return of(Uuid.newUuidV4().stringValue());
   }

   @Override
   public RandomUuidVersion nextVersion(DomainObject domainObject) {
      notNull(domainObject, "domainObject");

      return initialVersion();
   }

   protected static String normalize(String uuid) {
      return uuid != null ? uuid.toLowerCase() : uuid;
   }

}
