/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport;
import com.tinubu.commons.lang.beans.Transient;
import com.tinubu.commons.lang.model.Builder;
import com.tinubu.commons.lang.validation.Validate;

/**
 * Defines a domain-enabled builder.
 * <p>
 * The reconstitute mode should be used to express and rebuild an object representation from persistence,
 * hence it should be able to access all object fields, including internal ones.
 * The rationale is to provide protective mechanisms for internal fields access : {@link
 * #ensureReconstitute()}, {@link #isReconstitute()}.
 * The originating builder can be selectively reconstitute-enabled using {@link #reconstitute()}.
 *
 * @param <T> final built type
 */
public abstract class DomainBuilder<T extends DomainObject> implements Builder<T> {
   private static final String DEFAULT_ENSURE_RECONSTITUTE_MESSAGE = "Reconstitute mode required";
   private static final String DEFAULT_ENSURE_NOT_RECONSTITUTE_MESSAGE = "Reconstitute mode rejected";

   private boolean reconstitute = false;

   /**
    * Returns this builder as requested implementation type. This is fundamentally unsafe until Java support
    * self-types.
    *
    * @param <B> builder implementation type
    *
    * @return this builder as requested implementation type
    */
   @SuppressWarnings("unchecked")
   public <B extends DomainBuilder<T>> B self() {
      return (B) this;
   }

   /**
    * Chains defined operation to current builder.
    * It's useful for immutable domain object implementations.
    *
    * @param builderOperation operation to apply to current builder
    * @param <B> original builder implementor type
    *
    * @return current builder with operation applied
    */
   @Transient
   public <B extends DomainBuilder<T>> B chain(UnaryOperator<B> builderOperation) {
      Validate.notNull(builderOperation, "builderOperation");

      return builderOperation.apply(self());
   }

   /**
    * Chains defined operation to current builder only if {@code predicate} is true.
    * It's useful for immutable domain object implementations.
    *
    * @param predicate chain condition
    * @param builderOperation operation to apply to current builder.
    * @param <B> original builder implementor type
    *
    * @return current builder with operation applied
    */
   public <B extends DomainBuilder<T>> B conditionalChain(Predicate<? extends B> predicate,
                                                          UnaryOperator<B> builderOperation) {
      Validate.notNull(predicate, "predicate");
      Validate.notNull(builderOperation, "builderOperation");

      if (predicate.test(self())) {
         return builderOperation.apply(self());
      } else {
         return self();
      }
   }

   /**
    * Chains defined operation to current builder only if {@code optional} value is present.
    * It's useful for immutable domain object implementations.
    *
    * @param <B> original builder implementor type
    * @param <U> optional type
    * @param optional chain condition
    * @param builderOperation operation to apply to current builder. encapsulated value parameter is
    *       passed to function.
    *
    * @return current builder with operation applied
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public <B extends DomainBuilder<T>, U> B optionalChain(Optional<? extends U> optional,
                                                          BiFunction<? super B, ? super U, ? extends B> builderOperation) {
      Validate.notNull(optional, "optional");
      Validate.notNull(builderOperation, "builderOperation");

      return optional.map(value -> builderOperation.apply(self(), value)).orElse(self());
   }

   /**
    * Switches the builder to "reconstitute" mode (disabled by default). Once enabled, reconstitute mode
    * can't be disabled.
    * <p>
    * Reconstitution mode enables to differentiate two ways to construct (class related) a domain object,
    * depending on its <em>functional</em> lifecycle :
    * <ul>
    *    <li>Object creation (functionally, not class related)</li>
    *    <li>Reconstitution (from persistence, other sources...)</li>
    * </ul>
    * <p>
    * Most of the time, reconstitution mode object construction consists in assigning all fields, and
    * revalidate the invariants. While object creation can be more complex, assign default values,
    * complete/adapt some values.
    * <p>
    * You can test if the builder {@link #isReconstitute() is in reconstitution mode} in the domain object to
    * adapt constructor. You can use {@link #ensureReconstitute()}/{@link #ensureNotReconstitute()} in complex
    * builders to reserve operations for both modes and instruct the user.
    *
    * @param <B> original builder implementor type
    *
    * @return this builder in reconstitution mode
    *
    * @see #isReconstitute()
    * @see #ensureReconstitute()
    * @see #ensureNotReconstitute()
    */
   public <B extends DomainBuilder<T>> B reconstitute() {
      this.reconstitute = true;

      return self();
   }

   /**
    * Ensure this builder is a "reconstitute" builder.
    *
    * @param message error message
    * @param values error message placeholder values
    *
    * @throws IllegalStateException if this builder is not in reconstitute mode
    * @see #reconstitute()
    */
   public void ensureReconstitute(String message, Object... values) {
      Validate.notNull(message, "message");

      if (!isReconstitute()) {
         throw new IllegalStateException(String.format(message, values));
      }
   }

   public void ensureReconstitute() {
      ensureReconstitute(DEFAULT_ENSURE_RECONSTITUTE_MESSAGE);
   }

   /**
    * Ensure this builder is not a "reconstitute" builder.
    *
    * @param message error message
    * @param values error message placeholder values
    *
    * @throws IllegalStateException if this builder is in reconstitute mode
    * @see #reconstitute()
    */
   public void ensureNotReconstitute(String message, Object... values) {
      Validate.notNull(message, "message");

      if (isReconstitute()) {
         throw new IllegalStateException(String.format(message, values));
      }
   }

   public void ensureNotReconstitute() {
      ensureNotReconstitute(DEFAULT_ENSURE_NOT_RECONSTITUTE_MESSAGE);
   }

   /**
    * Returns true if this builder is a "reconstitute" builder.
    *
    * @return true if this builder is a "reconstitute" builder
    */
   public boolean isReconstitute() {
      return this.reconstitute;
   }

   /**
    * Overrides this method to finalize the builder before building it.
    * Build finalization is automatically applied in {@link #build()}, but you can have to manually apply it
    * in other cases.
    */
   protected <B extends DomainBuilder<T>> B finalizeBuild() {
      return self();
   }

   /**
    * Overrides this method to create a raw instance of the build object.
    *
    * @return built object instance
    */
   protected abstract T buildDomainObject();

   /**
    * Evaluates this builder. You should not override it, override {@link #buildDomainObject()} instead.
    *
    * @return built object
    */
   @Override
   public T build() {
      return DomainObjectSupport.build(() -> finalizeBuild().buildDomainObject());
   }

   /**
    * Evaluates this builder validating only specified groups. You should not override it, override
    * {@link #buildDomainObject()} instead.
    *
    * @param groups validation groups
    *
    * @return built object
    */
   public T build(String... groups) {
      return DomainObjectSupport.build(() -> finalizeBuild().buildDomainObject(), groups);
   }

}
