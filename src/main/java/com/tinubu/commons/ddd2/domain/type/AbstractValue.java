package com.tinubu.commons.ddd2.domain.type;

import com.tinubu.commons.ddd2.domain.type.support.CachedFields;

/**
 * A DDD default {@link Value} implementation.
 *
 * @implNote Implementation is immutable.
 */
public abstract class AbstractValue implements Value {
   protected final CachedFields<? extends Value> domainFields = new CachedFields<>(this::defineDomainFields);

   /**
    * Overridable domain fields definitions.
    *
    * @return domain fields
    */
   protected Fields<? extends Value> defineDomainFields() {
      return Fields.<Value>builder().empty();
   }

   /**
    * Returns cached domain fields.
    *
    * @return domain fields
    */
   @Override
   public Fields<? extends Value> domainFields() {
      return domainFields.get();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }
}
