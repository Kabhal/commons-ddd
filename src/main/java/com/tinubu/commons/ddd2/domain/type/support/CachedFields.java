package com.tinubu.commons.ddd2.domain.type.support;

import java.util.function.Supplier;

import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;

/**
 * Loads fields lazily. Once loaded, fields are kept indefinitely in cache until invalidated.
 *
 * @param <T> domain object type
 */
public class CachedFields<T extends DomainObject> {

   private Fields<T> fields;
   private final Supplier<Fields<T>> fieldsSupplier;

   public CachedFields(Supplier<Fields<T>> fieldsSupplier) {
      this.fieldsSupplier = fieldsSupplier;
   }

   public CachedFields() {
      this(null);
   }

   public Fields<T> get() {
      if (this.fieldsSupplier == null) {
         throw new IllegalStateException("Fields supplier not set");
      }
      return cachedFields(this.fieldsSupplier);
   }

   public Fields<T> get(Supplier<Fields<T>> fieldsSupplier) {
      return cachedFields(fieldsSupplier);
   }

   /**
    * Invalidates cache, reload will be forced at next {@link #get()} access.
    */
   public void invalidate() {
      fields = null;
   }

   private Fields<T> cachedFields(Supplier<Fields<T>> fieldsSupplier) {
      if (this.fields == null) {
         this.fields = fieldsSupplier.get();
      }

      return this.fields;
   }
}
