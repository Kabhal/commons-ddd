package com.tinubu.commons.ddd2.domain.specification;

/**
 * Visitor for composite specifications.
 * Visit methods return type {@code V} is genericized.
 * You must call this visitor using {@code V result = mySpecification.accept(visitor)}.
 */
public interface CompositeSpecificationVisitor<T, V> {

   /**
    * General specification visit method for all available specifications defined for {@code T} domain entity
    * type.
    *
    * @param specification specification to visit
    *
    * @return specification visitor result
    */
   V visit(final Specification<T> specification);

   /**
    * Visit method for a {@link NotSpecification}.
    *
    * @param specification specification to visit
    *
    * @return specification visitor result
    */
   V visit(final NotSpecification<T> specification);

   /**
    * Visit method for a {@link AndSpecification}.
    *
    * @param specification specification to visit
    *
    * @return specification visitor result
    */
   V visit(final AndSpecification<T> specification);

   /**
    * Visit method for a {@link OrSpecification}.
    *
    * @param specification specification to visit
    *
    * @return specification visitor result
    */
   V visit(final OrSpecification<T> specification);

}
