package com.tinubu.commons.ddd2.domain.ids;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.bytesLength;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.AbstractSimpleId;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.rules.ComparableRules;

/**
 * A DDD {@link Id} represented as a {@link String}.
 * Valid value (when set) :
 * <ul>
 *    <li>value cannot be blank</li>
 *    <li>value cannot have more than {@value #MAX_VALUE_LENGTH} bytes (caution {@link String#length()} is in chars)</li>
 * </ul>
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class StringId extends AbstractSimpleId<String> {
   protected static final int MAX_VALUE_LENGTH = 1024;
   protected static final String URN_ID_TYPE = "string";

   protected StringId(String value, boolean newObject) {
      super(value, newObject);
   }

   protected StringId(String value) {
      super(value);
   }

   protected StringId() {
      super();
   }

   /**
    * Creates a new {@link StringId} for a reconstituting object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static StringId of(String value) {
      return checkInvariants(new StringId(value));
   }

   /**
    * Creates a new {@link StringId} for a reconstituting object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static StringId of(URI urn) {
      return of(nullable(urn).map(u -> idUrnValue(u, URN_ID_TYPE)).orElse(null));
   }

   /**
    * Creates a new {@link StringId} for a new object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static StringId ofNewObject(String value) {
      return checkInvariants(new StringId(value, true));
   }

   /**
    * Creates a new {@link StringId} for a new object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static StringId ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> idUrnValue(u, URN_ID_TYPE)).orElse(null));
   }

   /**
    * Creates a new {@link StringId} for a new object with uninitialized value.
    *
    * @return new id
    */
   public static StringId ofNewObject() {
      return checkInvariants(new StringId());
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends StringId> defineDomainFields() {
      return Fields
            .<StringId>builder()
            .superFields((Fields<StringId>) super.defineDomainFields())
            .field("value",
                   v -> v.value,
                   isNull().orValue(isNotBlank().andValue(bytesLength(ComparableRules.isLessThanOrEqualTo(
                         ParameterValue.value(MAX_VALUE_LENGTH, "MAX_VALUE_LENGTH")))))).build();
   }

   @Override
   public StringId value(String value) {
      return checkInvariants(new StringId(value, newObject));
   }

   @Override
   public URI urnValue() {
      return idUrn(URN_ID_TYPE);
   }

}
