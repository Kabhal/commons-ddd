/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.isNotEmptyHierarchicalServer;
import static com.tinubu.commons.ddd2.uri.UriEncoder.decode;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodePath;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.Try.ofThrownBy;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.PredicateInvariantRule;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.rules.ClassRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.ComponentUriRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.ComponentUriRules.PathRules;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleSchemeSpecific;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;

/**
 * SFTP {@link Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>{@code sftp://[<user>[:<password>]@]<host>[:<port>]</[/]path>[?<query>][#<fragment>]}</li>
 * </ul>
 * However, supported format depends on {@link SftpUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link SftpUriRestrictions#emptyPath() emptyPath} : path must not be empty</li>
 *    <li>{@link SftpUriRestrictions#portRestriction() portRestriction} : no port restriction</li>
 *    <li>{@link SftpUriRestrictions#portMapper() portMapper} : default port = {@value DEFAULT_PORT}</li>
 *    <li>{@link SftpUriRestrictions#query() query} : query is not supported</li>
 *    <li>{@link SftpUriRestrictions#fragment() fragment} : fragment is not supported</li>
 * </ul>
 */
public class SftpUri extends AbstractComponentUri<SftpUri> {

   protected static final String SCHEME = "sftp";
   protected static final int DEFAULT_PORT = 22;

   protected SftpUri(URI uri,
                     UriRestrictions restrictions,
                     Scheme scheme,
                     SchemeSpecific schemeSpecific,
                     Authority authority,
                     Path path,
                     Query query,
                     Fragment fragment,
                     boolean newObject) {
      super(uri,
            sftpRestrictions(restrictions),
            scheme,
            schemeSpecific,
            sftpUserInfo(authority),
            ofThrownBy(() -> sftpPath(path)).orElseThrow(e -> new IncompatibleUriException(uri).subMessage(e)),
            query,
            fragment,
            newObject);
   }

   protected SftpUri(UriRestrictions restrictions,
                     Scheme scheme,
                     SchemeSpecific schemeSpecific,
                     Authority authority,
                     Path path,
                     Query query,
                     Fragment fragment) {
      super(sftpRestrictions(restrictions),
            scheme,
            schemeSpecific,
            sftpUserInfo(authority),
            ofThrownBy(() -> sftpPath(path)).orElseThrow(e -> new IncompatibleUriException(uri(scheme,
                                                                                               schemeSpecific,
                                                                                               authority,
                                                                                               path,
                                                                                               query,
                                                                                               fragment)).subMessage(
                  e)),
            query,
            fragment);
   }

   protected SftpUri(URI uri, UriRestrictions restrictions) {
      this(uri,
           restrictions,
           SimpleScheme.of(uri).orElse(null),
           SimpleSchemeSpecific.of(uri).orElse(null),
           SimpleAuthority.of(uri).orElse(null),
           SimplePath.of(uri).orElse(null),
           SimpleQuery.of(uri).orElse(null),
           SimpleFragment.of(uri).orElse(null),
           false);
   }

   protected SftpUri(Uri uri, UriRestrictions restrictions) {
      super(sftpUri(nullableInstanceOf(uri, ComponentUri.class, ComponentUri::ofUri)),
            sftpRestrictions(restrictions));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends SftpUri> defineDomainFields() {
      return Fields
            .<SftpUri>builder()
            .superFields((Fields<? super SftpUri>) super.defineDomainFields())
            .field("authority",
                   v -> v.authority,
                   isNotNull().andValue(ClassRules.instanceOf(ServerAuthority.class,
                                                              as(ServerAuthority::userInfo,
                                                                 optionalValue(isInstanceOf(ParameterValue.value(
                                                                       UsernamePasswordUserInfo.class)))))))
            .field("path", v -> v.path, isInstanceOf(ParameterValue.value(SftpPath.class)))
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      var uriCompatibility = Invariant
            .of(() -> this,
                UriRules.isAbsolute(ParameterValue.value(SCHEME)),
                UriRules.isHierarchicalServer().andValue(isNotEmptyHierarchicalServer()))
            .groups(COMPATIBILITY_GROUP);

      var uriValidity = Invariant.of(() -> this,
                                     ComponentUriRules
                                           .path(PathRules.isNotEmpty())
                                           .ifIsSatisfied(() -> restrictions().emptyPath()));

      return super.domainInvariants().withInvariants(uriCompatibility, uriValidity);
   }

   public static SftpUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new SftpUri(uri, restrictions));
   }

   public static SftpUri ofUri(URI uri) {
      return ofUri(uri, SftpUriRestrictions.ofDefault());
   }

   public static SftpUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new SftpUri(uri, restrictions));
   }

   public static SftpUri ofUri(Uri uri) {
      return ofUri(uri, SftpUriRestrictions.ofDefault());
   }

   public static SftpUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static SftpUri ofUri(String uri) {
      return ofUri(uri, SftpUriRestrictions.ofDefault());
   }

   public static SftpUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   public static SftpUri ofUrn(URI urn) {
      return ofUrn(urn, SftpUriRestrictions.ofDefault());
   }

   public static SftpUri hierarchical(ServerAuthority authority, Path path, Query query, Fragment fragment) {
      validate(authority, "authority", isNotNull()).and(validate(path, "path", isNotNull())).orThrow();

      return buildUri(() -> new SftpUri(SftpUriRestrictions.automatic(path, query, fragment),
                                        SimpleScheme.of(SCHEME),
                                        null,
                                        authority,
                                        path,
                                        query,
                                        fragment));
   }

   public static SftpUri hierarchical(ServerAuthority authority, Path path, Query query) {
      return hierarchical(authority, path, query, null);
   }

   public static SftpUri hierarchical(ServerAuthority authority, Path path) {
      return hierarchical(authority, path, null, null);
   }

   public static SftpUri hierarchical(ServerAuthority authority, Path path, Fragment fragment) {
      return hierarchical(authority, path, null, fragment);
   }

   @Override
   protected SftpUri recreate(URI uri,
                              UriRestrictions restrictions,
                              Scheme scheme,
                              SchemeSpecific schemeSpecific,
                              Authority authority,
                              Path path,
                              Query query,
                              Fragment fragment,
                              boolean newObject) {
      return buildUri(() -> new SftpUri(uri,
                                        restrictions,
                                        scheme,
                                        schemeSpecific,
                                        authority,
                                        path,
                                        query,
                                        fragment,
                                        newObject));
   }

   @Override
   public SftpUriRestrictions restrictions() {
      return (SftpUriRestrictions) super.restrictions();
   }

   public SftpUri sftpRestrictions(Function<? super SftpUriRestrictions, ? extends SftpUriRestrictions> restrictionsMapper) {
      return restrictions(restrictionsMapper.apply(restrictions()));
   }

   /**
    * @implNote path must be normalized separately to preserve double-slashed sftp paths.
    */
   @Override
   public SftpUri normalize() {
      var path = this.path;
      return super.normalize().component().path(__ -> path.normalize());
   }

   @Override
   public SftpUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public SftpUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   public Optional<String> username() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .map(ui -> ui.username(false));
   }

   public Optional<String> password() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .flatMap(ui -> ui.password(false));
   }

   public boolean defaultPort() {
      return portOrDefault() == DEFAULT_PORT;
   }

   public int portOrDefault() {
      return port().orElse(DEFAULT_PORT);
   }

   public String sftpPath(boolean encoded) {
      var sftpPath = component().sftpPath();

      return encoded ? sftpPath.encodedSftpValue() : sftpPath.sftpValue();
   }

   public java.nio.file.Path sftpPathValue() {
      var sftpPath = component().sftpPath();

      return sftpPath.sftpPathValue();
   }

   public SftpUri sftpPath(boolean encoded, Function<String, ? extends String> pathMapper) {
      Check.notNull(pathMapper, "pathMapper");

      return component().sftpPath(path -> {
         String newPath = pathMapper.apply(encoded ? path.encodedSftpValue() : path.sftpValue());

         return encoded ? SftpPath.ofEncodedSftpPath(newPath) : SftpPath.ofSftpPath(newPath);
      });
   }

   public SftpUri conditionalSftpPath(boolean encoded, Function<String, ? extends String> pathMapper) {
      Check.notNull(pathMapper, "pathMapper");

      return component().conditionalSftpPath(path -> {
         String newPath = pathMapper.apply(encoded ? path.encodedSftpValue() : path.sftpValue());

         return encoded ? SftpPath.ofEncodedSftpPath(newPath) : SftpPath.ofSftpPath(newPath);
      });
   }

   @Override
   public Component component() {
      return new Component();
   }

   private static SftpUriRestrictions sftpRestrictions(UriRestrictions restrictions) {
      return nullable(restrictions)
            .map(r -> SftpUriRestrictions.of(r).defaultPortMapper(DEFAULT_PORT))
            .orElse(null);
   }

   private static ComponentUri sftpUri(ComponentUri uri) {
      try {
         return nullable(uri)
               .map(u -> u
                     .component()
                     .conditionalUserInfo(SftpUri::sftpUserInfo)
                     .component()
                     .conditionalPath(SftpUri::sftpPath))
               .orElse(null);
      } catch (Exception e) {
         throw new IncompatibleUriException(uri).subMessage(e);
      }
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static Authority sftpUserInfo(Authority authority) {
      return nullable(authority)
            .map(a -> instanceOf(a, ServerAuthority.class)
                  .map(sa -> (Authority) sa.userInfo(HttpUri::httpUserInfo))
                  .orElse(a))
            .orElse(null);
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static UsernamePasswordUserInfo sftpUserInfo(UserInfo ui) {
      return nullable(ui).map(UsernamePasswordUserInfo::of).orElse(null);
   }

   /**
    * Transforms path to {@link SftpPath} if needed.
    */
   protected static SftpPath sftpPath(Path path) {
      return nullable(path).map(SftpPath::of).orElse(null);
   }

   public class Component extends AbstractComponent {

      public Port portOrDefault() {
         return super.port().orElseGet(() -> SimplePort.of(DEFAULT_PORT));
      }

      public SftpPath sftpPath() {
         return nullableInstanceOf(path, SftpPath.class).orElseThrow();
      }

      public SftpUri sftpPath(Function<? super SftpPath, ? extends SftpPath> pathMapper) {
         Check.notNull(pathMapper, "pathMapper");

         return path(__ -> pathMapper.apply(sftpPath()));
      }

      public SftpUri conditionalSftpPath(Function<? super SftpPath, ? extends SftpPath> pathMapper) {
         Check.notNull(pathMapper, "pathMapper");

         return conditionalPath(__ -> pathMapper.apply(sftpPath()));
      }

   }

   public static class SftpUriRestrictions extends DefaultUriRestrictions {
      protected static final boolean DEFAULT_EMPTY_PATH = true;
      protected static final boolean DEFAULT_QUERY = true;
      protected static final boolean DEFAULT_FRAGMENT = true;

      protected final boolean emptyPath;

      protected SftpUriRestrictions(boolean emptyPath,
                                    PortRestriction portRestriction,
                                    BiFunction<String, Integer, Integer> portMapper,
                                    boolean query,
                                    boolean fragment) {
         super(portRestriction, portMapper, query, fragment);
         this.emptyPath = emptyPath;
      }

      public static SftpUriRestrictions of(UriRestrictions restrictions) {
         if (restrictions instanceof SftpUriRestrictions) {
            var sftpRestrictions = (SftpUriRestrictions) restrictions;
            return new SftpUriRestrictions(sftpRestrictions.emptyPath,
                                           sftpRestrictions.portRestriction,
                                           sftpRestrictions.portMapper,
                                           sftpRestrictions.query,
                                           sftpRestrictions.fragment);
         } else if (restrictions instanceof DefaultUriRestrictions) {
            var defaultRestrictions = (DefaultUriRestrictions) restrictions;
            return new SftpUriRestrictions(DEFAULT_EMPTY_PATH,
                                           defaultRestrictions.portRestriction(),
                                           defaultRestrictions.portMapper(),
                                           defaultRestrictions.query(),
                                           defaultRestrictions.fragment());
         } else {
            return new SftpUriRestrictions(DEFAULT_EMPTY_PATH, null, null, DEFAULT_QUERY, DEFAULT_FRAGMENT);
         }
      }

      public static SftpUriRestrictions ofRestrictions(boolean emptyPath, boolean query, boolean fragment) {
         return new SftpUriRestrictions(emptyPath, null, null, query, fragment);
      }

      public static SftpUriRestrictions ofDefault() {
         return ofRestrictions(DEFAULT_EMPTY_PATH, DEFAULT_QUERY, DEFAULT_FRAGMENT);
      }

      public static SftpUriRestrictions noRestrictions() {
         return ofRestrictions(false, false, false);
      }

      public static SftpUriRestrictions automatic(Path path, Query query, Fragment fragment) {
         return ofRestrictions(!path.isEmpty(), query == null, fragment == null);
      }

      @Override
      final protected SftpUriRestrictions recreate(PortRestriction portRestriction,
                                                   BiFunction<String, Integer, Integer> portMapper,
                                                   boolean query,
                                                   boolean fragment) {
         return recreate(emptyPath, portRestriction, portMapper, query, fragment);
      }

      protected SftpUriRestrictions recreate(boolean emptyPath,
                                             PortRestriction portRestriction,
                                             BiFunction<String, Integer, Integer> portMapper,
                                             boolean query,
                                             boolean fragment) {
         return new SftpUriRestrictions(emptyPath, portRestriction, portMapper, query, fragment);
      }

      /**
       * Returns {@code true} if empty paths are restricted.
       * <p>
       * This restriction is interpreted as a URI invalidity at build time.
       */
      public boolean emptyPath() {
         return emptyPath;
      }

      /**
       * Whether to restrict empty paths.
       * <p>
       * This restriction is interpreted as a URI invalidity at build time.
       */
      public SftpUriRestrictions emptyPath(boolean emptyPath) {
         return recreate(emptyPath, portRestriction, portMapper, query, fragment);
      }

      @Override
      public SftpUriRestrictions portRestriction(PortRestriction portRestriction) {
         return (SftpUriRestrictions) super.portRestriction(portRestriction);
      }

      @Override
      public SftpUriRestrictions portMapper(BiFunction<String, Integer, Integer> portMapper) {
         return (SftpUriRestrictions) super.portMapper(portMapper);
      }

      @Override
      public SftpUriRestrictions portMapper(Function<String, Integer> portMapper) {
         return (SftpUriRestrictions) super.portMapper(portMapper);
      }

      @Override
      public SftpUriRestrictions portMapper(Integer mappedPort) {
         return (SftpUriRestrictions) super.portMapper(mappedPort);
      }

      @Override
      public SftpUriRestrictions defaultPortMapper(BiFunction<String, Integer, Integer> portMapper) {
         return (SftpUriRestrictions) super.defaultPortMapper(portMapper);
      }

      @Override
      public SftpUriRestrictions defaultPortMapper(Function<String, Integer> portMapper) {
         return (SftpUriRestrictions) super.defaultPortMapper(portMapper);
      }

      @Override
      public SftpUriRestrictions defaultPortMapper(Integer mappedPort) {
         return (SftpUriRestrictions) super.defaultPortMapper(mappedPort);
      }

      @Override
      public SftpUriRestrictions query(boolean query) {
         return (SftpUriRestrictions) super.query(query);
      }

      @Override
      public SftpUriRestrictions fragment(boolean fragment) {
         return (SftpUriRestrictions) super.fragment(fragment);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SftpUriRestrictions.class.getSimpleName() + "[", "]")
               .add("emptyPath=" + emptyPath)
               .add("portRestriction=" + portRestriction)
               .add("query=" + query)
               .add("fragment=" + fragment)
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SftpUriRestrictions restrictions = (SftpUriRestrictions) o;
         return portRestriction == restrictions.portRestriction
                && query == restrictions.query
                && fragment == restrictions.fragment
                && emptyPath == restrictions.emptyPath;
      }

      @Override
      public int hashCode() {
         return Objects.hash(super.hashCode(), emptyPath);
      }
   }

   public static class SftpPath extends SimplePath implements Path {
      private static final SftpPath ROOT_PATH = new SftpPath("//", "//");
      private static final SftpPath EMPTY_PATH = new SftpPath("", "");

      protected java.nio.file.Path sftpTypedPath;

      protected SftpPath(String path, String encodedPath) {
         super(Check.validate(path, "path", isValidPath()),
               Check.validate(encodedPath, "encodedPath", isValidPath()));
      }

      public static SftpPath emptyPath() {
         return EMPTY_PATH;
      }

      public static SftpPath rootPath() {
         return ROOT_PATH;
      }

      public static SftpPath of(String path) {
         Check.notNull(path, "path");

         return new SftpPath(path, encodePath(path).orElseThrow(identity()));
      }

      public static SftpPath of(java.nio.file.Path path) {
         Check.notNull(path, "path");

         return of(path.toString());
      }

      public static SftpPath ofEncoded(String encodedPath) {
         Check.notNull(encodedPath, "encodedPath");

         return new SftpPath(decode(encodedPath).orElseThrow(identity()), encodedPath);
      }

      public static Optional<? extends SftpPath> of(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getPath()).map(__ -> new SftpPath(uri.getPath(), uri.getRawPath()));
      }

      public static SftpPath of(Path path) {
         Check.notNull(path, "path");

         if (path instanceof SftpPath) {
            return (SftpPath) path;
         } else {
            return new SftpPath(path.value(), path.encodedValue());
         }
      }

      public static SftpPath ofSftpPath(String path) {
         Check.notNull(path, "path");

         return of(uriPath(path));
      }

      public static SftpPath ofSftpPath(java.nio.file.Path path) {
         Check.notNull(path, "path");

         return ofSftpPath(path.toString());
      }

      public static SftpPath ofEncodedSftpPath(String encodedPath) {
         Check.notNull(encodedPath, "encodedPath");

         var uriPath = uriPath(encodedPath);

         return ofEncoded(uriPath);
      }

      @Override
      public Optional<? extends SftpPath> recreate(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getPath()).map(__ -> recreate(uri.getPath(), uri.getRawPath()));
      }

      @Override
      public SftpPath recreate(String path, String encodedPath) {
         return new SftpPath(path, encodedPath);
      }

      @Override
      public SftpPath value(String value) {
         return (SftpPath) super.value(value);
      }

      @Override
      public SftpPath encodedValue(String encodedPath) {
         return (SftpPath) super.encodedValue(encodedPath);
      }

      public String sftpValue() {
         return sftpPath(super.value());
      }

      public String encodedSftpValue() {
         return sftpPath(super.encodedValue());
      }

      public java.nio.file.Path sftpPathValue() {
         if (sftpTypedPath == null) {
            sftpTypedPath = java.nio.file.Path.of(sftpPath(path));
         }
         return sftpTypedPath;
      }

      public SftpPath sftpValue(String value) {
         Check.notNull(path, "path");

         var uriPath = uriPath(value);

         return recreate(uriPath, encodePath(uriPath).orElseThrow(identity()));
      }

      public SftpPath encodedSftpValue(String encodedPath) {
         Check.notNull(encodedPath, "encodedPath");

         var uriPath = uriPath(encodedPath);

         return recreate(decode(uriPath).orElseThrow(identity()), uriPath);
      }

      @Override
      public SftpPath normalize() {
         return (SftpPath) super.normalize(true, false);
      }

      public static String sftpPath(String uriPath) {
         if (uriPath == null) {
            return null;
         }
         return StringUtils.removeStart(uriPath, "/");
      }

      public static String uriPath(String sftpPath) {
         if (sftpPath == null) {
            return null;
         }
         return sftpPath.isEmpty() ? "" : "/" + sftpPath;
      }

      private static InvariantRule<String> isValidPath() {
         return PredicateInvariantRule.satisfiesValue(path -> path.startsWith("/") || path.isEmpty(),
                                                      FastStringFormat.of("'",
                                                                          validatingObject(),
                                                                          "' must be absolute or empty"));
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SftpPath that = (SftpPath) o;
         return Objects.equals(path, that.path) && Objects.equals(encodedPath, that.encodedPath);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SftpPath.class.getSimpleName() + "[", "]")
               .add("path=" + path)
               .add("encodedPath=" + encodedPath)
               .toString();
      }
   }

}
