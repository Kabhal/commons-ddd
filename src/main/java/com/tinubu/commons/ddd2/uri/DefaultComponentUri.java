/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.Validate.notNull;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.anySatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotStartWith;
import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.net.URI;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.ParameterValue;

/**
 * Default {@link ComponentUri} implementation.
 */
public class DefaultComponentUri extends AbstractComponentUri<DefaultComponentUri> implements ComponentUri {

   /**
    * Main and re-creation constructor, from {@link URI} and specified {@link Part parts}.
    *
    * @param uri URI
    * @param restrictions restrictions
    * @param newObject whether {@link Uri} is a new object
    */
   protected DefaultComponentUri(URI uri,
                                 UriRestrictions restrictions,
                                 Scheme scheme,
                                 SchemeSpecific schemeSpecific,
                                 Authority authority,
                                 Path path,
                                 Query query,
                                 Fragment fragment,
                                 boolean newObject) {
      super(uri, restrictions, scheme, schemeSpecific, authority, path, query, fragment, newObject);
   }

   /**
    * Alternative constructor, only using specified {@link Part parts}. {@link URI} is constructed from
    * specified parts.
    *
    * @param restrictions restrictions
    *
    * @implNote newObject is always set to false because it's a design decision to use
    *       {@link #asNewObject()} on constructed object to change new object's new object state.
    */
   protected DefaultComponentUri(UriRestrictions restrictions,
                                 Scheme scheme,
                                 SchemeSpecific schemeSpecific,
                                 Authority authority,
                                 Path path,
                                 Query query,
                                 Fragment fragment) {
      super(restrictions, scheme, schemeSpecific, authority, path, query, fragment);
   }

   /**
    * Alternative constructor, only using {@link URI}. Parts are created from specified URI using "simple"
    * implementations.
    *
    * @param uri URI
    * @param restrictions restrictions
    *
    * @implNote newObject is always set to false because it's a design decision to use
    *       {@link #asNewObject()} on constructed object to change new object's new object state.
    */
   protected DefaultComponentUri(URI uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   /**
    * Alternative constructor from existing {@link Uri}.
    * <p>
    * Specified {@code uri}'s {@link Uri#restrictions() restrictions} and {@link Uri#newObject()} are ignored
    * because it does not make sense for this different {@link Uri} implementation.
    *
    * @param uri component URI
    * @param restrictions restrictions
    *
    * @implNote newObject is always set to false because it's a design decision to use
    *       {@link #asNewObject()} on constructed object to change new object's new object state.
    */
   protected DefaultComponentUri(Uri uri, UriRestrictions restrictions) {
      super(uri, restrictions);
   }

   /**
    * Alternative constructor from existing {@link Uri}.
    * <p>
    * Specified {@code uri}'s {@link Uri#restrictions() restrictions} and {@link Uri#newObject()} are ignored
    * because it does not make sense for this different {@link Uri} implementation.
    *
    * @param uri URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultComponentUri} instance
    */
   public static DefaultComponentUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new DefaultComponentUri(uri, restrictions));
   }

   /**
    * Creates a {@link DefaultComponentUri} from an existing {@link Uri}, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    * <p>
    * Specified {@code uri}'s {@link Uri#restrictions() restrictions} and {@link Uri#newObject()} are ignored
    * because it does not make sense for this different {@link Uri} implementation.
    *
    * @param uri URI
    *
    * @return new {@link DefaultComponentUri} instance
    */
   public static DefaultComponentUri ofUri(Uri uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a {@link DefaultComponentUri} from an existing {@link URI}.
    *
    * @param uri URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultComponentUri} instance
    */
   public static DefaultComponentUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new DefaultComponentUri(uri, restrictions));
   }

   /**
    * Creates a {@link DefaultComponentUri} from an existing {@link URI}, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param uri URI
    *
    * @return new {@link DefaultComponentUri} instance
    */
   public static DefaultComponentUri ofUri(URI uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a {@link DefaultComponentUri} from an existing <em>encoded</em> URI.
    *
    * @param uri encoded URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultComponentUri} instance
    */
   public static DefaultComponentUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   /**
    * Creates a {@link DefaultComponentUri} from an existing <em>encoded</em> URI, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param uri encoded URI
    *
    * @return new {@link DefaultComponentUri} instance
    */
   public static DefaultComponentUri ofUri(String uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a new {@link DefaultComponentUri} for a reconstituting object with specified URN.
    *
    * @param urn URN
    * @param restrictions URI restrictions
    *
    * @return new id
    */
   public static DefaultComponentUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   /**
    * Creates a new {@link DefaultComponentUri} for a reconstituting object with specified URN, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static DefaultComponentUri ofUrn(URI urn) {
      return ofUrn(urn, DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param authority optional authority, or {@code null}
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    *
    * @implSpec Path must not be {@code null} by design. An empty path must be set if URI has
    *       no path, so that path value is symmetrical in read and write.
    */
   public static DefaultComponentUri hierarchical(Scheme scheme,
                                                  Authority authority,
                                                  Path path,
                                                  Query query,
                                                  Fragment fragment) {
      InvariantRule<Path> pathRule;

      if (authority == null) {
         pathRule = satisfies(Path::isAbsolute, "'%s' must be absolute", validatingObject());
      } else {
         pathRule = satisfies(p -> p.isAbsolute() || p.isEmpty(),
                              "'%s' must be absolute or empty",
                              validatingObject());
      }

      notNull(scheme, "scheme").and(validate(path, "path", pathRule)).orThrow();

      return buildUri(() -> new DefaultComponentUri(DefaultUriRestrictions.ofDefault(),
                                                    scheme,
                                                    null,
                                                    authority,
                                                    path,
                                                    query,
                                                    fragment));
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param authority optional authority, or {@code null}
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri hierarchical(Scheme scheme,
                                                  Authority authority,
                                                  Path path,
                                                  Query query) {
      return hierarchical(scheme, authority, path, query, null);
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param authority optional authority, or {@code null}
    * @param path absolute or empty path
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri hierarchical(Scheme scheme,
                                                  Authority authority,
                                                  Path path,
                                                  Fragment fragment) {
      return hierarchical(scheme, authority, path, null, fragment);
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param authority optional authority, or {@code null}
    * @param path absolute or empty path
    *
    * @return new instance
    */
   public static DefaultComponentUri hierarchical(Scheme scheme, Authority authority, Path path) {
      return hierarchical(scheme, authority, path, null, null);
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri hierarchical(Scheme scheme, Path path, Query query, Fragment fragment) {
      return hierarchical(scheme, null, path, query, fragment);
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri hierarchical(Scheme scheme, Path path, Query query) {
      return hierarchical(scheme, null, path, query, null);
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param path absolute or empty path
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri hierarchical(Scheme scheme, Path path, Fragment fragment) {
      return hierarchical(scheme, null, path, null, fragment);
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri.
    *
    * @param scheme scheme
    * @param path absolute or empty path
    *
    * @return new instance
    */
   public static DefaultComponentUri hierarchical(Scheme scheme, Path path) {
      return hierarchical(scheme, null, path, null, null);
   }

   /**
    * Creates an (<em>absolute</em>) <em>opaque</em> Uri.
    *
    * @param scheme scheme
    * @param schemeSpecific scheme-specific part, does not start with {@code /}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri opaque(Scheme scheme, SchemeSpecific schemeSpecific, Fragment fragment) {
      notNull(scheme, "scheme")
            .and(validate(schemeSpecific,
                          "schemeSpecific",
                          isNotNull().andValue(as(SchemeSpecific::value,
                                                  doesNotStartWith(ParameterValue.value("/"))))))
            .orThrow();

      return buildUri(() -> new DefaultComponentUri(DefaultUriRestrictions.ofDefault(),
                                                    scheme,
                                                    schemeSpecific,
                                                    null,
                                                    null,
                                                    null,
                                                    fragment));
   }

   /**
    * Creates an (<em>absolute</em>) <em>opaque</em> Uri.
    *
    * @param scheme scheme
    * @param schemeSpecific scheme-specific part, does not start with {@code /}
    *
    * @return new instance
    */
   public static DefaultComponentUri opaque(Scheme scheme, SchemeSpecific schemeSpecific) {
      return opaque(scheme, schemeSpecific, null);
   }

   /**
    * Creates a (<em>hierarchical</em>) <em>relative</em> Uri.
    *
    * @param authority optional authority, or {@code null}
    * @param path relative (if authority is not set), absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    *
    * @implSpec Path must not be {@code null} by design. An empty path must be set if URI has
    *       no path, so that path value is symmetrical in read and write.
    */
   public static DefaultComponentUri relative(Authority authority,
                                              Path path,
                                              Query query,
                                              Fragment fragment) {
      validate(list(authority, path, query, fragment),
               anySatisfies(isNotNull(), "At least one part must be set"))
            .and(validate(path,
                          "path",
                          isNotNull().andValue(satisfies((Path p) -> p.isAbsolute() || p.isEmpty(),
                                                         "'%s' must be absolute or empty",
                                                         validatingObject()).ifNonNull(authority))))
            .orThrow();

      return buildUri(() -> new DefaultComponentUri(DefaultUriRestrictions.ofDefault(),
                                                    null,
                                                    null,
                                                    authority,
                                                    path,
                                                    query,
                                                    fragment));
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative authority and path.
    *
    * @param authority optional authority, or {@code null}
    * @param path relative (if authority is not set), absolute or empty path
    *
    * @return new instance
    */
   public static DefaultComponentUri relative(Authority authority, Path path) {
      return relative(authority, path, null, null);
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative path.
    *
    * @param path relative, absolute or empty path
    *
    * @return new instance
    */
   public static DefaultComponentUri relative(Path path) {
      return relative(null, path, null, null);
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative query.
    *
    * @param path relative, absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri relative(Path path, Query query) {
      return relative(null, path, query, null);
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative fragment.
    *
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultComponentUri relative(Fragment fragment) {
      return relative(null, null, null, fragment);
   }

   @Override
   protected DefaultComponentUri recreate(URI uri,
                                          UriRestrictions restrictions,
                                          Scheme scheme,
                                          SchemeSpecific schemeSpecific,
                                          Authority authority,
                                          Path path,
                                          Query query,
                                          Fragment fragment,
                                          boolean newObject) {
      return buildUri(() -> new DefaultComponentUri(uri,
                                                    restrictions,
                                                    scheme,
                                                    schemeSpecific,
                                                    authority,
                                                    path,
                                                    query,
                                                    fragment,
                                                    newObject));
   }

}
