/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.uri.UriEncoder.decode;
import static com.tinubu.commons.ddd2.uri.UriEncoder.decodeSchemeSpecific;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeSchemeSpecific;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.SchemeSpecific;

public class SimpleSchemeSpecific implements SchemeSpecific {

   protected final String schemeSpecific;
   protected final String encodedSchemeSpecific;

   protected SimpleSchemeSpecific(String schemeSpecific, String encodedSchemeSpecific) {
      this.schemeSpecific = Check.validate(schemeSpecific, "schemeSpecific", isNotBlank());
      this.encodedSchemeSpecific =
            Check.validate(encodedSchemeSpecific, "encodedSchemeSpecific", isNotBlank());
   }

   public static SimpleSchemeSpecific of(String schemeSpecific) {
      Check.validate(schemeSpecific, "schemeSpecific", isNotBlank());

      return new SimpleSchemeSpecific(schemeSpecific,
                                      encodeSchemeSpecific(schemeSpecific).orElseThrow(identity()));
   }

   public static SimpleSchemeSpecific ofEncoded(String encodedSchemeSpecific) {
      Check.validate(encodedSchemeSpecific, "encodedSchemeSpecific", isNotBlank());

      return new SimpleSchemeSpecific(decode(encodedSchemeSpecific).orElseThrow(identity()),
                                      encodedSchemeSpecific);
   }

   public static Optional<? extends SimpleSchemeSpecific> of(URI uri) {
      Check.notNull(uri, "uri");

      if (uri.isAbsolute()) {
         return optional(new SimpleSchemeSpecific(uri.getSchemeSpecificPart(),
                                                  uri.getRawSchemeSpecificPart()));
      } else {
         return optional();
      }
   }

   public static SimpleSchemeSpecific of(SchemeSpecific scheme) {
      Check.notNull(scheme, "scheme");

      return new SimpleSchemeSpecific(scheme.value(), scheme.encodedValue());
   }

   @Override
   public Optional<? extends SimpleSchemeSpecific> recreate(URI uri) {
      Check.notNull(uri, "uri");

      if (uri.isAbsolute()) {
         return optional(new SimpleSchemeSpecific(uri.getSchemeSpecificPart(),
                                                  uri.getRawSchemeSpecificPart()));
      } else {
         return optional();
      }
   }

   public SimpleSchemeSpecific recreate(String value, String encodedValue) {
      return new SimpleSchemeSpecific(value, encodedValue);
   }

   @Override
   public String value() {
      return schemeSpecific;
   }

   @Override
   public String encodedValue() {
      return encodedSchemeSpecific;
   }

   @Override
   public StringBuilder appendEncodedValue(StringBuilder builder) {
      return builder.append(encodedSchemeSpecific);
   }

   @Override
   public SimpleSchemeSpecific value(String value) {
      Check.notNull(value, "value");

      return recreate(value, encodeSchemeSpecific(value).orElseThrow(identity()));
   }

   @Override
   public SimpleSchemeSpecific encodedValue(String encodedValue) {
      Check.notNull(encodedValue, "encodedValue");

      return recreate(decodeSchemeSpecific(encodedValue).orElseThrow(identity()), encodedValue);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimpleSchemeSpecific that = (SimpleSchemeSpecific) o;
      return Objects.equals(schemeSpecific, that.schemeSpecific) && Objects.equals(encodedSchemeSpecific,
                                                                                   that.encodedSchemeSpecific);
   }

   @Override
   public int hashCode() {
      return Objects.hash(schemeSpecific, encodedSchemeSpecific);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SimpleSchemeSpecific.class.getSimpleName() + "[", "]")
            .add("schemeSpecific='" + schemeSpecific + "'")
            .add("encodedSchemeSpecific='" + encodedSchemeSpecific + "'")
            .toString();
   }
}
