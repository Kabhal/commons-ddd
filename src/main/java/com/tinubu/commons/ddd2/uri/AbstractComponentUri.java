/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;

import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.parts.SimpleAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleSchemeSpecific;

/**
 * URI implementations that supports flexible URI build and part replacement, always preserving original
 * encoding.
 * Each URI part can be specified/updated, as encoded or decoded string. Once encoded, a part encoding is
 * preserved if URI is updated in any way.
 *
 * @implSpec To support {@code scheme:///path} or {@code //?query} URI, server-based authority can be
 *       empty, contrarily to {@link URI}.
 */
public abstract class AbstractComponentUri<T extends AbstractComponentUri<T>> extends AbstractUri<T>
      implements ComponentUri {

   protected final Scheme scheme;
   protected final SchemeSpecific schemeSpecific;
   protected final Authority authority;
   protected final Path path;
   protected final Query query;
   protected final Fragment fragment;

   protected final Component component = new AbstractComponent() {};

   /**
    * Main and re-creation constructor, from {@link URI} and specified {@link Part parts}.
    *
    * @param uri URI
    * @param restrictions restrictions
    * @param newObject whether {@link Uri} is a new object
    *
    * @implNote SchemeSpecific must be derived from transformed URI here if missing, and if it applied
    *       to current URI format.
    *       Authority must be recreated to align on super's URI authority transformation.
    */
   protected AbstractComponentUri(URI uri,
                               UriRestrictions restrictions,
                               Scheme scheme,
                               SchemeSpecific schemeSpecific,
                               Authority authority,
                               Path path,
                               Query query,
                               Fragment fragment,
                               boolean newObject) {
      super(uri, restrictions, newObject);

      var transformedUri = this.uri;

      this.scheme = scheme;
      this.schemeSpecific = nullable(schemeSpecific, SimpleSchemeSpecific.of(transformedUri).orElse(null));
      this.authority = nullable(authority).flatMap(a -> a.recreate(transformedUri)).orElse(null);
      this.path = path;
      this.query = query;
      this.fragment = fragment;
   }

   /**
    * Alternative constructor, only using specified {@link Part parts}. {@link URI} is constructed from
    * specified parts.
    *
    * @param restrictions restrictions
    *
    * @implNote newObject is always set to false because it's a design decision to use
    *       {@link #asNewObject()} on constructed object to change new object's new object state.
    */
   protected AbstractComponentUri(UriRestrictions restrictions,
                               Scheme scheme,
                               SchemeSpecific schemeSpecific,
                               Authority authority,
                               Path path,
                               Query query,
                               Fragment fragment) {
      this(uri(scheme, schemeSpecific, authority, path, query, fragment),
           restrictions,
           scheme,
           schemeSpecific,
           authority,
           path,
           query,
           fragment,
           false);
   }

   /**
    * Alternative constructor, only using {@link URI}. Parts are created from specified URI using "simple"
    * implementations.
    *
    * @param uri URI
    * @param restrictions restrictions
    *
    * @implNote newObject is always set to false because it's a design decision to use
    *       {@link #asNewObject()} on constructed object to change new object's new object state.
    */
   protected AbstractComponentUri(URI uri, UriRestrictions restrictions) {
      this(uri,
           restrictions,
           SimpleScheme.of(uri).orElse(null),
           SimpleSchemeSpecific.of(uri).orElse(null),
           SimpleAuthority.of(uri).orElse(null),
           SimplePath.of(uri).orElse(null),
           SimpleQuery.of(uri).orElse(null),
           SimpleFragment.of(uri).orElse(null),
           false);
   }

   /**
    * Alternative constructor from existing {@link ComponentUri}.
    * <p>
    * Specified {@code uri}'s {@link Uri#restrictions() restrictions} and {@link Uri#newObject()} are ignored
    * because it does not make sense for this different {@link Uri} implementation.
    *
    * @param uri component URI
    * @param restrictions restrictions
    *
    * @implNote newObject is always set to false because it's a design decision to use
    *       {@link #asNewObject()} on constructed object to change new object's new object state.
    */
   protected AbstractComponentUri(ComponentUri uri, UriRestrictions restrictions) {
      this(Check.notNull(uri, "uri").toURI(),
           restrictions,
           uri.component().scheme().orElse(null),
           uri.component().schemeSpecific().orElse(null),
           uri.component().authority().orElse(null),
           uri.component().path().orElse(null),
           uri.component().query().orElse(null),
           uri.component().fragment().orElse(null),
           false);
   }

   /**
    * Alternative constructor from existing {@link Uri}.
    * <p>
    * Specified {@code uri}'s {@link Uri#restrictions() restrictions} and {@link Uri#newObject()} are ignored
    * because it does not make sense for this different {@link Uri} implementation.
    *
    * @param uri component URI
    * @param restrictions restrictions
    *
    * @implNote newObject is always set to false because it's a design decision to use
    *       {@link #asNewObject()} on constructed object to change new object's new object state.
    *       This constructor supports {@link ComponentUri} by always redirecting to
    *       {@link #AbstractComponentUri(ComponentUri, UriRestrictions)}. If specified {@link Uri} is not a
    *       {@link ComponentUri} it is created using {@link ComponentUri#ofUri(URI)} for both performance,
    *       and to prevent an infinite loop when called from
    *       {@link DefaultComponentUri#DefaultComponentUri(Uri, UriRestrictions)}
    */
   protected AbstractComponentUri(Uri uri, UriRestrictions restrictions) {
      this(nullableInstanceOf(uri, ComponentUri.class, u -> ComponentUri.ofUri(u.toURI())), restrictions);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends T> defineDomainFields() {
      return Fields
            .<T>builder()
            .superFields((Fields<? super T>) super.defineDomainFields())
            .field("scheme", v -> v.scheme)
            .field("schemeSpecific", v -> v.schemeSpecific)
            .field("authority", v -> v.authority)
            .field("path", v -> v.path)
            .field("query", v -> v.query)
            .field("fragment", v -> v.fragment)
            .build();
   }

   /**
    * @implNote Implementors must override
    *       {@link #recreate(URI, UriRestrictions, Scheme, SchemeSpecific, Authority, Path, Query, Fragment,
    *       boolean)} instead.
    */
   @Override
   final protected T recreate(URI uri, UriRestrictions restrictions, boolean newObject) {
      return recreate(uri,
                      restrictions,
                      nullable(scheme).flatMap(s -> s.recreate(uri)).orElse(null),
                      nullable(schemeSpecific).flatMap(s -> s.recreate(uri)).orElse(null),
                      nullable(authority).flatMap(s -> s.recreate(uri)).orElse(null),
                      nullable(path).flatMap(s -> s.recreate(uri)).orElse(null),
                      nullable(query).flatMap(s -> s.recreate(uri)).orElse(null),
                      nullable(fragment).flatMap(s -> s.recreate(uri)).orElse(null),
                      newObject);
   }

   protected abstract T recreate(URI uri,
                                 UriRestrictions restrictions,
                                 Scheme scheme,
                                 SchemeSpecific schemeSpecific,
                                 Authority authority,
                                 Path path,
                                 Query query,
                                 Fragment fragment,
                                 boolean newObject);

   /**
    * @implNote Overrides for optimization, because parts does not need to be recreated in this case.
    */
   @Override
   public T asNewObject() {
      return recreate(uri, restrictions, scheme, schemeSpecific, authority, path, query, fragment, true);
   }

   @Override
   public Component component() {
      return component;
   }

   protected static URI uri(Scheme scheme,
                            SchemeSpecific schemeSpecific,
                            Authority authority,
                            Path path,
                            Query query,
                            Fragment fragment) {
      StringBuilder builder = new StringBuilder();

      nullable(scheme).ifPresent(s -> s.appendEncodedValue(builder));
      if (path != null && authority == null && schemeSpecific != null && schemeSpecific
            .value()
            .startsWith("//")) {
         builder.append("//");
         nullable(path).ifPresent(p -> p.appendEncodedValue(builder));
         nullable(query).ifPresent(q -> q.appendEncodedValue(builder));
      } else if (authority != null || path != null || query != null) {
         nullable(authority).ifPresent(a -> a.appendEncodedValue(builder));
         nullable(path).ifPresent(p -> p.appendEncodedValue(builder));
         nullable(query).ifPresent(q -> q.appendEncodedValue(builder));
      } else {
         nullable(schemeSpecific).ifPresent(ss -> ss.appendEncodedValue(builder));
      }
      nullable(fragment).ifPresent(f -> f.appendEncodedValue(builder));

      return Uri.uri(builder.toString());
   }

   public abstract class AbstractComponent implements Component {

      public Optional<Scheme> scheme() {
         return nullable(scheme);
      }

      public Optional<SchemeSpecific> schemeSpecific() {
         return nullable(schemeSpecific);
      }

      public Optional<Authority> authority() {
         return nullable(authority);
      }

      public Optional<ServerAuthority> serverAuthority() {
         return nullableInstanceOf(authority, ServerAuthority.class);
      }

      public Optional<RegistryAuthority> registryAuthority() {
         return nullableInstanceOf(authority, RegistryAuthority.class);
      }

      public Optional<UserInfo> userInfo() {
         return serverAuthority().flatMap(ServerAuthority::userInfo);
      }

      public Optional<Host> host() {
         return serverAuthority().flatMap(ServerAuthority::host);
      }

      public Optional<Port> port() {
         return serverAuthority().flatMap(ServerAuthority::port);
      }

      public Optional<Path> path() {
         return nullable(path);
      }

      public Optional<Query> query() {
         return nullable(query);
      }

      public Optional<Fragment> fragment() {
         return nullable(fragment);
      }

      /**
       * Updates scheme with specified value if {@link Uri} is {@link #isAbsolute() absolute}.
       *
       * @param schemeMapper scheme function of current scheme, current scheme is never {@code null},
       *       function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not absolute
       * @see #conditionalScheme(Function)
       */
      public T scheme(Function<? super Scheme, ? extends Scheme> schemeMapper) {
         Check.validate(AbstractComponentUri.this, "this", UriRules.isAbsolute());
         Check.notNull(schemeMapper, "schemeMapper");

         var newScheme = Check.notNull(schemeMapper.apply(scheme), "scheme");
         var uri = uri(newScheme, schemeSpecific, authority, path, query, fragment);

         return recreate(uri,
                         restrictions,
                         newScheme,
                         schemeSpecific,
                         authority,
                         path,
                         query,
                         fragment,
                         newObject);
      }

      /**
       * Updates scheme with specified value only if {@link Uri} is {@link #isAbsolute() absolute}, otherwise
       * does nothing.
       *
       * @param schemeMapper scheme function of current scheme, current scheme is never {@code null},
       *       function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #scheme(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalScheme(Function<? super Scheme, ? extends Scheme> schemeMapper) {
         Check.notNull(schemeMapper, "schemeMapper");

         if (isAbsolute()) {
            return scheme(schemeMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates scheme-specific with specified value if {@link Uri} is {@link #isOpaque() opaque}.
       *
       * @param schemeSpecificMapper scheme-specific function of current scheme-specific,
       *       scheme-specific is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not opaque
       * @see #conditionalSchemeSpecific(Function)
       */
      public T schemeSpecific(Function<? super SchemeSpecific, ? extends SchemeSpecific> schemeSpecificMapper) {
         Check.validate(AbstractComponentUri.this, "this", UriRules.isOpaque());
         Check.notNull(schemeSpecificMapper, "schemeSpecificMapper");

         var newSchemeSpecific = Check.notNull(schemeSpecificMapper.apply(schemeSpecific), "schemeSpecific");
         var uri = uri(scheme, newSchemeSpecific, null, null, null, fragment);
         return recreate(uri,
                         restrictions,
                         scheme,
                         newSchemeSpecific,
                         nullable(authority).flatMap(a -> a.recreate(uri)).orElse(null),
                         nullable(path).flatMap(p -> p.recreate(uri)).orElse(null),
                         nullable(query).flatMap(q -> q.recreate(uri)).orElse(null),
                         fragment,
                         newObject);
      }

      /**
       * Updates scheme with specified value only if {@link Uri} is {@link #isOpaque() opaque}, otherwise does
       * nothing.
       *
       * @param schemeSpecificMapper scheme-specific function of current scheme-specific,
       *       scheme-specific is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #schemeSpecific(boolean, String)
       */
      @SuppressWarnings("unchecked")
      public T conditionalSchemeSpecific(Function<? super SchemeSpecific, ? extends SchemeSpecific> schemeSpecificMapper) {
         Check.notNull(schemeSpecificMapper, "schemeSpecificMapper");

         if (isOpaque()) {
            return schemeSpecific(schemeSpecificMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates server-based authority with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based}.
       *
       * @param authorityMapper server-based authority function of current server authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based
       * @see #conditionalServerAuthority(Function)
       */
      public T serverAuthority(Function<? super ServerAuthority, ? extends ServerAuthority> authorityMapper) {
         Check.validate(AbstractComponentUri.this, "this", UriRules.isHierarchicalServer());
         Check.notNull(authorityMapper, "authorityMapper");

         var newAuthority = Check.notNull(authorityMapper.apply((ServerAuthority) authority), "authority");
         var uri = uri(scheme, schemeSpecific, newAuthority, path, query, fragment);
         return recreate(uri,
                         restrictions,
                         scheme,
                         nullable(schemeSpecific).flatMap(ss -> ss.recreate(uri)).orElse(null),
                         newAuthority,
                         path,
                         query,
                         fragment,
                         newObject);
      }

      /**
       * Updates server-based authority with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param authorityMapper server-based authority function of current server authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #serverAuthority(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalServerAuthority(Function<? super ServerAuthority, ? extends ServerAuthority> authorityMapper) {
         Check.notNull(authorityMapper, "authorityMapper");

         if (isHierarchicalServer()) {
            return serverAuthority(authorityMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates registry-based authority with specified value if {@link Uri} is
       * {@link #isHierarchicalRegistry() hierarchical registry-based}.
       *
       * @param authorityMapper registry-based authority function of current registry authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical registry-based
       * @see #conditionalRegistryAuthority(Function)
       */
      public T registryAuthority(Function<? super RegistryAuthority, ? extends RegistryAuthority> authorityMapper) {
         Check.validate(AbstractComponentUri.this, "this", UriRules.isHierarchicalRegistry());
         Check.notNull(authorityMapper, "authorityMapper");

         var newAuthority = Check.notNull(authorityMapper.apply((RegistryAuthority) authority), "authority");
         var uri = uri(scheme, schemeSpecific, newAuthority, path, query, fragment);
         return recreate(uri,
                         restrictions,
                         scheme,
                         nullable(schemeSpecific).flatMap(ss -> ss.recreate(uri)).orElse(null),
                         newAuthority,
                         path,
                         query,
                         fragment,
                         newObject);
      }

      /**
       * Updates registry-based authority with specified value only if {@link Uri} is
       * {@link #isHierarchicalRegistry() hierarchical registry-based}, otherwise does
       * nothing.
       *
       * @param authorityMapper registry-based authority function of current registry authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #registryAuthority(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalRegistryAuthority(Function<? super RegistryAuthority, ? extends RegistryAuthority> authorityMapper) {
         Check.notNull(authorityMapper, "authorityMapper");

         if (isHierarchicalRegistry()) {
            return registryAuthority(authorityMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates user-info with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
       *
       * @param userInfoMapper user-info function of current user-info, current user-info can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
       *       hierarchical server-based
       * @see #conditionalUserInfo(Function)
       */
      public T userInfo(Function<? super UserInfo, ? extends UserInfo> userInfoMapper) {
         Check.validate(AbstractComponentUri.this,
                        "this",
                        UriRules.isHierarchicalServer().andValue(UriRules.isNotEmptyHierarchicalServer()));
         Check.notNull(userInfoMapper, "userInfoMapper");

         var newAuthority = ((ServerAuthority) authority).userInfo(userInfoMapper);
         var uri = uri(scheme, schemeSpecific, newAuthority, path, query, fragment);

         return recreate(uri,
                         restrictions,
                         scheme,
                         nullable(schemeSpecific).flatMap(ss -> ss.recreate(uri)).orElse(null),
                         newAuthority,
                         path,
                         query,
                         fragment,
                         newObject);
      }

      /**
       * Updates user-info with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param userInfoMapper user-info function of current user-info, current user-info can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #userInfo(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalUserInfo(Function<? super UserInfo, ? extends UserInfo> userInfoMapper) {
         Check.notNull(userInfoMapper, "userInfoMapper");

         if (isHierarchicalServer() && !isEmptyHierarchicalServer()) {
            return userInfo(userInfoMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates host with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
       *
       * @param hostMapper host function of current host, current host can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
       *       hierarchical server-based
       * @see #conditionalHost(Function)
       */
      public T host(Function<? super Host, ? extends Host> hostMapper) {
         Check.validate(AbstractComponentUri.this,
                        "this",
                        UriRules.isHierarchicalServer().andValue(UriRules.isNotEmptyHierarchicalServer()));
         Check.notNull(hostMapper, "hostMapper");

         var newAuthority = ((ServerAuthority) authority).host(hostMapper);
         var uri = uri(scheme, schemeSpecific, newAuthority, path, query, fragment);

         return recreate(uri,
                         restrictions,
                         scheme,
                         nullable(schemeSpecific).flatMap(ss -> ss.recreate(uri)).orElse(null),
                         newAuthority,
                         path,
                         query,
                         fragment,
                         newObject);
      }

      /**
       * Updates host with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param hostMapper host function of current host, current host can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #host(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalHost(Function<? super Host, ? extends Host> hostMapper) {
         Check.notNull(hostMapper, "hostMapper");

         if (isHierarchicalServer() && !isEmptyHierarchicalServer()) {
            return host(hostMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates port with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
       *
       * @param portMapper port function of current port, current port can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
       *       hierarchical server-based
       * @see #conditionalPort(Function)
       */
      public T port(Function<? super Port, ? extends Port> portMapper) {
         Check.validate(AbstractComponentUri.this,
                        "this",
                        UriRules.isHierarchicalServer().andValue(UriRules.isNotEmptyHierarchicalServer()));
         Check.notNull(portMapper, "portMapper");

         var newAuthority = ((ServerAuthority) AbstractComponentUri.this.authority).port(portMapper);
         var uri = uri(scheme, schemeSpecific, newAuthority, path, query, fragment);

         return recreate(uri,
                         restrictions,
                         scheme,
                         nullable(schemeSpecific).flatMap(ss -> ss.recreate(uri)).orElse(null),
                         newAuthority,
                         path,
                         query,
                         fragment,
                         newObject);
      }

      /**
       * Updates port with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param portMapper port function of current port, current port can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #port(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalPort(Function<? super Port, ? extends Port> portMapper) {
         Check.notNull(portMapper, "portMapper");

         if (isHierarchicalServer() && !isEmptyHierarchicalServer()) {
            return port(portMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates path with specified value if {@link Uri} is {@link #isHierarchical() hierarchical}.
       *
       * @param pathMapper port function of current path, current path is never
       *       {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical, or path does not comply with
       *       rules
       * @see #conditionalPath(Function)
       */
      public T path(Function<? super Path, ? extends Path> pathMapper) {
         Check.validate(AbstractComponentUri.this, "this", UriRules.isHierarchical());
         Check.notNull(pathMapper, "pathMapper");

         var newPath = Check.notNull(pathMapper.apply(path), "path");

         Check.validate(newPath,
                        "path",
                        isNotNull().andValue(satisfies(Path::isAbsolute,
                                                       "'%s' must be absolute",
                                                       validatingObject())
                                                   .ifIsSatisfied(() -> isAbsolute()
                                                                        && !(isHierarchicalServer()
                                                                             || isHierarchicalRegistry()))
                                                   .andValue(satisfies((Path p) -> p.isAbsolute()
                                                                                   || p.isEmpty(),
                                                                       "'%s' must be absolute or empty",
                                                                       validatingObject()).ifIsSatisfied(() -> isAbsolute()
                                                                                                               || isHierarchicalServer()
                                                                                                               || isHierarchicalRegistry()))));

         var uri = uri(scheme, schemeSpecific, authority, newPath, query, fragment);
         return recreate(uri,
                         restrictions,
                         scheme,
                         nullable(schemeSpecific).flatMap(ss -> ss.recreate(uri)).orElse(null),
                         authority,
                         newPath,
                         query,
                         fragment,
                         newObject);
      }

      /**
       * Updates path with specified value only if {@link Uri} is {@link #isHierarchical() hierarchical},
       * otherwise does nothing.
       *
       * @param pathMapper port function of current path, current path is never
       *       {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if path does not comply with rules
       * @see #path(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalPath(Function<? super Path, ? extends Path> pathMapper) {
         Check.notNull(pathMapper, "pathMapper");

         if (isHierarchical()) {
            return path(pathMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates query with specified value if {@link Uri} is {@link #isHierarchical() hierarchical}.
       *
       * @param queryMapper query function of current query, current query can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical
       * @see #conditionalQuery(Function)
       */
      public T query(Function<? super Query, ? extends Query> queryMapper) {
         Check.validate(AbstractComponentUri.this, "this", UriRules.isHierarchical());
         Check.notNull(queryMapper, "queryMapper");

         var newQuery = queryMapper.apply(query);
         var uri = uri(scheme, schemeSpecific, authority, path, newQuery, fragment);

         return recreate(uri,
                         restrictions,
                         scheme,
                         nullable(schemeSpecific).flatMap(ss -> ss.recreate(uri)).orElse(null),
                         authority,
                         path,
                         newQuery,
                         fragment,
                         newObject);
      }

      /**
       * Updates query with specified value only if {@link Uri} is {@link #isHierarchical() hierarchical},
       * otherwise does nothing.
       *
       * @param queryMapper query function of current query, current query can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #query(Function)
       */
      @SuppressWarnings("unchecked")
      public T conditionalQuery(Function<? super Query, ? extends Query> queryMapper) {
         Check.notNull(queryMapper, "queryMapper");

         if (isHierarchical()) {
            return query(queryMapper);
         } else {
            return (T) AbstractComponentUri.this;
         }
      }

      /**
       * Updates fragment with specified value.
       *
       * @param fragmentMapper fragment function of current fragment, current fragment can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #conditionalFragment(Function)
       */
      public T fragment(Function<? super Fragment, ? extends Fragment> fragmentMapper) {
         Check.notNull(fragmentMapper, "fragmentMapper");

         var newFragment = fragmentMapper.apply(fragment);
         var uri = uri(scheme, schemeSpecific, authority, path, query, newFragment);

         return recreate(uri,
                         restrictions,
                         scheme,
                         schemeSpecific,
                         authority,
                         path,
                         query,
                         newFragment,
                         newObject);
      }

      /**
       * Updates fragment with specified value.
       *
       * @param fragmentMapper fragment function of current fragment, current fragment can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @implSpec This is an alias for {@link #fragment(Function)} because updating fragment
       *       is always possible whatever the URI format.
       * @see #fragment(Function)
       */
      public T conditionalFragment(Function<? super Fragment, ? extends Fragment> fragmentMapper) {
         Check.notNull(fragmentMapper, "fragmentMapper");

         return fragment(fragmentMapper);
      }

   }

}
