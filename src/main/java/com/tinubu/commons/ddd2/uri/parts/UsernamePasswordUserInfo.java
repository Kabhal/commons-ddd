/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.uri.UriEncoder.decode;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.UriEncoder;

public class UsernamePasswordUserInfo extends SimpleUserInfo {

   protected String username;
   protected String encodedUsername;
   protected Optional<String> password;
   protected Optional<String> encodedPassword;

   protected UsernamePasswordUserInfo(String userInfo, String encodedUserInfo) {
      super(userInfo, encodedUserInfo);
   }

   protected UsernamePasswordUserInfo(UserInfo userInfo) {
      super(userInfo.value(), userInfo.encodedValue());
   }

   public static UsernamePasswordUserInfo of(String userInfo) {
      return new UsernamePasswordUserInfo(SimpleUserInfo.of(userInfo));
   }

   /**
    * Creates {@link UsernamePasswordUserInfo} from decoded username and password.
    *
    * @param username username, {@code ':@'} characters in username will be encoded
    * @param password password, {@code ':@'} characters in password will be encoded
    *
    * @return new instance
    *
    * @implSpec it could be possible to not encode {@code :} in password if we split userInfo at
    *       fist encountered {@code :}, however, other implementations could not apply the same logic, so
    *       we always encode {@code :} characters.
    */
   public static UsernamePasswordUserInfo of(String username, String password) {
      Check.notNull(username, "username");

      var userInfo =
            encodeUserInfo(username, true) + (password != null ? ':' + encodeUserInfo(password, true) : "");

      return ofEncoded(userInfo);
   }

   /**
    * Encodes user-info.
    * If specified user-info is separated username or password, then {@code :} characters are also encoded.
    *
    * @param userInfo userInfo, or username, or password
    * @param encodeUsernameOrPassword whether specified userInfo is a whole userInfo string, or
    *       username or password
    *
    * @return encoded query, or {@code null} if specified query is {@code null}
    */
   protected static String encodeUserInfo(String userInfo, boolean encodeUsernameOrPassword) {
      var encoded = UriEncoder.encodeUserInfo(userInfo).orElseThrow(identity());

      if (encoded != null && encodeUsernameOrPassword) {
         encoded = encoded.replace(":", "%3A");
      }

      return encoded;
   }

   public static UsernamePasswordUserInfo ofEncoded(String encodedUserInfo) {
      return new UsernamePasswordUserInfo(SimpleUserInfo.ofEncoded(encodedUserInfo));
   }

   public static Optional<? extends UsernamePasswordUserInfo> of(URI uri) {
      return SimpleUserInfo.of(uri).map(UsernamePasswordUserInfo::new);
   }

   public static UsernamePasswordUserInfo of(UserInfo userInfo) {
      Check.notNull(userInfo, "userInfo");

      if (userInfo instanceof UsernamePasswordUserInfo) {
         return (UsernamePasswordUserInfo) userInfo;
      } else {
         return new UsernamePasswordUserInfo(userInfo);
      }
   }

   @Override
   public Optional<? extends UsernamePasswordUserInfo> recreate(URI uri) {
      return super.recreate(uri).map(UsernamePasswordUserInfo::new);
   }

   @Override
   protected UsernamePasswordUserInfo recreate(String userInfo, String encodedUserInfo) {
      return new UsernamePasswordUserInfo(userInfo, encodedUserInfo);
   }

   /**
    * @implNote we must always work with the encoded value to correctly support encoded colon in URI.
    */
   public String username(boolean encoded) {
      if (encoded && encodedUsername != null) {
         return encodedUsername;
      } else if (!encoded && username != null) {
         return username;
      }

      var sepIdx = encodedUserInfo.indexOf(':');

      if (sepIdx == -1) {
         encodedUsername = encodedValue();
         username = value();
         return encoded ? encodedUsername : username;
      } else {
         encodedUsername = encodedUserInfo.substring(0, sepIdx);
         if (encoded) {
            return encodedUsername;
         } else {
            username = decode(encodedUsername).orElseThrow(identity());
            return username;
         }
      }
   }

   /**
    * @implNote we must always work with the encoded value to correctly support encoded colon in URI.
    */
   @SuppressWarnings("OptionalAssignedToNull")
   public Optional<String> password(boolean encoded) {
      if (encoded && encodedPassword != null) {
         return encodedPassword;
      } else if (!encoded && password != null) {
         return password;
      }

      var sepIdx = encodedUserInfo.indexOf(':');

      if (sepIdx == -1) {
         return password = encodedPassword = optional();
      } else {
         encodedPassword = optional(encodedUserInfo.substring(sepIdx + 1));
         if (encoded) {
            return encodedPassword;
         } else {
            return password = encodedPassword.map(p -> decode(p).orElseThrow(identity()));
         }
      }
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      UsernamePasswordUserInfo that = (UsernamePasswordUserInfo) o;
      return Objects.equals(userInfo, that.userInfo) && Objects.equals(encodedUserInfo, that.encodedUserInfo);
   }

   @Override
   public int hashCode() {
      return Objects.hash(userInfo, encodedUserInfo);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", UsernamePasswordUserInfo.class.getSimpleName() + "[", "]")
            .add("userInfo='" + userInfo + "'")
            .add("encodedUserInfo='" + encodedUserInfo + "'")
            .toString();
   }
}
