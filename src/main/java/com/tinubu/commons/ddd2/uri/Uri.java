/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.invariant.Validate.notNull;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import com.tinubu.commons.ddd2.domain.type.SimpleId;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.lang.util.Try;

/**
 * URI id.
 * <p>
 * {@link #stringValue()} returns encoded URI string representation.
 */
public interface Uri extends SimpleId<String> {

   /**
    * Creates a {@link Uri} from an existing <em>encoded</em> URI.
    *
    * @param uri encoded URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultUri} instance
    */
   static Uri ofUri(String uri, UriRestrictions restrictions) {
      return DefaultUri.ofUri(uri, restrictions);
   }

   /**
    * Creates a {@link Uri} from an existing <em>encoded</em> URI.
    *
    * @param uri encoded URI
    *
    * @return new {@link DefaultUri} instance
    */
   static Uri ofUri(String uri) {
      return DefaultUri.ofUri(uri);
   }

   /**
    * Creates a {@link Uri} from an existing {@link URI}.
    *
    * @param uri URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultUri} instance
    */
   static Uri ofUri(URI uri, UriRestrictions restrictions) {
      return DefaultUri.ofUri(uri, restrictions);
   }

   /**
    * Creates a {@link Uri} from an existing {@link URI}.
    *
    * @param uri URI
    *
    * @return new {@link DefaultUri} instance
    */
   static Uri ofUri(URI uri) {
      return DefaultUri.ofUri(uri);
   }

   /**
    * Creates a new {@link Uri} for a reconstituting object with specified URN.
    *
    * @param urn URN
    *
    * @return new {@link DefaultUri} instance
    */
   static Uri ofUrn(URI urn) {
      return DefaultUri.ofUrn(urn);
   }

   /**
    * Creates a new {@link Uri} from specified factory, or return an {@link IncompatibleUriException} failure.
    * <p>
    * Alternative to {@link #compatibleUri(Supplier)} when more convenient.
    *
    * @param <T> {@link Uri} type
    *
    * @return new {@link Uri} instance, or {@link Optional#empty()}
    */
   static <T extends Uri> Try<T, IncompatibleUriException> compatibleUri(String uri,
                                                                         Function<? super String, ? extends T> factory) {
      notNull(uri, "uri").and(notNull(factory, "factory")).orThrow();

      return compatibleUri(() -> factory.apply(uri));
   }

   /**
    * Creates a new {@link Uri} from specified factory, or return an {@link IncompatibleUriException} failure.
    * <p>
    * Alternative to {@link #compatibleUri(Supplier)} when more convenient.
    *
    * @param <T> {@link Uri} type
    *
    * @return new {@link Uri} instance, or {@link Optional#empty()}
    */
   static <T extends Uri> Try<T, IncompatibleUriException> compatibleUri(String uri,
                                                                         UriRestrictions restrictions,
                                                                         BiFunction<? super String, ? super UriRestrictions, ? extends T> factory) {
      notNull(uri, "uri")
            .and(notNull(restrictions, "restrictions"))
            .and(notNull(factory, "factory"))
            .orThrow();

      return compatibleUri(() -> factory.apply(uri, restrictions));
   }

   /**
    * Creates a new {@link Uri} from specified factory, or return an {@link IncompatibleUriException} failure.
    * <p>
    * Alternative to {@link #compatibleUri(Supplier)} when more convenient.
    *
    * @param <T> {@link Uri} type
    *
    * @return new {@link Uri} instance, or {@link Optional#empty()}
    */
   static <T extends Uri> Try<T, IncompatibleUriException> compatibleUri(URI uri,
                                                                         Function<? super URI, ? extends T> factory) {
      notNull(uri, "uri").and(notNull(factory, "factory")).orThrow();

      return compatibleUri(() -> factory.apply(uri));
   }

   /**
    * Creates a new {@link Uri} from specified factory, or return an {@link IncompatibleUriException} failure.
    * <p>
    * Alternative to {@link #compatibleUri(Supplier)} when more convenient.
    *
    * @param <T> {@link Uri} type
    *
    * @return new {@link Uri} instance, or {@link Optional#empty()}
    */
   static <T extends Uri> Try<T, IncompatibleUriException> compatibleUri(URI uri,
                                                                         UriRestrictions restrictions,
                                                                         BiFunction<? super URI, ? super UriRestrictions, ? extends T> factory) {
      notNull(uri, "uri")
            .and(notNull(restrictions, "restrictions"))
            .and(notNull(factory, "factory"))
            .orThrow();

      return compatibleUri(() -> factory.apply(uri, restrictions));
   }

   /**
    * Creates a new {@link Uri} from specified factory, or return an {@link IncompatibleUriException} failure.
    * <p>
    * Alternative to {@link #compatibleUri(Supplier)} when more convenient.
    *
    * @param <T> {@link Uri} type
    *
    * @return new {@link Uri} instance, or {@link Optional#empty()}
    */
   static <T extends Uri> Try<T, IncompatibleUriException> compatibleUri(Uri uri,
                                                                         Function<? super Uri, ? extends T> factory) {
      Check.notNull(factory, "factory");

      return compatibleUri(() -> factory.apply(uri));
   }

   /**
    * Creates a new {@link Uri} from specified factory, or return an {@link IncompatibleUriException} failure.
    * <p>
    * Alternative to {@link #compatibleUri(Supplier)} when more convenient.
    *
    * @param <T> {@link Uri} type
    *
    * @return new {@link Uri} instance, or {@link Optional#empty()}
    */
   static <T extends Uri> Try<T, IncompatibleUriException> compatibleUri(Uri uri,
                                                                         UriRestrictions restrictions,
                                                                         BiFunction<? super Uri, ? super UriRestrictions, ? extends T> factory) {
      notNull(uri, "uri")
            .and(notNull(restrictions, "restrictions"))
            .and(notNull(factory, "factory"))
            .orThrow();

      return compatibleUri(() -> factory.apply(uri, restrictions));
   }

   /**
    * Creates a new {@link Uri} from specified factory, or return an {@link IncompatibleUriException} failure.
    *
    * @param <T> {@link Uri} type
    *
    * @return new compatible {@link Uri} instance, or {@link IncompatibleUriException} failure
    *
    * @implSpec This operation can be adapted to any {@link Uri} implementation to reduce the number of
    *       factory methods in each one, but implementations can also provide their own dedicated
    *       operations.
    */
   static <T extends Uri> Try<T, IncompatibleUriException> compatibleUri(Supplier<? extends T> factory) {
      Check.notNull(factory, "factory");

      return Try.ofThrownBy(checkedSupplier(factory::get), IncompatibleUriException.class);
   }

   UriRestrictions restrictions();

   URI toURI();

   Uri normalize();

   Uri resolve(Uri uri);

   Uri resolve(URI uri);

   Uri relativize(Uri uri);

   Uri relativize(URI uri);

   /**
    * Whether Uri is <em>absolute</em>, i.e., having a scheme.
    * <p>
    * Format :
    * <ul>
    * <li>{@code <scheme>:<[/]scheme-specific>}</li>
    * </ul>
    */
   default boolean isAbsolute() {
      return isAbsolute(toURI());
   }

   /**
    * Whether Uri is <em>absolute</em>, i.e., having a scheme.
    * <p>
    * Format :
    * <ul>
    * <li>{@code <scheme>:<[/]scheme-specific>}</li>
    * </ul>
    */
   static boolean isAbsolute(URI uri) {
      Check.notNull(uri, "uri");

      return uri.isAbsolute();
   }

   /**
    * Whether Uri is <em>relative</em>, i.e., not having a scheme.
    * <p>
    * Path can be either <em>absolute</em>, <em>relative</em> or <em>empty</em>.
    * Alias for !{@link #isAbsolute()}.
    * <p>
    * Format :
    * <ul>
    * <li>{@code [//<authority>][<[/]path>][?<query>][#<fragment>]}</li>
    * </ul>
    */
   default boolean isRelative() {
      return isRelative(toURI());
   }

   /**
    * Whether Uri is <em>relative</em>, i.e., not having a scheme.
    * <p>
    * Path can be either <em>absolute</em>, <em>relative</em> or <em>empty</em>.
    * Alias for !{@link #isAbsolute()}.
    * <p>
    * Format :
    * <ul>
    * <li>{@code [//<authority>][<[/]path>][?<query>][#<fragment>]}</li>
    * </ul>
    */
   static boolean isRelative(URI uri) {
      Check.notNull(uri, "uri");

      return !isAbsolute(uri);
   }

   /**
    * Whether Uri is <em>opaque</em>, i.e., having a scheme, but a scheme-specific part not starting with
    * {@code /}.
    * <p>
    * Format : {@code <scheme>:<scheme-specific>}
    */
   default boolean isOpaque() {
      return isOpaque(toURI());
   }

   /**
    * Whether Uri is <em>opaque</em>, i.e., having a scheme, but a scheme-specific part not starting with
    * {@code /}.
    * <p>
    * Format : {@code <scheme>:<scheme-specific>}
    */
   static boolean isOpaque(URI uri) {
      Check.notNull(uri, "uri");

      return uri.isOpaque();
   }

   /**
    * Whether Uri is <em>hierarchical</em>, i.e., having a path.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path can be either <em>absolute</em>,
    * <em>relative</em> or <em>empty</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>:[//<authority>]</path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code [//<authority>]<[/]path>[?<query>][#<fragment>]}</li>
    * </ul>
    */
   default boolean isHierarchical() {
      return isHierarchical(toURI());
   }

   /**
    * Whether Uri is <em>hierarchical</em>, i.e., having a path.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path can be either <em>absolute</em>,
    * <em>relative</em> or <em>empty</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>:[//<authority>]</path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code [//<authority>]<[/]path>[?<query>][#<fragment>]}</li>
    * </ul>
    */
   static boolean isHierarchical(URI uri) {
      Check.notNull(uri, "uri");

      return uri.getRawPath() != null;
   }

   /**
    * Whether Uri is <em>hierarchical</em> and have a <em>empty</em> <em>server-based authority</em>.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path is always <em>absolute</em> or
    * <em>empty</em>. Authority has no <em>user-info</em>, no <em>host</em> and no <em>port</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>://</path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code //</path>[?<query>][#<fragment>]}</li>
    * </ul>
    */
   default boolean isEmptyHierarchicalServer() {
      return isEmptyHierarchicalServer(toURI());
   }

   /**
    * Whether Uri is <em>hierarchical</em> and have a <em>empty</em> <em>server-based authority</em>.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path is always <em>absolute</em> or
    * <em>empty</em>. Authority has no <em>user-info</em>, no <em>host</em> and no <em>port</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>://</path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code //</path>[?<query>][#<fragment>]}</li>
    * </ul>
    */
   static boolean isEmptyHierarchicalServer(URI uri) {
      Check.notNull(uri, "uri");

      return uri.getRawPath() != null
             && uri.getRawAuthority() == null
             && uri.getHost() == null
             && uri.getRawSchemeSpecificPart() != null
             && uri.getRawSchemeSpecificPart().startsWith("//");
   }

   /**
    * Whether Uri is <em>hierarchical</em> and have a <em>server-based authority</em>.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path is always <em>absolute</em> or
    * <em>empty</em>. Authority can be {@link #isEmptyHierarchicalServer() empty}, in this case authority has
    * not <em>host</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>://<server-authority></path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code //<server-authority></path>[?<query>][#<fragment>]}</li>
    * </ul>
    * with :
    * <ul>
    * <li>{@code <server-authority>} : {@code [<userinfo>@]<host>[:<port>]}</li>
    * <li>empty {@code <server-authority>}
    * </ul>
    */
   default boolean isHierarchicalServer() {
      return isHierarchicalServer(toURI());
   }

   /**
    * Whether Uri is <em>hierarchical</em> and have a <em>server-based authority</em>.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path is always <em>absolute</em> or
    * <em>empty</em>. Authority can be {@link #isEmptyHierarchicalServer(URI) empty}, in this case authority
    * has not <em>host</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>://<server-authority></path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code //<server-authority></path>[?<query>][#<fragment>]}</li>
    * </ul>
    * with :
    * <ul>
    * <li>{@code <server-authority>} : {@code [<userinfo>@]<host>[:<port>]}</li>
    * <li>empty {@code <server-authority>}
    * </ul>
    */
   static boolean isHierarchicalServer(URI uri) {
      Check.notNull(uri, "uri");

      return (uri.getRawAuthority() != null && uri.getHost() != null) || isEmptyHierarchicalServer(uri);
   }

   /**
    * Whether Uri is <em>hierarchical</em> and have a <em>registry-based authority</em>.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path is always <em>absolute</em> or
    * <em>empty</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>://<registry-authority></path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code //<registry-authority></path>[?<query>][#<fragment>]}</li>
    * </ul>
    */
   default boolean isHierarchicalRegistry() {
      return isHierarchicalRegistry(toURI());
   }

   /**
    * Whether Uri is <em>hierarchical</em> and have a <em>registry-based authority</em>.
    * <p>
    * Uri can be either <em>absolute</em> or <em>relative</em>. Path is always <em>absolute</em> or
    * <em>empty</em>.
    * <p>
    * Format :
    * <ul>
    * <li>Absolute Uri : {@code <scheme>://<registry-authority></path>[?<query>][#<fragment>]}</li>
    * <li>Absolute Uri : {@code <scheme>:</scheme-specific>}</li>
    * <li>Relative Uri : {@code //<registry-authority></path>[?<query>][#<fragment>]}</li>
    * </ul>
    */
   static boolean isHierarchicalRegistry(URI uri) {
      Check.notNull(uri, "uri");

      return uri.getRawPath() != null && uri.getRawAuthority() != null && uri.getHost() == null;
   }

   Optional<String> scheme();

   Optional<String> schemeSpecific(boolean encoded);

   Optional<String> authority(boolean encoded);

   Optional<String> serverAuthority(boolean encoded);

   Optional<String> registryAuthority(boolean encoded);

   Optional<String> userInfo(boolean encoded);

   Optional<String> host();

   Optional<Integer> port();

   Optional<String> path(boolean encoded);

   Optional<String> query(boolean encoded);

   Optional<String> fragment(boolean encoded);

   static URI uri(String uri) {
      if (uri == null) {
         return null;
      }

      try {
         return new URI(uri);
      } catch (URISyntaxException e) {
         throw new InvalidUriException(uri, e).subMessage(e);
      }
   }

   /**
    * Updates scheme with specified value if {@link Uri} is {@link #isAbsolute() absolute}.
    *
    * @param schemeMapper scheme function of current scheme, current scheme is never blank,
    *       function result must not be blank
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not absolute
    * @see #conditionalScheme(Function)
    */
   Uri scheme(Function<String, ? extends String> schemeMapper);

   Uri scheme(String scheme);

   /**
    * Updates scheme with specified value only if {@link Uri} is {@link #isAbsolute() absolute}, otherwise
    * does nothing.
    *
    * @param schemeMapper scheme function of current scheme, current scheme is never blank,
    *       function result must not be blank
    *
    * @return new instance
    *
    * @see #scheme(Function)
    */
   Uri conditionalScheme(Function<String, ? extends String> schemeMapper);

   Uri conditionalScheme(String scheme);

   /**
    * Updates scheme-specific with specified value if {@link Uri} is {@link #isOpaque() opaque}.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param schemeSpecificMapper scheme-specific function of current scheme-specific, current
    *       scheme-specific is never blank, function result must not be blank and must not start with
    *       {@code /}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not opaque
    * @see #conditionalSchemeSpecific(boolean, Function)
    */
   Uri schemeSpecific(boolean encoded, Function<String, ? extends String> schemeSpecificMapper);

   Uri schemeSpecific(boolean encoded, String schemeSpecific);

   /**
    * Updates scheme with specified value only if {@link Uri} is {@link #isOpaque() opaque}, otherwise does
    * nothing.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param schemeSpecificMapper scheme-specific function of current scheme-specific, current
    *       scheme-specific is never blank, function result must not be blank and must not start with
    *       {@code /}
    *
    * @return new instance
    *
    * @see #schemeSpecific(boolean, Function)
    */
   Uri conditionalSchemeSpecific(boolean encoded, Function<String, ? extends String> schemeSpecificMapper);

   Uri conditionalSchemeSpecific(boolean encoded, String schemeSpecific);

   /**
    * Updates server-based authority with specified value if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based}.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param serverAuthorityMapper server-based authority function of current authority, current
    *       authority is never {@code null}, function result must not be {@code null}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical server-based
    * @see #conditionalServerAuthority(boolean, Function)
    */
   Uri serverAuthority(boolean encoded, Function<String, ? extends String> serverAuthorityMapper);

   Uri serverAuthority(boolean encoded, String serverAuthority);

   /**
    * Updates server-based authority with specified value only if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based}, otherwise does
    * nothing.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param serverAuthorityMapper server-based authority function of current authority, current
    *       authority is never {@code null}, function result must not be {@code null}
    *
    * @return new instance
    *
    * @see #serverAuthority(boolean, Function)
    */
   Uri conditionalServerAuthority(boolean encoded, Function<String, ? extends String> serverAuthorityMapper);

   Uri conditionalServerAuthority(boolean encoded, String serverAuthority);

   /**
    * Updates registry-based authority with specified value if {@link Uri} is
    * {@link #isHierarchicalRegistry() hierarchical registry-based}.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param registryAuthorityMapper registry-based authority function of current authority, current
    *       authority is never blank, function result must not be blank
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical registry-based
    * @see #conditionalRegistryAuthority(boolean, Function)
    */
   Uri registryAuthority(boolean encoded, Function<String, ? extends String> registryAuthorityMapper);

   Uri registryAuthority(boolean encoded, String registryAuthority);

   /**
    * Updates registry-based authority with specified value only if {@link Uri} is
    * {@link #isHierarchicalRegistry() hierarchical registry-based}, otherwise does
    * nothing.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param registryAuthorityMapper registry-based authority function of current authority, current
    *       authority is never blank, function result must not be blank
    *
    * @return new instance
    *
    * @see #registryAuthority(boolean, Function)
    */
   Uri conditionalRegistryAuthority(boolean encoded,
                                    Function<String, ? extends String> registryAuthorityMapper);

   Uri conditionalRegistryAuthority(boolean encoded, String registryAuthority);

   /**
    * Updates user-info with specified value if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param userInfoMapper user-info function of current user-info, current
    *       user-info can be {@code null}, function result can be {@code null}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
    *       hierarchical server-based
    * @see #conditionalUserInfo(boolean, Function)
    */
   Uri userInfo(boolean encoded, Function<String, ? extends String> userInfoMapper);

   Uri userInfo(boolean encoded, String userInfo);

   /**
    * Updates user-info with specified value only if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
    * nothing.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param userInfoMapper user-info function of current user-info, current
    *       user-info can be {@code null}, function result can be {@code null}
    *
    * @return new instance
    *
    * @see #userInfo(boolean, Function)
    */
   Uri conditionalUserInfo(boolean encoded, Function<String, ? extends String> userInfoMapper);

   Uri conditionalUserInfo(boolean encoded, String userInfo);

   /**
    * Updates host with specified value if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
    *
    * @param hostMapper host function of current host, current
    *       host is never blank, function result must not be blank
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
    *       hierarchical server-based
    * @see #conditionalHost(Function)
    */
   Uri host(Function<String, ? extends String> hostMapper);

   Uri host(String host);

   /**
    * Updates host with specified value only if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
    * nothing.
    *
    * @param hostMapper host function of current host, current
    *       host is never blank, function result must not be blank
    *
    * @return new instance
    *
    * @see #host(Function)
    */
   Uri conditionalHost(Function<String, ? extends String> hostMapper);

   Uri conditionalHost(String host);

   /**
    * Updates port with specified value if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
    *
    * @param portMapper port function of current port, current port can be {@code null} and is never
    *       {@code -1}, function result can be {@code null} or {@code -1}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
    *       hierarchical server-based
    * @see #conditionalPort(Function)
    */
   Uri port(Function<Integer, Integer> portMapper);

   /**
    * Updates port with specified value if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
    *
    * @param port port, can be {@code null} or {@code -1}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
    *       hierarchical server-based
    */
   Uri port(Integer port);

   /**
    * Updates port with specified value only if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
    * nothing.
    *
    * @param portMapper port function of current port, current port can be {@code null} and is never
    *       {@code -1}, function result can be {@code null} or {@code -1}
    *
    * @return new instance
    *
    * @see #port(Function)
    */
   Uri conditionalPort(Function<Integer, Integer> portMapper);

   /**
    * Updates port with specified value only if {@link Uri} is
    * {@link #isHierarchicalServer() hierarchical server-based} but not
    * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
    * nothing.
    *
    * @param port port, can be {@code null} or {@code -1}
    *
    * @return new instance
    */
   Uri conditionalPort(Integer port);

   /**
    * Updates path with specified value if {@link Uri} is {@link #isHierarchical() hierarchical}.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param pathMapper path function of current path, current
    *       path is never {@code null}, function result must not be {@code null}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical, or mapped path does not comply
    *       with rules
    * @see #conditionalPath(boolean, Function)
    */
   Uri path(boolean encoded, Function<String, ? extends String> pathMapper);

   Uri path(boolean encoded, String path);

   /**
    * Updates path with specified value only if {@link Uri} is {@link #isHierarchical() hierarchical},
    * otherwise does nothing.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param pathMapper path function of current path, current
    *       path is never {@code null}, function result must not be {@code null}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if path does not comply with rules
    * @see #path(boolean, Function)
    */
   Uri conditionalPath(boolean encoded, Function<String, ? extends String> pathMapper);

   Uri conditionalPath(boolean encoded, String path);

   /**
    * Updates query with specified value if {@link Uri} is {@link #isHierarchical() hierarchical}.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param queryMapper query function of current query, current
    *       query can be {@code null}, function result can be {@code null}
    *
    * @return new instance
    *
    * @throws InvariantValidationException if URI is not hierarchical
    * @see #conditionalQuery(boolean, Function)
    */
   Uri query(boolean encoded, Function<String, ? extends String> queryMapper);

   Uri query(boolean encoded, String query);

   /**
    * Updates query with specified value only if {@link Uri} is {@link #isHierarchical() hierarchical},
    * otherwise does nothing.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param queryMapper query function of current query, current
    *       query can be {@code null}, function result can be {@code null}
    *
    * @return new instance
    *
    * @see #query(boolean, Function)
    */
   Uri conditionalQuery(boolean encoded, Function<String, ? extends String> queryMapper);

   Uri conditionalQuery(boolean encoded, String query);

   /**
    * Updates fragment with specified value.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param fragmentMapper fragment function of current fragment, current
    *       fragment can be {@code null}, function result can be {@code null}
    *
    * @return new instance
    *
    * @see #conditionalFragment(boolean, Function)
    */
   Uri fragment(boolean encoded, Function<String, ? extends String> fragmentMapper);

   Uri fragment(boolean encoded, String fragment);

   /**
    * Updates fragment with specified value.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param fragmentMapper fragment function of current fragment, current
    *       fragment can be {@code null}, function result can be {@code null}
    *
    * @return new instance
    *
    * @implSpec This is an alias for {@link #fragment(boolean, Function)} because updating fragment is
    *       always possible whatever the URI format.
    * @see #fragment(boolean, Function)
    */
   Uri conditionalFragment(boolean encoded, Function<String, ? extends String> fragmentMapper);

   /**
    * Updates fragment with specified value.
    *
    * @param encoded whether mapper input and output value is encoded
    * @param fragment fragment, can be {@code null}
    *
    * @return new instance
    *
    * @implSpec This is an alias for {@link #fragment(boolean, Function)} because updating fragment is
    *       always possible whatever the URI format.
    * @see #fragment(boolean, Function)
    */
   Uri conditionalFragment(boolean encoded, String fragment);
}
