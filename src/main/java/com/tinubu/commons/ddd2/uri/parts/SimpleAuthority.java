/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Authority;
import com.tinubu.commons.ddd2.uri.ComponentUri.Host;
import com.tinubu.commons.ddd2.uri.ComponentUri.PartFactory;
import com.tinubu.commons.ddd2.uri.ComponentUri.Port;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.util.Try;

/**
    * Authority factory for either {@link SimpleServerAuthority} or {@link SimpleRegistryAuthority}.
    * This authority delegates to created authority.
    */
   public class SimpleAuthority implements Authority {

      Authority authority;

      protected SimpleAuthority(Authority authority) {
         this.authority = Check.notNull(authority, "authority");
      }

      public static Authority of(String authority) {
         Check.notNull(authority, "authority");

         return Try
               .<Authority, InvalidUriException>ofThrownBy(() -> SimpleServerAuthority.of(authority),
                                                           InvalidUriException.class)
               .or(() -> Try.<Authority, InvalidUriException>ofThrownBy(() -> SimpleRegistryAuthority.of(
                     authority), InvalidUriException.class))
               .orElseThrow(identity());
      }

      public static Authority ofEncoded(String encodedAuthority) {
         Check.notNull(encodedAuthority, "encodedAuthority");

         return Try
               .<Authority, InvalidUriException>ofThrownBy(() -> SimpleServerAuthority.ofEncoded(
                     encodedAuthority), InvalidUriException.class)
               .or(() -> Try.<Authority, InvalidUriException>ofThrownBy(() -> SimpleRegistryAuthority.ofEncoded(
                     encodedAuthority), InvalidUriException.class))
               .orElseThrow(identity());
      }

      public static Optional<? extends Authority> of(URI uri,
                                                     PartFactory<UserInfo> userInfoFactory,
                                                     PartFactory<Host> hostFactory,
                                                     PartFactory<Port> portFactory) {
         Check.notNull(uri, "uri");

         return Optional
               .<Authority>empty()
               .or(() -> SimpleServerAuthority.of(uri, userInfoFactory, hostFactory, portFactory))
               .or(() -> SimpleRegistryAuthority.of(uri));
      }

      public static Optional<? extends Authority> of(URI uri) {
         return of(uri, SimpleUserInfo::of, SimpleHost::of, SimplePort::of);
      }

      @Override
      public Optional<? extends Authority> recreate(URI uri) {
         return authority.recreate(uri);
      }

      @Override
      public String value() {
         return authority.value();
      }

      @Override
      public String encodedValue() {
         return authority.encodedValue();
      }

      @Override
      public StringBuilder appendEncodedValue(StringBuilder builder) {
         return authority.appendEncodedValue(builder);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SimpleAuthority that = (SimpleAuthority) o;
         return Objects.equals(authority, that.authority);
      }

      @Override
      public int hashCode() {
         return Objects.hash(authority);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SimpleAuthority.class.getSimpleName() + "[", "]")
               .add("authority='" + authority + "'")
               .toString();
      }
   }
