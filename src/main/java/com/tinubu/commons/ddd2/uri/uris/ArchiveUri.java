/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.hideField;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.hasNoFragment;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeSchemeSpecific;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.PathUtils.isEmpty;
import static com.tinubu.commons.lang.util.PathUtils.isRoot;
import static java.util.function.Function.identity;
import static org.apache.commons.lang3.StringUtils.contains;
import static org.apache.commons.lang3.StringUtils.endsWith;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.PathRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriEncoder;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleSchemeSpecific;

/**
 * Generic archive {@link Uri}, that targets a specified archive and an entry in the archive.
 * <p>
 * If archive entry is not present, or is {@code /}, or is empty, the following archive URI will be used
 * to directly represent the target URI archive.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>{@code zip:<target-uri>[!</archive-entry}][?<archive-query>]</li>
 *    <li>{@code jar:<target-uri>[!</archive-entry}][?<archive-query>]</li>
 * </ul>
 * Nested archive URI format, when {@code target-uri} is an archive URI :
 * <ul>
 *    <li>{@code zip:zip:<inner-target-uri>[!</inner-archive-entry][?<inner-archive-query>][!</outer-archive-entry}][?<outer-archive-query>]}</li>
 * </ul>
 * <p>
 * Archive URI is an opaque format, with a target URI and an archive entry path. Archive entry
 * path must be absolute, and can be {@code /} or empty.
 * Target URI can contain any URI format, but the following characters must be URL-encoded : {@code #}.
 * <p>
 * Archive URI itself has no fragment support, and, by default, no query support. Query is not accessible through
 * regular {@link #query(boolean)} accessors, but using special {@link #archiveQuery()} accessor.
 * If there is a {@code ?<query>} string and no {@code !} in the URI, the query string is assumed to belong to {@code <target-uri>}.
 * <p>
 * However, supported format depends on {@link ArchiveUriRestrictions} restrictions. By default,
 * <ul>
 * <li>{@link ArchiveUriRestrictions#archiveQuery() archiveQuery} : archive query is restricted</li>
 * <li>{@link ArchiveUriRestrictions#schemes() schemes} : supported schemes are restricted to [zip,jar,war,ear]</li>
 * <li>{@link ArchiveUriRestrictions#archiveEntry() archiveEntry} : archive entry is not optionally supported</li>
 * </ul>
 */
// FIXME to check : it seems recursive archive URIs does not escape ! in target-uri
public class ArchiveUri extends AbstractComponentUri<ArchiveUri> {

   protected static final char ARCHIVE_ENTRY_SEPARATOR = '!';
   protected static final char ARCHIVE_QUERY_SEPARATOR = '?';

   protected static final String ZIP_SCHEME = "zip";
   protected static final String JAR_SCHEME = "jar";

   protected Uri targetUri;
   protected java.nio.file.Path archiveEntry;
   protected Query archiveQuery;

   protected ArchiveUri(URI uri,
                        UriRestrictions restrictions,
                        Scheme scheme,
                        SchemeSpecific schemeSpecific,
                        Authority authority,
                        Path path,
                        Query query,
                        Fragment fragment,
                        boolean newObject) {
      super(uri,
            archiveRestrictions(restrictions),
            scheme,
            schemeSpecific,
            authority,
            path,
            query,
            fragment,
            newObject);
   }

   protected ArchiveUri(UriRestrictions restrictions,
                        Scheme scheme,
                        SchemeSpecific schemeSpecific,
                        Authority authority,
                        Path path,
                        Query query,
                        Fragment fragment) {
      super(archiveRestrictions(restrictions), scheme, schemeSpecific, authority, path, query, fragment);
   }

   protected ArchiveUri(URI uri, UriRestrictions restrictions) {
      super(uri, archiveRestrictions(restrictions));
   }

   protected ArchiveUri(Uri uri, UriRestrictions restrictions) {
      super(uri, archiveRestrictions(restrictions));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends ArchiveUri> defineDomainFields() {
      return Fields
            .<ArchiveUri>builder()
            .superFields((Fields<? super ArchiveUri>) super.defineDomainFields())
            .field("authority", v -> v.authority, hideField())
            .field("path", v -> v.path, hideField())
            .field("query", v -> v.query, hideField())
            .field("fragment", v -> v.fragment, hideField())
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      List<InvariantRule<ComponentUri>> compatibleUriRules = list(UriRules.isOpaque(),
                                                                  UriRules.scheme(as(String::toLowerCase,
                                                                                     isIn(ParameterValue.value(
                                                                                           restrictions().schemes())))));
      var uriCompatibility = Invariant.of(() -> this, compatibleUriRules).groups(COMPATIBILITY_GROUP);

      var uriValidity = Invariant.of(() -> this, hasNoFragment());

      return super.domainInvariants().withInvariants(uriCompatibility, uriValidity);
   }

   /**
    * @implNote Parsing must work with encoded value to correctly support recursive zip URIs and encoded
    *       {@code !, ?} characters.
    */
   @Override
   public ArchiveUri postValidate() {
      var ss = schemeSpecific(true).orElseThrow();
      var entrySepIdx = ss.lastIndexOf(ARCHIVE_ENTRY_SEPARATOR);
      var querySepIdx = entrySepIdx == -1 ? -1 : ss.indexOf(ARCHIVE_QUERY_SEPARATOR, entrySepIdx);

      if (restrictions().archiveQuery() && querySepIdx != -1) {
         throw new InvalidUriException(this).subMessage("Restricted archive query");
      }

      String parseTargetUri;
      java.nio.file.Path parseArchiveEntry;

      if (entrySepIdx == -1) {
         parseTargetUri = ss;
         parseArchiveEntry = null;
      } else {
         parseTargetUri = ss.substring(0, entrySepIdx);
         parseArchiveEntry = optional(java.nio.file.Path.of(ss.substring(entrySepIdx + 1,
                                                                         querySepIdx != -1
                                                                         ? querySepIdx
                                                                         : ss.length())))
               .filter(ad -> !isRoot(ad) && !isEmpty(ad))
               .orElse(null);
      }

      if (restrictions().archiveEntry() && parseArchiveEntry != null) {
         throw new InvalidUriException(this).subMessage("Restricted archive entry");
      }

      parseArchiveEntry = validate(nullable(parseArchiveEntry, java.nio.file.Path::normalize),
                                   "archiveEntry",
                                   isNull().orValue(PathRules
                                                          .isAbsolute()
                                                          .andValue(PathRules.hasFileName())
                                                          .andValue(PathRules.hasNoTraversal()))).orThrow(
            uriInvariantResultHandler(this));

      targetUri = Uri.ofUri(UriEncoder.decode(parseTargetUri).orElseThrow(identity()));
      archiveEntry = parseArchiveEntry;

      if (querySepIdx != -1) {
         archiveQuery = SimpleQuery.ofEncoded(ss.substring(querySepIdx + 1));
      }

      return this;
   }

   public static ArchiveUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new ArchiveUri(uri, restrictions));
   }

   public static ArchiveUri ofUri(URI uri) {
      return ofUri(uri, ArchiveUriRestrictions.ofDefault());
   }

   public static ArchiveUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new ArchiveUri(uri, restrictions));
   }

   public static ArchiveUri ofUri(Uri uri) {
      return ofUri(uri, ArchiveUriRestrictions.ofDefault());
   }

   public static ArchiveUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static ArchiveUri ofUri(String uri) {
      return ofUri(uri, ArchiveUriRestrictions.ofDefault());
   }

   public static ArchiveUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   public static ArchiveUri ofUrn(URI urn) {
      return ofUrn(urn, ArchiveUriRestrictions.ofDefault());
   }

   /**
    * Creates an archive URI with no specific archive entry.
    * Archive query type is not preserved and is replaced with a {@link SimpleQuery}.
    *
    * @param scheme archive scheme to use (case-sensitive)
    * @param targetUri archive target {@link Uri}
    * @param archiveQuery optional archive query. Target URI automatic encoding can vary depending on
    *       query is present or not
    *
    * @implSpec {@code !/} is always appended as it is the most standard format to reference the whole
    *       archive instead of an entry in the archive.
    */
   public static ArchiveUri archive(String scheme, Uri targetUri, Query archiveQuery) {
      Check.notBlank(scheme, "scheme");
      Check.notNull(targetUri, "targetUri");

      var schemePart = SimpleScheme.of(scheme);
      var archiveUriHasQuery = archiveQuery != null && archiveQuery.isPresent();
      var targetUriValue = encodeTargetUri(targetUri);

      var schemeSpecific = SimpleSchemeSpecific.ofEncoded(targetUriValue + ARCHIVE_ENTRY_SEPARATOR + "/" + (
            archiveUriHasQuery
            ? ARCHIVE_QUERY_SEPARATOR + archiveQuery.value()
            : ""));

      return buildUri(() -> new ArchiveUri(ArchiveUriRestrictions.automatic(schemePart,
                                                                            schemeSpecific,
                                                                            archiveQuery),
                                           schemePart,
                                           schemeSpecific,
                                           null,
                                           null,
                                           null,
                                           null));
   }

   public static ArchiveUri archive(String scheme, Uri targetUri) {
      return archive(scheme, targetUri, (Query) null);
   }

   /**
    * Creates an archive URI with an archive entry. If archive entry is {@code /} or {@code ""}, returned
    * archive URI have {@code !/} archive entry specifier.
    * Archive query type is not preserved and is replaced with a {@link SimpleQuery}.
    *
    * @param scheme archive scheme to use (case-sensitive)
    * @param targetUri archive target {@link Uri}
    * @param archiveEntry archive entry, or {@code /}, or {@code ""}
    * @param archiveQuery optional archive query. Target URI automatic encoding can vary depending on
    *       query is present or not
    */
   public static ArchiveUri archive(String scheme,
                                    Uri targetUri,
                                    java.nio.file.Path archiveEntry,
                                    Query archiveQuery) {
      Check.notBlank(scheme, "scheme");
      Check.notNull(targetUri, "targetUri");
      var normalizedArchiveEntry = nullable(archiveEntry, java.nio.file.Path::normalize);

      if (normalizedArchiveEntry != null && (isRoot(normalizedArchiveEntry)
                                             || isEmpty(normalizedArchiveEntry))) {
         return archive(scheme, targetUri, archiveQuery);
      }

      Check.validate(normalizedArchiveEntry,
                     "archiveEntry",
                     PathRules
                           .isAbsolute()
                           .andValue(PathRules.hasFileName())
                           .andValue(PathRules.hasNoTraversal()));

      var schemePart = SimpleScheme.of(scheme);
      var archiveUriHasQuery = archiveQuery != null && archiveQuery.isPresent();
      var targetUriValue = encodeTargetUri(targetUri);
      var schemeSpecific =
            SimpleSchemeSpecific.ofEncoded(targetUriValue + "!" + normalizedArchiveEntry.toString() + (
                  archiveUriHasQuery
                  ? ARCHIVE_QUERY_SEPARATOR + archiveQuery.value()
                  : ""));
      return buildUri(() -> new ArchiveUri(ArchiveUriRestrictions.automatic(schemePart,
                                                                            schemeSpecific,
                                                                            archiveQuery),
                                           schemePart,
                                           schemeSpecific,
                                           null,
                                           null,
                                           null,
                                           null));
   }

   public static ArchiveUri archive(String scheme, Uri targetUri, java.nio.file.Path archiveEntry) {
      return archive(scheme, targetUri, archiveEntry, null);
   }

   /**
    * Encodes target URI to be set in a scheme specific part.
    */
   private static String encodeTargetUri(Uri targetUri) {
      return encodeSchemeSpecific(targetUri.value()).orElseThrow(identity())
            /*.replace(String.valueOf(ARCHIVE_ENTRY_SEPARATOR), "%21")*/;
   }

   /**
    * Alias for {@link #archive(String, Uri, Query)} for ZIP archives.
    */
   public static ArchiveUri zip(Uri targetUri, Query archiveQuery) {
      return archive(ZIP_SCHEME, targetUri, archiveQuery);
   }

   /**
    * Alias for {@link #archive(String, Uri)} for ZIP archives.
    */
   public static ArchiveUri zip(Uri targetUri) {
      return zip(targetUri, (Query) null);
   }

   /**
    * Alias for {@link #archive(String, Uri, java.nio.file.Path, Query)} for ZIP archives.
    */
   public static ArchiveUri zip(Uri targetUri, java.nio.file.Path archiveEntry, Query archiveQuery) {
      return archive(ZIP_SCHEME, targetUri, archiveEntry, archiveQuery);
   }

   /**
    * Alias for {@link #archive(String, Uri, java.nio.file.Path)} for ZIP archives.
    */
   public static ArchiveUri zip(Uri targetUri, java.nio.file.Path archiveEntry) {
      return zip(targetUri, archiveEntry, null);
   }

   /**
    * Alias for {@link #archive(String, Uri, Query)} for JAR archives.
    */
   public static ArchiveUri jar(Uri targetUri, Query archiveQuery) {
      return archive(JAR_SCHEME, targetUri, archiveQuery);
   }

   /**
    * Alias for {@link #archive(String, Uri)} for JAR archives.
    */
   public static ArchiveUri jar(Uri targetUri) {
      return jar(targetUri, (Query) null);
   }

   /**
    * Alias for {@link #archive(String, Uri, java.nio.file.Path, Query)} for JAR archives.
    */
   public static ArchiveUri jar(Uri targetUri, java.nio.file.Path archiveEntry, Query archiveQuery) {
      return archive(JAR_SCHEME, targetUri, archiveEntry, archiveQuery);
   }

   /**
    * Alias for {@link #archive(String, Uri, java.nio.file.Path)} for JAR archives.
    */
   public static ArchiveUri jar(Uri targetUri, java.nio.file.Path archiveEntry) {
      return jar(targetUri, archiveEntry, null);
   }

   /**
    * Returns current archive URI scheme.
    */
   public String archiveScheme() {
      return scheme().orElseThrow();
   }

   /**
    * Returns new archive URI with updated archive scheme.
    *
    * @param archiveScheme archive scheme
    *
    * @return new instance
    */
   public ArchiveUri archiveScheme(String archiveScheme) {
      Check.notBlank(archiveScheme, "archiveScheme");

      return archiveEntry()
            .map(ae -> ArchiveUri.archive(archiveScheme, targetUri, ae, archiveQuery))
            .orElseGet(() -> ArchiveUri.archive(archiveScheme, targetUri, archiveQuery));

   }

   /**
    * Returns target {@link Uri} in archive URI.
    */
   public Uri targetUri() {
      return targetUri;
   }

   /**
    * Returns new archive URI with updated target URI.
    *
    * @param targetUri target URI
    *
    * @return new instance
    */
   public ArchiveUri targetUri(Uri targetUri) {
      Check.notNull(targetUri, "targetUri");

      return archiveEntry()
            .map(ae -> ArchiveUri.archive(archiveScheme(), targetUri, ae, archiveQuery))
            .orElseGet(() -> ArchiveUri.archive(archiveScheme(), targetUri, archiveQuery));
   }

   /**
    * Returns archive entry in archive URI.
    */
   public Optional<java.nio.file.Path> archiveEntry() {
      return nullable(archiveEntry);
   }

   /**
    * Returns new archive URI with updated archive entry.
    *
    * @param archiveEntry archive entry
    *
    * @return new instance
    */
   public ArchiveUri archiveEntry(java.nio.file.Path archiveEntry) {
      Check.notNull(archiveEntry, "archiveEntry");

      return ArchiveUri.archive(archiveScheme(), targetUri, archiveEntry, archiveQuery);
   }

   /**
    * Returns archive query.
    */
   public Optional<Query> archiveQuery() {
      return nullable(archiveQuery);
   }

   /**
    * Returns new archive URI with updated archive query.
    *
    * @param archiveQuery archive query
    *
    * @return new instance
    */
   public ArchiveUri archiveQuery(Query archiveQuery) {
      Check.notNull(archiveQuery, "archiveQuery");

      return archiveEntry()
            .map(ae -> ArchiveUri.archive(archiveScheme(), targetUri, ae, archiveQuery))
            .orElseGet(() -> ArchiveUri.archive(archiveScheme(), targetUri, archiveQuery));
   }

   @Override
   protected ArchiveUri recreate(URI uri,
                                 UriRestrictions restrictions,
                                 Scheme scheme,
                                 SchemeSpecific schemeSpecific,
                                 Authority authority,
                                 Path path,
                                 Query query,
                                 Fragment fragment,
                                 boolean newObject) {
      return buildUri(() -> new ArchiveUri(uri,
                                           restrictions,
                                           scheme,
                                           schemeSpecific,
                                           authority,
                                           path,
                                           query,
                                           fragment,
                                           newObject));
   }

   @Override
   public ArchiveUriRestrictions restrictions() {
      return (ArchiveUriRestrictions) restrictions;
   }

   public ArchiveUri archiveRestrictions(Function<? super ArchiveUriRestrictions, ? extends ArchiveUriRestrictions> restrictionsMapper) {
      return restrictions(restrictionsMapper.apply(restrictions()));
   }

   protected static UriRestrictions archiveRestrictions(UriRestrictions restrictions) {
      return nullable(restrictions, ArchiveUriRestrictions::of);
   }

   public static class ArchiveUriRestrictions implements UriRestrictions {
      protected static final boolean DEFAULT_ARCHIVE_QUERY = true;
      protected static final List<String> DEFAULT_SCHEMES = list("zip", "jar");
      protected static final boolean DEFAULT_ARCHIVE_ENTRY = false;

      protected final List<String> schemes;
      protected final boolean archiveEntry;
      protected final boolean archiveQuery;

      protected ArchiveUriRestrictions(List<String> schemes, boolean archiveEntry, boolean archiveQuery) {
         this.schemes =
               immutable(list(Check.noNullElements(schemes, "schemes").stream().map(String::toLowerCase)));
         this.archiveEntry = archiveEntry;
         this.archiveQuery = archiveQuery;
      }

      public static ArchiveUriRestrictions of(UriRestrictions restrictions) {
         if (restrictions instanceof ArchiveUriRestrictions) {
            var archiveRestrictions = (ArchiveUriRestrictions) restrictions;
            return new ArchiveUriRestrictions(archiveRestrictions.schemes,
                                              archiveRestrictions.archiveEntry,
                                              archiveRestrictions.archiveQuery);
         } else {
            return new ArchiveUriRestrictions(DEFAULT_SCHEMES, DEFAULT_ARCHIVE_ENTRY, DEFAULT_ARCHIVE_QUERY);
         }
      }

      public static ArchiveUriRestrictions ofRestrictions(List<String> aliasSchemes,
                                                          boolean archiveEntry,
                                                          boolean archiveQuery) {
         return new ArchiveUriRestrictions(aliasSchemes, archiveEntry, archiveQuery);
      }

      public static ArchiveUriRestrictions ofDefault() {
         return ofRestrictions(DEFAULT_SCHEMES, DEFAULT_ARCHIVE_ENTRY, DEFAULT_ARCHIVE_QUERY);
      }

      public static ArchiveUriRestrictions noRestrictions() {
         return ofRestrictions(DEFAULT_SCHEMES, false, false);
      }

      /**
       * This automatic restrictions builder will accept any specified scheme as an alias scheme.
       */
      public static ArchiveUriRestrictions automatic(Scheme scheme,
                                                     SchemeSpecific schemeSpecific,
                                                     Query archiveQuery) {
         return ofRestrictions(list(scheme.value()),
                               !schemeSpecific.encodedValue().contains("!")
                               || endsWith(schemeSpecific.encodedValue(), "!")
                               || endsWith(schemeSpecific.encodedValue(), "!/")
                               || contains(schemeSpecific.encodedValue(), "!?")
                               || contains(schemeSpecific.encodedValue(), "!/?"),
                               archiveQuery == null || !archiveQuery.isPresent());
      }

      protected ArchiveUriRestrictions recreate(List<String> aliasSchemes,
                                                boolean archiveEntry,
                                                boolean archiveQuery) {
         return new ArchiveUriRestrictions(aliasSchemes, archiveEntry, archiveQuery);
      }

      /**
       * Returns supported schemes.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       *
       * @return alias schemes, or empty list
       */
      public List<String> schemes() {
         return schemes;
      }

      /**
       * Supported schemes.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public ArchiveUriRestrictions schemes(List<String> schemes) {
         return recreate(schemes, archiveEntry, archiveQuery);
      }

      /**
       * Supported schemes.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public ArchiveUriRestrictions schemes(String... schemes) {
         return schemes(list(schemes));
      }

      /**
       * Returns whether archive entry is restricted or mandatory.
       * If restricted to {@code true}, the archive URI will represent the target archive, otherwise, the
       * archive URI will represent the archive entry inside the target archive.
       * <p>
       * This restriction is interpreted as a URI invalidity at build time.
       */
      public boolean archiveEntry() {
         return archiveEntry;
      }

      /**
       * Whether archive entry is restricted or mandatory.
       * If restricted to {@code true}, the archive URI will represent the target archive, otherwise, the
       * archive URI will represent the archive entry inside the target archive.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public ArchiveUriRestrictions archiveEntry(boolean archiveEntry) {
         return recreate(schemes, archiveEntry, archiveQuery);
      }

      /**
       * Returns whether archive query is restricted.
       * <p>
       * This restriction is interpreted as a URI invalidity at build time.
       */
      public boolean archiveQuery() {
         return archiveQuery;
      }

      /**
       * Whether archive query is restricted.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public ArchiveUriRestrictions archiveQuery(boolean archiveQuery) {
         return recreate(schemes, archiveEntry, archiveQuery);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", ArchiveUriRestrictions.class.getSimpleName() + "[", "]")
               .add("schemes=" + schemes)
               .add("archiveEntry=" + archiveEntry)
               .add("archiveQuery=" + archiveQuery)
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         ArchiveUriRestrictions that = (ArchiveUriRestrictions) o;
         return archiveEntry == that.archiveEntry
                && Objects.equals(schemes, that.schemes)
                && archiveQuery == that.archiveQuery;
      }

      @Override
      public int hashCode() {
         return Objects.hash(super.hashCode(), schemes, archiveEntry, archiveQuery);
      }
   }

}
