/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;

/**
 * URI is not compatible. Most of the time, a compatible URI is a URI that has the minimum set of mandatory
 * URI parts, and correct scheme.
 * An incompatible URI exception is a subset of {@link InvalidUriException}.
 */
public class IncompatibleUriException extends InvalidUriException {
   private static final String DEFAULT_MESSAGE = "Incompatible '%s' URI";

   protected IncompatibleUriException(String uri, String message) {
      super(uri, message);
   }

   protected IncompatibleUriException(String uri, String message, Throwable cause) {
      super(uri, message, cause);
   }

   public IncompatibleUriException(String uri) {
      this(uri, formatMessage(DEFAULT_MESSAGE, uri));
   }

   public IncompatibleUriException(URI uri) {
      this(nullable(uri).map(String::valueOf).orElse("<null>"));
   }

   public IncompatibleUriException(URI uri, Throwable cause) {
      this(nullable(uri).map(String::valueOf).orElse("<null>"), cause);
   }

   public IncompatibleUriException(Uri uri) {
      this(nullable(uri).map(Uri::stringValue).orElse("<null>"));
   }

   public IncompatibleUriException(Uri uri, Throwable cause) {
      this(nullable(uri).map(Uri::stringValue).orElse("<null>"), cause);
   }

   public IncompatibleUriException(String uri, Throwable cause) {
      this(uri, formatMessage(DEFAULT_MESSAGE, uri), cause);
   }

   public IncompatibleUriException message(String message, Object... args) {
      return new IncompatibleUriException(uri(), formatMessage(message, args));
   }

   public IncompatibleUriException message(Throwable throwable) {
      return message(throwableMessage(throwable));
   }

   public IncompatibleUriException subMessage(String message, Object... args) {
      return new IncompatibleUriException(uri,
                                          formatMessage(DEFAULT_MESSAGE, uri) + ": " + formatMessage(message,
                                                                                                     args));
   }

   public IncompatibleUriException subMessage(Throwable throwable) {
      return subMessage(throwableMessage(throwable));
   }

}
