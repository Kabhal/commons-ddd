/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.ComponentUriRules.PathRules;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.ComponentUriRules;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;

/**
 * File {@link Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>hierarchical : {@code file:</path>[?<query>][#<fragment>]}</li>
 *    <li>relative : {@code <[/]path>[?<query>][#<fragment>]}</li>
 * </ul>
 * However, supported format depends on {@link FileUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link FileUriRestrictions#relativeFormat() relativeFormat} : relative format is not supported</li>
 *    <li>{@link FileUriRestrictions#relativePath() relativePath} : path must be absolute when format is absolute or relative</li>
 *    <li>{@link FileUriRestrictions#emptyPath() emptyPath} : path must not be empty when format is relative</li>
 *    <li>{@link FileUriRestrictions#query() query} : query is not supported</li>
 *    <li>{@link FileUriRestrictions#fragment() fragment} : fragment is not supported</li>
 * </ul>
 */
public class FileUri extends AbstractComponentUri<FileUri> {

   protected static final String SCHEME = "file";

   protected FileUri(URI uri,
                     UriRestrictions restrictions,
                     Scheme scheme,
                     SchemeSpecific schemeSpecific,
                     Authority authority,
                     Path path,
                     Query query,
                     Fragment fragment,
                     boolean newObject) {
      super(uri,
            fileRestrictions(restrictions),
            scheme,
            schemeSpecific,
            authority,
            path,
            query,
            fragment,
            newObject);
   }

   protected FileUri(UriRestrictions restrictions,
                     Scheme scheme,
                     SchemeSpecific schemeSpecific,
                     Authority authority,
                     Path path,
                     Query query,
                     Fragment fragment) {
      super(fileRestrictions(restrictions), scheme, schemeSpecific, authority, path, query, fragment);
   }

   protected FileUri(URI uri, UriRestrictions restrictions) {
      super(uri, fileRestrictions(restrictions));
   }

   protected FileUri(Uri uri, UriRestrictions restrictions) {
      super(uri, fileRestrictions(restrictions));
   }

   @Override
   public Invariants domainInvariants() {
      List<InvariantRule<ComponentUri>> compatibleUriRules = list();
      if (restrictions().relativeFormat()) {
         compatibleUriRules.add(UriRules.isAbsolute(ParameterValue.value(SCHEME)));
      } else {
         compatibleUriRules.add(UriRules
                                      .isRelative()
                                      .orValue(UriRules.isAbsolute(ParameterValue.value(SCHEME))));
      }

      compatibleUriRules.add(UriRules
                                   .isHierarchical()
                                   .andValue(UriRules
                                                   .hasNoAuthority()
                                                   .orValue(UriRules.isEmptyHierarchicalServer())));

      if (restrictions().relativePath() && restrictions().emptyPath()) {
         compatibleUriRules.add(ComponentUriRules.path(PathRules.isAbsolute()));
      }

      var uriCompatibility = Invariant.of(() -> this, compatibleUriRules).groups(COMPATIBILITY_GROUP);

      var uriValidity = Invariant.of(() -> this,
                                     ComponentUriRules
                                           .path(PathRules.isNotEmpty())
                                           .ifIsSatisfied(() -> restrictions().emptyPath()));

      return super.domainInvariants().withInvariants(uriCompatibility, uriValidity);
   }

   public static FileUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new FileUri(uri, restrictions));
   }

   public static FileUri ofUri(URI uri) {
      return ofUri(uri, FileUriRestrictions.ofDefault());
   }

   public static FileUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new FileUri(uri, restrictions));
   }

   public static FileUri ofUri(Uri uri) {
      return ofUri(uri, FileUriRestrictions.ofDefault());
   }

   public static FileUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static FileUri ofUri(String uri) {
      return ofUri(uri, FileUriRestrictions.ofDefault());
   }

   public static FileUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   public static FileUri ofUrn(URI urn) {
      return ofUrn(urn, FileUriRestrictions.ofDefault());
   }

   public static FileUri ofPath(java.nio.file.Path path) {
      return hierarchical(SimplePath.of(path), null, null);
   }

   public static FileUri hierarchical(Path path, Query query, Fragment fragment) {
      Check.notNull(path, "path");

      var scheme = path.isAbsolute() ? SimpleScheme.of(SCHEME) : null;
      return buildUri(() -> new FileUri(FileUriRestrictions.automatic(scheme, path, query, fragment),
                                        scheme,
                                        null,
                                        null,
                                        path,
                                        query,
                                        fragment));
   }

   public static FileUri hierarchical(Path path, Query query) {
      return hierarchical(path, query, null);
   }

   public static FileUri hierarchical(Path path) {
      return hierarchical(path, null, null);
   }

   public static FileUri hierarchical(Path path, Fragment fragment) {
      return hierarchical(path, null, fragment);
   }

   public static FileUri relative(Path path, Query query, Fragment fragment) {
      Check.notNull(path, "path");

      return buildUri(() -> new FileUri(FileUriRestrictions.automatic(null, path, query, fragment),
                                        null,
                                        null,
                                        null,
                                        path,
                                        query,
                                        fragment));
   }

   public static FileUri relative(Path path, Query query) {
      return relative(path, query, null);
   }

   public static FileUri relative(Path path) {
      return relative(path, null, null);
   }

   public static FileUri relative(Path path, Fragment fragment) {
      return relative(path, null, fragment);
   }

   @Override
   protected FileUri recreate(URI uri,
                              UriRestrictions restrictions,
                              Scheme scheme,
                              SchemeSpecific schemeSpecific,
                              Authority authority,
                              Path path,
                              Query query,
                              Fragment fragment,
                              boolean newObject) {
      return buildUri(() -> new FileUri(uri,
                                        restrictions,
                                        scheme,
                                        schemeSpecific,
                                        authority,
                                        path,
                                        query,
                                        fragment,
                                        newObject));
   }

   @Override
   public FileUriRestrictions restrictions() {
      return (FileUriRestrictions) restrictions;
   }

   public FileUri fileRestrictions(Function<? super FileUriRestrictions, ? extends FileUriRestrictions> restrictionsMapper) {
      return restrictions(restrictionsMapper.apply(restrictions()));
   }

   /**
    * Returns this filesystem {@link Uri} as a {@link java.nio.file.Path}.
    *
    * @return path
    */
   public java.nio.file.Path toPath() {
      return java.nio.file.Path.of(path(false).orElseThrow());
   }

   protected static UriRestrictions fileRestrictions(UriRestrictions restrictions) {
      return nullable(restrictions, FileUriRestrictions::of);
   }

   public static class FileUriRestrictions extends DefaultUriRestrictions {
      protected static final boolean DEFAULT_RELATIVE_FORMAT = true;
      protected static final boolean DEFAULT_RELATIVE_PATH = true;
      protected static final boolean DEFAULT_EMPTY_PATH = true;
      protected static final boolean DEFAULT_QUERY = true;
      protected static final boolean DEFAULT_FRAGMENT = true;

      protected final boolean relativeFormat;
      protected final boolean relativePath;
      protected final boolean emptyPath;

      protected FileUriRestrictions(boolean relativeFormat,
                                    boolean relativePath,
                                    boolean emptyPath,
                                    boolean query,
                                    boolean fragment) {
         super(query, fragment);
         this.relativeFormat = relativeFormat;
         this.relativePath = relativePath;
         this.emptyPath = emptyPath;
      }

      public static FileUriRestrictions of(UriRestrictions restrictions) {
         if (restrictions instanceof FileUriRestrictions) {
            var fileRestrictions = (FileUriRestrictions) restrictions;
            return new FileUriRestrictions(fileRestrictions.relativeFormat,
                                           fileRestrictions.relativePath,
                                           fileRestrictions.emptyPath,
                                           fileRestrictions.query,
                                           fileRestrictions.fragment);
         } else if (restrictions instanceof DefaultUriRestrictions) {
            var defaultRestrictions = (DefaultUriRestrictions) restrictions;
            return new FileUriRestrictions(DEFAULT_RELATIVE_FORMAT,
                                           DEFAULT_RELATIVE_PATH,
                                           DEFAULT_EMPTY_PATH,
                                           defaultRestrictions.query(),
                                           defaultRestrictions.fragment());
         } else {
            return new FileUriRestrictions(DEFAULT_RELATIVE_FORMAT,
                                           DEFAULT_RELATIVE_PATH,
                                           DEFAULT_EMPTY_PATH,
                                           DEFAULT_QUERY,
                                           DEFAULT_FRAGMENT);
         }
      }

      public static FileUriRestrictions ofRestrictions(boolean relativeFormat,
                                                       boolean relativePath,
                                                       boolean emptyPath,
                                                       boolean query,
                                                       boolean fragment) {
         return new FileUriRestrictions(relativeFormat, relativePath, emptyPath, query, fragment);
      }

      public static FileUriRestrictions ofDefault() {
         return ofRestrictions(DEFAULT_RELATIVE_FORMAT,
                               DEFAULT_RELATIVE_PATH,
                               DEFAULT_EMPTY_PATH,
                               true,
                               true);
      }

      public static FileUriRestrictions noRestrictions() {
         return ofRestrictions(false, false, false, false, false);
      }

      public static FileUriRestrictions automatic(Scheme scheme, Path path, Query query, Fragment fragment) {
         return ofRestrictions(scheme != null,
                               path.isAbsolute(),
                               !path.isEmpty(),
                               query == null,
                               fragment == null);
      }

      /**
       * @implNote portRestriction and portMapper are discarded from this step.
       */
      @Override
      final protected FileUriRestrictions recreate(PortRestriction portRestriction,
                                                   BiFunction<String, Integer, Integer> portMapper,
                                                   boolean query,
                                                   boolean fragment) {
         return recreate(relativeFormat, relativePath, emptyPath, query, fragment);
      }

      protected FileUriRestrictions recreate(boolean relativeFormat,
                                             boolean relativePath,
                                             boolean emptyPath,
                                             boolean query,
                                             boolean fragment) {
         return new FileUriRestrictions(relativeFormat, relativePath, emptyPath, query, fragment);
      }

      /**
       * Returns {@code true} if relative paths are restricted.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public boolean relativeFormat() {
         return relativeFormat;
      }

      /**
       * Whether to restrict relative paths.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public FileUriRestrictions relativeFormat(boolean relativeFormat) {
         return recreate(relativeFormat, relativePath, emptyPath, query, fragment);
      }

      /**
       * Returns {@code true} if relative paths are restricted.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public boolean relativePath() {
         return relativePath;
      }

      /**
       * Whether to restrict relative paths.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public FileUriRestrictions relativePath(boolean relativePath) {
         return recreate(relativeFormat, relativePath, emptyPath, query, fragment);
      }

      /**
       * Returns {@code true} if empty paths are restricted.
       * <p>
       * This restriction is interpreted as a URI invalidity at build time.
       */
      public boolean emptyPath() {
         return emptyPath;
      }

      /**
       * Whether to restrict empty paths.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public FileUriRestrictions emptyPath(boolean emptyPath) {
         return recreate(relativeFormat, relativePath, emptyPath, query, fragment);
      }

      @Override
      public FileUriRestrictions query(boolean query) {
         return (FileUriRestrictions) super.query(query);
      }

      @Override
      public FileUriRestrictions fragment(boolean fragment) {
         return (FileUriRestrictions) super.fragment(fragment);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", FileUriRestrictions.class.getSimpleName() + "[", "]")
               .add("relativeFormat=" + relativeFormat)
               .add("relativePath=" + relativePath)
               .add("emptyPath=" + emptyPath)
               .add("query=" + query)
               .add("fragment=" + fragment)
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         FileUriRestrictions restrictions = (FileUriRestrictions) o;
         return query == restrictions.query
                && fragment == restrictions.fragment
                && relativeFormat == restrictions.relativeFormat
                && relativePath == restrictions.relativePath
                && emptyPath == restrictions.emptyPath;
      }

      @Override
      public int hashCode() {
         return Objects.hash(super.hashCode(), relativeFormat, relativePath, emptyPath);
      }
   }

}
