/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeAuthority;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.Try.ofThrownBy;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.RegistryAuthority;
import com.tinubu.commons.ddd2.uri.InvalidUriException;

public class SimpleRegistryAuthority implements RegistryAuthority {
      protected final String authority;
      protected final String encodedAuthority;

      protected SimpleRegistryAuthority(String authority, String encodedAuthority) {
         this.authority = Check.notBlank(authority, "value");
         this.encodedAuthority = Check.notBlank(encodedAuthority, "encodedValue");
      }

      public static SimpleRegistryAuthority of(String authority) {
         Check.notBlank(authority, "authority");

         return new SimpleRegistryAuthority(authority, encodeAuthority(authority).orElseThrow(identity()));
      }

      public static SimpleRegistryAuthority ofEncoded(String encodedAuthority) {
         Check.notBlank(encodedAuthority, "encodedAuthority");

         var encodedUri = "//" + encodedAuthority;

         URI uri =
               ofThrownBy(() -> new URI(encodedUri)).orElseThrow(e -> new InvalidUriException(encodedUri, e));

         return of(uri).orElseThrow(() -> new InvalidUriException(uri).subMessage(
               "URI has no registry-based authority"));
      }

      public static Optional<? extends SimpleRegistryAuthority> of(URI uri) {
         Check.notNull(uri, "uri");

         if (uri.getAuthority() != null && uri.getHost() == null) {
            return optional(new SimpleRegistryAuthority(uri.getAuthority(), uri.getRawAuthority()));
         } else {
            return optional();
         }
      }

      public static SimpleRegistryAuthority of(RegistryAuthority authority) {
         Check.notNull(authority, "authority");

         return new SimpleRegistryAuthority(authority.value(), authority.encodedValue());
      }

      @Override
      public Optional<? extends SimpleRegistryAuthority> recreate(URI uri) {
         return of(uri);
      }

      @Override
      public String value() {
         return authority;
      }

      @Override
      public String encodedValue() {
         return encodedAuthority;
      }

      @Override
      public StringBuilder appendEncodedValue(StringBuilder builder) {
         builder.append("//");
         builder.append(encodedAuthority);
         return builder;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SimpleRegistryAuthority that = (SimpleRegistryAuthority) o;
         return Objects.equals(authority, that.authority) && Objects.equals(encodedAuthority,
                                                                            that.encodedAuthority);
      }

      @Override
      public int hashCode() {
         return Objects.hash(authority, encodedAuthority);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SimpleRegistryAuthority.class.getSimpleName() + "[", "]")
               .add("value='" + authority + "'")
               .add("encodedValue='" + encodedAuthority + "'")
               .toString();
      }
   }
