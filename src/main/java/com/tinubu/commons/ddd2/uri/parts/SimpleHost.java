/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Host;

public class SimpleHost implements Host {
      protected final String host;

      protected SimpleHost(String host) {
         this.host = Check.notBlank(host, "host");
      }

      public static SimpleHost of(String host) {
         return new SimpleHost(host);
      }

      public static Optional<? extends SimpleHost> of(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getHost()).map(SimpleHost::new);
      }

      public static SimpleHost of(Host host) {
         Check.notNull(host, "host");

         return new SimpleHost(host.value());
      }

      @Override
      public Optional<? extends Host> recreate(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getHost()).map(this::recreate);
      }

      protected SimpleHost recreate(String host) {
         return new SimpleHost(host);
      }

      @Override
      public String value() {
         return host;
      }

      @Override
      public StringBuilder appendEncodedValue(StringBuilder builder) {
         return builder.append(host);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SimpleHost that = (SimpleHost) o;
         return Objects.equals(host, that.host);
      }

      @Override
      public int hashCode() {
         return Objects.hashCode(host);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SimpleHost.class.getSimpleName() + "[", "]")
               .add("host='" + host + "'")
               .toString();
      }
   }
