/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.uri.UriEncoder.decode;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeUserInfo;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;

public class SimpleUserInfo implements UserInfo {
   protected final String userInfo;
   protected final String encodedUserInfo;

   protected SimpleUserInfo(String userInfo, String encodedUserInfo) {
      this.userInfo = Check.notNull(userInfo, "value");
      this.encodedUserInfo = Check.notNull(encodedUserInfo, "encodedValue");
   }

   public static SimpleUserInfo of(String userInfo) {
      Check.notNull(userInfo, "userInfo");

      return new SimpleUserInfo(userInfo, encodeUserInfo(userInfo).orElseThrow(identity()));
   }

   public static SimpleUserInfo ofEncoded(String encodedUserInfo) {
      Check.notNull(encodedUserInfo, "encodedUserInfo");

      return new SimpleUserInfo(decode(encodedUserInfo).orElseThrow(identity()), encodedUserInfo);
   }

   public static Optional<? extends SimpleUserInfo> of(URI uri) {
      Check.notNull(uri, "uri");

      return nullable(uri.getUserInfo()).map(__ -> new SimpleUserInfo(uri.getUserInfo(),
                                                                      uri.getRawUserInfo()));
   }

   public static SimpleUserInfo of(UserInfo userInfo) {
      Check.notNull(userInfo, "userInfo");

      return new SimpleUserInfo(userInfo.value(), userInfo.encodedValue());
   }

   protected SimpleUserInfo recreate(String userInfo, String encodedUserInfo) {
      return new SimpleUserInfo(userInfo, encodedUserInfo);
   }

   @Override
   public Optional<? extends SimpleUserInfo> recreate(URI uri) {
      Check.notNull(uri, "uri");

      return nullable(uri.getUserInfo()).map(__ -> recreate(uri.getUserInfo(), uri.getRawUserInfo()));
   }

   @Override
   public String value() {
      return userInfo;
   }

   @Override
   public String encodedValue() {
      return encodedUserInfo;
   }

   @Override
   public StringBuilder appendEncodedValue(StringBuilder builder) {
      return builder.append(encodedUserInfo).append('@');
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimpleUserInfo that = (SimpleUserInfo) o;
      return Objects.equals(userInfo, that.userInfo) && Objects.equals(encodedUserInfo, that.encodedUserInfo);
   }

   @Override
   public int hashCode() {
      return Objects.hash(userInfo, encodedUserInfo);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SimpleUserInfo.class.getSimpleName() + "[", "]")
            .add("userInfo='" + userInfo + "'")
            .add("encodedUserInfo='" + encodedUserInfo + "'")
            .toString();
   }
}

