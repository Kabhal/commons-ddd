/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.uri.UriEncoder.decode;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodePath;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.Try.ofThrownBy;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Path;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.lang.util.StringUtils;

public class SimplePath implements Path {
   private static final SimplePath ROOT_PATH = new SimplePath("/", "/");
   private static final SimplePath EMPTY_PATH = new SimplePath("", "");

   protected final String path;
   protected final String encodedPath;
   protected java.nio.file.Path typedPath;

   protected SimplePath(String path, String encodedPath) {
      this.path = Check.notNull(path, "path");
      this.encodedPath = Check.notNull(encodedPath, "encodedPath");
   }

   public static SimplePath emptyPath() {
      return EMPTY_PATH;
   }

   public static SimplePath rootPath() {
      return ROOT_PATH;
   }

   public static SimplePath of(String path) {
      Check.notNull(path, "path");

      return new SimplePath(path, encodePath(path).orElseThrow(identity()));
   }

   public static SimplePath ofEncoded(String encodedPath) {
      Check.notNull(encodedPath, "encodedPath");

      return new SimplePath(decode(encodedPath).orElseThrow(identity()), encodedPath);
   }

   public static Optional<? extends SimplePath> of(URI uri) {
      Check.notNull(uri, "uri");

      return nullable(uri.getPath()).map(__ -> new SimplePath(uri.getPath(), uri.getRawPath()));
   }

   public static SimplePath of(Path path) {
      Check.notNull(path, "path");

      return new SimplePath(path.value(), path.encodedValue());
   }

   public static SimplePath of(java.nio.file.Path path) {
      Check.notNull(path, "path");

      return of(path.toString());
   }

   @Override
   public Optional<? extends SimplePath> recreate(URI uri) {
      Check.notNull(uri, "uri");

      return nullable(uri.getPath()).map(__ -> recreate(uri.getPath(), uri.getRawPath()));
   }

   public SimplePath recreate(String path, String encodedPath) {
      return new SimplePath(path, encodedPath);
   }

   @Override
   public java.nio.file.Path pathValue() {
      if (typedPath == null) {
         typedPath = java.nio.file.Path.of(path);
      }
      return typedPath;
   }

   @Override
   public String value() {
      return path;
   }

   @Override
   public String encodedValue() {
      return encodedPath;
   }

   @Override
   public Path resolve(java.nio.file.Path path) {
      return value(pathValue().resolve(path).toString());
   }

   @Override
   public Path resolve(Path path) {
      return resolve(path.pathValue());
   }

   @Override
   public SimplePath value(String path) {
      Check.notNull(path, "path");

      return recreate(path, encodePath(path).orElseThrow(identity()));
   }

   @Override
   public SimplePath encodedValue(String encodedPath) {
      Check.notNull(encodedPath, "encodedPath");

      return recreate(decode(encodedPath).orElseThrow(identity()), encodedPath);
   }

   @Override
   public StringBuilder appendEncodedValue(StringBuilder builder) {
      return builder.append(encodedPath);
   }

   @Override
   public SimplePath normalize() {
      return normalize(false, false);
   }

   /**
    * @param preserveDoubleSlashedRoots whether to keep double-slashed root paths after normalization
    * @param normalizeEndingSlashes whether to remove ending path slashes in the normalization
    *       process
    *
    * @implNote an empty <em>registry</em> is prefixed to absolute paths to be able to normalize
    *       "double-slashed" root paths (e.g.: SFTP), otherwise, double-slashed paths are mistakenly
    *       considered as a registry-based authority. Double-slashed root paths are normalized to
    *       one-slashed root paths excepting if {@code preserveDoubleSlashedRoots} is set.
    */
   public SimplePath normalize(boolean preserveDoubleSlashedRoots, boolean normalizeEndingSlashes) {
      var authority = path.startsWith("/") ? "//" : null;
      var preserveDoubleSlashedRoot = preserveDoubleSlashedRoots && path.startsWith("//");

      URI uri = ofThrownBy(() -> new URI(null, authority, path, null, null))
            .orElseThrow(e -> new InvalidUriException(null, authority, path, null, null, e))
            .normalize();

      Function<String, String> finalize =
            p -> (preserveDoubleSlashedRoot ? "/" : "") + (normalizeEndingSlashes
                                                           ? StringUtils.removeEnd(p,
                                                                                   "/")
                                                           : p);

      return recreate(finalize.apply(uri.getPath()), finalize.apply(uri.getRawPath()));
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimplePath that = (SimplePath) o;
      return Objects.equals(path, that.path) && Objects.equals(encodedPath, that.encodedPath);
   }

   @Override
   public int hashCode() {
      return Objects.hash(path, encodedPath);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SimplePath.class.getSimpleName() + "[", "]")
            .add("path=" + path)
            .add("encodedPath=" + encodedPath)
            .toString();
   }
}
