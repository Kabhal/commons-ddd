/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.hideField;
import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.tryValidateInvariants;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotStartWith;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.hasNoFragment;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.hasNoQuery;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.NONE;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeAuthority;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeFragment;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodePath;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeQuery;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeSchemeSpecific;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeUserInfo;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.tinubu.commons.ddd2.domain.type.AbstractSimpleId;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantResult;
import com.tinubu.commons.ddd2.invariant.InvariantResults;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;

/**
 * Abstract URI implementation.
 */
public abstract class AbstractUri<T extends AbstractUri<T>> extends AbstractSimpleId<String> implements Uri {

   /** Compatibility invariants group name. */
   protected static final String COMPATIBILITY_GROUP = "compatibility";

   protected static final String URN_ID_TYPE = "uri";

   protected final URI uri;
   protected final UriRestrictions restrictions;

   /**
    * Main and re-creation constructor working with specified {@link URI}.
    *
    * @param uri URI
    * @param restrictions restrictions
    * @param newObject whether {@link Uri} is a new object
    */
   protected AbstractUri(URI uri, UriRestrictions restrictions, boolean newObject) {
      super((uri = restrictUri(Check.notNull(uri, "uri"),
                               Check.notNull(restrictions, "restrictions"))).toString(), newObject);
      this.uri = uri;
      this.restrictions = restrictions;
   }

   /**
    * Alternative constructor from existing {@link Uri}.
    * <p>
    * Specified {@code uri}'s {@link Uri#restrictions() restrictions} and {@link Uri#newObject()} are ignored
    * because it does not make sense for this different {@link Uri} implementation.
    *
    * @param uri URI
    * @param restrictions restrictions
    */
   protected AbstractUri(Uri uri, UriRestrictions restrictions) {
      this(Check.notNull(uri, "uri").toURI(), restrictions, false);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends T> defineDomainFields() {
      return Fields
            .<T>builder()
            .superFields((Fields<? super T>) super.defineDomainFields())
            .field("value", v -> v.value, isNotNull())
            .technicalField("uri", v -> v.uri, hideField(), isNotNull())
            .field("restrictions", v -> v.restrictions, isNotNull())
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      List<InvariantRule<Uri>> validUriRules = list();

      instanceOf(restrictions, DefaultUriRestrictions.class).ifPresent(restrictions -> {
         if (restrictions.query()) {
            validUriRules.add(hasNoQuery());
         }
         if (restrictions.fragment()) {
            validUriRules.add(hasNoFragment());
         }
      });

      if (validUriRules.isEmpty()) {
         return Invariants.empty();
      } else {
         return Invariants.of(Invariant.of(() -> this, validUriRules));
      }
   }

   /**
    * Applies restrictions on specified URI.
    *
    * @param uri URI to apply restrictions on
    * @param restrictions optional restrictions, or {@code null}
    *
    * @return new URI with applied restrictions, or original URI if restrictions is {@code null} or no
    *       restrictions are applicable
    */
   protected static URI restrictUri(URI uri, UriRestrictions restrictions) {
      if (restrictions instanceof DefaultUriRestrictions) {
         var defaultRestrictions = (DefaultUriRestrictions) restrictions;

         if (!Uri.isHierarchicalServer(uri) || defaultRestrictions.portRestriction == NONE) {
            return uri;
         } else {
            var port = uri.getPort();

            switch (defaultRestrictions.portRestriction) {
               case REMOVE_PORT:
                  port = -1;
                  break;
               case REMOVE_MAPPED_PORT:
                  var removeDefaultPort = defaultRestrictions.portMapper.apply(uri.getScheme(), port);
                  if (removeDefaultPort != null && removeDefaultPort == port) {
                     port = -1;
                  }
                  break;
               case FORCE_MAPPED_PORT:
                  if (port == -1) {
                     var forceDefaultPort = defaultRestrictions.portMapper.apply(uri.getScheme(), port);
                     port = nullable(forceDefaultPort, -1);
                  }
                  break;
               default:
                  throw new IllegalStateException("Unknown port restriction");
            }

            if (port != uri.getPort()) {
               return uri(true,
                          uri.getScheme(),
                          uri.getRawSchemeSpecificPart(),
                          uriServerAuthority(true, uri.getRawUserInfo(), uri.getHost(), port),
                          uri.getRawPath(),
                          uri.getRawQuery(),
                          uri.getRawFragment());
            } else {
               return uri;
            }
         }
      } else {
         return uri;
      }
   }

   /**
    * @implNote When validating multiple invariants, compatibility invariants violations are thrown in
    *       priority.
    */
   protected static <T, U extends Uri> Function<Object, RuntimeException> uriInvariantResultHandler(U uri) {
      return result -> {
         if (result instanceof InvariantResult) {
            var invariantResult = (InvariantResult<?>) result;
            if (invariantResult.invariantMatchingGroups().contains(COMPATIBILITY_GROUP)) {
               return new IncompatibleUriException(uri).subMessage(invariantResult.errorMessage());
            } else {
               return new InvalidUriException(uri).subMessage(invariantResult.errorMessage());
            }
         } else if (result instanceof InvariantResults) {
            var invariantResults = (InvariantResults) result;

            if (invariantResults.anyErrorMatchInvariantMatchingGroup(COMPATIBILITY_GROUP)) {
               return new IncompatibleUriException(uri).subMessage(invariantResults
                                                                         .filter(r -> r
                                                                               .invariantMatchingGroups()
                                                                               .contains(COMPATIBILITY_GROUP))
                                                                         .errorMessage(false, false));
            } else {
               return new InvalidUriException(uri).subMessage(invariantResults.errorMessage(false, false));
            }
         } else {
            throw new IllegalStateException(String.format("Unsupported '%s' result", result));
         }
      };
   }

   // FIXME overridable DomainObject::resultHandler ?
   protected static <T extends Uri> T checkUriInvariants(T uri, String... groups) {
      return tryValidateInvariants(uri, groups).orElseThrow(uriInvariantResultHandler(uri));
   }

   @SuppressWarnings("unchecked")
   protected static <T extends Uri> T buildUri(Supplier<? extends T> uriFactory, String... groups) {
      T domainObjectValue = uriFactory.get();

      return checkedSupplier(() -> (T) checkUriInvariants(domainObjectValue.postConstruct(),
                                                          groups).postValidate())
            .tryCatch(failure -> domainObjectValue.onBuildFailure(failure))
            .getChecked();
   }

   /**
    * Parses urn and returns urn value as {@link URI}.
    *
    * @param urn URN
    *
    * @return URN's value
    */
   public static URI parseUrn(URI urn) {
      return nullable(urn).map(u -> idUrnValue(u, URN_ID_TYPE)).map(Uri::uri).orElse(null);
   }

   /**
    * Recreates current object. Invariants should be checked in implementation.
    */
   protected abstract T recreate(URI uri, UriRestrictions restrictions, boolean newObject);

   /**
    * Recreates current object. Invariants should be checked in implementation.
    */
   protected T recreate(String uri, UriRestrictions restrictions, boolean newObject) {
      return recreate(Uri.uri(uri), restrictions, newObject);
   }

   @Override
   public URI urnValue() {
      return idUrn(URN_ID_TYPE);
   }

   @Override
   public URI toURI() {
      return uri;
   }

   public T asNewObject() {
      return recreate(uri, restrictions, true);
   }

   @Override
   public T value(String value) {
      return recreate(value, restrictions, newObject);
   }

   @Override
   public UriRestrictions restrictions() {
      return restrictions;
   }

   public T restrictions(UriRestrictions restrictions) {
      return recreate(uri, restrictions, newObject);
   }

   public T restrictions(Function<? super UriRestrictions, ? extends UriRestrictions> restrictionsMapper) {
      return restrictions(restrictionsMapper.apply(restrictions));
   }

   @Override
   public T normalize() {
      return recreate(uri.normalize(), restrictions, newObject);
   }

   @Override
   public T resolve(Uri uri) {
      return resolve(uri.toURI());
   }

   @Override
   public T resolve(URI uri) {
      return recreate(this.uri.resolve(uri), restrictions, newObject);
   }

   @Override
   public T relativize(Uri uri) {
      return relativize(uri.toURI());
   }

   @Override
   public T relativize(URI uri) {
      return recreate(this.uri.relativize(uri), restrictions, newObject);
   }

   @Override
   public Optional<String> scheme() {
      return nullable(uri.getScheme());
   }

   @Override
   public Optional<String> schemeSpecific(boolean encoded) {
      if (encoded) {
         return isAbsolute() ? nullable(uri.getRawSchemeSpecificPart()) : optional();
      } else {
         return isAbsolute() ? nullable(uri.getSchemeSpecificPart()) : optional();
      }
   }

   @Override
   public Optional<String> authority(boolean encoded) {
      return serverAuthority(encoded).or(() -> registryAuthority(encoded)).or(() -> {
         if (encoded) {
            return nullable(uri.getRawAuthority());
         } else {
            return nullable(uri.getAuthority());
         }
      });
   }

   @Override
   public Optional<String> serverAuthority(boolean encoded) {
      if (encoded) {
         return isHierarchicalServer() ? optional(nullable(uri.getRawAuthority(), "")) : optional();
      } else {
         return isHierarchicalServer() ? optional(nullable(uri.getAuthority(), "")) : optional();
      }
   }

   @Override
   public Optional<String> registryAuthority(boolean encoded) {
      if (encoded) {
         return isHierarchicalRegistry() ? nullable(uri.getRawAuthority()) : optional();
      } else {
         return isHierarchicalRegistry() ? nullable(uri.getAuthority()) : optional();
      }
   }

   @Override
   public Optional<String> userInfo(boolean encoded) {
      if (encoded) {
         return isHierarchicalServer() ? nullable(uri.getRawUserInfo()) : optional();
      } else {
         return isHierarchicalServer() ? nullable(uri.getUserInfo()) : optional();
      }
   }

   @Override
   public Optional<String> host() {
      return isHierarchicalServer() ? nullable(uri.getHost()) : optional();
   }

   @Override
   public Optional<Integer> port() {
      return isHierarchicalServer() ? optional(uri.getPort()).filter(port -> port != -1) : optional();
   }

   @Override
   public Optional<String> path(boolean encoded) {
      if (encoded) {
         return nullable(uri.getRawPath());
      } else {
         return nullable(uri.getPath());
      }
   }

   @Override
   public Optional<String> query(boolean encoded) {
      if (encoded) {
         return nullable(uri.getRawQuery());
      } else {
         return nullable(uri.getQuery());
      }
   }

   @Override
   public Optional<String> fragment(boolean encoded) {
      if (encoded) {
         return nullable(uri.getRawFragment());
      } else {
         return nullable(uri.getFragment());
      }
   }

   @Override
   public T scheme(Function<String, ? extends String> schemeMapper) {
      Check.validate(this, "this", UriRules.isAbsolute());
      Check.notNull(schemeMapper, "schemeMapper");

      var newScheme = Check.notBlank(schemeMapper.apply(scheme().orElseThrow()), "scheme");

      return recreate(uri(true,
                          newScheme,
                          schemeSpecific(true).orElse(null),
                          authority(true).orElse(null),
                          path(true).orElse(null),
                          query(true).orElse(null),
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T scheme(String scheme) {
      return scheme(__ -> scheme);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalScheme(Function<String, ? extends String> schemeMapper) {
      Check.notNull(schemeMapper, "schemeMapper");

      if (isAbsolute()) {
         return scheme(schemeMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalScheme(String scheme) {
      return conditionalScheme(__ -> scheme);
   }

   @Override
   public T schemeSpecific(boolean encoded, Function<String, ? extends String> schemeSpecificMapper) {
      Check.validate(this, "this", UriRules.isOpaque());
      Check.notNull(schemeSpecificMapper, "schemeSpecificMapper");

      var newSchemeSpecific = encoded
                              ? schemeSpecificMapper.apply(schemeSpecific(true).orElseThrow())
                              : nullable(schemeSpecificMapper.apply(schemeSpecific(false).orElseThrow()))
                                    .map(ss -> encodeSchemeSpecific(ss).orElseThrow(identity()))
                                    .orElse(null);
      Check.validate(newSchemeSpecific,
                     "schemeSpecific",
                     isNotBlank().andValue(doesNotStartWith(ParameterValue.value("/"))));

      return recreate(uri(true, scheme().orElse(null), newSchemeSpecific, null, null, null, null),
                      restrictions,
                      newObject);
   }

   @Override
   public T schemeSpecific(boolean encoded, String schemeSpecific) {
      return schemeSpecific(encoded, __ -> schemeSpecific);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalSchemeSpecific(boolean encoded,
                                      Function<String, ? extends String> schemeSpecificMapper) {
      Check.notNull(schemeSpecificMapper, "schemeSpecificMapper");

      if (isOpaque()) {
         return schemeSpecific(encoded, schemeSpecificMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalSchemeSpecific(boolean encoded, String schemeSpecific) {
      return conditionalSchemeSpecific(encoded, __ -> schemeSpecific);
   }

   @Override
   public T serverAuthority(boolean encoded, Function<String, ? extends String> serverAuthorityMapper) {
      Check.validate(this, "this", UriRules.isHierarchicalServer());
      Check.notNull(serverAuthorityMapper, "serverAuthorityMapper");

      var serverAuthority = encoded
                            ? serverAuthorityMapper.apply(serverAuthority(true).orElseThrow())
                            : nullable(serverAuthorityMapper.apply(serverAuthority(false).orElseThrow()))
                                  .map(a -> encodeAuthority(a).orElseThrow(identity()))
                                  .orElse(null);
      var newServerAuthority = Check.notNull(serverAuthority, "serverAuthority");
      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          newServerAuthority,
                          path(true).orElse(null),
                          query(true).orElse(null),
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T serverAuthority(boolean encoded, String serverAuthority) {
      return serverAuthority(encoded, __ -> serverAuthority);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalServerAuthority(boolean encoded,
                                       Function<String, ? extends String> serverAuthorityMapper) {
      Check.notNull(serverAuthorityMapper, "serverAuthorityMapper");

      if (isHierarchicalServer()) {
         return serverAuthority(encoded, serverAuthorityMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalServerAuthority(boolean encoded, String serverAuthority) {
      return conditionalServerAuthority(encoded, __ -> serverAuthority);
   }

   @Override
   public T registryAuthority(boolean encoded, Function<String, ? extends String> registryAuthorityMapper) {
      Check.validate(this, "this", UriRules.isHierarchicalRegistry());
      Check.notNull(registryAuthorityMapper, "registryAuthorityMapper");

      var registryAuthority = encoded
                              ? registryAuthorityMapper.apply(registryAuthority(true).orElseThrow())
                              : nullable(registryAuthorityMapper.apply(registryAuthority(false).orElseThrow()))
                                    .map(a -> encodeAuthority(a).orElseThrow(identity()))
                                    .orElse(null);
      var newRegistryAuthority = Check.notBlank(registryAuthority, "registryAuthority");

      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          newRegistryAuthority,
                          path(true).orElse(null),
                          query(true).orElse(null),
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T registryAuthority(boolean encoded, String registryAuthority) {
      return registryAuthority(encoded, __ -> registryAuthority);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalRegistryAuthority(boolean encoded,
                                         Function<String, ? extends String> registryAuthorityMapper) {
      Check.notNull(registryAuthorityMapper, "registryAuthorityMapper");

      if (isHierarchicalRegistry()) {
         return registryAuthority(encoded, registryAuthorityMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalRegistryAuthority(boolean encoded, String registryAuthority) {
      return conditionalRegistryAuthority(encoded, __ -> registryAuthority);
   }

   @Override
   public T userInfo(boolean encoded, Function<String, ? extends String> userInfoMapper) {
      Check.validate(this,
                     "this",
                     UriRules.isHierarchicalServer().andValue(UriRules.isNotEmptyHierarchicalServer()));
      Check.notNull(userInfoMapper, "userInfoMapper");

      var newUserInfo = encoded
                        ? userInfoMapper.apply(userInfo(true).orElse(null))
                        : encodeUserInfo(userInfoMapper.apply(userInfo(false).orElse(null))).orElseThrow(
                              identity());
      var newAuthority = uriServerAuthority(true, newUserInfo, host().orElse(null), port().orElse(-1));

      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          newAuthority,
                          path(true).orElse(null),
                          query(true).orElse(null),
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T userInfo(boolean encoded, String userInfo) {
      return userInfo(encoded, __ -> userInfo);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalUserInfo(boolean encoded, Function<String, ? extends String> userInfoMapper) {
      Check.notNull(userInfoMapper, "userInfoMapper");

      if (isHierarchicalServer() && !isEmptyHierarchicalServer()) {
         return userInfo(encoded, userInfoMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalUserInfo(boolean encoded, String userInfo) {
      return conditionalUserInfo(encoded, __ -> userInfo);
   }

   @Override
   public T host(Function<String, ? extends String> hostMapper) {
      Check.validate(this,
                     "this",
                     UriRules.isHierarchicalServer().andValue(UriRules.isNotEmptyHierarchicalServer()));
      Check.notNull(hostMapper, "hostMapper");

      var newHost = Check.notBlank(hostMapper.apply(host().orElseThrow()), "host");
      var newAuthority = uriServerAuthority(true, userInfo(true).orElse(null), newHost, port().orElse(-1));

      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          newAuthority,
                          path(true).orElse(null),
                          query(true).orElse(null),
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T host(String host) {
      return host(__ -> host);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalHost(Function<String, ? extends String> hostMapper) {
      Check.notNull(hostMapper, "hostMapper");

      if (isHierarchicalServer() && !isEmptyHierarchicalServer()) {
         return host(hostMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalHost(String host) {
      return conditionalHost(__ -> host);
   }

   @Override
   public T port(Function<Integer, Integer> portMapper) {
      Check.validate(this,
                     "this",
                     UriRules.isHierarchicalServer().andValue(UriRules.isNotEmptyHierarchicalServer()));
      Check.notNull(portMapper, "portMapper");

      int newPort = nullable(portMapper.apply(port().orElse(null)), -1);
      var newAuthority = uriServerAuthority(true, userInfo(true).orElse(null), host().orElse(null), newPort);
      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          newAuthority,
                          path(true).orElse(null),
                          query(true).orElse(null),
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T port(Integer port) {
      return port(__ -> port);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalPort(Function<Integer, Integer> portMapper) {
      Check.notNull(portMapper, "portMapper");

      if (isHierarchicalServer() && !isEmptyHierarchicalServer()) {
         return port(portMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalPort(Integer port) {
      return conditionalPort(__ -> port);
   }

   @Override
   public T path(boolean encoded, Function<String, ? extends String> pathMapper) {
      Check.validate(this, "this", UriRules.isHierarchical());
      Check.notNull(pathMapper, "pathMapper");

      var newPath = encoded
                    ? pathMapper.apply(path(true).orElseThrow())
                    : encodePath(pathMapper.apply(path(false).orElseThrow())).orElseThrow(identity());
      Check.validate(newPath,
                     "path",
                     isNotNull().andValue(satisfies((String p) -> p.startsWith("/"),
                                                    "'%s' must be absolute",
                                                    validatingObject())
                                                .ifIsSatisfied(() -> isAbsolute() && !(isHierarchicalServer()
                                                                                       || isHierarchicalRegistry()))
                                                .andValue(satisfies((String p) -> p.startsWith("/")
                                                                                  || p.isEmpty(),
                                                                    "'%s' must be absolute or empty",
                                                                    validatingObject()).ifIsSatisfied(() -> isAbsolute()
                                                                                                            || isHierarchicalServer()
                                                                                                            || isHierarchicalRegistry()))));

      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          authority(true).orElse(null),
                          newPath,
                          query(true).orElse(null),
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T path(boolean encoded, String path) {
      return path(encoded, __ -> path);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalPath(boolean encoded, Function<String, ? extends String> pathMapper) {
      Check.notNull(pathMapper, "pathMapper");

      if (isHierarchical()) {
         return path(encoded, pathMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalPath(boolean encoded, String path) {
      return conditionalPath(encoded, __ -> path);
   }

   @Override
   public T query(boolean encoded, Function<String, ? extends String> queryMapper) {
      Check.validate(this, "this", UriRules.isHierarchical());
      Check.notNull(queryMapper, "queryMapper");

      var newQuery = encoded
                     ? queryMapper.apply(query(true).orElse(null))
                     : encodeQuery(queryMapper.apply(query(false).orElse(null))).orElseThrow(identity());

      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          authority(true).orElse(null),
                          path(true).orElse(null),
                          newQuery,
                          fragment(true).orElse(null)), restrictions, newObject);
   }

   @Override
   public T query(boolean encoded, String query) {
      return query(encoded, __ -> query);
   }

   @SuppressWarnings("unchecked")
   @Override
   public T conditionalQuery(boolean encoded, Function<String, ? extends String> queryMapper) {
      Check.notNull(queryMapper, "queryMapper");

      if (isHierarchical()) {
         return query(encoded, queryMapper);
      } else {
         return (T) this;
      }
   }

   @Override
   public T conditionalQuery(boolean encoded, String query) {
      return conditionalQuery(encoded, __ -> query);
   }

   @Override
   public T fragment(boolean encoded, Function<String, ? extends String> fragmentMapper) {
      Check.notNull(fragmentMapper, "fragmentMapper");

      var newFragment = encoded
                        ? fragmentMapper.apply(fragment(true).orElse(null))
                        : encodeFragment(fragmentMapper.apply(fragment(false).orElse(null))).orElseThrow(
                              identity());

      return recreate(uri(true,
                          scheme().orElse(null),
                          schemeSpecific(true).orElse(null),
                          authority(true).orElse(null),
                          path(true).orElse(null),
                          query(true).orElse(null),
                          newFragment), restrictions, newObject);
   }

   @Override
   public T fragment(boolean encoded, String fragment) {
      return fragment(encoded, __ -> fragment);
   }

   @Override
   public T conditionalFragment(boolean encoded, Function<String, ? extends String> fragmentMapper) {
      Check.notNull(fragmentMapper, "fragmentMapper");

      return fragment(encoded, fragmentMapper);
   }

   @Override
   public T conditionalFragment(boolean encoded, String fragment) {
      return conditionalFragment(encoded, __ -> fragment);
   }

   protected static String uriServerAuthority(boolean encoded, String userInfo, String host, int port) {
      StringBuilder builder = new StringBuilder();

      if (userInfo != null) {
         builder.append(encoded ? userInfo : encodeUserInfo(userInfo).orElseThrow(identity())).append('@');
      }
      if (host != null) {
         builder.append(host);
      }
      if (port != -1) {
         builder.append(':').append(port);
      }

      return builder.toString();
   }

   /**
    * Reconstitutes a URI from components.
    *
    * @param encoded whether specified parts are encoded
    */
   protected static URI uri(boolean encoded,
                            String scheme,
                            String schemeSpecific,
                            String authority,
                            String path,
                            String query,
                            String fragment) {
      StringBuilder builder = new StringBuilder();

      if (scheme != null) {
         builder.append(scheme).append(':');
      }
      if (authority != null || path != null || query != null) {
         if (authority != null) {
            builder
                  .append("//")
                  .append(encoded ? authority : encodeAuthority(authority).orElseThrow(identity()));
         }
         if (path != null) {
            builder.append(encoded ? path : encodePath(path).orElseThrow(identity()));
         }
         if (query != null) {
            builder.append('?').append(encoded ? query : encodeQuery(query).orElseThrow(identity()));
         }
      } else {
         if (schemeSpecific != null) {
            builder.append(encoded
                           ? schemeSpecific
                           : encodeSchemeSpecific(schemeSpecific).orElseThrow(identity()));
         }
      }
      if (fragment != null) {
         builder.append('#').append(encoded ? fragment : encodeFragment(fragment).orElseThrow(identity()));
      }

      return Uri.uri(builder.toString());
   }

}
