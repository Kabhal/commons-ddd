/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Port;

public class SimplePort implements Port {
   protected final int port;

   protected SimplePort(int port) {
      this.port = Check.validate(port, "port", isPositive());
   }

   public static SimplePort of(int port) {
      return new SimplePort(port);
   }

   public static Optional<? extends SimplePort> of(URI uri) {
      Check.notNull(uri, "uri");

      return optional(uri.getPort()).filter(port -> port != -1).map(SimplePort::new);
   }

   public static SimplePort of(Port port) {
      Check.notNull(port, "port");

      return new SimplePort(port.port());
   }

   @Override
   public Optional<? extends SimplePort> recreate(URI uri) {
      Check.notNull(uri, "uri");

      return optional(uri.getPort()).filter(port -> port != -1).map(SimplePort::new);
   }

   protected SimplePort recreate(int port) {
      return new SimplePort(port);
   }

   @Override
   public int port() {
      return port;
   }

   @Override
   public String value() {
      return String.valueOf(port);
   }

   @Override
   public StringBuilder appendEncodedValue(StringBuilder builder) {
      return builder.append(':').append(port);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimplePort that = (SimplePort) o;
      return Objects.equals(port, that.port);
   }

   @Override
   public int hashCode() {
      return Objects.hashCode(port);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SimplePort.class.getSimpleName() + "[", "]")
            .add("port='" + port + "'")
            .toString();
   }
}
