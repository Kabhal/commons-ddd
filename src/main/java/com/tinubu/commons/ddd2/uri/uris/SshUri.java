/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.isNotEmptyHierarchicalServer;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.rules.ClassRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.ComponentUriRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.ComponentUriRules.PathRules;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleSchemeSpecific;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;

/**
 * SSH {@link Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>{@code ssh://[<user>[:<password>]@]<host>[:<port>]</path>[?<query>][#<fragment>]}</li>
 * </ul>
 * However, supported format depends on {@link SshUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link SshUriRestrictions#emptyPath() emptyPath} : path can be empty</li>
 *    <li>{@link SshUriRestrictions#portRestriction() portRestriction} : no port restriction</li>
 *    <li>{@link SshUriRestrictions#portMapper() portMapper} : default port = {@value DEFAULT_PORT}</li>
 *    <li>{@link SshUriRestrictions#query() query} : query is not supported</li>
 *    <li>{@link SshUriRestrictions#fragment() fragment} : fragment is not supported</li>
 * </ul>
 */
public class SshUri extends AbstractComponentUri<SshUri> {

   protected static final String SCHEME = "ssh";
   protected static final int DEFAULT_PORT = 22;

   protected SshUri(URI uri,
                    UriRestrictions restrictions,
                    Scheme scheme,
                    SchemeSpecific schemeSpecific,
                    Authority authority,
                    Path path,
                    Query query,
                    Fragment fragment,
                    boolean newObject) {
      super(uri,
            sshRestrictions(restrictions),
            scheme,
            schemeSpecific,
            sshUserInfo(authority),
            path,
            query,
            fragment,
            newObject);
   }

   protected SshUri(UriRestrictions restrictions,
                    Scheme scheme,
                    SchemeSpecific schemeSpecific,
                    Authority authority,
                    Path path,
                    Query query,
                    Fragment fragment) {
      super(sshRestrictions(restrictions),
            scheme,
            schemeSpecific,
            sshUserInfo(authority),
            path,
            query,
            fragment);
   }

   protected SshUri(URI uri, UriRestrictions restrictions) {
      this(uri,
           restrictions,
           SimpleScheme.of(uri).orElse(null),
           SimpleSchemeSpecific.of(uri).orElse(null),
           SimpleAuthority.of(uri).orElse(null),
           SimplePath.of(uri).orElse(null),
           SimpleQuery.of(uri).orElse(null),
           SimpleFragment.of(uri).orElse(null),
           false);
   }

   protected SshUri(Uri uri, UriRestrictions restrictions) {
      super(sshUserInfo(nullableInstanceOf(uri, ComponentUri.class, ComponentUri::ofUri)),
            sshRestrictions(restrictions));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends SshUri> defineDomainFields() {
      return Fields
            .<SshUri>builder()
            .superFields((Fields<? super SshUri>) super.defineDomainFields())
            .field("authority",
                   v -> v.authority,
                   isNotNull().andValue(ClassRules.instanceOf(ServerAuthority.class,
                                                              as(ServerAuthority::userInfo,
                                                                 optionalValue(isInstanceOf(ParameterValue.value(
                                                                       UsernamePasswordUserInfo.class)))))))
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      var uriCompatibility = Invariant
            .of(() -> this,
                UriRules.isAbsolute(ParameterValue.value(SCHEME)),
                UriRules.isHierarchicalServer().andValue(isNotEmptyHierarchicalServer()))
            .groups(COMPATIBILITY_GROUP);

      var uriValidity = Invariant.of(() -> this,
                                     ComponentUriRules
                                           .path(PathRules.isNotEmpty())
                                           .ifIsSatisfied(() -> restrictions().emptyPath()));

      return super.domainInvariants().withInvariants(uriCompatibility, uriValidity);
   }

   public static SshUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new SshUri(uri, restrictions));
   }

   public static SshUri ofUri(URI uri) {
      return ofUri(uri, SshUriRestrictions.ofDefault());
   }

   public static SshUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new SshUri(uri, restrictions));
   }

   public static SshUri ofUri(Uri uri) {
      return ofUri(uri, SshUriRestrictions.ofDefault());
   }

   public static SshUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static SshUri ofUri(String uri) {
      return ofUri(uri, SshUriRestrictions.ofDefault());
   }

   public static SshUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   public static SshUri ofUrn(URI urn) {
      return ofUrn(urn, SshUriRestrictions.ofDefault());
   }

   public static SshUri hierarchical(ServerAuthority authority, Path path, Query query, Fragment fragment) {
      validate(authority, "authority", isNotNull()).and(validate(path, "path", isNotNull())).orThrow();

      return buildUri(() -> new SshUri(SshUriRestrictions.automatic(path, query, fragment),
                                       SimpleScheme.of(SCHEME),
                                       null,
                                       authority,
                                       path,
                                       query,
                                       fragment));
   }

   public static SshUri hierarchical(ServerAuthority authority, Path path, Query query) {
      return hierarchical(authority, path, query, null);
   }

   public static SshUri hierarchical(ServerAuthority authority, Path path) {
      return hierarchical(authority, path, null, null);
   }

   public static SshUri hierarchical(ServerAuthority authority, Path path, Fragment fragment) {
      return hierarchical(authority, path, null, fragment);
   }

   private static SshUriRestrictions sshRestrictions(UriRestrictions restrictions) {
      return nullable(restrictions)
            .map(r -> SshUriRestrictions.of(r).defaultPortMapper(DEFAULT_PORT))
            .orElse(null);
   }

   private static ComponentUri sshUserInfo(ComponentUri uri) {
      return nullable(uri).map(u -> u.component().conditionalUserInfo(SshUri::sshUserInfo)).orElse(null);
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static Authority sshUserInfo(Authority authority) {
      return nullable(authority)
            .map(a -> instanceOf(a, ServerAuthority.class)
                  .map(sa -> (Authority) sa.userInfo(HttpUri::httpUserInfo))
                  .orElse(a))
            .orElse(null);
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static UserInfo sshUserInfo(UserInfo ui) {
      return nullable(ui, UsernamePasswordUserInfo::of);
   }

   @Override
   protected SshUri recreate(URI uri,
                             UriRestrictions restrictions,
                             Scheme scheme,
                             SchemeSpecific schemeSpecific,
                             Authority authority,
                             Path path,
                             Query query,
                             Fragment fragment,
                             boolean newObject) {
      return buildUri(() -> new SshUri(uri,
                                       restrictions,
                                       scheme,
                                       schemeSpecific,
                                       authority,
                                       path,
                                       query,
                                       fragment,
                                       newObject));
   }

   @Override
   public SshUriRestrictions restrictions() {
      return (SshUriRestrictions) super.restrictions();
   }

   public SshUri sftpRestrictions(Function<? super SshUriRestrictions, ? extends SshUriRestrictions> restrictionsMapper) {
      return restrictions(restrictionsMapper.apply(restrictions()));
   }

   /**
    * @implNote path must be normalized separately to preserve double-slashed sftp paths.
    */
   @Override
   public SshUri normalize() {
      var path = this.path;
      return super.normalize().component().path(__ -> path.normalize());
   }

   @Override
   public SshUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public SshUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   public Optional<String> username() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .map(ui -> ui.username(false));
   }

   public Optional<String> password() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .flatMap(ui -> ui.password(false));
   }

   public boolean defaultPort() {
      return portOrDefault() == DEFAULT_PORT;
   }

   public int portOrDefault() {
      return port().orElse(DEFAULT_PORT);
   }

   @Override
   public Component component() {
      return new Component();
   }

   public class Component extends AbstractComponent {

      public Port portOrDefault() {
         return super.port().orElseGet(() -> SimplePort.of(DEFAULT_PORT));
      }

   }

   public static class SshUriRestrictions extends DefaultUriRestrictions {
      protected static final boolean DEFAULT_EMPTY_PATH = false;
      protected static final boolean DEFAULT_QUERY = true;
      protected static final boolean DEFAULT_FRAGMENT = true;

      protected final boolean emptyPath;

      protected SshUriRestrictions(boolean emptyPath,
                                   PortRestriction portRestriction,
                                   BiFunction<String, Integer, Integer> portMapper,
                                   boolean query,
                                   boolean fragment) {
         super(portRestriction, portMapper, query, fragment);
         this.emptyPath = emptyPath;
      }

      public static SshUriRestrictions of(UriRestrictions restrictions) {
         if (restrictions instanceof SshUriRestrictions) {
            var sftpRestrictions = (SshUriRestrictions) restrictions;
            return new SshUriRestrictions(sftpRestrictions.emptyPath,
                                          sftpRestrictions.portRestriction,
                                          sftpRestrictions.portMapper,
                                          sftpRestrictions.query,
                                          sftpRestrictions.fragment);
         } else if (restrictions instanceof DefaultUriRestrictions) {
            var defaultRestrictions = (DefaultUriRestrictions) restrictions;
            return new SshUriRestrictions(DEFAULT_EMPTY_PATH,
                                          defaultRestrictions.portRestriction(),
                                          defaultRestrictions.portMapper(),
                                          defaultRestrictions.query(),
                                          defaultRestrictions.fragment());
         } else {
            return new SshUriRestrictions(DEFAULT_EMPTY_PATH, null, null, DEFAULT_QUERY, DEFAULT_FRAGMENT);
         }
      }

      public static SshUriRestrictions ofRestrictions(boolean emptyPath, boolean query, boolean fragment) {
         return new SshUriRestrictions(emptyPath, null, null, query, fragment);
      }

      public static SshUriRestrictions ofDefault() {
         return ofRestrictions(DEFAULT_EMPTY_PATH, DEFAULT_QUERY, DEFAULT_FRAGMENT);
      }

      public static SshUriRestrictions noRestrictions() {
         return ofRestrictions(false, false, false);
      }

      public static SshUriRestrictions automatic(Path path, Query query, Fragment fragment) {
         return ofRestrictions(!path.isEmpty(),
                               query == null,
                               fragment == null);
      }

      @Override
      final protected SshUriRestrictions recreate(PortRestriction portRestriction,
                                                  BiFunction<String, Integer, Integer> portMapper,
                                                  boolean query,
                                                  boolean fragment) {
         return recreate(emptyPath, portRestriction, portMapper, query, fragment);
      }

      protected SshUriRestrictions recreate(boolean emptyPath,
                                            PortRestriction portRestriction,
                                            BiFunction<String, Integer, Integer> portMapper,
                                            boolean query,
                                            boolean fragment) {
         return new SshUriRestrictions(emptyPath, portRestriction, portMapper, query, fragment);
      }

      /**
       * Returns {@code true} if empty paths are restricted.
       * <p>
       * This restriction is interpreted as a URI invalidity at build time.
       */
      public boolean emptyPath() {
         return emptyPath;
      }

      /**
       * Whether to restrict empty paths.
       * <p>
       * This restriction is interpreted as a URI invalidity at build time.
       */
      public SshUriRestrictions emptyPath(boolean emptyPath) {
         return recreate(emptyPath, portRestriction, portMapper, query, fragment);
      }

      @Override
      public SshUriRestrictions portRestriction(PortRestriction portRestriction) {
         return (SshUriRestrictions) super.portRestriction(portRestriction);
      }

      @Override
      public SshUriRestrictions portMapper(BiFunction<String, Integer, Integer> portMapper) {
         return (SshUriRestrictions) super.portMapper(portMapper);
      }

      @Override
      public SshUriRestrictions portMapper(Function<String, Integer> portMapper) {
         return (SshUriRestrictions) super.portMapper(portMapper);
      }

      @Override
      public SshUriRestrictions portMapper(Integer mappedPort) {
         return (SshUriRestrictions) super.portMapper(mappedPort);
      }

      @Override
      public SshUriRestrictions defaultPortMapper(BiFunction<String, Integer, Integer> portMapper) {
         return (SshUriRestrictions) super.defaultPortMapper(portMapper);
      }

      @Override
      public SshUriRestrictions defaultPortMapper(Function<String, Integer> portMapper) {
         return (SshUriRestrictions) super.defaultPortMapper(portMapper);
      }

      @Override
      public SshUriRestrictions defaultPortMapper(Integer mappedPort) {
         return (SshUriRestrictions) super.defaultPortMapper(mappedPort);
      }

      @Override
      public SshUriRestrictions query(boolean query) {
         return (SshUriRestrictions) super.query(query);
      }

      @Override
      public SshUriRestrictions fragment(boolean fragment) {
         return (SshUriRestrictions) super.fragment(fragment);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SshUriRestrictions.class.getSimpleName() + "[", "]")
               .add("emptyPath=" + emptyPath)
               .add("portRestriction=" + portRestriction)
               .add("query=" + query)
               .add("fragment=" + fragment)
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SshUriRestrictions restrictions = (SshUriRestrictions) o;
         return portRestriction == restrictions.portRestriction
                && query == restrictions.query
                && fragment == restrictions.fragment
                && emptyPath == restrictions.emptyPath;
      }

      @Override
      public int hashCode() {
         return Objects.hash(super.hashCode(), emptyPath);
      }
   }

}
