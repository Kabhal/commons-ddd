/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import java.net.URI;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Function;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.util.PathUtils;

/**
 * URI implementations that supports flexible URI build and part replacement, always preserving original
 * encoding.
 * Each URI part can be specified/updated, as encoded or decoded string. Once encoded, a part encoding is
 * preserved if URI is updated in any way.
 *
 * @implSpec To support {@code scheme:///path} or {@code //?query} URI, server-based authority can be
 *       empty, contrarily to {@link URI}.
 */
public interface ComponentUri extends Uri {

   /**
    * Creates a {@link ComponentUri} from an existing <em>encoded</em> URI.
    *
    * @param uri encoded URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUri(String uri, UriRestrictions restrictions) {
      return DefaultComponentUri.ofUri(uri, restrictions);
   }

   /**
    * Creates a {@link ComponentUri} from an existing <em>encoded</em> URI, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param uri encoded URI
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUri(String uri) {
      return DefaultComponentUri.ofUri(uri);
   }

   /**
    * Creates a {@link ComponentUri} from an existing {@link URI}.
    *
    * @param uri URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUri(URI uri, UriRestrictions restrictions) {
      return DefaultComponentUri.ofUri(uri, restrictions);
   }

   /**
    * Creates a {@link ComponentUri} from an existing {@link URI}, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param uri URI
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUri(URI uri) {
      return DefaultComponentUri.ofUri(uri);
   }

   /**
    * Creates a {@link ComponentUri} from an existing {@link Uri}.
    * <p>
    * Specified {@code uri}'s {@link Uri#restrictions() restrictions} and {@link Uri#newObject()} are ignored
    * because it does not make sense for this different {@link Uri} implementation.
    *
    * @param uri URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUri(Uri uri, UriRestrictions restrictions) {
      return DefaultComponentUri.ofUri(uri, restrictions);
   }

   /**
    * Creates a {@link ComponentUri} from an existing {@link Uri}, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param uri URI
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUri(Uri uri) {
      return DefaultComponentUri.ofUri(uri);
   }

   /**
    * Creates a new {@link ComponentUri} for a reconstituting object with specified URN.
    *
    * @param urn URN
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUrn(URI urn, UriRestrictions restrictions) {
      return DefaultComponentUri.ofUrn(urn, restrictions);
   }

   /**
    * Creates a new {@link ComponentUri} for a reconstituting object with specified URN, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param urn URN
    *
    * @return new {@link DefaultComponentUri} instance
    */
   static ComponentUri ofUrn(URI urn) {
      return DefaultComponentUri.ofUrn(urn);
   }

   Component component();

   interface Component {
      Optional<Scheme> scheme();

      Optional<SchemeSpecific> schemeSpecific();

      Optional<Authority> authority();

      Optional<ServerAuthority> serverAuthority();

      Optional<RegistryAuthority> registryAuthority();

      Optional<UserInfo> userInfo();

      Optional<Host> host();

      Optional<Port> port();

      Optional<Path> path();

      Optional<Query> query();

      Optional<Fragment> fragment();

      /**
       * Updates scheme with specified value if {@link Uri} is {@link #isAbsolute() absolute}.
       *
       * @param schemeMapper scheme function of current scheme, current scheme is never {@code null},
       *       function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not absolute
       * @see #conditionalScheme(Function)
       */
      ComponentUri scheme(Function<? super Scheme, ? extends Scheme> schemeMapper);

      /**
       * Updates scheme with specified value only if {@link Uri} is {@link #isAbsolute() absolute}, otherwise
       * does nothing.
       *
       * @param schemeMapper scheme function of current scheme, current scheme is never {@code null},
       *       function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #scheme(Function)
       */
      ComponentUri conditionalScheme(Function<? super Scheme, ? extends Scheme> schemeMapper);

      /**
       * Updates scheme-specific with specified value if {@link Uri} is {@link #isOpaque() opaque}.
       *
       * @param schemeSpecificMapper scheme-specific function of current scheme-specific,
       *       scheme-specific is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not opaque
       * @see #conditionalSchemeSpecific(Function)
       */
      ComponentUri schemeSpecific(Function<? super SchemeSpecific, ? extends SchemeSpecific> schemeSpecificMapper);

      /**
       * Updates scheme with specified value only if {@link Uri} is {@link #isOpaque() opaque}, otherwise does
       * nothing.
       *
       * @param schemeSpecificMapper scheme-specific function of current scheme-specific,
       *       scheme-specific is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #schemeSpecific(Function)
       */
      ComponentUri conditionalSchemeSpecific(Function<? super SchemeSpecific, ? extends SchemeSpecific> schemeSpecificMapper);

      /**
       * Updates server-based authority with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based}.
       *
       * @param authorityMapper server-based authority function of current server authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based
       * @see #conditionalServerAuthority(Function)
       */
      ComponentUri serverAuthority(Function<? super ServerAuthority, ? extends ServerAuthority> authorityMapper);

      /**
       * Updates server-based authority with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param authorityMapper server-based authority function of current server authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #serverAuthority(Function)
       */
      ComponentUri conditionalServerAuthority(Function<? super ServerAuthority, ? extends ServerAuthority> authorityMapper);

      /**
       * Updates registry-based authority with specified value if {@link Uri} is
       * {@link #isHierarchicalRegistry() hierarchical registry-based}.
       *
       * @param authorityMapper registry-based authority function of current registry authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical registry-based
       * @see #conditionalRegistryAuthority(Function)
       */
      ComponentUri registryAuthority(Function<? super RegistryAuthority, ? extends RegistryAuthority> authorityMapper);

      /**
       * Updates registry-based authority with specified value only if {@link Uri} is
       * {@link #isHierarchicalRegistry() hierarchical registry-based}, otherwise does
       * nothing.
       *
       * @param authorityMapper registry-based authority function of current registry authority, current
       *       authority is never {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @see #registryAuthority(Function)
       */
      ComponentUri conditionalRegistryAuthority(Function<? super RegistryAuthority, ? extends RegistryAuthority> authorityMapper);

      /**
       * Updates user-info with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
       *
       * @param userInfoMapper user-info function of current user-info, current user-info can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
       *       hierarchical server-based
       * @see #conditionalUserInfo(Function)
       */
      ComponentUri userInfo(Function<? super UserInfo, ? extends UserInfo> userInfoMapper);

      /**
       * Updates user-info with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param userInfoMapper user-info function of current user-info, current user-info can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #userInfo(Function)
       */
      ComponentUri conditionalUserInfo(Function<? super UserInfo, ? extends UserInfo> userInfoMapper);

      /**
       * Updates host with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
       *
       * @param hostMapper host function of current host, current host can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
       *       hierarchical server-based
       * @see #conditionalHost(Function)
       */
      ComponentUri host(Function<? super Host, ? extends Host> hostMapper);

      /**
       * Updates host with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param hostMapper host function of current host, current host can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #host(Function)
       */
      ComponentUri conditionalHost(Function<? super Host, ? extends Host> hostMapper);

      /**
       * Updates port with specified value if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}.
       *
       * @param portMapper port function of current port, current port can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical server-based or is empty
       *       hierarchical server-based
       * @see #conditionalPort(Function)
       */
      ComponentUri port(Function<? super Port, ? extends Port> portMapper);

      /**
       * Updates port with specified value only if {@link Uri} is
       * {@link #isHierarchicalServer() hierarchical server-based} but not
       * {@link #isEmptyHierarchicalServer() empty hierarchical server-based}, otherwise does
       * nothing.
       *
       * @param portMapper port function of current port, current port can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #port(Function)
       */
      ComponentUri conditionalPort(Function<? super Port, ? extends Port> portMapper);

      /**
       * Updates path with specified value if {@link Uri} is {@link #isHierarchical() hierarchical}.
       *
       * @param pathMapper port function of current path, current path is never
       *       {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical, or path does not comply with
       *       rules
       * @see #conditionalPath(Function)
       */
      ComponentUri path(Function<? super Path, ? extends Path> pathMapper);

      /**
       * Updates path with specified value only if {@link Uri} is {@link #isHierarchical() hierarchical},
       * otherwise does nothing.
       *
       * @param pathMapper port function of current path, current path is never
       *       {@code null}, function result must not be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if path does not comply with rules
       * @see #path(Function)
       */
      ComponentUri conditionalPath(Function<? super Path, ? extends Path> pathMapper);

      /**
       * Updates query with specified value if {@link Uri} is {@link #isHierarchical() hierarchical}.
       *
       * @param queryMapper query function of current query, current query can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @throws InvariantValidationException if URI is not hierarchical
       * @see #conditionalQuery(Function)
       */
      ComponentUri query(Function<? super Query, ? extends Query> queryMapper);

      /**
       * Updates query with specified value only if {@link Uri} is {@link #isHierarchical() hierarchical},
       * otherwise does nothing.
       *
       * @param queryMapper query function of current query, current query can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #query(Function)
       */
      ComponentUri conditionalQuery(Function<? super Query, ? extends Query> queryMapper);

      /**
       * Updates fragment with specified value.
       *
       * @param fragmentMapper fragment function of current fragment, current fragment can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @see #conditionalFragment(Function)
       */
      ComponentUri fragment(Function<? super Fragment, ? extends Fragment> fragmentMapper);

      /**
       * Updates fragment with specified value.
       *
       * @param fragmentMapper fragment function of current fragment, current fragment can be
       *       {@code null}, function result can be {@code null}
       *
       * @return new instance
       *
       * @implSpec This is an alias for {@link #fragment(Function)} because updating fragment
       *       is always possible whatever the URI format.
       * @see #fragment(Function)
       */
      ComponentUri conditionalFragment(Function<? super Fragment, ? extends Fragment> fragmentMapper);
   }

   interface Part {

      Optional<? extends Part> recreate(URI uri);

      String value();

      default String encodedValue() {
         return value();
      }

      StringBuilder appendEncodedValue(StringBuilder builder);

   }

   interface PartFactory<P extends Part> extends Function<URI, Optional<? extends P>> {}

   interface Scheme extends Part {

      @Override
      Optional<? extends Scheme> recreate(URI uri);

   }

   interface SchemeSpecific extends Part {

      @Override
      Optional<? extends SchemeSpecific> recreate(URI uri);

      SchemeSpecific value(String value);

      SchemeSpecific encodedValue(String encodedValue);

   }

   interface UserInfo extends Part {

      @Override
      Optional<? extends UserInfo> recreate(URI uri);

   }

   interface Host extends Part {

      @Override
      Optional<? extends Host> recreate(URI uri);

   }

   interface Port extends Part {

      int port();

      @Override
      Optional<? extends Port> recreate(URI uri);

   }

   interface Authority extends Part {

      @Override
      Optional<? extends Authority> recreate(URI uri);

   }

   /**
    * @implSpec host can be {@code null} if server-based authority is empty, e.g.:
    *       {@code scheme:///path}.
    */
   interface ServerAuthority extends Authority {

      Optional<UserInfo> userInfo();

      @Override
      Optional<? extends ServerAuthority> recreate(URI uri);

      /**
       * Updates user-info.
       *
       * @param userInfoMapper user-info function of current user-info, current user-info can be
       *       {@code null}, function result can be {@code null}
       */
      ServerAuthority userInfo(Function<? super UserInfo, ? extends UserInfo> userInfoMapper);

      Optional<Host> host();

      /**
       * Updates host.
       *
       * @param hostMapper host function of current host, current host can be
       *       {@code null}, function result can be {@code null}
       */
      ServerAuthority host(Function<? super Host, ? extends Host> hostMapper);

      Optional<Port> port();

      /**
       * Updates port.
       *
       * @param portMapper port function of current port, current port can be
       *       {@code null}, function result can be {@code null}
       */
      ServerAuthority port(Function<? super Port, ? extends Port> portMapper);

   }

   interface RegistryAuthority extends Authority {

      @Override
      Optional<? extends RegistryAuthority> recreate(URI uri);
   }

   interface Path extends Part {

      @Override
      Optional<? extends Path> recreate(URI uri);

      default java.nio.file.Path pathValue() {
         return Paths.get(value());
      }

      default boolean isAbsolute() {
         return pathValue().isAbsolute();
      }

      default boolean isRoot() {
         return PathUtils.isRoot(pathValue());
      }

      default boolean isEmpty() {
         return PathUtils.isEmpty(pathValue());
      }

      Path resolve(java.nio.file.Path path);

      Path resolve(Path path);

      Path value(String value);

      Path encodedValue(String encodedPath);

      Path normalize();
   }

   interface Query extends Part {

      @Override
      Optional<? extends Query> recreate(URI uri);

      boolean isEmpty();

      boolean noQueryIfEmpty();

      default boolean isPresent() {
         return !(isEmpty() && noQueryIfEmpty());
      }

   }

   interface Fragment extends Part {

      @Override
      Optional<? extends Fragment> recreate(URI uri);
   }

}
