/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.tinubu.commons.ddd2.uri.ComponentUri.Fragment;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;

/**
 * {@link Uri} restrictions base class.
 * <p>
 * Each feature is set to {@code true} if feature is restricted for a given {@link Uri}.
 */
public class DefaultUriRestrictions implements UriRestrictions {

   protected static final BiFunction<String, Integer, Integer> NOOP_PORT_MAPPER = (scheme, port) -> null;
   protected static final PortRestriction DEFAULT_PORT_RESTRICTION = PortRestriction.NONE;

   protected final PortRestriction portRestriction;
   protected final BiFunction<String, Integer, Integer> portMapper;
   protected final boolean query;
   protected final boolean fragment;

   protected DefaultUriRestrictions(PortRestriction portRestriction,
                                    BiFunction<String, Integer, Integer> portMapper,
                                    boolean query,
                                    boolean fragment) {
      this.portRestriction = nullable(portRestriction, DEFAULT_PORT_RESTRICTION);
      this.portMapper = nullable(portMapper, NOOP_PORT_MAPPER);
      this.query = query;
      this.fragment = fragment;
   }

   protected DefaultUriRestrictions(boolean query, boolean fragment) {
      this(null, null, query, fragment);
   }

   public static DefaultUriRestrictions ofRestrictions(boolean query, boolean fragment) {
      return new DefaultUriRestrictions(null, null, query, fragment);
   }

   public static DefaultUriRestrictions ofDefault() {
      return noRestrictions();
   }

   public static DefaultUriRestrictions noRestrictions() {
      return new DefaultUriRestrictions(false, false);
   }

   public static DefaultUriRestrictions automatic(Query query, Fragment fragment) {
      return new DefaultUriRestrictions(query == null, fragment == null);
   }

   public static DefaultUriRestrictions automatic(String query, String fragment) {
      return new DefaultUriRestrictions(query == null, fragment == null);
   }

   protected DefaultUriRestrictions recreate(PortRestriction portRestriction,
                                             BiFunction<String, Integer, Integer> portMapper,
                                             boolean query,
                                             boolean fragment) {
      return new DefaultUriRestrictions(portRestriction, portMapper, query, fragment);
   }

   /**
    * Returns port restriction.
    */
   public PortRestriction portRestriction() {
      return portRestriction;
   }

   /**
    * Sets port restriction.
    */
   public DefaultUriRestrictions portRestriction(PortRestriction portRestriction) {
      notNull(portRestriction, "portRestriction");

      return recreate(portRestriction, portMapper, query, fragment);
   }

   /**
    * Returns port mapper as a function of (scheme, current port) to mapped port number.
    * Scheme is case-insensitive, function takes an un-normalized scheme value.
    * If {@link Uri} has no scheme, or no current port, function takes {@code null} values.
    * If there's no mapped port for a given scheme, function must return {@code null}, or {@code -1}.
    * <p>
    * Port mapper is used in conjunction to {@link #portRestriction()}, but can have other purpose in a
    * specific {@link Uri} implementation.
    */
   public BiFunction<String, Integer, Integer> portMapper() {
      return portMapper;
   }

   /**
    * Sets port mapper.
    * <p>
    * Port mapper is used in conjunction to {@link #portRestriction()}, but can have other purpose in a
    * specific {@link Uri} implementation.
    *
    * @param portMapper port mapper as a function of (scheme, current port) to mapped port number.
    *       Scheme is case-insensitive, function takes an un-normalized scheme value.
    *       If {@link Uri} has no scheme, or no current port, function takes {@code null} values.
    *       If there's no mapped port for a given scheme, function must return {@code null}, or {@code -1}
    */
   public DefaultUriRestrictions portMapper(BiFunction<String, Integer, Integer> portMapper) {
      notNull(portMapper, "portMapper");

      return recreate(portRestriction, portMapper, query, fragment);
   }

   /**
    * Sets port mapper.
    * <p>
    * Port mapper is used in conjunction to {@link #portRestriction()}, but can have other purpose in a
    * specific {@link Uri} implementation.
    *
    * @param portMapper port mapper as a function of (scheme) to mapped port number.
    *       Scheme is case-insensitive, function takes an un-normalized scheme value.
    *       If {@link Uri} has no scheme, function takes {@code null} value.
    *       If there's no mapped port for a given scheme, function must return {@code null}, or {@code -1}
    */
   public DefaultUriRestrictions portMapper(Function<String, Integer> portMapper) {
      notNull(portMapper, "portMapper");

      return portMapper((scheme, port) -> portMapper.apply(scheme));
   }

   /**
    * Sets mapped port.
    * <p>
    * Port mapper is used in conjunction to {@link #portRestriction()}, but can have other purpose in a
    * specific {@link Uri} implementation.
    *
    * @param mappedPort mapped port, or {@code null} or {@code -1}
    */
   public DefaultUriRestrictions portMapper(Integer mappedPort) {
      return portMapper((scheme, port) -> mappedPort);
   }

   /**
    * Sets default port mapper if no port mapper is already defined.
    *
    * @param portMapper port mapper as a function of (scheme, current port) to mapped port number.
    *       Scheme is case-insensitive, function takes an un-normalized scheme value.
    *       If {@link Uri} has no scheme, or no current port, function takes {@code null} values.
    *       If there's no mapped port for a given scheme, function must return {@code null}, or {@code -1}
    */
   public DefaultUriRestrictions defaultPortMapper(BiFunction<String, Integer, Integer> portMapper) {
      notNull(portMapper, "portMapper");

      if (this.portMapper == null || this.portMapper == NOOP_PORT_MAPPER) {
         return portMapper(portMapper);
      } else {
         return this;
      }
   }

   /**
    * Sets default port mapper if no port mapper is already defined.
    *
    * @param portMapper port mapper as a function of (scheme) to mapped port number.
    *       Scheme is case-insensitive, function takes an un-normalized scheme value.
    *       If {@link Uri} has no scheme, function takes {@code null} value.
    *       If there's no mapped port for a given scheme, function must return {@code null}, or {@code -1}
    */
   public DefaultUriRestrictions defaultPortMapper(Function<String, Integer> portMapper) {
      notNull(portMapper, "portMapper");

      return defaultPortMapper((scheme, port) -> portMapper.apply(scheme));
   }

   /**
    * Sets default mapped port if no port mapper is already defined.
    *
    * @param mappedPort mapped port, or {@code null} or {@code -1}
    */
   public DefaultUriRestrictions defaultPortMapper(Integer mappedPort) {
      return defaultPortMapper((scheme, port) -> mappedPort);
   }

   /**
    * Returns {@code true} if query part is restricted.
    * <p>
    * This restriction is interpreted as a URI invalidity at build time.
    */
   public boolean query() {
      return query;
   }

   /**
    * Whether to restrict query part.
    * <p>
    * This restriction is interpreted as a URI invalidity at build time.
    */
   public DefaultUriRestrictions query(boolean query) {
      return recreate(portRestriction, portMapper, query, fragment);
   }

   /**
    * Returns {@code true} if fragment part is restricted.
    * <p>
    * This restriction is interpreted as a URI invalidity at build time.
    */
   public boolean fragment() {
      return fragment;
   }

   /**
    * Whether to restrict fragment part.
    * <p>
    * This restriction is interpreted as a URI invalidity at build time.
    */
   public DefaultUriRestrictions fragment(boolean fragment) {
      return recreate(portRestriction, portMapper, query, fragment);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DefaultUriRestrictions.class.getSimpleName() + "[", "]")
            .add("portRestriction=" + portRestriction)
            .add("query=" + query)
            .add("fragment=" + fragment)
            .toString();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      DefaultUriRestrictions that = (DefaultUriRestrictions) o;
      return portRestriction == that.portRestriction && query == that.query && fragment == that.fragment;
   }

   @Override
   public int hashCode() {
      return Objects.hash(portRestriction, query, fragment);
   }

   public enum PortRestriction {
      /**
       * No restriction, defined port remains as-is, or absent if not set.
       */
      NONE,
      /**
       * Always removes port, when supported.
       */
      REMOVE_PORT,
      /**
       * If port matches {@link DefaultUriRestrictions#portMapper() mapped} port, then remove it, when
       * supported.
       */
      REMOVE_MAPPED_PORT,
      /**
       * If port is missing, explicitly set {@link DefaultUriRestrictions#portMapper() mapped} port, when
       * supported.
       */
      FORCE_MAPPED_PORT
   }
}
