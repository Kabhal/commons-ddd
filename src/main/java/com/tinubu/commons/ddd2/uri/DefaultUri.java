/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.Validate.notBlank;
import static com.tinubu.commons.ddd2.invariant.Validate.notNull;
import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.anySatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotStartWith;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeFragment;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodePath;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeQuery;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.function.Function.identity;

import java.net.URI;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.Validate.Check;

/**
 * URI implementations that supports flexible URI build and part replacement, always preserving original
 * encoding.
 * Each URI part can be specified/updated, as encoded or decoded string. Once encoded, a part encoding is
 * preserved if URI is updated in any way.
 *
 * @implSpec To support {@code scheme:///path} or {@code //?query} URI, server-based authority can be
 *       empty, contrarily to {@link URI}.
 */
public class DefaultUri extends AbstractUri<DefaultUri> {

   /**
    * Main and re-creation constructor.
    *
    * @param uri URI
    * @param restrictions URI restrictions
    * @param newObject whether {@link Uri} is a new object
    */
   protected DefaultUri(URI uri, UriRestrictions restrictions, boolean newObject) {
      super(uri, restrictions, newObject);
   }

   /**
    * Creates a {@link DefaultUri} from an existing <em>encoded</em> URI.
    *
    * @param uri encoded URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultUri} instance
    */
   public static DefaultUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   /**
    * Creates a {@link DefaultUri} from an existing <em>encoded</em> URI, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param uri encoded URI
    *
    * @return new {@link DefaultUri} instance
    */
   public static DefaultUri ofUri(String uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a {@link DefaultUri} from an existing {@link URI}.
    *
    * @param uri URI
    * @param restrictions URI restrictions
    *
    * @return new {@link DefaultUri} instance
    */
   public static DefaultUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new DefaultUri(uri, restrictions, false));
   }

   /**
    * Creates a {@link DefaultUri} from an existing {@link URI} using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions.
    *
    * @param uri URI
    *
    * @return new {@link DefaultUri} instance
    */
   public static DefaultUri ofUri(URI uri) {
      return ofUri(uri, DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a new {@link DefaultUri} for a reconstituting object with specified URN.
    *
    * @param urn URN
    * @param restrictions URI restrictions
    *
    * @return new id
    */
   public static DefaultUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   /**
    * Creates a new {@link DefaultUri} for a reconstituting object with specified URN, using
    * {@link DefaultUriRestrictions#ofDefault() default} restrictions
    *
    * @param urn URN
    *
    * @return new id
    */
   public static DefaultUri ofUrn(URI urn) {
      return ofUri(parseUrn(urn), DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a (<em>absolute</em>) </em><em>hierarchical</em> Uri, without <em>authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    *
    * @implSpec Path must not be {@code null} by design. An empty path must be set if URI has
    *       no path, so that path value is symmetrical in read and write.
    */
   public static DefaultUri hierarchical(boolean encoded,
                                         String scheme,
                                         String path,
                                         String query,
                                         String fragment) {
      notBlank(scheme, "scheme")
            .and(validate(path,
                          "path",
                          satisfies(p -> p.startsWith("/"), "'%s' must be absolute", validatingObject())))
            .orThrow();

      return DefaultUri.ofUri(uri(encoded, scheme, null, null, path, query, fragment),
                              DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, without <em>authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri hierarchical(boolean encoded, String scheme, String path, String query) {
      return hierarchical(encoded, scheme, path, query, null);
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, without <em>authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param path absolute or empty path
    *
    * @return new instance
    */
   public static DefaultUri hierarchical(boolean encoded, String scheme, String path) {
      return hierarchical(encoded, scheme, path, null, null);
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>server-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param authority server-based authority, can be empty
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    *
    * @implSpec Path must not be {@code null} by design. An empty path must be set if URI has
    *       no path, so that path value is symmetrical in read and write.
    */
   public static DefaultUri hierarchicalServer(boolean encoded,
                                               String scheme,
                                               String authority,
                                               String path,
                                               String query,
                                               String fragment) {

      InvariantRule<String> pathRule;

      if (authority == null) {
         pathRule = satisfies(p -> p.startsWith("/"), "'%s' must be absolute", validatingObject());
      } else {
         pathRule = satisfies(p -> p.startsWith("/") || p.isEmpty(),
                              "'%s' must be absolute or empty",
                              validatingObject());
      }
      notBlank(scheme, "scheme")
            .and(notNull(authority, "authority"))
            .and(validate(path, "path", pathRule))
            .orThrow();

      return DefaultUri.ofUri(uri(encoded, scheme, null, authority, path, query, fragment),
                              DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>server-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param authority server-based authority, can be empty
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri hierarchicalServer(boolean encoded,
                                               String scheme,
                                               String authority,
                                               String path,
                                               String query) {
      return hierarchicalServer(encoded, scheme, authority, path, query, null);
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>server-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param authority server-based authority, can be empty
    * @param path absolute or empty path
    *
    * @return new instance
    */
   public static DefaultUri hierarchicalServer(boolean encoded,
                                               String scheme,
                                               String authority,
                                               String path) {
      return hierarchicalServer(encoded, scheme, authority, path, null, null);
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>server-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param userInfo optional user-info, can be empty
    * @param host host
    * @param port optional port, or {@code -1}
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri hierarchicalServer(boolean encoded,
                                               String scheme,
                                               String userInfo,
                                               String host,
                                               int port,
                                               String path,
                                               String query,
                                               String fragment) {
      Check.notBlank(host, "host");

      return hierarchicalServer(true,
                                scheme,
                                uriServerAuthority(encoded, userInfo, host, port),
                                encoded ? path : encodePath(path).orElseThrow(identity()),
                                encoded ? query : encodeQuery(query).orElseThrow(identity()),
                                encoded ? fragment : encodeFragment(fragment).orElseThrow(identity()));
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>server-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param userInfo optional user-info, can be empty
    * @param host host
    * @param port optional port, or {@code -1}
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri hierarchicalServer(boolean encoded,
                                               String scheme,
                                               String userInfo,
                                               String host,
                                               int port,
                                               String path,
                                               String query) {
      return hierarchicalServer(encoded, scheme, userInfo, host, port, path, query, null);
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>server-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param userInfo optional user-info, can be empty
    * @param host host
    * @param port optional port, or {@code -1}
    * @param path absolute or empty path
    *
    * @return new instance
    */
   public static DefaultUri hierarchicalServer(boolean encoded,
                                               String scheme,
                                               String userInfo,
                                               String host,
                                               int port,
                                               String path) {
      return hierarchicalServer(encoded, scheme, userInfo, host, port, path, null, null);
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>registry-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param authority registry-based authority
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    *
    * @implSpec Path must not be {@code null} by design. An empty path must be set if URI has
    *       no path, so that path value is symmetrical in read and write.
    */
   public static DefaultUri hierarchicalRegistry(boolean encoded,
                                                 String scheme,
                                                 String authority,
                                                 String path,
                                                 String query,
                                                 String fragment) {
      notBlank(scheme, "scheme")
            .and(notNull(authority, "authority"))
            .and(validate(path,
                          "path",
                          satisfies(p -> p.startsWith("/") || p.isEmpty(),
                                    "'%s' must be absolute or empty",
                                    validatingObject())))
            .orThrow();

      return DefaultUri.ofUri(uri(encoded, scheme, null, authority, path, query, fragment),
                              DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>registry-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param authority registry-based authority
    * @param path absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri hierarchicalRegistry(boolean encoded,
                                                 String scheme,
                                                 String authority,
                                                 String path,
                                                 String query) {
      return hierarchicalRegistry(encoded, scheme, authority, path, query, null);
   }

   /**
    * Creates a (<em>absolute</em>) <em>hierarchical</em> Uri, with <em>registry-based authority</em>.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param authority registry-based authority
    * @param path absolute or empty path
    *
    * @return new instance
    */
   public static DefaultUri hierarchicalRegistry(boolean encoded,
                                                 String scheme,
                                                 String authority,
                                                 String path) {
      return hierarchicalRegistry(encoded, scheme, authority, path, null, null);
   }

   /**
    * Creates an (<em>absolute</em>) <em>opaque</em> Uri.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param schemeSpecific scheme-specific part, does not start with {@code /}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri opaque(boolean encoded, String scheme, String schemeSpecific, String fragment) {
      notBlank(scheme, "scheme")
            .and(validate(schemeSpecific,
                          "schemeSpecific",
                          isNotBlank().andValue(doesNotStartWith(ParameterValue.value("/")))))
            .orThrow();

      return DefaultUri.ofUri(uri(encoded, scheme, schemeSpecific, null, null, null, fragment),
                              DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates an (<em>absolute</em>) <em>opaque</em> Uri.
    *
    * @param encoded whether specified parts are already encoded
    * @param scheme scheme
    * @param schemeSpecific scheme-specific part, does not start with {@code /}
    *
    * @return new instance
    */
   public static DefaultUri opaque(boolean encoded, String scheme, String schemeSpecific) {
      return opaque(encoded, scheme, schemeSpecific, null);
   }

   /**
    * Creates a (<em>hierarchical</em>) <em>relative</em> Uri.
    *
    * @param encoded whether specified parts are already encoded
    * @param authority optional authority, or {@code null}
    * @param path relative (if authority is not set), absolute or empty path
    * @param query optional query, or {@code null}
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    *
    * @implSpec Path must not be {@code null} by design. An empty path must be set if URI has
    *       no path, so that path value is symmetrical in read and write.
    */
   public static DefaultUri relative(boolean encoded,
                                     String authority,
                                     String path,
                                     String query,
                                     String fragment) {
      validate(list(authority, path, query, fragment),
               anySatisfies(isNotNull(), "At least one part must be set"))
            .and(validate(path,
                          "path",
                          isNotNull().andValue(satisfies((String p) -> p.startsWith("/") || p.isEmpty(),
                                                         "'%s' must be absolute or empty",
                                                         validatingObject()).ifNonNull(authority))))
            .orThrow();

      return DefaultUri.ofUri(uri(encoded, null, null, authority, path, query, fragment),
                              DefaultUriRestrictions.ofDefault());
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative authority and path.
    *
    * @param encoded whether specified parts are already encoded
    * @param authority optional authority, or {@code null}
    * @param path relative (if authority is not set), absolute or empty path
    *
    * @return new instance
    */
   public static DefaultUri relativeAuthority(boolean encoded, String authority, String path) {
      return relative(encoded, authority, path, null, null);
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative path.
    *
    * @param encoded whether specified parts are already encoded
    * @param path relative, absolute or empty path
    *
    * @return new instance
    */
   public static DefaultUri relativePath(boolean encoded, String path) {
      return relative(encoded, null, path, null, null);
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative query.
    *
    * @param encoded whether specified parts are already encoded
    * @param path relative, absolute or empty path
    * @param query optional query, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri relativeQuery(boolean encoded, String path, String query) {
      return relative(encoded, null, path, query, null);
   }

   /**
    * Creates a (<em>hierarchical</em>)<em>relative</em> Uri with only relative fragment.
    *
    * @param encoded whether specified parts are already encoded
    * @param fragment optional fragment, or {@code null}
    *
    * @return new instance
    */
   public static DefaultUri relativeFragment(boolean encoded, String fragment) {
      return relative(encoded, null, null, null, fragment);
   }

   @Override
   protected DefaultUri recreate(URI uri, UriRestrictions restrictions, boolean newObject) {
      return buildUri(() -> new DefaultUri(uri, restrictions, newObject));
   }

}
