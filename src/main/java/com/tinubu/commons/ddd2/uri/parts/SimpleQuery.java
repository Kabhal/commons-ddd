/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.uri.UriEncoder.decode;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeQuery;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;

public class SimpleQuery implements Query {
      protected static final boolean DEFAULT_NO_QUERY_IF_EMPTY = false;

      protected final String query;
      protected final String encodedQuery;
      protected final boolean noQueryIfEmpty;

      protected SimpleQuery(String query, String encodedQuery, boolean noQueryIfEmpty) {
         this.query = Check.notNull(query, "query");
         this.encodedQuery = Check.notNull(encodedQuery, "encodedQuery");
         this.noQueryIfEmpty = noQueryIfEmpty;
      }

      protected SimpleQuery(String query, String encodedQuery) {
         this(query, encodedQuery, DEFAULT_NO_QUERY_IF_EMPTY);
      }

      public static SimpleQuery of(String query) {
         Check.notNull(query, "query");

         return new SimpleQuery(query, encodeQuery(query).orElseThrow(identity()));
      }

      public static SimpleQuery ofEncoded(String encodedQuery) {
         Check.notNull(encodedQuery, "encodedQuery");

         return new SimpleQuery(decode(encodedQuery).orElseThrow(identity()), encodedQuery);
      }

      public static Optional<? extends SimpleQuery> of(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getQuery()).map(__ -> new SimpleQuery(uri.getQuery(), uri.getRawQuery()));
      }

      public static SimpleQuery of(Query query) {
         Check.notNull(query, "query");

         return new SimpleQuery(query.value(), query.encodedValue());
      }

      /**
       * Creates an empty {@link SimpleQuery} .
       *
       * @return new instance
       */
      public static SimpleQuery ofEmpty() {
         return new SimpleQuery("", "");
      }

      @Override
      public Optional<? extends SimpleQuery> recreate(URI uri) {
         Check.notNull(uri, "uri");

         if (noQueryIfEmpty && isEmpty() && uri.getQuery() == null) {
            return optional(this);
         } else {
            return nullable(uri.getQuery()).map(__ -> recreate(uri.getQuery(),
                                                               uri.getRawQuery(),
                                                               noQueryIfEmpty));
         }
      }

      protected SimpleQuery recreate(String query, String encodedQuery, boolean noQueryIfEmpty) {
         return new SimpleQuery(query, encodedQuery, noQueryIfEmpty);
      }

      @Override
      public boolean isEmpty() {
         return query.isEmpty();
      }

      @Override
      public boolean noQueryIfEmpty() {
         return noQueryIfEmpty;
      }

      public SimpleQuery noQueryIfEmpty(boolean noQueryIfEmpty) {
         return recreate(query, encodedQuery, noQueryIfEmpty);
      }

      @Override
      public String value() {
         return query;
      }

      @Override
      public String encodedValue() {
         return encodedQuery;
      }

      @Override
      public StringBuilder appendEncodedValue(StringBuilder builder) {
         if (noQueryIfEmpty && isEmpty()) {
            return builder;
         } else {
            return builder.append("?").append(encodedQuery);
         }
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SimpleQuery that = (SimpleQuery) o;
         return Objects.equals(query, that.query) && Objects.equals(encodedQuery, that.encodedQuery);
      }

      @Override
      public int hashCode() {
         return Objects.hash(query, encodedQuery);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SimpleQuery.class.getSimpleName() + "[", "]")
               .add("query=" + query)
               .add("encodedQuery=" + encodedQuery)
               .toString();
      }
   }
