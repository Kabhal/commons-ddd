/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.uri.UriEncoder.decode;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encodeFragment;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.function.Function.identity;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Fragment;

public class SimpleFragment implements Fragment {
      protected final String fragment;
      protected final String encodedFragment;

      protected SimpleFragment(String fragment, String encodedFragment) {
         this.fragment = Check.notNull(fragment, "fragment");
         this.encodedFragment = Check.notNull(encodedFragment, "encodedFragment");
      }

      public static SimpleFragment of(String fragment) {
         Check.notNull(fragment, "fragment");

         return new SimpleFragment(fragment, encodeFragment(fragment).orElseThrow(identity()));
      }

      public static SimpleFragment ofEncoded(String encodedFragment) {
         Check.notNull(encodedFragment, "encodedFragment");

         return new SimpleFragment(decode(encodedFragment).orElseThrow(identity()), encodedFragment);
      }

      public static Optional<? extends SimpleFragment> of(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getFragment()).map(__ -> new SimpleFragment(uri.getFragment(),
                                                                         uri.getRawFragment()));
      }

      public static SimpleFragment of(Fragment fragment) {
         Check.notNull(fragment, "fragment");

         return new SimpleFragment(fragment.value(), fragment.encodedValue());
      }

      @Override
      public Optional<? extends SimpleFragment> recreate(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getFragment()).map(__ -> recreate(uri.getFragment(), uri.getRawFragment()));
      }

      protected SimpleFragment recreate(String fragment, String encodedFragment) {
         return new SimpleFragment(fragment, encodedFragment);
      }

      @Override
      public String value() {
         return fragment;
      }

      @Override
      public String encodedValue() {
         return encodedFragment;
      }

      @Override
      public StringBuilder appendEncodedValue(StringBuilder builder) {
         return builder.append("#").append(encodedFragment);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SimpleFragment that = (SimpleFragment) o;
         return Objects.equals(fragment, that.fragment) && Objects.equals(encodedFragment,
                                                                          that.encodedFragment);
      }

      @Override
      public int hashCode() {
         return Objects.hash(fragment, encodedFragment);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SimpleFragment.class.getSimpleName() + "[", "]")
               .add("fragment=" + fragment)
               .add("encodedFragment=" + encodedFragment)
               .toString();
      }
   }
