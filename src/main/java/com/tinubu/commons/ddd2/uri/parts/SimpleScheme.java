/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Scheme;

public  class SimpleScheme implements Scheme {

      protected final String scheme;

      protected SimpleScheme(String scheme) {
         this.scheme = Check.notBlank(scheme, "scheme");
      }

      public static SimpleScheme of(String scheme) {
         return new SimpleScheme(scheme);
      }

      public static Optional<? extends SimpleScheme> of(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getScheme()).map(SimpleScheme::new);
      }

      public static SimpleScheme of(Scheme scheme) {
         Check.notNull(scheme, "scheme");

         return new SimpleScheme(scheme.value());
      }

      @Override
      public Optional<? extends SimpleScheme> recreate(URI uri) {
         Check.notNull(uri, "uri");

         return nullable(uri.getScheme()).map(SimpleScheme::new);
      }

      @Override
      public String value() {
         return scheme;
      }

      @Override
      public StringBuilder appendEncodedValue(StringBuilder builder) {
         return builder.append(scheme).append(':');
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SimpleScheme that = (SimpleScheme) o;
         return Objects.equals(scheme, that.scheme);
      }

      @Override
      public int hashCode() {
         return Objects.hash(scheme);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", SimpleScheme.class.getSimpleName() + "[", "]")
               .add("scheme='" + scheme + "'")
               .toString();
      }
   }
