/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.Try.ofThrownBy;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Host;
import com.tinubu.commons.ddd2.uri.ComponentUri.PartFactory;
import com.tinubu.commons.ddd2.uri.ComponentUri.Port;
import com.tinubu.commons.ddd2.uri.ComponentUri.ServerAuthority;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;

/**
 * @implSpec To support {@code scheme:///path} or {@code //?query} URI, server-based authority can be
 *       empty, contrarily to {@link URI} implementation.
 */
public class SimpleServerAuthority implements ServerAuthority {
   protected final UserInfo userInfo;
   protected final Host host;
   protected final Port port;

   protected SimpleServerAuthority(UserInfo userInfo, Host host, Port port) {
      this.userInfo = userInfo;
      this.host =
            Check.validate(host, "host", isNotNull().ifIsSatisfied(() -> userInfo != null || port != null));
      this.port = port;
   }

   public static SimpleServerAuthority of(UserInfo userInfo, Host host, Port port) {
      return new SimpleServerAuthority(userInfo, host, port);
   }

   public static SimpleServerAuthority of(UserInfo userInfo, Host host) {
      return new SimpleServerAuthority(userInfo, host, null);
   }

   public static SimpleServerAuthority of(Host host, Port port) {
      return new SimpleServerAuthority(null, host, port);
   }

   public static SimpleServerAuthority of(Host host) {
      return new SimpleServerAuthority(null, host, null);
   }

   public static SimpleServerAuthority ofEmpty() {
      return new SimpleServerAuthority(null, null, null);
   }

   private static SimpleServerAuthority of(String authority,
                                           PartFactory<UserInfo> userInfoFactory,
                                           PartFactory<Host> hostFactory,
                                           PartFactory<Port> portFactory) {
      Check.notNull(authority, "authority");

      if (authority.isEmpty()) {
         return SimpleServerAuthority.ofEmpty();
      }

      URI uri = ofThrownBy(() -> new URI(null,
                                         authority,
                                         null,
                                         null,
                                         null)).orElseThrow(e -> new InvalidUriException(null,
                                                                                         authority,
                                                                                         null,
                                                                                         null,
                                                                                         null,
                                                                                         e));

      return of(uri,
                userInfoFactory,
                hostFactory,
                portFactory).orElseThrow(() -> new InvalidUriException(uri).subMessage("URI has no host"));
   }

   public static SimpleServerAuthority of(String authority) {
      return of(authority, SimpleUserInfo::of, SimpleHost::of, SimplePort::of);
   }

   private static SimpleServerAuthority ofEncoded(String encodedAuthority,
                                                  PartFactory<UserInfo> userInfoFactory,
                                                  PartFactory<Host> hostFactory,
                                                  PartFactory<Port> portFactory) {
      Check.notNull(encodedAuthority, "encodedAuthority");

      if (encodedAuthority.isEmpty()) {
         return SimpleServerAuthority.ofEmpty();
      }

      var encodedUri = "//" + encodedAuthority;

      URI uri =
            ofThrownBy(() -> new URI(encodedUri)).orElseThrow(e -> new InvalidUriException(encodedUri, e));

      return of(uri,
                userInfoFactory,
                hostFactory,
                portFactory).orElseThrow(() -> new InvalidUriException(uri).subMessage("URI has no host"));
   }

   public static SimpleServerAuthority ofEncoded(String encodedAuthority) {
      return ofEncoded(encodedAuthority, SimpleUserInfo::of, SimpleHost::of, SimplePort::of);
   }

   /**
    * @implSpec Empty server-based authority (e.g.: {@code file:///path}) must be detected.
    */
   public static Optional<? extends SimpleServerAuthority> of(URI uri,
                                                              PartFactory<UserInfo> userInfoFactory,
                                                              PartFactory<Host> hostFactory,
                                                              PartFactory<Port> portFactory) {
      Check.notNull(uri, "uri");

      if (Uri.isEmptyHierarchicalServer(uri)) {
         return optional(SimpleServerAuthority.ofEmpty());
      } else {
         return hostFactory
               .apply(uri)
               .map(host -> SimpleServerAuthority.of(userInfoFactory.apply(uri).orElse(null),
                                                     host,
                                                     portFactory.apply(uri).orElse(null)));
      }
   }

   public static Optional<? extends SimpleServerAuthority> of(URI uri) {
      return of(uri, SimpleUserInfo::of, SimpleHost::of, SimplePort::of);
   }

   public static SimpleServerAuthority of(ServerAuthority authority) {
      Check.notNull(authority, "authority");

      return SimpleServerAuthority.of(authority.userInfo().orElse(null),
                                      authority.host().orElse(null),
                                      authority.port().orElse(null));
   }

   @Override
   public Optional<? extends SimpleServerAuthority> recreate(URI uri) {
      return of(uri,
                u -> userInfo()
                      .<Optional<? extends UserInfo>>map(ui -> ui.recreate(u))
                      .orElseGet(() -> SimpleUserInfo.of(u)),
                u -> host()
                      .<Optional<? extends Host>>map(h -> h.recreate(u))
                      .orElseGet(() -> SimpleHost.of(u)),
                u -> port()
                      .<Optional<? extends Port>>map(p -> p.recreate(u))
                      .orElseGet(() -> SimplePort.of(u)));
   }

   @Override
   public String value() {
      var builder = new StringBuilder();

      if (userInfo != null) {
         builder.append(userInfo.value()).append('@');
      }
      if (host != null) {
         builder.append(host.value());
      }
      if (port != null) {
         builder.append(':').append(port.value());
      }

      return builder.toString();
   }

   @Override
   public String encodedValue() {
      var builder = new StringBuilder();

      if (userInfo != null) {
         userInfo.appendEncodedValue(builder);
      }
      if (host != null) {
         host.appendEncodedValue(builder);
      }
      if (port != null) {
         port.appendEncodedValue(builder);
      }

      return builder.toString();
   }

   @Override
   public StringBuilder appendEncodedValue(StringBuilder builder) {
      return builder.append("//").append(encodedValue());
   }

   @Override
   public Optional<UserInfo> userInfo() {
      return nullable(userInfo);
   }

   @Override
   public ServerAuthority userInfo(Function<? super UserInfo, ? extends UserInfo> userInfoMapper) {
      Check.notNull(userInfoMapper, "userInfoMapper");

      return new SimpleServerAuthority(userInfoMapper.apply(userInfo), host, port);
   }

   @Override
   public Optional<Host> host() {
      return nullable(host);
   }

   @Override
   public ServerAuthority host(Function<? super Host, ? extends Host> hostMapper) {
      Check.notNull(hostMapper, "hostMapper");

      return new SimpleServerAuthority(userInfo, hostMapper.apply(host), port);
   }

   @Override
   public Optional<Port> port() {
      return nullable(port);
   }

   @Override
   public ServerAuthority port(Function<? super Port, ? extends Port> portMapper) {
      Check.notNull(portMapper, "portMapper");

      return new SimpleServerAuthority(userInfo, host, portMapper.apply(port));
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimpleServerAuthority that = (SimpleServerAuthority) o;
      return Objects.equals(userInfo, that.userInfo)
             && Objects.equals(host, that.host)
             && Objects.equals(port, that.port);
   }

   @Override
   public int hashCode() {
      return Objects.hash(userInfo, host, port);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SimpleServerAuthority.class.getSimpleName() + "[", "]")
            .add("userInfo='" + userInfo + "'")
            .add("host='" + host + "'")
            .add("port='" + port + "'")
            .toString();
   }
}
