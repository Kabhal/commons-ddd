/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.lang.util.Try.ofThrownBy;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Success;

public class UriEncoder {

   public static Try<String, InvalidUriException> encodeSchemeSpecific(String schemeSpecific) {
      if (schemeSpecific == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI("scheme", schemeSpecific, null))
            .mapFailure(e -> new InvalidUriException("scheme", schemeSpecific, null, e))
            .map(URI::getRawSchemeSpecificPart);
   }

   public static Try<String, InvalidUriException> decodeSchemeSpecific(String schemeSpecific) {
      if (schemeSpecific == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI("scheme:" + schemeSpecific))
            .mapFailure(e -> new InvalidUriException("scheme", schemeSpecific, null, e))
            .map(URI::getSchemeSpecificPart);
   }

   public static Try<String, InvalidUriException> encodeAuthority(String authority) {
      if (authority == null) {
         return Success.of(null);
      } else if (authority.isEmpty()) {
         return Success.of(authority);
      }

      return ofThrownBy(() -> new URI(null, authority, null, null, null))
            .mapFailure(e -> new InvalidUriException(null, authority, null, null, null, e))
            .map(URI::getRawAuthority);
   }

   public static Try<String, InvalidUriException> decodeAuthority(String authority) {
      if (authority == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI("//" + authority))
            .mapFailure(e -> new InvalidUriException(null, authority, null, null, null, e))
            .map(URI::getAuthority);
   }

   public static Try<String, InvalidUriException> encodeUserInfo(String userInfo) {
      if (userInfo == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI(null, userInfo, "host", -1, null, null, null))
            .mapFailure(e -> new InvalidUriException(null, userInfo, "host", -1, null, null, null, e))
            .map(URI::getRawUserInfo);
   }

   public static Try<String, InvalidUriException> decodeUserInfo(String userInfo) {
      if (userInfo == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI("//" + userInfo + "@host"))
            .mapFailure(e -> new InvalidUriException(null, userInfo, "host", -1, null, null, null, e))
            .map(URI::getUserInfo);
   }

   /**
    * @implNote an empty <em>registry</em> is prefixed to absolute paths to support "double-slashed"
    *       paths (e.g.: SFTP), otherwise, double-slashed paths are mistakenly considered as a registry-based
    *       authority.
    */
   public static Try<String, InvalidUriException> encodePath(String path) {
      if (path == null) {
         return Success.of(null);
      }
      var encodingPath = path.startsWith("/") ? "//" + path : path;
      return ofThrownBy(() -> new URI(null, encodingPath, null))
            .mapFailure(e -> new InvalidUriException(null, encodingPath, null, e))
            .map(URI::getRawPath);
   }

   public static Try<String, InvalidUriException> decodePath(String path) {
      if (path == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI(path))
            .mapFailure(e -> new InvalidUriException(null, path, null, e))
            .map(URI::getPath);
   }

   public static Try<String, InvalidUriException> encodeQuery(String query) {
      if (query == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI(null, "?" + query, null))
            .mapFailure(e -> new InvalidUriException(null, "?" + query, null, e))
            .map(URI::getRawQuery);
   }

   public static Try<String, InvalidUriException> decodeQuery(String query) {
      if (query == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI("?" + query))
            .mapFailure(e -> new InvalidUriException(null, "?" + query, null, e))
            .map(URI::getQuery);
   }

   public static Try<String, InvalidUriException> encodeFragment(String fragment) {
      if (fragment == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> new URI(null, null, fragment))
            .mapFailure(e -> new InvalidUriException(null, null, fragment, e))
            .map(URI::getRawFragment);
   }

   public static Try<String, InvalidUriException> decodeFragment(String fragment) {
      if (fragment == null) {
         return Success.of(null);
      }

      return ofThrownBy(() -> new URI("#" + fragment))
            .mapFailure(e -> new InvalidUriException(null, null, fragment, e))
            .map(URI::getFragment);
   }

   public static String encode(String uri) {
      if (uri == null) {
         return null;
      }
      return URLEncoder.encode(uri, UTF_8);
   }

   public static Try<String, InvalidUriException> decode(String uri) {
      if (uri == null) {
         return Success.of(null);
      }
      return ofThrownBy(() -> URLDecoder.decode(uri, UTF_8)).mapFailure(e -> new InvalidUriException(uri, e));
   }
}
