/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.invariant.Validate.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules.isNotEmptyHierarchicalServer;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.rules.ClassRules;
import com.tinubu.commons.ddd2.invariant.rules.domain.ids.UriRules;
import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;
import com.tinubu.commons.ddd2.uri.parts.SimpleAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleSchemeSpecific;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;

/**
 * HTTP/HTTPS {@link Uri}.
 * <p>
 * Supported URI formats :
 * <ul>
 *    <li>HTTP : {@code http://[<user>[:<password>]@]<host>[:<port>]</path>[?<query>][#<fragment>]}</li>
 *    <li>HTTPS : {@code https://[<user>[:<password>]@]<host>[:<port>]</path>[?<query>][#<fragment>]}</li>
 * </ul>
 * {@code path} can be empty.
 * <p>
 * However, supported format depends on {@link HttpUriRestrictions} restrictions. By default,
 * <ul>
 *    <li>{@link HttpUriRestrictions#http() http} : HTTP protocol is supported</li>
 *    <li>{@link HttpUriRestrictions#https() https} : HTTPS protocol is supported</li>
 *    <li>{@link HttpUriRestrictions#portRestriction() portRestriction} : no port restriction</li>
 *    <li>{@link HttpUriRestrictions#portMapper() portMapper} : default HTTP port = {@value DEFAULT_HTTP_PORT} / HTTPS port = {@value DEFAULT_HTTPS_PORT}</li>
 *    <li>{@link HttpUriRestrictions#query() query} : query is supported</li>
 *    <li>{@link HttpUriRestrictions#fragment() fragment} : fragment is supported</li>
 * </ul>
 */
public class HttpUri extends AbstractComponentUri<HttpUri> {

   protected static final String HTTP_SCHEME = "http";
   protected static final String HTTPS_SCHEME = "https";
   protected static final int DEFAULT_HTTP_PORT = 80;
   protected static final int DEFAULT_HTTPS_PORT = 443;

   protected HttpUri(URI uri,
                     UriRestrictions restrictions,
                     Scheme scheme,
                     SchemeSpecific schemeSpecific,
                     Authority authority,
                     Path path,
                     Query query,
                     Fragment fragment,
                     boolean newObject) {
      super(uri,
            httpRestrictions(restrictions),
            scheme,
            schemeSpecific,
            httpUserInfo(authority),
            path,
            query,
            fragment,
            newObject);
   }

   protected HttpUri(UriRestrictions restrictions,
                     Scheme scheme,
                     SchemeSpecific schemeSpecific,
                     Authority authority,
                     Path path,
                     Query query,
                     Fragment fragment) {
      super(httpRestrictions(restrictions),
            scheme,
            schemeSpecific,
            httpUserInfo(authority),
            path,
            query,
            fragment);
   }

   protected HttpUri(URI uri, UriRestrictions restrictions) {
      this(uri,
           restrictions,
           SimpleScheme.of(uri).orElse(null),
           SimpleSchemeSpecific.of(uri).orElse(null),
           SimpleAuthority.of(uri).orElse(null),
           SimplePath.of(uri).orElse(null),
           SimpleQuery.of(uri).orElse(null),
           SimpleFragment.of(uri).orElse(null),
           false);
   }

   protected HttpUri(Uri uri, UriRestrictions restrictions) {
      super(httpUserInfo(nullableInstanceOf(uri, ComponentUri.class, ComponentUri::ofUri)),
            httpRestrictions(restrictions));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends HttpUri> defineDomainFields() {
      return Fields
            .<HttpUri>builder()
            .superFields((Fields<? super HttpUri>) super.defineDomainFields())
            .field("authority",
                   v -> v.authority,
                   isNotNull().andValue(ClassRules.instanceOf(ServerAuthority.class,
                                                              as(ServerAuthority::userInfo,
                                                                 optionalValue(isInstanceOf(ParameterValue.value(
                                                                       UsernamePasswordUserInfo.class)))))))
            .build();
   }

   @Override
   public Invariants domainInvariants() {
      var supportedSchemes = list();
      if (!restrictions().http) {
         supportedSchemes.add(HTTP_SCHEME);
      }
      if (!restrictions().https) {
         supportedSchemes.add(HTTPS_SCHEME);
      }
      var uriCompatibility = Invariant
            .of(() -> this,
                UriRules
                      .isAbsolute()
                      .andValue(UriRules.scheme(as(String::toLowerCase,
                                                   isIn(ParameterValue.value(supportedSchemes))))),
                UriRules.isHierarchicalServer().andValue(isNotEmptyHierarchicalServer()))
            .groups(COMPATIBILITY_GROUP);

      return super.domainInvariants().withInvariants(uriCompatibility);
   }

   public static HttpUri ofUri(URI uri, UriRestrictions restrictions) {
      return buildUri(() -> new HttpUri(uri, restrictions));
   }

   public static HttpUri ofUri(URI uri) {
      return ofUri(uri, HttpUriRestrictions.ofDefault());
   }

   public static HttpUri ofUri(Uri uri, UriRestrictions restrictions) {
      return buildUri(() -> new HttpUri(uri, restrictions));
   }

   public static HttpUri ofUri(Uri uri) {
      return ofUri(uri, HttpUriRestrictions.ofDefault());
   }

   public static HttpUri ofUri(String uri, UriRestrictions restrictions) {
      return ofUri(Uri.uri(uri), restrictions);
   }

   public static HttpUri ofUri(String uri) {
      return ofUri(uri, HttpUriRestrictions.ofDefault());
   }

   public static HttpUri ofUrn(URI urn, UriRestrictions restrictions) {
      return ofUri(parseUrn(urn), restrictions);
   }

   public static HttpUri ofUrn(URI urn) {
      return ofUrn(urn, HttpUriRestrictions.ofDefault());
   }

   public static HttpUri hierarchical(boolean secured,
                                      ServerAuthority authority,
                                      Path path,
                                      Query query,
                                      Fragment fragment) {
      validate(authority, "authority", isNotNull()).and(validate(path, "path", isNotNull())).orThrow();

      var scheme = SimpleScheme.of(secured ? HTTPS_SCHEME : HTTP_SCHEME);
      return buildUri(() -> new HttpUri(HttpUriRestrictions.automatic(scheme, query, fragment),
                                        scheme,
                                        null,
                                        authority,
                                        path,
                                        query,
                                        fragment));
   }

   public static HttpUri hierarchical(boolean secured, ServerAuthority authority, Path path, Query query) {
      return hierarchical(secured, authority, path, query, null);
   }

   public static HttpUri hierarchical(boolean secured, ServerAuthority authority, Path path) {
      return hierarchical(secured, authority, path, null, null);
   }

   public static HttpUri hierarchical(boolean secured,
                                      ServerAuthority authority,
                                      Path path,
                                      Fragment fragment) {
      return hierarchical(secured, authority, path, null, fragment);
   }

   @Override
   protected HttpUri recreate(URI uri,
                              UriRestrictions restrictions,
                              Scheme scheme,
                              SchemeSpecific schemeSpecific,
                              Authority authority,
                              Path path,
                              Query query,
                              Fragment fragment,
                              boolean newObject) {
      return buildUri(() -> new HttpUri(uri,
                                        restrictions,
                                        scheme,
                                        schemeSpecific,
                                        authority,
                                        path,
                                        query,
                                        fragment,
                                        newObject));
   }

   @Override
   public HttpUriRestrictions restrictions() {
      return (HttpUriRestrictions) restrictions;
   }

   public HttpUri httpRestrictions(Function<? super HttpUriRestrictions, ? extends HttpUriRestrictions> restrictionsMapper) {
      return restrictions(restrictionsMapper.apply(restrictions()));
   }

   public boolean http() {
      return scheme.value().equalsIgnoreCase(HTTP_SCHEME);
   }

   public boolean https() {
      return scheme.value().equalsIgnoreCase(HTTPS_SCHEME);
   }

   @Override
   public HttpUri relativize(Uri uri) {
      throw new UnsupportedOperationException();
   }

   @Override
   public HttpUri relativize(URI uri) {
      throw new UnsupportedOperationException();
   }

   public Optional<String> username() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .map(ui -> ui.username(false));
   }

   public Optional<String> password() {
      return component()
            .serverAuthority()
            .flatMap(ServerAuthority::userInfo)
            .map(UsernamePasswordUserInfo.class::cast)
            .flatMap(ui -> ui.password(false));
   }

   public boolean defaultPort() {
      return (http() && portOrDefault() == DEFAULT_HTTP_PORT) || (https()
                                                                  && portOrDefault() == DEFAULT_HTTPS_PORT);
   }

   public int portOrDefault() {
      return port().orElseGet(() -> nullable(defaultHttpPortMapper().apply(scheme.value()), -1));
   }

   @Override
   public Component component() {
      return new Component();
   }

   protected static UriRestrictions httpRestrictions(UriRestrictions restrictions) {
      return nullable(restrictions,
                      r -> HttpUriRestrictions.of(r).defaultPortMapper(defaultHttpPortMapper()));
   }

   protected static ComponentUri httpUserInfo(ComponentUri uri) {
      return nullable(uri).map(u -> u.component().conditionalUserInfo(HttpUri::httpUserInfo)).orElse(null);
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static Authority httpUserInfo(Authority authority) {
      return nullable(authority)
            .map(a -> instanceOf(a, ServerAuthority.class)
                  .map(sa -> (Authority) sa.userInfo(HttpUri::httpUserInfo))
                  .orElse(a))
            .orElse(null);
   }

   /**
    * Transforms user-info to {@link UsernamePasswordUserInfo} if needed.
    */
   protected static UserInfo httpUserInfo(UserInfo ui) {
      return nullable(ui, UsernamePasswordUserInfo::of);
   }

   /**
    * Returns default port for given scheme.
    *
    * @return default port for given scheme, or {@code null} if scheme is unsupported
    */
   protected static Function<String, Integer> defaultHttpPortMapper() {
      return s -> {
         switch (s.toLowerCase()) {
            case HTTP_SCHEME:
               return DEFAULT_HTTP_PORT;
            case HTTPS_SCHEME:
               return DEFAULT_HTTPS_PORT;
            default:
               return null;
         }
      };
   }

   public class Component extends AbstractComponent {

      public Port portOrDefault() {
         return super.port().orElseGet(() -> SimplePort.of(HttpUri.this.portOrDefault()));
      }

   }

   public static class HttpUriRestrictions extends DefaultUriRestrictions {
      protected static final boolean DEFAULT_HTTP = false;
      protected static final boolean DEFAULT_HTTPS = false;
      protected static final boolean DEFAULT_QUERY = false;
      protected static final boolean DEFAULT_FRAGMENT = false;

      protected final boolean http;
      protected final boolean https;

      protected HttpUriRestrictions(boolean http,
                                    boolean https,
                                    PortRestriction portRestriction,
                                    BiFunction<String, Integer, Integer> portMapper,
                                    boolean query,
                                    boolean fragment) {
         super(portRestriction, portMapper, query, fragment);
         this.http = http;
         this.https = https;
      }

      public static HttpUriRestrictions of(UriRestrictions restrictions) {
         if (restrictions instanceof HttpUriRestrictions) {
            var httpRestrictions = (HttpUriRestrictions) restrictions;
            return new HttpUriRestrictions(httpRestrictions.http,
                                           httpRestrictions.https,
                                           httpRestrictions.portRestriction,
                                           httpRestrictions.portMapper,
                                           httpRestrictions.query,
                                           httpRestrictions.fragment);
         } else if (restrictions instanceof DefaultUriRestrictions) {
            var defaultRestrictions = (DefaultUriRestrictions) restrictions;
            return new HttpUriRestrictions(DEFAULT_HTTP,
                                           DEFAULT_HTTPS,
                                           defaultRestrictions.portRestriction(),
                                           defaultRestrictions.portMapper(),
                                           defaultRestrictions.query(),
                                           defaultRestrictions.fragment());
         } else {
            return new HttpUriRestrictions(DEFAULT_HTTP,
                                           DEFAULT_HTTPS,
                                           null,
                                           null,
                                           DEFAULT_QUERY,
                                           DEFAULT_FRAGMENT);
         }
      }

      public static HttpUriRestrictions ofRestrictions(boolean http,
                                                       boolean https,
                                                       boolean query,
                                                       boolean fragment) {
         return new HttpUriRestrictions(http, https, null, null, query, fragment);
      }

      public static HttpUriRestrictions ofDefault() {
         return ofRestrictions(DEFAULT_HTTP, DEFAULT_HTTPS, DEFAULT_QUERY, DEFAULT_FRAGMENT);
      }

      public static HttpUriRestrictions noRestrictions() {
         return ofRestrictions(false, false, false, false);
      }

      /**
       * Restricts HTTP protocol.
       */
      public static HttpUriRestrictions secured() {
         return ofRestrictions(true, DEFAULT_HTTPS, DEFAULT_QUERY, DEFAULT_FRAGMENT);
      }

      public static HttpUriRestrictions automatic(Scheme scheme, Query query, Fragment fragment) {
         return ofRestrictions(scheme == null || !scheme.value().equalsIgnoreCase(HTTP_SCHEME),
                               scheme == null || !scheme.value().equalsIgnoreCase(HTTPS_SCHEME),
                               query == null,
                               fragment == null);
      }

      @Override
      final protected HttpUriRestrictions recreate(PortRestriction portRestriction,
                                                   BiFunction<String, Integer, Integer> portMapper,
                                                   boolean query,
                                                   boolean fragment) {
         return recreate(http, https, portRestriction, portMapper, query, fragment);
      }

      protected HttpUriRestrictions recreate(boolean http,
                                             boolean https,
                                             PortRestriction portRestriction,
                                             BiFunction<String, Integer, Integer> portMapper,
                                             boolean query,
                                             boolean fragment) {
         return new HttpUriRestrictions(http, https, portRestriction, portMapper, query, fragment);
      }

      /**
       * Returns {@code true} if HTTP protocol is restricted.
       * <p>
       * Note that {@link HttpUri} can't be built if both {@link #http()} and {@link #https()} are restricted.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public boolean http() {
         return http;
      }

      /**
       * Whether to restrict HTTP protocol.
       * <p>
       * Note that {@link HttpUri} can't be built if both {@link #http()} and {@link #https()} are restricted.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public HttpUriRestrictions http(boolean http) {
         return recreate(http, https, portRestriction, portMapper, query, fragment);
      }

      /**
       * Returns {@code true} if HTTPS protocol is restricted.
       * <p>
       * Note that {@link HttpUri} can't be built if both {@link #http()} and {@link #https()} are restricted.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public boolean https() {
         return https;
      }

      /**
       * Whether to restrict HTTPS protocol.
       * <p>
       * Note that {@link HttpUri} can't be built if both {@link #http()} and {@link #https()} are restricted.
       * <p>
       * This restriction is interpreted as a URI incompatibility at build time.
       */
      public HttpUriRestrictions https(boolean https) {
         return recreate(http, https, portRestriction, portMapper, query, fragment);
      }

      @Override
      public HttpUriRestrictions portRestriction(PortRestriction portRestriction) {
         return (HttpUriRestrictions) super.portRestriction(portRestriction);
      }

      @Override
      public HttpUriRestrictions portMapper(BiFunction<String, Integer, Integer> portMapper) {
         return (HttpUriRestrictions) super.portMapper(portMapper);
      }

      @Override
      public HttpUriRestrictions portMapper(Function<String, Integer> portMapper) {
         return (HttpUriRestrictions) super.portMapper(portMapper);
      }

      @Override
      public HttpUriRestrictions portMapper(Integer mappedPort) {
         return (HttpUriRestrictions) super.portMapper(mappedPort);
      }

      @Override
      public HttpUriRestrictions defaultPortMapper(BiFunction<String, Integer, Integer> portMapper) {
         return (HttpUriRestrictions) super.defaultPortMapper(portMapper);
      }

      @Override
      public HttpUriRestrictions defaultPortMapper(Function<String, Integer> portMapper) {
         return (HttpUriRestrictions) super.defaultPortMapper(portMapper);
      }

      @Override
      public HttpUriRestrictions defaultPortMapper(Integer mappedPort) {
         return (HttpUriRestrictions) super.defaultPortMapper(mappedPort);
      }

      @Override
      public HttpUriRestrictions query(boolean query) {
         return (HttpUriRestrictions) super.query(query);
      }

      @Override
      public HttpUriRestrictions fragment(boolean fragment) {
         return (HttpUriRestrictions) super.fragment(fragment);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", HttpUriRestrictions.class.getSimpleName() + "[", "]")
               .add("http=" + http)
               .add("https=" + https)
               .add("portRestriction=" + portRestriction)
               .add("query=" + query)
               .add("fragment=" + fragment)
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         HttpUriRestrictions restrictions = (HttpUriRestrictions) o;
         return portRestriction == restrictions.portRestriction
                && query == restrictions.query
                && fragment == restrictions.fragment
                && http == restrictions.http
                && https == restrictions.https;
      }

      @Override
      public int hashCode() {
         return Objects.hash(super.hashCode(), http, https);
      }
   }

}
