/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.parts;

import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullKeys;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;

import java.net.URI;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;
import com.tinubu.commons.ddd2.uri.UriEncoder;
import com.tinubu.commons.lang.util.Pair;

public class KeyValueQuery extends SimpleQuery {
   protected final Map<String, List<String>> query;
   protected final Map<String, List<String>> encodedQuery;

   /**
    * You must ensure validation of parameters prior to calling this constructor.
    * You must ensure parameters consistency between all parameters.
    */
   protected KeyValueQuery(String query,
                           String encodedQuery,
                           boolean noQueryIfEmpty,
                           Map<String, List<String>> queryMap,
                           Map<String, List<String>> encodedQueryMap) {
      super(query, encodedQuery, noQueryIfEmpty);
      this.query = immutable(queryMap);
      this.encodedQuery = immutable(encodedQueryMap);
   }

   /**
    * You must ensure parameters consistency between all parameters.
    */
   protected KeyValueQuery(String query, String encodedQuery, boolean noQueryIfEmpty) {
      this(query,
           encodedQuery,
           noQueryIfEmpty,
           immutable(parseQuery(encodedQuery, true)),
           immutable(parseQuery(encodedQuery, false)));
   }

   /**
    * You must ensure parameters consistency between all parameters.
    */
   protected KeyValueQuery(Map<String, List<String>> query,
                           Map<String, List<String>> encodedQuery,
                           boolean noQueryIfEmpty) {
      this(buildQuery(Check.validate(query, "query", hasNoNullKeys())),
           buildQuery(Check.validate(encodedQuery, "encodedQuery", hasNoNullKeys())),
           noQueryIfEmpty,
           query,
           encodedQuery);
   }

   /**
    * Creates {@link KeyValueQuery} from un-encoded query string.
    * Each key and value are encoded following encoding rules for query.
    * <p>
    * If specified query string is empty, an empty query will be generated.
    *
    * @param query un-encoded query string
    *
    * @return new instance
    *
    * @see #ofEncoded(String)
    */
   public static KeyValueQuery of(String query) {
      Check.notNull(query, "query");

      return new KeyValueQuery(query, encodeQuery(query, false), DEFAULT_NO_QUERY_IF_EMPTY);
   }

   /**
    * Creates {@link KeyValueQuery} from structured map of un-encoded parameters.
    * Specified map associates parameter name to one or more values. If values list is {@code null} or
    * empty, parameter is ignored. Each parameter value in list can be empty (empty parameter value) or
    * {@code null} (unset parameter value).
    * <p>
    * Each key and value are encoded following encoding rules for query. Moreover, {@code &} are
    * also encoded specifically when using this method.
    * <p>
    * If specified map is empty, an empty query string will be generated.
    *
    * @param query map of query parameters indexed by parameter name
    *
    * @return new instance
    *
    * @see #ofEncoded(Map)
    */
   public static KeyValueQuery of(Map<String, List<String>> query) {
      Check.notNull(query, "query");

      return new KeyValueQuery(filterQueryMap(query), encodeQueryMap(query), DEFAULT_NO_QUERY_IF_EMPTY);
   }

   /**
    * Creates {@link KeyValueQuery} from encoded query string.
    * <p>
    * If specified query string is empty, an empty query will be generated ({@code ?} alone).
    *
    * @param encodedQuery encoded query string
    *
    * @return new instance
    *
    * @see #of(String)
    */
   public static KeyValueQuery ofEncoded(String encodedQuery) {
      Check.notNull(encodedQuery, "encodedQuery");

      return new KeyValueQuery(decodeQuery(encodedQuery), encodedQuery, DEFAULT_NO_QUERY_IF_EMPTY);
   }

   /**
    * Creates {@link KeyValueQuery} from structured map of encoded parameters.
    * Specified map associates parameter name to one or more values. If values list is {@code null} or
    * empty, parameter is ignored. Each parameter value in list can be empty (empty parameter value) or
    * {@code null} (unset parameter value).
    * <p>
    * If specified map is empty, an empty query string will be generated.
    *
    * @param encodedQuery map of encoded query parameters indexed by parameter name
    *
    * @return new instance
    *
    * @see #of(Map)
    */
   public static KeyValueQuery ofEncoded(Map<String, List<String>> encodedQuery) {
      Check.notNull(encodedQuery, "encodedQuery");

      return new KeyValueQuery(decodeQueryMap(encodedQuery),
                               filterQueryMap(encodedQuery),
                               DEFAULT_NO_QUERY_IF_EMPTY);
   }

   public static Optional<? extends KeyValueQuery> of(URI uri) {
      Check.notNull(uri, "uri");

      return nullable(uri.getQuery()).map(__ -> new KeyValueQuery(uri.getQuery(),
                                                                  uri.getRawQuery(),
                                                                  DEFAULT_NO_QUERY_IF_EMPTY));
   }

   /**
    * Creates {@link KeyValueQuery} from existing {@link Query}.
    *
    * @param query existing query
    *
    * @return new instance
    */
   public static KeyValueQuery of(Query query) {
      Check.notNull(query, "query");

      if (query instanceof KeyValueQuery) {
         return (KeyValueQuery) query;
      } else {
         return new KeyValueQuery(query.value(), query.encodedValue(), query.noQueryIfEmpty());
      }
   }

   /**
    * Creates an empty {@link KeyValueQuery}.
    *
    * @return new instance
    */
   public static KeyValueQuery ofEmpty() {
      return new KeyValueQuery("", "", false, map(), map());
   }

   @Override
   public Optional<? extends KeyValueQuery> recreate(URI uri) {
      Check.notNull(uri, "uri");

      if (noQueryIfEmpty && isEmpty() && uri.getQuery() == null) {
         return optional(this);
      } else {
         return nullable(uri.getQuery()).map(__ -> recreate(uri.getQuery(),
                                                            uri.getRawQuery(),
                                                            noQueryIfEmpty));
      }
   }

   @Override
   protected KeyValueQuery recreate(String query, String encodedQuery, boolean noQueryIfEmpty) {
      return new KeyValueQuery(query, encodedQuery, noQueryIfEmpty);
   }

   protected KeyValueQuery recreate(String query,
                                    String encodedQuery,
                                    boolean noQueryIfEmpty,
                                    Map<String, List<String>> queryMap,
                                    Map<String, List<String>> encodedQueryMap) {
      return new KeyValueQuery(query, encodedQuery, noQueryIfEmpty, queryMap, encodedQueryMap);
   }

   @Override
   public KeyValueQuery noQueryIfEmpty(boolean noQueryIfEmpty) {
      return recreate(value(), encodedValue(), noQueryIfEmpty, query, encodedQuery);
   }

   /**
    * Updates query parameters. Existing parameter is replaced.
    * If values list is {@code null} or empty, parameter is removed. Each parameter value in list can be empty
    * (empty parameter value) or  {@code null} (unset parameter value).
    *
    * @param encoded whether specified parameter key and values are encoded
    * @param query query parameters
    */
   public KeyValueQuery setParameters(boolean encoded, Map<String, List<String>> query) {
      Check.notNull(query, "query");

      Map<String, List<String>> updatedQuery = new LinkedHashMap<>(query());
      Map<String, List<String>> updatedEncodedQuery = new LinkedHashMap<>(encodedQuery());

      if (encoded) {
         updatedQuery.putAll(decodeQueryMap(query));
         updatedEncodedQuery.putAll(filterQueryMap(query));
      } else {
         updatedQuery.putAll(filterQueryMap(query));
         updatedEncodedQuery.putAll(encodeQueryMap(query));
      }

      return recreate(buildQuery(updatedQuery),
                      buildQuery(updatedEncodedQuery),
                      noQueryIfEmpty,
                      updatedQuery,
                      updatedEncodedQuery);
   }

   /**
    * Updates a query parameter. Existing parameter is replaced.
    * If values list is {@code null} or empty, parameter is removed. Each parameter value in list can be empty
    * (empty parameter value) or  {@code null} (unset parameter value).
    *
    * @param encoded whether specified parameter key and values are encoded
    * @param key parameter name
    * @param values overrides values for parameter, or {@code null} or {@link Collections#emptyList()}
    *       to remove all values for parameter
    */
   public KeyValueQuery setParameter(boolean encoded, String key, List<String> values) {
      Check.notBlank(key, "key");

      return setParameters(encoded, map(LinkedHashMap::new, stream(Pair.of(key, values))));
   }

   /**
    * Adds query parameters. Values are appended to existing parameter values if any.
    * If values list is {@code null} or empty, parameter is ignored. Each parameter value in list can be empty
    * (empty parameter value) or  {@code null} (unset parameter value).
    *
    * @param encoded whether specified parameter key and values are encoded
    * @param query query parameters
    */
   public KeyValueQuery addParameters(boolean encoded, Map<String, List<String>> query) {
      Check.notNull(query, "query");

      if (encoded) {
         var updatedEncodedQuery = map(LinkedHashMap::new, encodedQuery());
         query.forEach((qk, qv) -> {
            updatedEncodedQuery.compute(qk, (ck, cv) -> {
               if (qv == null || qv.isEmpty()) {
                  return cv;
               } else if (cv == null) {
                  return qv;
               } else {
                  return listConcat(cv, qv);
               }
            });
         });

         return setParameters(encoded, updatedEncodedQuery);
      } else {
         var updatedQuery = map(LinkedHashMap::new, query());
         query.forEach((qk, qv) -> {
            updatedQuery.compute(qk, (ck, cv) -> {
               if (qv == null || qv.isEmpty()) {
                  return cv;
               } else if (cv == null) {
                  return qv;
               } else {
                  return listConcat(cv, qv);
               }
            });
         });

         return setParameters(encoded, updatedQuery);
      }
   }

   /**
    * Adds a query parameter. Values are appended to existing parameter values if any.
    * If values list is {@code null} or empty, parameter is ignored. Each parameter value in list can be empty
    * (empty parameter value) or  {@code null} (unset parameter value).
    *
    * @param encoded whether specified parameter key and values are encoded
    * @param key parameter name
    * @param values appends values for parameter, or {@code null} or {@link Collections#emptyList()}
    *       to do nothing
    */
   public KeyValueQuery addParameter(boolean encoded, String key, List<String> values) {
      Check.notBlank(key, "key");

      if (values != null && !values.isEmpty()) {
         return addParameters(encoded, map(LinkedHashMap::new, stream(Pair.of(key, values))));
      } else {
         return this;
      }
   }

   /**
    * Returns un-encoded query parameters indexed by keys.
    * Returned parameter order is preserved from original query string, excepting for multi-valued keys
    * that are regrouped at first occurrence position.
    * Each returned string value can be :
    * <ul>
    *    <li>parameter value</li>
    *    <li>empty string if parameter value is empty</li>
    *    <li>{@code null} string if parameter value is not defined</li>
    * </ul>
    *
    * @return query parameters, or empty map if no parameters defined
    */
   public Map<String, List<String>> query() {
      return query;
   }

   /**
    * Returns encoded query parameters indexed by keys.
    * Returned parameter order is preserved from original query string, excepting for multi-valued keys
    * that are regrouped at first occurrence position.
    * Each returned string value can be :
    * <ul>
    *    <li>parameter value</li>
    *    <li>empty string if parameter value is empty</li>
    *    <li>{@code null} string if parameter value is not defined</li>
    * </ul>
    *
    * @return query parameters, or empty map if no parameters defined
    */
   public Map<String, List<String>> encodedQuery() {
      return encodedQuery;
   }

   /**
    * Returns list of query parameter values for given key.
    * Returned values order is preserved from original query string.
    * Each returned string can be :
    * <ul>
    *    <li>parameter value</li>
    *    <li>empty string if parameter value is emtpy</li>
    *    <li>{@code null} string if parameter value is not defined</li>
    * </ul>
    *
    * @param key query parameter key
    *
    * @return list of query parameter values, or empty list if parameter is not defined
    */
   public List<String> query(String key) {
      Check.notNull(key, "key");

      return nullable(query.get(key), list());
   }

   /**
    * Returns list of query parameter encoded values for given key.
    * Returned values order is the same as in the query string.
    * Returned string can be :
    * <ul>
    *    <li>parameter value</li>
    *    <li>empty string if parameter value is emtpy</li>
    *    <li>{@code null} string if parameter value is not defined</li>
    * </ul>
    *
    * @param key query parameter key
    *
    * @return list of query parameter values, or empty list if parameter is not defined
    */
   public List<String> encodedQuery(String key) {
      Check.notNull(key, "key");

      return nullable(encodedQuery.get(key), list());
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      KeyValueQuery that = (KeyValueQuery) o;
      return Objects.equals(query, that.query) && Objects.equals(encodedQuery, that.encodedQuery);
   }

   @Override
   public int hashCode() {
      return Objects.hash(query, encodedQuery);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", KeyValueQuery.class.getSimpleName() + "[", "]")
            .add("query=" + query)
            .add("encodedQuery=" + encodedQuery)
            .toString();
   }

   protected static LinkedHashMap<String, List<String>> filterQueryMap(Map<String, List<String>> query) {
      return map(LinkedHashMap::new,
                 stream(query.entrySet()).map(e -> Pair.of(e.getKey(), list(e.getValue()))));
   }

   protected static LinkedHashMap<String, List<String>> encodeQueryMap(Map<String, List<String>> query) {
      return map(LinkedHashMap::new,
                 stream(query.entrySet())
                       .map(e -> Pair.of(e.getKey(), list(e.getValue())))
                       .map(e -> Pair.of(encodeQuery(e.getKey(), true),
                                         list(stream(e.getValue()).map(v -> encodeQuery(v, true))))));
   }

   protected static LinkedHashMap<String, List<String>> decodeQueryMap(Map<String, List<String>> encodedQuery) {
      return map(LinkedHashMap::new,
                 stream(encodedQuery.entrySet())
                       .map(e -> Pair.of(e.getKey(), list(e.getValue())))
                       .map(e -> Pair.of(decodeQuery(e.getKey()),
                                         list(stream(e.getValue()).map(KeyValueQuery::decodeQuery)))));
   }

   /**
    * Decode specified query.
    *
    * @param query query
    *
    * @return decoded query, or {@code null} if specified query is {@code null}
    */
   protected static String decodeQuery(String query) {
      return UriEncoder.decode(query).orElseThrow(identity());
   }

   /**
    * Encode specified query.
    * If specified query is a structured key or value, then {@code &} characters are also encoded.
    *
    * @param query query
    * @param encodeForKeyValues whether specified query is a whole query string, or a structured
    *       key/value
    *
    * @return encoded query, or {@code null} if specified query is {@code null}
    */
   /* TODO : replace encodeQuery with a more performant encoder. */
   protected static String encodeQuery(String query, boolean encodeForKeyValues) {
      var encoded = UriEncoder.encodeQuery(query).orElseThrow(identity());

      if (encoded != null && encodeForKeyValues) {
         encoded = encoded.replace("&", "%26");
      }

      return encoded;
   }

   protected static String buildQuery(Map<String, List<String>> query) {
      return query
            .entrySet()
            .stream()
            .flatMap(e -> nullable(e.getValue(), list())
                  .stream()
                  .map(value -> e.getKey() + (value != null ? "=" + value : "")))
            .collect(joining("&"));
   }

   protected static LinkedHashMap<String, List<String>> parseQuery(String query, boolean decode) {
      LinkedHashMap<String, List<String>> parameters = new LinkedHashMap<>();

      if (query == null || query.isEmpty()) {
         return parameters;
      }

      String[] pairs = StringUtils.split(query, '&');
      for (String pair : pairs) {
         int idx = pair.indexOf("=");
         String key = idx > 0 ? pair.substring(0, idx) : pair;
         String value = idx > 0 ? pair.substring(idx + 1) : null;

         if (decode) {
            key = decodeQuery(key);
            value = decodeQuery(value);
         }

         parameters.computeIfAbsent(key, k -> list()).add(value);
      }

      return parameters;
   }

}
