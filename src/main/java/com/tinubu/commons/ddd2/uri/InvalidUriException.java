/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;

/**
 * Invalid URI.
 */
public class InvalidUriException extends RuntimeException {
   private static final String DEFAULT_MESSAGE = "Invalid '%s' URI";

   protected final String uri;

   protected InvalidUriException(String uri, String message) {
      super(Check.notEmpty(message, "message"));
      this.uri = Check.notNull(uri, "uri");
   }

   protected InvalidUriException(String uri, String message, Throwable cause) {
      super(Check.notEmpty(message), Check.notNull(cause));
      this.uri = Check.notEmpty(uri);
   }

   public InvalidUriException(String uri) {
      this(uri, formatMessage(DEFAULT_MESSAGE, uri));
   }

   public InvalidUriException(URI uri) {
      this(nullable(uri).map(String::valueOf).orElse("<null>"));
   }

   public InvalidUriException(URI uri, Throwable cause) {
      this(nullable(uri).map(String::valueOf).orElse("<null>"), cause);
   }

   public InvalidUriException(Uri uri) {
      this(nullable(uri).map(Uri::stringValue).orElse("<null>"));
   }

   public InvalidUriException(Uri uri, Throwable cause) {
      this(nullable(uri).map(Uri::stringValue).orElse("<null>"), cause);
   }

   public InvalidUriException(String uri, Throwable cause) {
      this(uri, formatMessage(DEFAULT_MESSAGE, uri), cause);
   }

   public InvalidUriException(String scheme,
                              String userInfo,
                              String host,
                              int port,
                              String path,
                              String query,
                              String fragment) {
      this(uri(scheme, userInfo, host, port, path, query, fragment));
   }

   public InvalidUriException(String scheme,
                              String userInfo,
                              String host,
                              int port,
                              String path,
                              String query,
                              String fragment,
                              Throwable cause) {
      this(uri(scheme, userInfo, host, port, path, query, fragment), cause);
   }

   public InvalidUriException(String scheme, String authority, String path, String query, String fragment) {
      this(uri(scheme, authority, path, query, fragment));
   }

   public InvalidUriException(String scheme,
                              String authority,
                              String path,
                              String query,
                              String fragment,
                              Throwable cause) {
      this(uri(scheme, authority, path, query, fragment), cause);
   }

   public InvalidUriException(String scheme, String ssp, String fragment) {
      this(uri(scheme, ssp, fragment));
   }

   public InvalidUriException(String scheme, String ssp, String fragment, Throwable cause) {
      this(uri(scheme, ssp, fragment), cause);
   }

   private static String uri(String scheme,
                             String userInfo,
                             String host,
                             int port,
                             String path,
                             String query,
                             String fragment) {
      var builder = new StringJoiner(", ", URI.class.getSimpleName() + "[", "]");
      if (scheme != null) builder.add("scheme='" + scheme + "'");
      if (userInfo != null) builder.add("userInfo='" + userInfo + "'");
      if (host != null) builder.add("host='" + host + "'");
      if (port != -1) builder.add("port='" + port + "'");
      if (path != null) builder.add("path='" + path + "'");
      if (query != null) builder.add("query='" + query + "'");
      if (fragment != null) builder.add("fragment='" + fragment + "'");
      return builder.toString();
   }

   private static String uri(String scheme, String authority, String path, String query, String fragment) {
      var builder = new StringJoiner(", ", URI.class.getSimpleName() + "[", "]");
      if (scheme != null) builder.add("scheme='" + scheme + "'");
      if (authority != null) builder.add("authority='" + authority + "'");
      if (path != null) builder.add("path='" + path + "'");
      if (query != null) builder.add("query='" + query + "'");
      if (fragment != null) builder.add("fragment='" + fragment + "'");
      return builder.toString();
   }

   private static String uri(String scheme, String ssp, String fragment) {
      var builder = new StringJoiner(", ", URI.class.getSimpleName() + "[", "]");
      if (scheme != null) builder.add("scheme='" + scheme + "'");
      if (ssp != null) builder.add("ssp='" + ssp + "'");
      if (fragment != null) builder.add("fragment='" + fragment + "'");
      return builder.toString();
   }

   public InvalidUriException message(String message, Object... args) {
      return new InvalidUriException(uri, formatMessage(message, args));
   }

   public InvalidUriException message(Throwable throwable) {
      return message(throwableMessage(throwable));
   }

   public InvalidUriException subMessage(String message, Object... args) {
      return new InvalidUriException(uri,
                                     formatMessage(DEFAULT_MESSAGE, uri) + ": " + formatMessage(message,
                                                                                                args));
   }

   public InvalidUriException subMessage(Throwable throwable) {
      return subMessage(throwableMessage(throwable));
   }

   public String uri() {
      return uri;
   }

   /**
    * Format specified message. If no args are specified, message is assumed to have no format, so that
    * placeholders are not replaced, or do not need to be escaped.
    *
    * @param message to format. If no args are specified, message is not formatted
    * @param args optional format args
    *
    * @return formatted message
    */
   protected static String formatMessage(String message, Object... args) {
      if (args.length > 0) {
         return String.format(message, args);
      } else {
         return message;
      }
   }

   protected static String throwableMessage(Throwable throwable) {
      return nullable(throwable.getMessage()).orElseGet(() -> throwable.getClass().getSimpleName());
   }

}
