/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.function.Predicate;

import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

/**
 * Simplified invariant rule implementation using a {@link Predicate}.
 * These class's rules can also be used by other rules implementations :
 * <ul>
 *    <li>{@link #satisfies(Predicate, String, MessageValue...)}</li>
 *    <li>{@link #satisfiesValue(Predicate, String, MessageValue...)}</li>
 *    <li>{@link #notSatisfies(Predicate, String, MessageValue...)}</li>
 *    <li>{@link #notSatisfiesValue(Predicate, String, MessageValue...)}</li>
 * </ul>
 *
 * @param <T> validated object type
 */
public class PredicateInvariantRule<T> implements InvariantRule<T> {

   private final Predicate<ValidatingObject<T>> predicate;
   private final MessageFormatter<T> messageFormatter;
   private final boolean nullable;

   protected PredicateInvariantRule(Predicate<ValidatingObject<T>> predicate,
                                    MessageFormatter<T> messageFormatter,
                                    boolean nullable) {
      this.predicate = notNull(predicate, "predicate");
      this.messageFormatter = notNull(messageFormatter, "messageFormatter");
      this.nullable = nullable;
   }

   public static <T> PredicateInvariantRule<T> satisfies(Predicate<ValidatingObject<T>> predicate,
                                                         MessageFormatter<T> messageFormatter) {
      return new PredicateInvariantRule<>(predicate, messageFormatter, false);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> satisfies(Predicate<ValidatingObject<T>> predicate,
                                                         String message,
                                                         MessageValue<T>... values) {
      return new PredicateInvariantRule<>(predicate, DefaultMessageFormat.of(message, values), false);
   }

   public static <T> PredicateInvariantRule<T> satisfiesValue(Predicate<? super T> predicate,
                                                              MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> predicate.test(validatingObject.value()),
                                          messageFormatter,
                                          false);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> satisfiesValue(Predicate<? super T> predicate,
                                                              String message,
                                                              MessageValue<T>... values) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> predicate.test(validatingObject.value()),
                                          DefaultMessageFormat.of(message, values),
                                          false);
   }

   public static <T> PredicateInvariantRule<T> notSatisfies(Predicate<ValidatingObject<T>> predicate,
                                                            MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(predicate.negate(), messageFormatter, false);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> notSatisfies(Predicate<ValidatingObject<T>> predicate,
                                                            String message,
                                                            MessageValue<T>... values) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(predicate.negate(),
                                          DefaultMessageFormat.of(message, values),
                                          false);
   }

   public static <T> PredicateInvariantRule<T> notSatisfiesValue(Predicate<? super T> predicate,
                                                                 MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> !predicate.test(validatingObject.value()),
                                          messageFormatter,
                                          false);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> notSatisfiesValue(Predicate<? super T> predicate,
                                                                 String message,
                                                                 MessageValue<T>... values) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> !predicate.test(validatingObject.value()),
                                          DefaultMessageFormat.of(message, values),
                                          false);
   }

   public static <T> PredicateInvariantRule<T> satisfiesNullable(Predicate<ValidatingObject<T>> predicate,
                                                                 MessageFormatter<T> messageFormatter) {
      return new PredicateInvariantRule<>(predicate, messageFormatter, true);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> satisfiesNullable(Predicate<ValidatingObject<T>> predicate,
                                                                 String message,
                                                                 MessageValue<T>... values) {
      return new PredicateInvariantRule<>(predicate, DefaultMessageFormat.of(message, values), true);
   }

   public static <T> PredicateInvariantRule<T> satisfiesNullableValue(Predicate<? super T> predicate,
                                                                      MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> predicate.test(validatingObject.value()),
                                          messageFormatter,
                                          true);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> satisfiesNullableValue(Predicate<? super T> predicate,
                                                                      String message,
                                                                      MessageValue<T>... values) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> predicate.test(validatingObject.value()),
                                          DefaultMessageFormat.of(message, values),
                                          true);
   }

   public static <T> PredicateInvariantRule<T> notSatisfiesNullable(Predicate<ValidatingObject<T>> predicate,
                                                                    MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(predicate.negate(), messageFormatter, true);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> notSatisfiesNullable(Predicate<ValidatingObject<T>> predicate,
                                                                    String message,
                                                                    MessageValue<T>... values) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(predicate.negate(), DefaultMessageFormat.of(message, values), true);
   }

   public static <T> PredicateInvariantRule<T> notSatisfiesNullableValue(Predicate<? super T> predicate,
                                                                         MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> !predicate.test(validatingObject.value()),
                                          messageFormatter,
                                          true);
   }

   @SafeVarargs
   public static <T> PredicateInvariantRule<T> notSatisfiesNullableValue(Predicate<? super T> predicate,
                                                                         String message,
                                                                         MessageValue<T>... values) {
      notNull(predicate, "predicate");

      return new PredicateInvariantRule<>(validatingObject -> !predicate.test(validatingObject.value()),
                                          DefaultMessageFormat.of(message, values),
                                          true);
   }

   @Override
   public InvariantResult<T> apply(ValidatingObject<T> validatingObject) {
      notNull(validatingObject, "validatingObject");

      if (!nullable && validatingObject.value() == null) {
         return InvariantResult.error(validatingObject,
                                      FastStringFormat.of("'", validatingObjectName(), "' must not be null"));
      }

      if (!predicate.test(validatingObject)) {
         return InvariantResult.error(validatingObject, messageFormatter);
      } else {
         return InvariantResult.success(validatingObject, messageFormatter);
      }
   }

}

