/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.booleanList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.byteList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.charList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.doubleList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.floatList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.intList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyBooleans;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyBytes;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyChars;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyDoubles;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyFloats;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyInts;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyLongs;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyShorts;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.longList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.shortList;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;

import java.util.Collection;
import java.util.function.Predicate;

import com.tinubu.commons.ddd2.invariant.rules.ArrayRules;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.ddd2.invariant.rules.PredicateRules;
import com.tinubu.commons.ddd2.invariant.rules.StringRules;
import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * Preset invariants for common validations using pre-instantiated rules for performance.
 */
// FIXME add map validators.
public final class Validate {

   private static final InvariantRule<?> NOT_NULL = isNotNull();

   private static final InvariantRule<? extends CharSequence> STRING_NOT_EMPTY = StringRules.isNotEmpty();
   private static final InvariantRule<? extends CharSequence> STRING_NOT_BLANK = StringRules.isNotBlank();

   private static final InvariantRule<? extends Iterable<?>> ITERABLE_NO_NULL_ELEMENTS = hasNoNullElements();
   private static final InvariantRule<? extends Object[]> ARRAY_NO_NULL_ELEMENTS =
         ArrayRules.list(hasNoNullElements());
   private static final InvariantRule<byte[]> ARRAY_BYTE_NO_NULL_ELEMENTS = byteList(hasNoNullElements());
   private static final InvariantRule<char[]> ARRAY_CHAR_NO_NULL_ELEMENTS = charList(hasNoNullElements());
   private static final InvariantRule<float[]> ARRAY_FLOAT_NO_NULL_ELEMENTS = floatList(hasNoNullElements());
   private static final InvariantRule<double[]> ARRAY_DOUBLE_NO_NULL_ELEMENTS =
         doubleList(hasNoNullElements());
   private static final InvariantRule<int[]> ARRAY_INT_NO_NULL_ELEMENTS = intList(hasNoNullElements());
   private static final InvariantRule<long[]> ARRAY_LONG_NO_NULL_ELEMENTS = longList(hasNoNullElements());
   private static final InvariantRule<short[]> ARRAY_SHORT_NO_NULL_ELEMENTS = shortList(hasNoNullElements());
   private static final InvariantRule<boolean[]> ARRAY_BOOLEAN_NO_NULL_ELEMENTS =
         booleanList(hasNoNullElements());

   private static final InvariantRule<? extends Collection<?>> COLLECTION_NOT_EMPTY =
         CollectionRules.isNotEmpty();
   private static final InvariantRule<? extends Object[]> ARRAY_NOT_EMPTY = ArrayRules.isNotEmpty();
   private static final InvariantRule<byte[]> ARRAY_BYTE_NOT_EMPTY = isNotEmptyBytes();
   private static final InvariantRule<char[]> ARRAY_CHAR_NOT_EMPTY = isNotEmptyChars();
   private static final InvariantRule<float[]> ARRAY_FLOAT_NOT_EMPTY = isNotEmptyFloats();
   private static final InvariantRule<double[]> ARRAY_DOUBLE_NOT_EMPTY = isNotEmptyDoubles();
   private static final InvariantRule<int[]> ARRAY_INT_NOT_EMPTY = isNotEmptyInts();
   private static final InvariantRule<long[]> ARRAY_LONG_NOT_EMPTY = isNotEmptyLongs();
   private static final InvariantRule<short[]> ARRAY_SHORT_NOT_EMPTY = isNotEmptyShorts();
   private static final InvariantRule<boolean[]> ARRAY_BOOLEAN_NOT_EMPTY = isNotEmptyBooleans();

   private Validate() {
   }

   @CheckReturnValue
   public static <T> InvariantResult<T> validate(final T value,
                                                 final String name,
                                                 final InvariantRule<T> rule) {
      return Invariant.validate(value, name, rule);
   }

   @CheckReturnValue
   public static <T> InvariantResult<T> validate(final T value, final InvariantRule<T> rule) {
      return Invariant.validate(value, rule);
   }

   @SafeVarargs
   @CheckReturnValue
   public static <T> InvariantResult<T> satisfies(final T value,
                                                  final String name,
                                                  final Predicate<T> predicate,
                                                  final String message,
                                                  final MessageValue<T>... values) {
      return Invariant.validate(value, name, PredicateRules.satisfies(predicate, message, values));
   }

   @CheckReturnValue
   public static <T> InvariantResult<T> satisfies(final T value,
                                                  final String name,
                                                  final Predicate<T> predicate) {
      return Invariant.validate(value, name, PredicateRules.satisfies(predicate));
   }

   @SafeVarargs
   @CheckReturnValue
   public static <T> InvariantResult<T> satisfies(final T value,
                                                  final Predicate<T> predicate,
                                                  final String message,
                                                  final MessageValue<T>... values) {
      return Invariant.validate(value, PredicateRules.satisfies(predicate, message, values));
   }

   @CheckReturnValue
   public static <T> InvariantResult<T> satisfies(final T value, final Predicate<T> predicate) {
      return Invariant.validate(value, PredicateRules.satisfies(predicate));
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T> InvariantResult<T> notNull(final T value, final String name) {
      return Invariant.validate(value, name, (InvariantRule<T>) NOT_NULL);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T> InvariantResult<T> notNull(final T value) {
      return Invariant.validate(value, (InvariantRule<T>) NOT_NULL);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends CharSequence> InvariantResult<T> notEmpty(final T value, final String name) {
      return Invariant.validate(value, name, (InvariantRule<T>) STRING_NOT_EMPTY);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends CharSequence> InvariantResult<T> notEmpty(final T value) {
      return Invariant.validate(value, (InvariantRule<T>) STRING_NOT_EMPTY);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends CharSequence> InvariantResult<T> notBlank(final T value, final String name) {
      return Invariant.validate(value, name, (InvariantRule<T>) STRING_NOT_BLANK);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends CharSequence> InvariantResult<T> notBlank(final T value) {
      return Invariant.validate(value, (InvariantRule<T>) STRING_NOT_BLANK);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends Iterable<?>> InvariantResult<T> noNullElements(final T iterable,
                                                                           final String name) {
      return Invariant.validate(iterable, name, (InvariantRule<T>) ITERABLE_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends Iterable<?>> InvariantResult<T> noNullElements(final T iterable) {
      return Invariant.validate(iterable, (InvariantRule<T>) ITERABLE_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T> InvariantResult<T[]> noNullElements(final T[] array, final String name) {
      return Invariant.validate(array, name, (InvariantRule<T[]>) ARRAY_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T> InvariantResult<T[]> noNullElements(final T[] array) {
      return Invariant.validate(array, (InvariantRule<T[]>) ARRAY_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<byte[]> noNullElements(final byte[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_BYTE_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<byte[]> noNullElements(final byte[] array) {
      return Invariant.validate(array, ARRAY_BYTE_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<char[]> noNullElements(final char[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_CHAR_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<char[]> noNullElements(final char[] array) {
      return Invariant.validate(array, ARRAY_CHAR_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<float[]> noNullElements(final float[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_FLOAT_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<float[]> noNullElements(final float[] array) {
      return Invariant.validate(array, ARRAY_FLOAT_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<double[]> noNullElements(final double[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_DOUBLE_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<double[]> noNullElements(final double[] array) {
      return Invariant.validate(array, ARRAY_DOUBLE_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<int[]> noNullElements(final int[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_INT_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<int[]> noNullElements(final int[] array) {
      return Invariant.validate(array, ARRAY_INT_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<long[]> noNullElements(final long[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_LONG_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<long[]> noNullElements(final long[] array) {
      return Invariant.validate(array, ARRAY_LONG_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<short[]> noNullElements(final short[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_SHORT_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<short[]> noNullElements(final short[] array) {
      return Invariant.validate(array, ARRAY_SHORT_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<boolean[]> noNullElements(final boolean[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_BOOLEAN_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   public static InvariantResult<boolean[]> noNullElements(final boolean[] array) {
      return Invariant.validate(array, ARRAY_BOOLEAN_NO_NULL_ELEMENTS);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends Collection<?>> InvariantResult<T> notEmpty(final T collection,
                                                                       final String name) {
      return Invariant.validate(collection, name, (InvariantRule<T>) COLLECTION_NOT_EMPTY);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T extends Collection<?>> InvariantResult<T> notEmpty(final T collection) {
      return Invariant.validate(collection, (InvariantRule<T>) COLLECTION_NOT_EMPTY);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T> InvariantResult<T[]> notEmpty(final T[] array, final String name) {
      return Invariant.validate(array, name, (InvariantRule<T[]>) ARRAY_NOT_EMPTY);
   }

   @CheckReturnValue
   @SuppressWarnings("unchecked")
   public static <T> InvariantResult<T[]> notEmpty(final T[] array) {
      return Invariant.validate(array, (InvariantRule<T[]>) ARRAY_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<byte[]> notEmpty(final byte[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_BYTE_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<byte[]> notEmpty(final byte[] array) {
      return Invariant.validate(array, ARRAY_BYTE_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<char[]> notEmpty(final char[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_CHAR_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<char[]> notEmpty(final char[] array) {
      return Invariant.validate(array, ARRAY_CHAR_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<float[]> notEmpty(final float[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_FLOAT_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<float[]> notEmpty(final float[] array) {
      return Invariant.validate(array, ARRAY_FLOAT_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<double[]> notEmpty(final double[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_DOUBLE_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<double[]> notEmpty(final double[] array) {
      return Invariant.validate(array, ARRAY_DOUBLE_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<int[]> notEmpty(final int[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_INT_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<int[]> notEmpty(final int[] array) {
      return Invariant.validate(array, ARRAY_INT_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<long[]> notEmpty(final long[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_LONG_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<long[]> notEmpty(final long[] array) {
      return Invariant.validate(array, ARRAY_LONG_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<short[]> notEmpty(final short[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_SHORT_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<short[]> notEmpty(final short[] array) {
      return Invariant.validate(array, ARRAY_SHORT_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<boolean[]> notEmpty(final boolean[] array, final String name) {
      return Invariant.validate(array, name, ARRAY_BOOLEAN_NOT_EMPTY);
   }

   @CheckReturnValue
   public static InvariantResult<boolean[]> notEmpty(final boolean[] array) {
      return Invariant.validate(array, ARRAY_BOOLEAN_NOT_EMPTY);
   }

   public static class Check {

      public static <T> T validate(final T value, final String name, final InvariantRule<T> rule) {
         return Validate.validate(value, name, rule).orThrow();
      }

      public static <T> T validate(final T value, final InvariantRule<T> rule) {
         return Validate.validate(value, rule).orThrow();
      }

      @SafeVarargs
      public static <T> T satisfies(final T value,
                                    final String name,
                                    final Predicate<T> predicate,
                                    final String message,
                                    final MessageValue<T>... values) {
         return Validate.satisfies(value, name, predicate, message, values).orThrow();
      }

      public static <T> T satisfies(final T value, final String name, final Predicate<T> predicate) {
         return Validate.satisfies(value, name, predicate).orThrow();
      }

      @SafeVarargs
      public static <T> T satisfies(final T value,
                                    final Predicate<T> predicate,
                                    final String message,
                                    final MessageValue<T>... values) {
         return Validate.satisfies(value, predicate, message, values).orThrow();
      }

      public static <T> T satisfies(final T value, final Predicate<T> predicate) {
         return Validate.satisfies(value, predicate).orThrow();
      }

      public static <T> T notNull(final T value, final String name) {
         return Validate.notNull(value, name).orThrow();
      }

      public static <T> T notNull(final T value) {
         return Validate.notNull(value).orThrow();
      }

      public static <T extends CharSequence> T notEmpty(final T value, final String name) {
         return Validate.notEmpty(value, name).orThrow();
      }

      public static <T extends CharSequence> T notEmpty(final T value) {
         return Validate.notEmpty(value).orThrow();
      }

      public static <T extends CharSequence> T notBlank(final T value, final String name) {
         return Validate.notBlank(value, name).orThrow();
      }

      public static <T extends CharSequence> T notBlank(final T value) {
         return Validate.notBlank(value).orThrow();
      }

      public static <T extends Iterable<?>> T noNullElements(final T iterable, final String name) {
         return Validate.noNullElements(iterable, name).orThrow();
      }

      public static <T extends Iterable<?>> T noNullElements(final T iterable) {
         return Validate.noNullElements(iterable).orThrow();
      }

      public static <T> T[] noNullElements(final T[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static <T> T[] noNullElements(final T[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static byte[] noNullElements(final byte[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static byte[] noNullElements(final byte[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static char[] noNullElements(final char[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static char[] noNullElements(final char[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static float[] noNullElements(final float[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static float[] noNullElements(final float[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static double[] noNullElements(final double[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static double[] noNullElements(final double[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static int[] noNullElements(final int[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static int[] noNullElements(final int[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static long[] noNullElements(final long[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static long[] noNullElements(final long[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static short[] noNullElements(final short[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static short[] noNullElements(final short[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static boolean[] noNullElements(final boolean[] array, final String name) {
         return Validate.noNullElements(array, name).orThrow();
      }

      public static boolean[] noNullElements(final boolean[] array) {
         return Validate.noNullElements(array).orThrow();
      }

      public static <T extends Collection<?>> T notEmpty(final T collection, final String name) {
         return Validate.notEmpty(collection, name).orThrow();
      }

      public static <T extends Collection<?>> T notEmpty(final T collection) {
         return Validate.notEmpty(collection).orThrow();
      }

      public static <T> T[] notEmpty(final T[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static <T> T[] notEmpty(final T[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static byte[] notEmpty(final byte[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static byte[] notEmpty(final byte[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static char[] notEmpty(final char[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static char[] notEmpty(final char[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static float[] notEmpty(final float[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static float[] notEmpty(final float[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static double[] notEmpty(final double[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static double[] notEmpty(final double[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static int[] notEmpty(final int[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static int[] notEmpty(final int[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static long[] notEmpty(final long[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static long[] notEmpty(final long[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static short[] notEmpty(final short[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static short[] notEmpty(final short[] array) {
         return Validate.notEmpty(array).orThrow();
      }

      public static boolean[] notEmpty(final boolean[] array, final String name) {
         return Validate.notEmpty(array, name).orThrow();
      }

      public static boolean[] notEmpty(final boolean[] array) {
         return Validate.notEmpty(array).orThrow();
      }
   }
}
