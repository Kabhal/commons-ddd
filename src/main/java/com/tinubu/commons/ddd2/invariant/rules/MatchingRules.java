/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.parameterValue;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.regex.Pattern;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

// FIXME tests regexp[negate] + *globs*
// FIXME regroup optionalized rules to matches(FLAGS) & doesNotMatch(FLAGS)
public class MatchingRules {

   /** Special 'escape' character used in match strings. */
   private static final Character GLOB_ESCAPE_CHAR = '\\';
   /** Special 'multiple-characters matcher' character used in match strings. */
   private static final Character GLOB_MULTI_CHAR = '*';
   /** Special 'single-character matcher' character used in match strings. */
   private static final Character GLOB_SINGLE_CHAR = '?';

   private MatchingRules() {
   }

   public static <T extends CharSequence> InvariantRule<T> matches(final ParameterValue<Pattern> pattern,
                                                                   final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> pattern.requireNonNullValue().matcher(v).matches(),
                                                 messageFormatter).ruleContext("MatchingRules.matches",
                                                                               pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> matches(final ParameterValue<Pattern> pattern,
                                                                   final String message,
                                                                   final MessageValue<T>... values) {
      return matches(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> matches(final ParameterValue<Pattern> pattern) {
      return matches(pattern,
                     FastStringFormat.of("'",
                                         validatingObject(),
                                         "' must match regexp /",
                                         parameterValue(0, Pattern::pattern),
                                         "/",
                                         parameterValue(0, Pattern::flags)));
   }

   public static <T extends CharSequence> InvariantRule<T> doesNotMatch(final ParameterValue<Pattern> pattern,
                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !pattern.requireNonNullValue().matcher(v).matches(),
                                                 messageFormatter).ruleContext("MatchingRules.doesNotMatch",
                                                                               pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> doesNotMatch(final ParameterValue<Pattern> pattern,
                                                                        final String message,
                                                                        final MessageValue<T>... values) {
      return doesNotMatch(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> doesNotMatch(final ParameterValue<Pattern> pattern) {
      return doesNotMatch(pattern,
                          FastStringFormat.of("'",
                                              validatingObject(),
                                              "' must not match regexp /",
                                              parameterValue(0, Pattern::pattern),
                                              "/",
                                              parameterValue(0, Pattern::flags)));
   }

   public static <T extends CharSequence> InvariantRule<T> partiallyMatches(final ParameterValue<Pattern> pattern,
                                                                            final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> pattern.requireNonNullValue().matcher(v).find(),
                                                 messageFormatter).ruleContext(
            "MatchingRules.partiallyMatches", pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> partiallyMatches(final ParameterValue<Pattern> pattern,
                                                                            final String message,
                                                                            final MessageValue<T>... values) {
      return partiallyMatches(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> partiallyMatches(final ParameterValue<Pattern> pattern) {
      return partiallyMatches(pattern,
                              FastStringFormat.of("'",
                                                  validatingObject(),
                                                  "' must partially match regexp /",
                                                  parameterValue(0, Pattern::pattern),
                                                  "/",
                                                  parameterValue(0, Pattern::flags)));
   }

   public static <T extends CharSequence> InvariantRule<T> doesNotPartiallyMatch(final ParameterValue<Pattern> pattern,
                                                                                 final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !pattern.requireNonNullValue().matcher(v).find(),
                                                 messageFormatter).ruleContext(
            "MatchingRules.doesNotPartiallyMatch", pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> doesNotPartiallyMatch(final ParameterValue<Pattern> pattern,
                                                                                 final String message,
                                                                                 final MessageValue<T>... values) {
      return doesNotPartiallyMatch(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> doesNotPartiallyMatch(final ParameterValue<Pattern> pattern) {
      return doesNotPartiallyMatch(pattern,
                                   FastStringFormat.of("'",
                                                       validatingObject(),
                                                       "' must not partially match regexp /",
                                                       parameterValue(0, Pattern::pattern),
                                                       "/",
                                                       parameterValue(0, Pattern::flags)));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> matchesGlob(final ParameterValue<U> pattern,
                                                                                               final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> globPattern(pattern.requireNonNullValue(), false)
            .matcher(v).matches(), messageFormatter).ruleContext("MatchingRules.matchesGlob", pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> matchesGlob(final ParameterValue<U> pattern,
                                                                                               final String message,
                                                                                               final MessageValue<T>... values) {
      return matchesGlob(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> matchesGlob(final ParameterValue<U> pattern) {
      return matchesGlob(pattern,
                         FastStringFormat.of("'",
                                             validatingObject(),
                                             "' must match glob '",
                                             parameter(0),
                                             "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotMatchGlob(final ParameterValue<U> pattern,
                                                                                                    final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !globPattern(pattern.requireNonNullValue(), false)
            .matcher(v).matches(), messageFormatter).ruleContext("MatchingRules.doesNotMatchGlob", pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotMatchGlob(final ParameterValue<U> pattern,
                                                                                                    final String message,
                                                                                                    final MessageValue<T>... values) {
      return doesNotMatchGlob(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotMatchGlob(final ParameterValue<U> pattern) {
      return doesNotMatchGlob(pattern,
                              FastStringFormat.of("'",
                                                  validatingObject(),
                                                  "' must not match glob '",
                                                  parameter(0),
                                                  "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> partiallyMatchesGlob(final ParameterValue<U> pattern,
                                                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> globPattern(pattern.requireNonNullValue(), false)
            .matcher(v).find(), messageFormatter).ruleContext("MatchingRules.partiallyMatchesGlob", pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> partiallyMatchesGlob(final ParameterValue<U> pattern,
                                                                                                        final String message,
                                                                                                        final MessageValue<T>... values) {
      return partiallyMatchesGlob(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> partiallyMatchesGlob(final ParameterValue<U> pattern) {
      return partiallyMatchesGlob(pattern,
                                  FastStringFormat.of("'",
                                                      validatingObject(),
                                                      "' must partially match glob '",
                                                      parameter(0),
                                                      "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotPartiallyMatchGlob(
         final ParameterValue<U> pattern,
         final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !globPattern(pattern.requireNonNullValue(), false)
            .matcher(v).find(), messageFormatter).ruleContext("MatchingRules.doesNotPartiallyMatchGlob",
                                                              pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotPartiallyMatchGlob(
         final ParameterValue<U> pattern,
         final String message,
         final MessageValue<T>... values) {
      return doesNotPartiallyMatchGlob(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotPartiallyMatchGlob(
         final ParameterValue<U> pattern) {
      return doesNotPartiallyMatchGlob(pattern,
                                       FastStringFormat.of("'",
                                                           validatingObject(),
                                                           "' must not partially match glob '",
                                                           parameter(0),
                                                           "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> matchesGlobIgnoreCase(final ParameterValue<U> pattern,
                                                                                                         final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> globPattern(pattern.requireNonNullValue(), true)
            .matcher(v).matches(), messageFormatter).ruleContext("MatchingRules.matchesGlobIgnoreCase",
                                                                 pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> matchesGlobIgnoreCase(final ParameterValue<U> pattern,
                                                                                                         final String message,
                                                                                                         final MessageValue<T>... values) {
      return matchesGlobIgnoreCase(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> matchesGlobIgnoreCase(final ParameterValue<U> pattern) {
      return matchesGlobIgnoreCase(pattern,
                                   FastStringFormat.of("'",
                                                       validatingObject(),
                                                       "' must match glob '",
                                                       parameter(0),
                                                       "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotMatchGlobIgnoreCase(
         final ParameterValue<U> pattern,
         final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !globPattern(pattern.requireNonNullValue(), true)
            .matcher(v).matches(), messageFormatter).ruleContext("MatchingRules.doesNotMatchGlobIgnoreCase",
                                                                 pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotMatchGlobIgnoreCase(
         final ParameterValue<U> pattern,
         final String message,
         final MessageValue<T>... values) {
      return doesNotMatchGlobIgnoreCase(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotMatchGlobIgnoreCase(
         final ParameterValue<U> pattern) {
      return doesNotMatchGlobIgnoreCase(pattern,
                                        FastStringFormat.of("'",
                                                            validatingObject(),
                                                            "' must not match glob '",
                                                            parameter(0),
                                                            "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> partiallyMatchesGlobIgnoreCase(
         final ParameterValue<U> pattern,
         final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> globPattern(pattern.requireNonNullValue(), true)
            .matcher(v).find(), messageFormatter).ruleContext("MatchingRules.partiallyMatchesGlobIgnoreCase",
                                                              pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> partiallyMatchesGlobIgnoreCase(
         final ParameterValue<U> pattern,
         final String message,
         final MessageValue<T>... values) {
      return partiallyMatchesGlobIgnoreCase(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> partiallyMatchesGlobIgnoreCase(
         final ParameterValue<U> pattern) {
      return partiallyMatchesGlobIgnoreCase(pattern,
                                            FastStringFormat.of("'",
                                                                validatingObject(),
                                                                "' must partially match glob '",
                                                                parameter(0),
                                                                "' (case-insensitive)"));

   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotPartiallyMatchGlobIgnoreCase(
         final ParameterValue<U> pattern,
         final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !globPattern(pattern.requireNonNullValue(), true)
            .matcher(v)
            .find(), messageFormatter).ruleContext("MatchingRules.doesNotPartiallyMatchGlobIgnoreCase",
                                                   pattern);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotPartiallyMatchGlobIgnoreCase(
         final ParameterValue<U> pattern,
         final String message,
         final MessageValue<T>... values) {
      return doesNotPartiallyMatchGlobIgnoreCase(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotPartiallyMatchGlobIgnoreCase(
         final ParameterValue<U> pattern) {
      return doesNotPartiallyMatchGlobIgnoreCase(pattern,
                                                 FastStringFormat.of("'",
                                                                     validatingObject(),
                                                                     "' must not partially match glob '",
                                                                     parameter(0),
                                                                     "' (case-insensitive)"));

   }

   private static <U extends CharSequence> Pattern globPattern(U globPattern, boolean ignoreCase) {
      return Pattern.compile(globPatternToRegexp(globPattern), ignoreCase ? Pattern.CASE_INSENSITIVE : 0b0);
   }

   /**
    * Converts glob pattern into a regexp.
    *
    * @implNote The first escape character replacement is a hack to create an alternative, neutral,
    *       notation that separate the backslash-pairs group from the last remaining backslash if any.
    */
   private static <U extends CharSequence> String globPatternToRegexp(U globPattern) {
      return Pattern
            .quote(String.valueOf(globPattern))
            .replace(String.valueOf(GLOB_ESCAPE_CHAR) + GLOB_ESCAPE_CHAR,
                     "\\E\\Q" + GLOB_ESCAPE_CHAR + "\\E\\Q")
            .replaceAll("(?<!\\\\)\\" + GLOB_SINGLE_CHAR, "\\\\E.\\\\Q")
            .replace("\\" + GLOB_SINGLE_CHAR, String.valueOf(GLOB_SINGLE_CHAR))
            .replaceAll("(?<!\\\\)\\" + GLOB_MULTI_CHAR, "\\\\E.*\\\\Q")
            .replace("\\" + GLOB_MULTI_CHAR, String.valueOf(GLOB_MULTI_CHAR));
   }

}
