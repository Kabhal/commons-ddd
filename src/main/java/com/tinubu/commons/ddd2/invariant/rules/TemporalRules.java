/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.valueformatter.TemporalInstantValueFormatter;

public class TemporalRules {

   private static final TemporalInstantValueFormatter TEMPORAL_INSTANT_FORMATTER =
         new TemporalInstantValueFormatter();

   private TemporalRules() {
   }

   public static <T extends Temporal> InvariantRule<T> isEqualTo(final ParameterValue<T> value,
                                                                 final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> temporalIsEqual(value.requireNonNullValue(), v),
                                                 messageFormatter).ruleContext("TemporalRules.isEqualTo",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Temporal> InvariantRule<T> isEqualTo(final ParameterValue<T> value,
                                                                 final String message,
                                                                 final MessageValue<T>... values) {
      return isEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Temporal> InvariantRule<T> isEqualTo(final ParameterValue<T> value) {
      return isEqualTo(value,
                       FastStringFormat.of("'",
                                           validatingObject(TEMPORAL_INSTANT_FORMATTER),
                                           "' must be equal to '", parameter(0, TEMPORAL_INSTANT_FORMATTER),
                                           "'"));
   }

   public static <T extends Temporal> InvariantRule<T> isNotEqualTo(final ParameterValue<T> value,
                                                                    final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> !temporalIsEqual(value.requireNonNullValue(), v),
                                                 messageFormatter).ruleContext("TemporalRules.isNotEqualTo",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Temporal> InvariantRule<T> isNotEqualTo(final ParameterValue<T> value,
                                                                    final String message,
                                                                    final MessageValue<T>... values) {
      return isNotEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Temporal> InvariantRule<T> isNotEqualTo(final ParameterValue<T> value) {
      return isNotEqualTo(value,
                          FastStringFormat.of("'",
                                              validatingObject(TEMPORAL_INSTANT_FORMATTER),
                                              "' must be equal to '",
                                              parameter(0, TEMPORAL_INSTANT_FORMATTER),
                                              "'"));
   }

   public static <T extends Temporal> InvariantRule<T> isBefore(final ParameterValue<T> value,
                                                                final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> temporalIsBefore(v, value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("TemporalRules.isBefore",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Temporal> InvariantRule<T> isBefore(final ParameterValue<T> value,
                                                                final String message,
                                                                final MessageValue<T>... values) {
      return isBefore(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Temporal> InvariantRule<T> isBefore(final ParameterValue<T> value) {
      return isBefore(value,
                      FastStringFormat.of("'",
                                          validatingObject(TEMPORAL_INSTANT_FORMATTER),
                                          "' must be before '", parameter(0, TEMPORAL_INSTANT_FORMATTER),
                                          "'"));
   }

   public static <T extends Temporal> InvariantRule<T> isNotBefore(final ParameterValue<T> value,
                                                                   final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> !temporalIsBefore(v, value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("TemporalRules.isNotBefore",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Temporal> InvariantRule<T> isNotBefore(final ParameterValue<T> value,
                                                                   final String message,
                                                                   final MessageValue<T>... values) {
      return isNotBefore(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Temporal> InvariantRule<T> isNotBefore(final ParameterValue<T> value) {
      return isNotBefore(value,
                         FastStringFormat.of("'",
                                             validatingObject(TEMPORAL_INSTANT_FORMATTER),
                                             "' must not be before '",
                                             parameter(0, TEMPORAL_INSTANT_FORMATTER),
                                             "'"));
   }

   public static <T extends Temporal> InvariantRule<T> isAfter(final ParameterValue<T> value,
                                                               final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> temporalIsAfter(v, value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("TemporalRules.isAfter",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Temporal> InvariantRule<T> isAfter(final ParameterValue<T> value,
                                                               final String message,
                                                               final MessageValue<T>... values) {
      return isAfter(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Temporal> InvariantRule<T> isAfter(final ParameterValue<T> value) {
      return isAfter(value,
                     FastStringFormat.of("'",
                                         validatingObject(TEMPORAL_INSTANT_FORMATTER),
                                         "' must be after '", parameter(0, TEMPORAL_INSTANT_FORMATTER),
                                         "'"));
   }

   public static <T extends Temporal> InvariantRule<T> isNotAfter(final ParameterValue<T> value,
                                                                  final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> !temporalIsAfter(v, value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("TemporalRules.isNotAfter",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Temporal> InvariantRule<T> isNotAfter(final ParameterValue<T> value,
                                                                  final String message,
                                                                  final MessageValue<T>... values) {
      return isNotAfter(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Temporal> InvariantRule<T> isNotAfter(final ParameterValue<T> value) {
      return isNotAfter(value,
                        FastStringFormat.of("'",
                                            validatingObject(TEMPORAL_INSTANT_FORMATTER),
                                            "' must not be after '", parameter(0, TEMPORAL_INSTANT_FORMATTER),
                                            "'"));
   }

   private static <T extends Temporal> boolean temporalIsEqual(T date, T value) {
      if (value instanceof OffsetDateTime) {
         return ((OffsetDateTime) date).isEqual((OffsetDateTime) value);
      } else if (value instanceof ZonedDateTime) {
         return ((ZonedDateTime) date).isEqual((ZonedDateTime) value);
      } else if (value instanceof LocalDateTime) {
         return ((LocalDateTime) date).isEqual((LocalDateTime) value);
      } else if (value instanceof LocalDate) {
         return ((LocalDate) date).isEqual((LocalDate) value);
      } else if (value instanceof Instant) {
         return date.equals(value);
      } else {
         throw new IllegalArgumentException(String.format("Unsupported temporal type : %s",
                                                          value.getClass().getName()));
      }
   }

   private static <T extends Temporal> boolean temporalIsBefore(T date, T value) {
      if (value instanceof OffsetDateTime) {
         return ((OffsetDateTime) date).isBefore((OffsetDateTime) value);
      } else if (value instanceof ZonedDateTime) {
         return ((ZonedDateTime) date).isBefore((ZonedDateTime) value);
      } else if (value instanceof LocalDateTime) {
         return ((LocalDateTime) date).isBefore((LocalDateTime) value);
      } else if (value instanceof LocalDate) {
         return ((LocalDate) date).isBefore((LocalDate) value);
      } else if (value instanceof Instant) {
         return ((Instant) date).isBefore((Instant) value);
      } else {
         throw new IllegalArgumentException(String.format("Unsupported temporal type : %s",
                                                          value.getClass().getName()));
      }
   }

   private static <T extends Temporal> boolean temporalIsAfter(T date, T value) {
      if (value instanceof OffsetDateTime) {
         return ((OffsetDateTime) date).isAfter((OffsetDateTime) value);
      } else if (value instanceof ZonedDateTime) {
         return ((ZonedDateTime) date).isAfter((ZonedDateTime) value);
      } else if (value instanceof LocalDateTime) {
         return ((LocalDateTime) date).isAfter((LocalDateTime) value);
      } else if (value instanceof LocalDate) {
         return ((LocalDate) date).isAfter((LocalDate) value);
      } else if (value instanceof Instant) {
         return ((Instant) date).isAfter((Instant) value);
      } else {
         throw new IllegalArgumentException(String.format("Unsupported temporal type : %s",
                                                          value.getClass().getName()));
      }
   }

}
