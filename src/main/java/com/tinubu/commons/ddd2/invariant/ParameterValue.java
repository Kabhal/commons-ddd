/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;

import java.util.Objects;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Type for rules parameters. This enables lazy evaluation and parameter naming.
 *
 * @param <P> parameter type
 */
@FunctionalInterface
public interface ParameterValue<P> {

   /**
    * Parameter value.
    *
    * @return parameter value
    */
   P value();

   /**
    * Parameter value when value must not be {@code null}.
    *
    * @return parameter value
    *
    * @throws NullPointerException if value is {@code null}
    */
   default P requireNonNullValue() {
      return notNull(value(), name().orElse("<parameter>"));
   }

   /**
    * Optional parameter name.
    *
    * @return parameter name or {@link Optional#empty} if no name defined for this parameter value
    */
   default Optional<String> name() {
      return Optional.empty();
   }

   /**
    * Returns {@code true} if parameter value is not {@code null}. Useful when testing parameter value in
    * {@link InvariantRule#ifIsSatisfied(BooleanSupplier)} for example.
    *
    * @return {@code true} if parameter value is not {@code null}
    */
   default boolean nonNull() {
      return Objects.nonNull(value());
   }

   /**
    * Creates a parameter from a lazily-evaluated value.
    *
    * @param value value supplier. Supplied value can be {@code null}
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P> ParameterValue<P> lazyValue(Supplier<? extends P> value) {
      notNull(value, "value");

      return value::get;
   }

   /** Alias to work-around import conflicts. */
   static <P> ParameterValue<P> pLazyValue(Supplier<? extends P> value) {
      return lazyValue(value);
   }

   /**
    * Creates a parameter from a lazily-evaluated value and a mapper.
    *
    * @param value value supplier. Supplied value can be {@code null}
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P0, P> ParameterValue<P> lazyValue(Supplier<? extends P0> value,
                                              Function<? super P0, ? extends P> valueMapper) {
      notNull(value, "value");
      notNull(valueMapper, "valueMapper");

      return () -> valueMapper.apply(value.get());
   }

   /** Alias to work-around import conflicts. */
   static <P0, P> ParameterValue<P> pLazyValue(Supplier<? extends P0> value,
                                               Function<? super P0, ? extends P> valueMapper) {
      return lazyValue(value, valueMapper);
   }

   /**
    * Creates a parameter from a simple value.
    *
    * @param value value. Value can be {@code null}
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P> ParameterValue<P> value(P value) {
      return () -> value;
   }

   /** Alias to work-around import conflicts. */
   static <P> ParameterValue<P> pValue(P value) {
      return value(value);
   }

   /**
    * Creates a parameter from a simple value and a mapper.
    *
    * @param value value. Value can be {@code null}
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P0, P> ParameterValue<P> value(P0 value, Function<? super P0, ? extends P> valueMapper) {
      return value(valueMapper.apply(value));
   }

   /** Alias to work-around import conflicts. */
   static <P0, P> ParameterValue<P> pValue(P0 value, Function<? super P0, ? extends P> valueMapper) {
      return value(value, valueMapper);
   }

   /**
    * Creates a named parameter from a lazily-evaluated value.
    *
    * @param value value supplier. Supplied value can be {@code null}
    * @param name parameter name
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P> ParameterValue<P> lazyValue(Supplier<? extends P> value, String name) {
      return lazyValue(value, identity(), name);
   }

   /** Alias to work-around import conflicts. */
   static <P> ParameterValue<P> pLazyValue(Supplier<? extends P> value, String name) {
      return lazyValue(value, name);
   }

   /**
    * Creates a named parameter from a lazily-evaluated value and a mapper.
    *
    * @param value value supplier. Supplied value can be {@code null}
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param name parameter name
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P0, P> ParameterValue<P> lazyValue(Supplier<? extends P0> value,
                                              Function<? super P0, ? extends P> valueMapper,
                                              String name) {
      notNull(value, "value");
      notNull(valueMapper, "valueMapper");
      notBlank(name, "name");

      return new ParameterValue<P>() {
         @Override
         public P value() {
            return valueMapper.apply(value.get());
         }

         @Override
         public Optional<String> name() {
            return Optional.of(name);
         }
      };
   }

   /** Alias to work-around import conflicts. */
   static <P0, P> ParameterValue<P> pLazyValue(Supplier<? extends P0> value,
                                               Function<? super P0, ? extends P> valueMapper,
                                               String name) {
      return lazyValue(value, valueMapper, name);
   }

   /**
    * Creates a named parameter from a simple value.
    *
    * @param value value. Value can be {@code null}
    * @param name parameter name
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P> ParameterValue<P> value(P value, String name) {
      return value(value, identity(), name);
   }

   /** Alias to work-around import conflicts. */
   static <P> ParameterValue<P> pValue(P value, String name) {
      return value(value, name);
   }

   /**
    * Creates a named parameter from a simple value and a mapper.
    *
    * @param value value. Value can be {@code null}
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param name parameter name
    * @param <P> parameter type
    *
    * @return parameter value
    */
   static <P0, P> ParameterValue<P> value(P0 value,
                                          Function<? super P0, ? extends P> valueMapper,
                                          String name) {
      notNull(valueMapper, "valueMapper");
      notBlank(name, "name");

      return new ParameterValue<P>() {
         @Override
         public P value() {
            return valueMapper.apply(value);
         }

         @Override
         public Optional<String> name() {
            return Optional.of(name);
         }
      };
   }

   /** Alias to work-around import conflicts. */
   static <P0, P> ParameterValue<P> pValue(P0 value,
                                           Function<? super P0, ? extends P> valueMapper,
                                           String name) {
      return value(value, valueMapper, name);
   }

}
