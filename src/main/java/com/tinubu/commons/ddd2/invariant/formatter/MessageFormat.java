/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.formatter;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Locale;

import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ValidatingObject;

/**
 * {@link java.text.MessageFormat#format}-based formatter. All arguments must be of type {@link MessageValue}.
 *
 * @param <T> validating object type.
 */
public class MessageFormat<T> extends AbstractMessageFormatter<T> implements MessageFormatter<T> {
   private final Locale locale;
   private final String pattern;
   private final MessageValue<T>[] values;

   private MessageFormat(Locale locale, String pattern, MessageValue<T>[] values) {
      this.locale = locale;
      this.pattern = notBlank(pattern, "pattern");
      this.values = notNull(values, "values");
   }

   @SafeVarargs
   public static <T> MessageFormat<T> of(Locale locale, String pattern, MessageValue<T>... values) {
      return new MessageFormat<>(locale, pattern, values);
   }

   @SafeVarargs
   public static <T> MessageFormat<T> of(String pattern, MessageValue<T>... values) {
      return new MessageFormat<>(InvariantConfiguration.instance().messageFormatterDefaultLocale(), pattern,
                                 values);
   }

   @Override
   public String format(ValidatingObject<T> validatingObject) {
      return new java.text.MessageFormat(pattern, locale).format(messageValues(values, validatingObject));
   }

}
