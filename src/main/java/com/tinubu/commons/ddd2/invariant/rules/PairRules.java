/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.lang.util.Pair;

public class PairRules {

   private PairRules() {
   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullKey(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(p -> p.getKey() != null, messageFormatter).ruleContext("PairRules.hasNoNullKey");
   }

   @SafeVarargs
   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullKey(final String message,
                                                                             final MessageValue<T>... values) {
      return hasNotNullKey(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullKey() {
      return hasNotNullKey(FastStringFormat.of("'", validatingObject(), "' must not have null key"));

   }

   public static <T extends Pair<K, V>, K extends CharSequence, V> InvariantRule<T> hasNotBlankKey(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(p -> StringUtils.isNotBlank(p.getKey()), messageFormatter).ruleContext(
            "PairRules.hasNotBlankKey");
   }

   @SafeVarargs
   public static <T extends Pair<K, V>, K extends CharSequence, V> InvariantRule<T> hasNotBlankKey(final String message,
                                                                                                   final MessageValue<T>... values) {
      return hasNotBlankKey(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Pair<K, V>, K extends CharSequence, V> InvariantRule<T> hasNotBlankKey() {
      return hasNotBlankKey(FastStringFormat.of("'", validatingObject(), "' must not have blank key"));

   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullValue(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(p -> p.getValue() != null, messageFormatter).ruleContext(
            "PairRules.hasNoNullValue");
   }

   @SafeVarargs
   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullValue(final String message,
                                                                               final MessageValue<T>... values) {
      return hasNotNullValue(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullValue() {
      return hasNotNullValue(FastStringFormat.of("'", validatingObject(), "' must not have null value"));

   }

   public static <T extends Pair<K, V>, K, V extends CharSequence> InvariantRule<T> hasNotBlankValue(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(p -> StringUtils.isNotBlank(p.getValue()), messageFormatter).ruleContext(
            "PairRules.hasNotBlankValue");
   }

   @SafeVarargs
   public static <T extends Pair<K, V>, K, V extends CharSequence> InvariantRule<T> hasNotBlankValue(final String message,
                                                                                                     final MessageValue<T>... values) {
      return hasNotBlankValue(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Pair<K, V>, K, V extends CharSequence> InvariantRule<T> hasNotBlankValue() {
      return hasNotBlankValue(FastStringFormat.of("'", validatingObject(), "' must not have blank value"));

   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullKeyValue(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(p -> p.getKey() != null && p.getValue() != null, messageFormatter).ruleContext(
            "PairRules.hasNoNullKeyValue");
   }

   @SafeVarargs
   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullKeyValue(final String message,
                                                                                  final MessageValue<T>... values) {
      return hasNotNullKeyValue(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> hasNotNullKeyValue() {
      return hasNotNullKeyValue(FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' must not have null key or null value"));

   }

   public static <T extends Pair<K, V>, K extends CharSequence, V extends CharSequence> InvariantRule<T> hasNotBlankKeyValue(
         final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(p -> StringUtils.isNotBlank(p.getKey()) && StringUtils.isNotBlank(p.getValue()),
                            messageFormatter).ruleContext("PairRules.hasNotBlankKeyValue");
   }

   @SafeVarargs
   public static <T extends Pair<K, V>, K extends CharSequence, V extends CharSequence> InvariantRule<T> hasNotBlankKeyValue(
         final String message,
         final MessageValue<T>... values) {
      return hasNotBlankKeyValue(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Pair<K, V>, K extends CharSequence, V extends CharSequence> InvariantRule<T> hasNotBlankKeyValue() {
      return hasNotBlankKeyValue(FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' must not have blank key or blank value"));

   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> key(final InvariantRule<? super K> rule) {
      return property(Pair::getKey, "key", rule);
   }

   public static <T extends Pair<K, V>, K, V> InvariantRule<T> value(final InvariantRule<? super V> rule) {
      return property(Pair::getValue, "value", rule);
   }

}
