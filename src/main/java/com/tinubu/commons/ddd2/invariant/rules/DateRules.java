/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;

import java.time.Instant;
import java.util.Date;

import com.tinubu.commons.ddd2.invariant.InvariantRule;

public class DateRules {

   private DateRules() {
   }

   /**
    * Applies an invariant to validating object date's instant.
    *
    * @param rule date's instant invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    *
    * @see TemporalRules to apply rules on date's instant
    */
   public static <T extends Date> InvariantRule<T> instant(final InvariantRule<? super Instant> rule) {
      return as(Date::toInstant, rule);
   }

}
