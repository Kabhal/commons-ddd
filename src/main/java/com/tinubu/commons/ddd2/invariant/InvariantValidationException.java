/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Arrays;
import java.util.List;

public class InvariantValidationException extends IllegalArgumentException {
   private final InvariantResults results;

   public InvariantValidationException(InvariantResults results) {
      super(errorMessage(notNull(results, "results")));

      this.results = results;
   }

   public InvariantValidationException(InvariantResult<?> result) {
      this(InvariantResults.of(result));
   }

   /**
    * Returns original invariant results.
    *
    * @return original invariant results
    */
   public InvariantResults invariantResults() {
      return this.results;
   }

   /**
    * Generates a new {@link InvariantValidationException} with updated context. You can use this method
    * when you intercept a validation exception and want to rethrow the exception with updated context.
    *
    * @param context supplementary context to add to this exception result's context
    *
    * @return new exception with updated context
    */
   public InvariantValidationException addContext(List<Object> context) {
      InvariantContext updatedContext;

      if (results.context() == null) {
         updatedContext = InvariantContext.of(context);
      } else {
         updatedContext = results.context().addContext(context);
      }

      return new InvariantValidationException(InvariantResults.of(updatedContext, results));
   }

   public InvariantValidationException addContext(Object... context) {
      return addContext(Arrays.asList(context));
   }

   public InvariantValidationException setContext(Object... context) {
      return new InvariantValidationException(InvariantResults.of(InvariantContext.of(context), results));
   }

   private static String errorMessage(InvariantResults results) {
      return results.errorMessage();
   }

}
