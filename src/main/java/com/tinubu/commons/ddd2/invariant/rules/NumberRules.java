/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

public class NumberRules {

   private NumberRules() {
   }

   public static <T extends Number> InvariantRule<T> isStrictlyNegative(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(value -> {
         if (value instanceof Short) {
            return (Short) value < 0;
         } else if (value instanceof Byte) {
            return (Byte) value < 0;
         } else if (value instanceof Integer) {
            return (Integer) value < 0;
         } else if (value instanceof Long) {
            return (Long) value < 0;
         } else if (value instanceof Float) {
            return ((Float) value) < 0.0f;
         } else if (value instanceof Double) {
            return ((Double) value) < 0.0f;
         } else if (value instanceof BigInteger) {
            return ((BigInteger) value).compareTo(BigInteger.ZERO) < 0;
         } else if (value instanceof BigDecimal) {
            return ((BigDecimal) value).compareTo(BigDecimal.ZERO) < 0;
         } else {
            throw new IllegalArgumentException(String.format("Unknown Number type : %s",
                                                             value.getClass().getName()));
         }
      }, messageFormatter).ruleContext("NumberRules.isStrictlyNegative");
   }

   @SafeVarargs
   public static <T extends Number> InvariantRule<T> isStrictlyNegative(final String message,
                                                                        final MessageValue<T>... values) {
      return isStrictlyNegative(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Number> InvariantRule<T> isStrictlyNegative() {
      return isStrictlyNegative(FastStringFormat.of("'", validatingObject(), "' must be strictly negative"));
   }

   public static <T extends Number> InvariantRule<T> isNegative(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(value -> {
         if (value instanceof Short) {
            return (Short) value <= 0;
         } else if (value instanceof Byte) {
            return (Byte) value <= 0;
         } else if (value instanceof Integer) {
            return (Integer) value <= 0;
         } else if (value instanceof Long) {
            return (Long) value <= 0;
         } else if (value instanceof Float) {
            return ((Float) value) <= 0.0f;
         } else if (value instanceof Double) {
            return ((Double) value) <= 0.0f;
         } else if (value instanceof BigInteger) {
            return ((BigInteger) value).compareTo(BigInteger.ZERO) <= 0;
         } else if (value instanceof BigDecimal) {
            return ((BigDecimal) value).compareTo(BigDecimal.ZERO) <= 0;
         } else {
            throw new IllegalArgumentException(String.format("Unknown Number type : %s",
                                                             value.getClass().getName()));
         }
      }, messageFormatter).ruleContext("NumberRules.isNegative");
   }

   @SafeVarargs
   public static <T extends Number> InvariantRule<T> isNegative(final String message,
                                                                final MessageValue<T>... values) {
      return isNegative(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Number> InvariantRule<T> isNegative() {
      return isNegative(FastStringFormat.of("'", validatingObject(), "' must be negative"));
   }

   public static <T extends Number> InvariantRule<T> isZero(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(value -> {
         if (value instanceof Short) {
            return (Short) value == 0;
         } else if (value instanceof Byte) {
            return (Byte) value == 0;
         } else if (value instanceof Integer) {
            return (Integer) value == 0;
         } else if (value instanceof Long) {
            return (Long) value == 0;
         } else if (value instanceof Float) {
            return ((Float) value) == 0.0f;
         } else if (value instanceof Double) {
            return ((Double) value) == 0.0f;
         } else if (value instanceof BigInteger) {
            return ((BigInteger) value).compareTo(BigInteger.ZERO) == 0;
         } else if (value instanceof BigDecimal) {
            return ((BigDecimal) value).compareTo(BigDecimal.ZERO) == 0;
         } else {
            throw new IllegalArgumentException(String.format("Unknown Number type : %s",
                                                             value.getClass().getName()));
         }
      }, messageFormatter).ruleContext("NumberRules.isZero");
   }

   @SafeVarargs
   public static <T extends Number> InvariantRule<T> isZero(final String message,
                                                            final MessageValue<T>... values) {
      return isZero(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Number> InvariantRule<T> isZero() {
      return isZero(FastStringFormat.of("'", validatingObject(), "' must be equal to zero"));
   }

   public static <T extends Number> InvariantRule<T> isPositive(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(value -> {
         if (value instanceof Short) {
            return (Short) value >= 0;
         } else if (value instanceof Byte) {
            return (Byte) value >= 0;
         } else if (value instanceof Integer) {
            return (Integer) value >= 0;
         } else if (value instanceof Long) {
            return (Long) value >= 0;
         } else if (value instanceof Float) {
            return ((Float) value) >= 0.0f;
         } else if (value instanceof Double) {
            return ((Double) value) >= 0.0f;
         } else if (value instanceof BigInteger) {
            return ((BigInteger) value).compareTo(BigInteger.ZERO) >= 0;
         } else if (value instanceof BigDecimal) {
            return ((BigDecimal) value).compareTo(BigDecimal.ZERO) >= 0;
         } else {
            throw new IllegalArgumentException(String.format("Unknown Number type : %s",
                                                             value.getClass().getName()));
         }
      }, messageFormatter).ruleContext("NumberRules.isPositive");
   }

   @SafeVarargs
   public static <T extends Number> InvariantRule<T> isPositive(final String message,
                                                                final MessageValue<T>... values) {
      return isPositive(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Number> InvariantRule<T> isPositive() {
      return isPositive(FastStringFormat.of("'", validatingObject(), "' must be positive"));
   }

   public static <T extends Number> InvariantRule<T> isStrictlyPositive(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(value -> {
         if (value instanceof Short) {
            return (Short) value > 0;
         } else if (value instanceof Byte) {
            return (Byte) value > 0;
         } else if (value instanceof Integer) {
            return (Integer) value > 0;
         } else if (value instanceof Long) {
            return (Long) value > 0;
         } else if (value instanceof Float) {
            return ((Float) value) > 0.0f;
         } else if (value instanceof Double) {
            return ((Double) value) > 0.0f;
         } else if (value instanceof BigInteger) {
            return ((BigInteger) value).compareTo(BigInteger.ZERO) > 0;
         } else if (value instanceof BigDecimal) {
            return ((BigDecimal) value).compareTo(BigDecimal.ZERO) > 0;
         } else {
            throw new IllegalArgumentException(String.format("Unknown Number type : %s",
                                                             value.getClass().getName()));
         }
      }, messageFormatter).ruleContext("NumberRules.isStrictlyPositive");
   }

   @SafeVarargs
   public static <T extends Number> InvariantRule<T> isStrictlyPositive(final String message,
                                                                        final MessageValue<T>... values) {
      return isStrictlyPositive(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Number> InvariantRule<T> isStrictlyPositive() {
      return isStrictlyPositive(FastStringFormat.of("'", validatingObject(), "' must be strictly positive"));
   }

}
