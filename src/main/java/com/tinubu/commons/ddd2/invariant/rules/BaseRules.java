/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.InvariantResult.error;
import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.propertyNameMapper;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesNullableValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

public class BaseRules {

   private BaseRules() {
   }

   public static <T> InvariantRule<T> isAlwaysValid(final MessageFormatter<T> messageFormatter) {
      return satisfiesNullableValue(__ -> true, messageFormatter).ruleContext("BaseRules.isAlwaysValid");
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isAlwaysValid(final String message, final MessageValue<T>... values) {
      return isAlwaysValid(DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isAlwaysValid() {
      return isAlwaysValid(FastStringFormat.of("'", validatingObject(), "' is always valid"));
   }

   public static <T> InvariantRule<T> isNeverValid(final MessageFormatter<T> messageFormatter) {
      return satisfiesNullableValue(__ -> false, messageFormatter).ruleContext("BaseRules.isNeverValid");
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isNeverValid(final String message, final MessageValue<T>... values) {
      return isNeverValid(DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isNeverValid() {
      return isNeverValid(FastStringFormat.of("'", validatingObject(), "' is never valid"));
   }

   public static <T> InvariantRule<T> isNull(final MessageFormatter<T> messageFormatter) {
      return satisfiesNullableValue(Objects::isNull, messageFormatter).ruleContext("BaseRules.isNull");
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isNull(final String message, final MessageValue<T>... values) {
      return isNull(DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isNull() {
      return isNull(FastStringFormat.of("'", validatingObject(), "' must be null"));
   }

   public static <T> InvariantRule<T> isNotNull(final MessageFormatter<T> messageFormatter) {
      return satisfiesNullableValue(Objects::nonNull, messageFormatter).ruleContext("BaseRules.isNotNull");
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isNotNull(final String message, final MessageValue<T>... values) {
      return isNotNull(DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isNotNull() {
      return isNotNull(FastStringFormat.of("'", validatingObjectName(), "' must not be null"));
   }

   public static InvariantRule<Boolean> isTrue(final MessageFormatter<Boolean> messageFormatter) {
      return satisfiesValue(Boolean.TRUE::equals, messageFormatter).ruleContext("BaseRules.isTrue");
   }

   @SafeVarargs
   public static InvariantRule<Boolean> isTrue(final String message, final MessageValue<Boolean>... values) {
      return isTrue(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Boolean> isTrue() {
      return isTrue(FastStringFormat.of("'", validatingObjectName(), "' must be true"));
   }

   public static InvariantRule<Boolean> isFalse(final MessageFormatter<Boolean> messageFormatter) {
      return satisfiesValue(Boolean.FALSE::equals, messageFormatter).ruleContext("BaseRules.isFalse");
   }

   @SafeVarargs
   public static InvariantRule<Boolean> isFalse(final String message, final MessageValue<Boolean>... values) {
      return isFalse(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Boolean> isFalse() {
      return isFalse(FastStringFormat.of("'", validatingObjectName(), "' must be false"));
   }

   public static <T> InvariantRule<T> isMutuallyExclusive(final ParameterValue<?> param,
                                                          final MessageFormatter<T> messageFormatter) {
      notNull(param, "param");

      return satisfiesNullableValue(v -> (Objects.nonNull(v) && !param.nonNull()) || (!Objects.nonNull(v)
                                                                                      && param.nonNull()),
                                    messageFormatter).ruleContext("BaseRules.isMutuallyExclusive", param);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isMutuallyExclusive(final ParameterValue<?> param,
                                                          final String message,
                                                          final MessageValue<T>... values) {
      return isMutuallyExclusive(param, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isMutuallyExclusive(final ParameterValue<?> param) {
      notNull(param, "param");

      return isMutuallyExclusive(param,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' and '",
                                                     parameter(param),
                                                     "' must be mutually exclusive"));
   }

   public static <T> InvariantRule<T> isMutuallyInclusive(final ParameterValue<?> param,
                                                          final MessageFormatter<T> messageFormatter) {
      notNull(param, "param");

      return satisfiesNullableValue(v -> (Objects.nonNull(v) && param.nonNull()) || (!Objects.nonNull(v)
                                                                                     && !param.nonNull()),
                                    messageFormatter).ruleContext("BaseRules.isMutuallyInclusive", param);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isMutuallyInclusive(final ParameterValue<?> param,
                                                          final String message,
                                                          final MessageValue<T>... values) {
      return isMutuallyInclusive(param, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isMutuallyInclusive(final ParameterValue<?> param) {
      notNull(param, "param");

      return isMutuallyInclusive(param,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' and '",
                                                     parameter(param),
                                                     "' must be mutually inclusive"));
   }

   /**
    * Maps specified invariant rule to a specified value mapped from validating object.
    *
    * @param valueMapper new value mapper to extract from validating object
    * @param nameMapper new name mapper to extract from validating object. Function argument can be
    *       {@code null} if name not set
    * @param rule invariant rule to apply to mapped value
    * @param <T> validating object type
    * @param <U> mapped object type
    *
    * @return invariant rule
    */
   public static <T, U> InvariantRule<T> as(final Function<? super T, ? extends U> valueMapper,
                                            final Function<? super String, String> nameMapper,
                                            final InvariantRule<U> rule) {
      notNull(valueMapper, "valueMapper");
      notNull(nameMapper, "nameMapper");
      notNull(rule, "rule");

      return isNotNull().andValue(rule.map(valueMapper, nameMapper));
   }

   /**
    * Maps specified invariant rule to a specified value mapped from validating object. Validating object name
    * is not mapped.
    *
    * @param valueMapper new value mapper to extract from validating object
    * @param rule invariant rule to apply to mapped value
    * @param <T> validating object type
    * @param <U> mapped object type
    *
    * @return invariant rule
    */
   public static <T, U> InvariantRule<T> as(final Function<? super T, ? extends U> valueMapper,
                                            final InvariantRule<U> rule) {
      return as(valueMapper, Function.identity(), rule);
   }

   /**
    * Maps specified invariant rule to a specified property of validating object.
    *
    * @param valueMapper property value mapper to extract from validating object
    * @param propertyName property name
    * @param rule invariant rule to apply to property value
    * @param <T> validating object type
    * @param <U> mapped object type
    *
    * @return invariant rule
    */
   public static <T, U> InvariantRule<T> property(final Function<? super T, ? extends U> valueMapper,
                                                  final String propertyName,
                                                  final InvariantRule<U> rule) {
      notNull(valueMapper, "valueMapper");
      notBlank(propertyName, "propertyName");
      notNull(rule, "rule");

      return as(valueMapper, propertyNameMapper(propertyName), rule);
   }

   /**
    * Maps specified invariant rule to a specified property of validating object.
    * Property value is introspected from the field identified by the specified {@code propertyName}.
    *
    * @param propertyName property name
    * @param rule invariant rule to apply to property value
    * @param <T> validating object type
    * @param <U> mapped object type
    *
    * @return invariant rule
    */
   public static <T, U> InvariantRule<T> property(final String propertyName, final InvariantRule<U> rule) {
      notBlank(propertyName, "propertyName");
      notNull(rule, "rule");

      return as(BaseRules.<T, U>introspectValidatingObjectPropertyValue(propertyName),
                propertyNameMapper(propertyName),
                rule);
   }

   /**
    * Maps specified invariant rule to validating object converted to string using {@link Object#toString}.
    *
    * @param rule invariant rule
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <T> InvariantRule<T> string(final InvariantRule<? super CharSequence> rule) {
      return as(Object::toString, rule);
   }

   public static <T, X extends Exception> InvariantRule<T> throwing(final InvariantRule<T> rule,
                                                                    final Class<? extends X> exceptionType,
                                                                    final Function<? super X, ? extends MessageFormatter<T>> messageFormatter) {
      notNull(rule, "rule");

      return v -> {
         try {
            return rule.evaluate(v);
         } catch (Exception e) {
            if (exceptionType.isAssignableFrom(e.getClass())) {
               Function<? super X, ? extends MessageFormatter<T>> mf =
                     nullable(messageFormatter, BaseRules::<T, X>throwingErrorMessage);

               return error(v, mf.apply(exceptionType.cast(e)));
            } else {
               throw e;
            }
         }
      };
   }

   public static <T, X extends Exception> InvariantRule<T> throwing(final InvariantRule<T> rule,
                                                                    final Class<? extends X> exceptionType,
                                                                    final Function<? super Exception, String> message,
                                                                    final MessageValue<T>... values) {
      return throwing(rule, exceptionType, e -> DefaultMessageFormat.of(message.apply(e), values));
   }

   public static <T, X extends Exception> InvariantRule<T> throwing(final InvariantRule<T> rule,
                                                                    final Class<? extends X> exceptionType) {
      return throwing(rule, exceptionType, null);
   }

   public static <T> InvariantRule<T> throwing(final InvariantRule<T> rule,
                                               final Function<? super Exception, ? extends MessageFormatter<T>> messageFormatter) {
      return throwing(rule, Exception.class, messageFormatter);
   }

   public static <T> InvariantRule<T> throwing(final InvariantRule<T> rule,
                                               final Function<? super Exception, String> message,
                                               final MessageValue<T>... values) {
      return throwing(rule, Exception.class, message, values);
   }

   public static <T> InvariantRule<T> throwing(final InvariantRule<T> rule) {
      return throwing(rule, Exception.class, null);
   }

   public static <T> InvariantRule<T> throwing(final InvariantRule<T> rule,
                                               final MessageFormatter<T> messageFormatter) {
      return throwing(rule, Exception.class, __ -> messageFormatter);
   }

   public static <T> InvariantRule<T> throwing(final InvariantRule<T> rule,
                                               final String message,
                                               final MessageValue<T>... values) {
      return throwing(rule, Exception.class, __ -> message, values);
   }

   private static <T, X extends Exception> Function<? super X, ? extends MessageFormatter<T>> throwingErrorMessage() {
      return e -> {
         String errorMessage = e.getMessage();

         if (errorMessage == null || StringUtils.isBlank(errorMessage)) {
            return __ -> e.getClass().getSimpleName();
         } else {
            return __ -> errorMessage;
         }
      };
   }

   @SuppressWarnings("unchecked")
   private static <T, U> Function<? super T, ? extends U> introspectValidatingObjectPropertyValue(String propertyName) {
      notBlank(propertyName, "propertyName");

      return validatingObjectValue -> {
         try {
            final Field field = validatingObjectValue.getClass().getDeclaredField(propertyName);
            AccessibleObject.setAccessible(new Field[] { field }, true);
            return (U) field.get(validatingObjectValue);
         } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException(String.format("Unknown '%s' property in '%s'",
                                                             propertyName,
                                                             validatingObjectValue.getClass().getName()));
         } catch (IllegalAccessException e) {
            throw new IllegalStateException(e);
         }
      };
   }

}