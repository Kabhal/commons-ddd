/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

// FIXME Throws error if an element in a Collection/Set is null ?
public class MapRules {

   private MapRules() {
   }

   public static <T extends Map<?, ?>> InvariantRule<T> isNotEmpty(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Map::isEmpty, messageFormatter).ruleContext("MapRules.isNotEmpty");
   }

   @SafeVarargs
   public static <T extends Map<?, ?>> InvariantRule<T> isNotEmpty(final String message,
                                                                   final MessageValue<T>... values) {
      return isNotEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map<?, ?>> InvariantRule<T> isNotEmpty() {
      return isNotEmpty(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullElements(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(m -> m.containsKey(null) || m.containsValue(null),
                               messageFormatter).ruleContext("MapRules.hasNoNullElements");
   }

   @SafeVarargs
   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullElements(final String message,
                                                                          final MessageValue<T>... values) {
      return hasNoNullElements(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullElements() {
      return hasNoNullElements(FastStringFormat.of("'",
                                                   validatingObjectName(),
                                                   "' must not have null key or null value"));
   }

   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullKeys(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(m -> m.containsKey(null), messageFormatter).ruleContext(
            "MapRules.hasNoNullKeys");
   }

   @SafeVarargs
   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullKeys(final String message,
                                                                      final MessageValue<T>... values) {
      return hasNoNullKeys(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullKeys() {
      return hasNoNullKeys(FastStringFormat.of("'", validatingObjectName(), "' must not have null key"));
   }

   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullValues(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(m -> m.containsValue(null), messageFormatter).ruleContext(
            "MapRules.hasNoNullValues");
   }

   @SafeVarargs
   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullValues(final String message,
                                                                        final MessageValue<T>... values) {
      return hasNoNullValues(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map<?, ?>> InvariantRule<T> hasNoNullValues() {
      return hasNoNullValues(FastStringFormat.of("'", validatingObjectName(), "' must not have null value"));
   }

   public static <T extends Map.Entry<?, ?>> InvariantRule<T> hasNoNullKey(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> v.getKey() != null, messageFormatter).ruleContext("MapRules.hasNoNullKey");
   }

   @SafeVarargs
   public static <T extends Map.Entry<?, ?>> InvariantRule<T> hasNoNullKey(final String message,
                                                                           final MessageValue<T>... values) {
      return hasNoNullKey(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map.Entry<?, ?>> InvariantRule<T> hasNoNullKey() {
      return hasNoNullKey(FastStringFormat.of("'", validatingObjectName(), "' must not have null key"));
   }

   public static <T extends Map.Entry<String, ?>> InvariantRule<T> hasNoBlankKey(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> !StringUtils.isBlank(v.getKey()), messageFormatter).ruleContext(
            "MapRules.hasNoBlankKey");
   }

   @SafeVarargs
   public static <T extends Map.Entry<String, ?>> InvariantRule<T> hasNoBlankKey(final String message,
                                                                                 final MessageValue<T>... values) {
      return hasNoBlankKey(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map.Entry<String, ?>> InvariantRule<T> hasNoBlankKey() {
      return hasNoBlankKey(FastStringFormat.of("'", validatingObjectName(), "' must not have blank key"));
   }

   public static <T extends Map.Entry<?, ?>> InvariantRule<T> hasNoNullValue(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> v.getValue() != null,
                            messageFormatter).ruleContext("MapRules.hasNoNullValue");
   }

   @SafeVarargs
   public static <T extends Map.Entry<?, ?>> InvariantRule<T> hasNoNullValue(final String message,
                                                                             final MessageValue<T>... values) {
      return hasNoNullValue(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map.Entry<?, ?>> InvariantRule<T> hasNoNullValue() {
      return hasNoNullValue(FastStringFormat.of("'", validatingObjectName(), "' must not have null value"));
   }

   public static <T extends Map.Entry<?, String>> InvariantRule<T> hasNoBlankValue(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> !StringUtils.isBlank(v.getValue()), messageFormatter).ruleContext(
            "MapRules.hasNoBlankValue");
   }

   @SafeVarargs
   public static <T extends Map.Entry<?, String>> InvariantRule<T> hasNoBlankValue(final String message,
                                                                                   final MessageValue<T>... values) {
      return hasNoBlankValue(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Map.Entry<?, String>> InvariantRule<T> hasNoBlankValue() {
      return hasNoBlankValue(FastStringFormat.of("'", validatingObjectName(), "' must not have blank value"));
   }

   /**
    * Applies an invariant to validating object map's size.
    *
    * @param rule map's size invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <T extends Map<?, ?>> InvariantRule<T> size(final InvariantRule<? super Integer> rule) {
      return property(Map::size, "size", rule);
   }

   /**
    * Applies an invariant to validating object map's keys.
    *
    * @param rule map's keys invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <K, T extends Map<K, ?>> InvariantRule<T> keys(final InvariantRule<? super Set<K>> rule) {
      return property(Map::keySet, "keys", rule);
   }

   public static <K, T extends Map.Entry<K, ?>> InvariantRule<T> key(final InvariantRule<? super K> rule) {
      return property(Map.Entry::getKey, "key", rule);
   }

   /**
    * Applies an invariant to validating object map's values.
    *
    * @param rule map's values invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <V, T extends Map<?, V>> InvariantRule<T> values(final InvariantRule<? super Collection<V>> rule) {
      return property(Map::values, "values", rule);
   }

   public static <V, T extends Map.Entry<?, V>> InvariantRule<T> value(final InvariantRule<? super V> rule) {
      return property(Map.Entry::getValue, "value", rule);
   }

   /**
    * Applies an invariant to validating object map's entry set.
    *
    * @param rule map's values invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <K, V, T extends Map<K, V>> InvariantRule<T> entrySet(final InvariantRule<? super Set<Map.Entry<K, V>>> rule) {
      return property(Map::entrySet, "entrySet", rule);
   }

}
