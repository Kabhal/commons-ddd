/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules.domain.ids;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.instanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.requiresValue;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;
import java.util.Optional;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.Fragment;
import com.tinubu.commons.ddd2.uri.ComponentUri.Host;
import com.tinubu.commons.ddd2.uri.ComponentUri.Port;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;
import com.tinubu.commons.ddd2.uri.ComponentUri.Scheme;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;

// FIXME tests
public class UriRules {

   private UriRules() {
   }

   public static <T extends Uri> InvariantRule<T> isAbsolute(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isAbsolute, messageFormatter).ruleContext("UriRules.isAbsolute");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isAbsolute(final String message,
                                                             final MessageValue<T>... values) {
      return isAbsolute(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isAbsolute() {
      return isAbsolute(FastStringFormat.of("'", validatingObject(), "' must be absolute"));
   }

   public static <T extends Uri> InvariantRule<T> isAbsolute(final ParameterValue<String> scheme,
                                                             final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.isAbsolute() && schemeIsEqualTo(uri, scheme),
                            messageFormatter).ruleContext("UriRules.isAbsoluteForScheme", scheme);
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isAbsolute(final ParameterValue<String> scheme,
                                                             final String message,
                                                             final MessageValue<T>... values) {
      return isAbsolute(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isAbsolute(final ParameterValue<String> scheme) {
      return isAbsolute(scheme,
                        FastStringFormat.of("'",
                                            validatingObject(),
                                            "' must be absolute, and scheme must be equal to '",
                                            parameter(0),
                                            "'"));
   }

   public static <T extends Uri> InvariantRule<T> isRelative(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isRelative, messageFormatter).ruleContext("UriRules.isRelative");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isRelative(final String message,
                                                             final MessageValue<T>... values) {
      return isRelative(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isRelative() {
      return isRelative(FastStringFormat.of("'", validatingObject(), "' must be relative"));
   }

   public static <T extends Uri> InvariantRule<T> isOpaque(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isOpaque, messageFormatter).ruleContext("UriRules.isOpaque");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isOpaque(final String message,
                                                           final MessageValue<T>... values) {
      return isOpaque(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isOpaque() {
      return isOpaque(FastStringFormat.of("'", validatingObject(), "' must be opaque"));
   }

   public static <T extends Uri> InvariantRule<T> isOpaque(final ParameterValue<String> scheme,
                                                           final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.isOpaque() && schemeIsEqualTo(uri, scheme),
                            messageFormatter).ruleContext("UriRules.isOpaqueForScheme", scheme);
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isOpaque(final ParameterValue<String> scheme,
                                                           final String message,
                                                           final MessageValue<T>... values) {
      return isOpaque(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isOpaque(final ParameterValue<String> scheme) {
      return isOpaque(scheme,
                      FastStringFormat.of("'",
                                          validatingObject(),
                                          "' must be opaque, and scheme must be equal to '",
                                          parameter(0),
                                          "'"));
   }

   public static <T extends Uri> InvariantRule<T> isNotOpaque(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isOpaque, messageFormatter).ruleContext("UriRules.isNotOpaque");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isNotOpaque(final String message,
                                                              final MessageValue<T>... values) {
      return isNotOpaque(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isNotOpaque() {
      return isNotOpaque(FastStringFormat.of("'", validatingObject(), "' must not be opaque"));
   }

   public static <T extends Uri> InvariantRule<T> isHierarchical(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isHierarchical, messageFormatter).ruleContext("UriRules.isHierarchical");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isHierarchical(final String message,
                                                                 final MessageValue<T>... values) {
      return isHierarchical(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isHierarchical() {
      return isHierarchical(FastStringFormat.of("'", validatingObject(), "' must be hierarchical"));
   }

   public static <T extends Uri> InvariantRule<T> isNotHierarchical(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isHierarchical,
                               messageFormatter).ruleContext("UriRules.isNotHierarchical");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isNotHierarchical(final String message,
                                                                    final MessageValue<T>... values) {
      return isNotHierarchical(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isNotHierarchical() {
      return isNotHierarchical(FastStringFormat.of("'", validatingObject(), "' must not be hierarchical"));
   }

   public static <T extends Uri> InvariantRule<T> isEmptyHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isEmptyHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isEmptyHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isEmptyHierarchicalServer(final String message,
                                                                            final MessageValue<T>... values) {
      return isEmptyHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isEmptyHierarchicalServer() {
      return isEmptyHierarchicalServer(FastStringFormat.of("'",
                                                           validatingObject(),
                                                           "' must be empty hierarchical server-based"));
   }

   public static <T extends Uri> InvariantRule<T> isNotEmptyHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isEmptyHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isNotEmptyHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isNotEmptyHierarchicalServer(final String message,
                                                                               final MessageValue<T>... values) {
      return isNotEmptyHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isNotEmptyHierarchicalServer() {
      return isNotEmptyHierarchicalServer(FastStringFormat.of("'",
                                                              validatingObject(),
                                                              "' must not be empty hierarchical server-based"));
   }

   public static <T extends Uri> InvariantRule<T> isHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isHierarchicalServer(final String message,
                                                                       final MessageValue<T>... values) {
      return isHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isHierarchicalServer() {
      return isHierarchicalServer(FastStringFormat.of("'",
                                                      validatingObject(),
                                                      "' must be hierarchical server-based"));
   }

   public static <T extends Uri> InvariantRule<T> isNotHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isNotHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isNotHierarchicalServer(final String message,
                                                                          final MessageValue<T>... values) {
      return isNotHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isNotHierarchicalServer() {
      return isNotHierarchicalServer(FastStringFormat.of("'",
                                                         validatingObject(),
                                                         "' must not be hierarchical server-based"));
   }

   public static <T extends Uri> InvariantRule<T> isHierarchicalRegistry(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isHierarchicalRegistry, messageFormatter).ruleContext(
            "UriRules.isHierarchicalRegistry");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isHierarchicalRegistry(final String message,
                                                                         final MessageValue<T>... values) {
      return isHierarchicalRegistry(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isHierarchicalRegistry() {
      return isHierarchicalRegistry(FastStringFormat.of("'",
                                                        validatingObject(),
                                                        "' must be hierarchical registry-based"));
   }

   public static <T extends Uri> InvariantRule<T> isNotHierarchicalRegistry(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isHierarchicalRegistry, messageFormatter).ruleContext(
            "UriRules.isNotHierarchicalRegistry");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> isNotHierarchicalRegistry(final String message,
                                                                            final MessageValue<T>... values) {
      return isNotHierarchicalRegistry(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> isNotHierarchicalRegistry() {
      return isNotHierarchicalRegistry(FastStringFormat.of("'",
                                                           validatingObject(),
                                                           "' must not be hierarchical registry-based"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoTraversal(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(path -> {
         return path.path(false).map(Path::of).map(parsePath -> {
            for (int i = 0; i < parsePath.getNameCount(); i++) {
               if (parsePath.getName(i).toString().equals("..")) {
                  return false;
               }
            }
            return true;
         }).orElse(true);
      }, messageFormatter).ruleContext("UriRules.hasNoTraversal");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoTraversal(final String message,
                                                                 final MessageValue<T>... values) {
      return hasNoTraversal(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoTraversal() {
      return hasNoTraversal(FastStringFormat.of("'", validatingObject(), "' must not have traversal paths"));
   }

   public static <T extends Uri, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme,
                                                                                    final MessageFormatter<T> messageFormatter) {
      notNull(scheme, "scheme");

      return isAbsolute().andValue(satisfiesValue(uri -> schemeIsEqualTo(uri, scheme),
                                                  messageFormatter).ruleContext("UriRules.schemeIsEqualTo",
                                                                                scheme));
   }

   @SafeVarargs
   public static <T extends Uri, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme,
                                                                                    final String message,
                                                                                    final MessageValue<T>... values) {
      return schemeIsEqualTo(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme) {
      return schemeIsEqualTo(scheme,
                             FastStringFormat.of("'",
                                                 validatingObject(),
                                                 "' scheme must be equal to '",
                                                 parameter(0),
                                                 "'"));
   }

   private static <T extends Uri, U extends String> boolean schemeIsEqualTo(T uri, ParameterValue<U> scheme) {
      return uri.scheme().map(s -> s.equalsIgnoreCase(scheme.requireNonNullValue())).orElse(false);
   }

   public static <T extends Uri, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme,
                                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(scheme, "scheme");

      return isAbsolute().andValue(satisfiesValue(uri -> !schemeIsEqualTo(uri, scheme),
                                                  messageFormatter).ruleContext("UriRules.schemeIsNotEqualTo",
                                                                                scheme));
   }

   @SafeVarargs
   public static <T extends Uri, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
      return schemeIsNotEqualTo(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme) {
      return schemeIsNotEqualTo(scheme,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' scheme must not be equal to '",
                                                    parameter(0),
                                                    "'"));
   }

   public static <T extends Uri> InvariantRule<T> hasAuthority(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.authority(true).isPresent(), messageFormatter).ruleContext(
            "UriRules.hasAuthority");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasAuthority(final String message,
                                                               final MessageValue<T>... values) {
      return hasAuthority(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasAuthority() {
      return hasAuthority(FastStringFormat.of("'", validatingObject(), "' must have authority"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoAuthority(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(uri -> uri.authority(true).isPresent(), messageFormatter).ruleContext(
            "UriRules.hasNoAuthority");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoAuthority(final String message,
                                                                 final MessageValue<T>... values) {
      return hasNoAuthority(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoAuthority() {
      return hasNoAuthority(FastStringFormat.of("'", validatingObject(), "' must not have authority"));
   }

   public static <T extends Uri> InvariantRule<T> hasUserInfo(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.userInfo(true).isPresent(), messageFormatter).ruleContext(
            "UriRules.hasUserInfo");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasUserInfo(final String message,
                                                              final MessageValue<T>... values) {
      return hasUserInfo(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasUserInfo() {
      return hasUserInfo(FastStringFormat.of("'", validatingObject(), "' must have user info"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoUserInfo(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.userInfo(true).isEmpty(), messageFormatter).ruleContext(
            "UriRules.hasNoUserInfo");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoUserInfo(final String message,
                                                                final MessageValue<T>... values) {
      return hasNoUserInfo(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoUserInfo() {
      return hasNoUserInfo(FastStringFormat.of("'", validatingObject(), "' must not have user info"));
   }

   public static <T extends Uri> InvariantRule<T> hasPassword(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.userInfo(true).map(ui -> ui.indexOf(':') >= 0).orElse(false),
                            messageFormatter).ruleContext("UriRules.hasPassword");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasPassword(final String message,
                                                              final MessageValue<T>... values) {
      return hasPassword(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasPassword() {
      return hasPassword(FastStringFormat.of("'", validatingObject(), "' must have password"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoPassword(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.userInfo(true).map(ui -> ui.indexOf(':') == -1).orElse(true),
                            messageFormatter).ruleContext("UriRules.hasNoPassword");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoPassword(final String message,
                                                                final MessageValue<T>... values) {
      return hasNoPassword(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoPassword() {
      return hasNoPassword(FastStringFormat.of("'", validatingObject(), "' must not have password"));
   }

   public static <T extends Uri> InvariantRule<T> hasHost(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.host().isPresent(), messageFormatter).ruleContext("UriRules.hasHost");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasHost(final String message,
                                                          final MessageValue<T>... values) {
      return hasHost(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasHost() {
      return hasHost(FastStringFormat.of("'", validatingObject(), "' must have a host"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoHost(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.host().isEmpty(), messageFormatter).ruleContext("UriRules.hasNoHost");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoHost(final String message,
                                                            final MessageValue<T>... values) {
      return hasNoHost(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoHost() {
      return hasNoHost(FastStringFormat.of("'", validatingObject(), "' must not have a host"));
   }

   public static <T extends Uri> InvariantRule<T> hasPort(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.port().isPresent(), messageFormatter).ruleContext("UriRules.hasPort");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasPort(final String message,
                                                          final MessageValue<T>... values) {
      return hasPort(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasPort() {
      return hasPort(FastStringFormat.of("'", validatingObject(), "' must have a port"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoPort(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.port().isEmpty(), messageFormatter).ruleContext("UriRules.hasNoPort");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoPort(final String message,
                                                            final MessageValue<T>... values) {
      return hasNoPort(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoPort() {
      return hasNoPort(FastStringFormat.of("'", validatingObject(), "' must not have a port"));
   }

   public static <T extends Uri> InvariantRule<T> hasPath(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.path(true).isPresent(), messageFormatter).ruleContext(
            "UriRules.hasPath");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasPath(final String message,
                                                          final MessageValue<T>... values) {
      return hasPath(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasPath() {
      return hasPath(FastStringFormat.of("'", validatingObject(), "' must have a path"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoPath(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.path(true).isEmpty(), messageFormatter).ruleContext(
            "UriRules.hasNoPath");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoPath(final String message,
                                                            final MessageValue<T>... values) {
      return hasNoPath(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoPath() {
      return hasNoPath(FastStringFormat.of("'", validatingObject(), "' must not have a path"));
   }

   public static <T extends Uri> InvariantRule<T> hasQuery(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.query(true).isPresent(), messageFormatter).ruleContext(
            "UriRules.hasQuery");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasQuery(final String message,
                                                           final MessageValue<T>... values) {
      return hasQuery(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasQuery() {
      return hasQuery(FastStringFormat.of("'", validatingObject(), "' must have a query"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoQuery(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.query(true).isEmpty(), messageFormatter).ruleContext(
            "UriRules.hasNoQuery");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoQuery(final String message,
                                                             final MessageValue<T>... values) {
      return hasNoQuery(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoQuery() {
      return hasNoQuery(FastStringFormat.of("'", validatingObject(), "' must not have a query"));
   }

   public static <T extends Uri> InvariantRule<T> hasFragment(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.fragment(true).isPresent(), messageFormatter).ruleContext(
            "UriRules.hasFragment");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasFragment(final String message,
                                                              final MessageValue<T>... values) {
      return hasFragment(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasFragment() {
      return hasFragment(FastStringFormat.of("'", validatingObject(), "' must have a fragment"));
   }

   public static <T extends Uri> InvariantRule<T> hasNoFragment(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.fragment(true).isEmpty(), messageFormatter).ruleContext(
            "UriRules.hasNoFragment");
   }

   @SafeVarargs
   public static <T extends Uri> InvariantRule<T> hasNoFragment(final String message,
                                                                final MessageValue<T>... values) {
      return hasNoFragment(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Uri> InvariantRule<T> hasNoFragment() {
      return hasNoFragment(FastStringFormat.of("'", validatingObject(), "' must not have a fragment"));
   }

   public static <T extends Uri> InvariantRule<T> normalized(final InvariantRule<? super Uri> rule) {
      return as(Uri::normalize, rule);
   }

   public static <T extends Uri> InvariantRule<T> scheme(final InvariantRule<? super String> rule) {
      return isAbsolute().andValue(property(Uri::scheme, "scheme", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalScheme(final InvariantRule<? super String> rule) {
      return property(Uri::scheme, "scheme", optionalValue(rule));
   }

   public static <T extends Uri> InvariantRule<T> userInfo(final InvariantRule<? super String> rule) {
      return hasUserInfo().andValue(property(uri -> uri.userInfo(false), "userInfo", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalUserInfo(final InvariantRule<? super String> rule) {
      return property(uri -> uri.userInfo(false), "userInfo", optionalValue(rule));
   }

   public static <T extends Uri> InvariantRule<T> username(final InvariantRule<? super String> rule) {
      return userInfo(property(UriRules::username, "username", rule));
   }

   public static <T extends Uri> InvariantRule<T> optionalUsername(final InvariantRule<? super String> rule) {
      return optionalUserInfo(property(UriRules::username, "username", rule));
   }

   public static <T extends Uri> InvariantRule<T> password(final InvariantRule<? super String> rule) {
      return userInfo(property(UriRules::password, "password", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalPassword(final InvariantRule<? super String> rule) {
      return optionalUserInfo(property(UriRules::password, "password", optionalValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> host(final InvariantRule<? super String> rule) {
      return hasHost().andValue(property(Uri::host, "host", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalHost(final InvariantRule<? super String> rule) {
      return property(Uri::host, "host", optionalValue(rule));
   }

   public static <T extends Uri> InvariantRule<T> port(final InvariantRule<? super Integer> rule) {
      return hasPort().andValue(property(Uri::port, "port", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalPort(final InvariantRule<? super Integer> rule) {
      return property(Uri::port, "port", optionalValue(rule));
   }

   public static <T extends Uri> InvariantRule<T> path(final InvariantRule<? super String> rule) {
      return hasPath().andValue(property(uri -> uri.path(false), "path", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalPath(final InvariantRule<? super String> rule) {
      return property(uri -> uri.path(false), "path", optionalValue(rule));
   }

   public static <T extends Uri> InvariantRule<T> query(final InvariantRule<? super String> rule) {
      return hasQuery().andValue(property(uri -> uri.query(false), "query", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalQuery(final InvariantRule<? super String> rule) {
      return property(uri -> uri.query(false), "query", optionalValue(rule));
   }

   public static <T extends Uri> InvariantRule<T> fragment(final InvariantRule<? super String> rule) {
      return hasFragment().andValue(property(uri -> uri.fragment(false), "fragment", requiresValue(rule)));
   }

   public static <T extends Uri> InvariantRule<T> optionalFragment(final InvariantRule<? super String> rule) {
      return property(uri -> uri.fragment(false), "fragment", optionalValue(rule));
   }

   static String username(String userInfo) {
      var sepIdx = userInfo.indexOf(':');
      if (sepIdx == -1) {
         return userInfo;
      } else {
         return userInfo.substring(0, sepIdx);
      }
   }

   static Optional<String> password(String userInfo) {
      var sepIdx = userInfo.indexOf(':');
      if (sepIdx == -1) {
         return optional();
      } else {
         return optional(userInfo.substring(sepIdx + 1));
      }
   }

   public static class ComponentUriRules {

      public static <T extends ComponentUri> InvariantRule<T> scheme(final InvariantRule<? super Scheme> rule) {
         return isAbsolute().andValue(property(uri -> uri.component().scheme(),
                                               "scheme",
                                               requiresValue(rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalScheme(final InvariantRule<? super Scheme> rule) {
         return property(uri -> uri.component().scheme(), "scheme", optionalValue(rule));
      }

      public static <T extends ComponentUri> InvariantRule<T> userInfo(final InvariantRule<? super UserInfo> rule) {
         return hasUserInfo().andValue(property(uri -> uri.component().userInfo(),
                                                "userInfo",
                                                requiresValue(rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalUserInfo(final InvariantRule<? super UserInfo> rule) {
         return property(uri -> uri.component().userInfo(), "userInfo", optionalValue(rule));
      }

      public static <T extends ComponentUri> InvariantRule<T> username(final InvariantRule<? super String> rule) {
         return userInfo(instanceOf(UsernamePasswordUserInfo.class,
                                    property(ui -> ui.username(false), "username", rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalUsername(final InvariantRule<? super String> rule) {
         return optionalUserInfo(instanceOf(UsernamePasswordUserInfo.class,
                                            property(ui -> ui.username(false), "username", rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> password(final InvariantRule<? super String> rule) {
         return userInfo(instanceOf(UsernamePasswordUserInfo.class,
                                    property(ui -> ui.password(false), "password", requiresValue(rule))));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalPassword(final InvariantRule<? super String> rule) {
         return optionalUserInfo(instanceOf(UsernamePasswordUserInfo.class,
                                            property(ui -> ui.password(false),
                                                     "password",
                                                     optionalValue(rule))));
      }

      public static <T extends ComponentUri> InvariantRule<T> host(final InvariantRule<? super Host> rule) {
         return hasHost().andValue(property(uri -> uri.component().host(), "host", requiresValue(rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalHost(final InvariantRule<? super Host> rule) {
         return property(uri -> uri.component().host(), "host", optionalValue(rule));
      }

      public static <T extends ComponentUri> InvariantRule<T> port(final InvariantRule<? super Port> rule) {
         return hasPort().andValue(property(uri -> uri.component().port(), "port", requiresValue(rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalPort(final InvariantRule<? super Port> rule) {
         return property(uri -> uri.component().port(), "port", optionalValue(rule));
      }

      public static <T extends ComponentUri> InvariantRule<T> path(final InvariantRule<? super ComponentUri.Path> rule) {
         return hasPath().andValue(property(uri -> uri.component().path(), "path", requiresValue(rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalPath(final InvariantRule<? super ComponentUri.Path> rule) {
         return property(uri -> uri.component().path(), "path", optionalValue(rule));
      }

      public static <T extends ComponentUri> InvariantRule<T> query(final InvariantRule<? super Query> rule) {
         return hasQuery().andValue(property(uri -> uri.component().query(), "query", requiresValue(rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalQuery(final InvariantRule<? super Query> rule) {
         return property(uri -> uri.component().query(), "query", optionalValue(rule));
      }

      public static <T extends ComponentUri> InvariantRule<T> fragment(final InvariantRule<? super Fragment> rule) {
         return hasFragment().andValue(property(uri -> uri.component().fragment(),
                                                "fragment",
                                                requiresValue(rule)));
      }

      public static <T extends ComponentUri> InvariantRule<T> optionalFragment(final InvariantRule<? super Fragment> rule) {
         return property(uri -> uri.component().fragment(), "fragment", optionalValue(rule));
      }

      public static class SchemeRules {

         public static <T extends Scheme, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme,
                                                                                             final MessageFormatter<T> messageFormatter) {
            notNull(scheme, "scheme");

            return satisfiesValue(s -> schemeIsEqualTo(s, scheme), messageFormatter).ruleContext(
                  "UriRules.schemeIsEqualTo",
                  scheme);
         }

         private static <T extends Scheme, U extends String> boolean schemeIsEqualTo(T schemeComponent,
                                                                                     ParameterValue<U> scheme) {
            return schemeComponent.value().equalsIgnoreCase(scheme.requireNonNullValue());
         }

         @SafeVarargs
         public static <T extends Scheme, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme,
                                                                                             final String message,
                                                                                             final MessageValue<T>... values) {
            return schemeIsEqualTo(scheme, DefaultMessageFormat.of(message, values));
         }

         public static <T extends Scheme, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme) {
            return schemeIsEqualTo(scheme,
                                   FastStringFormat.of("'",
                                                       validatingObject(),
                                                       "' scheme must be equal to '",
                                                       parameter(0),
                                                       "'"));
         }

         public static <T extends Scheme, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme,
                                                                                                final MessageFormatter<T> messageFormatter) {
            notNull(scheme, "scheme");

            return satisfiesValue(s -> !schemeIsEqualTo(s, scheme), messageFormatter).ruleContext(
                  "ComponentUriRules.SchemeRules.schemeIsNotEqualTo",
                  scheme);
         }

         @SafeVarargs
         public static <T extends Scheme, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme,
                                                                                                final String message,
                                                                                                final MessageValue<T>... values) {
            return schemeIsNotEqualTo(scheme, DefaultMessageFormat.of(message, values));
         }

         public static <T extends Scheme, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme) {
            return schemeIsNotEqualTo(scheme,
                                      FastStringFormat.of("'",
                                                          validatingObject(),
                                                          "' scheme must not be equal to '",
                                                          parameter(0),
                                                          "'"));
         }

      }

      public static class PathRules {

         public static <T extends ComponentUri.Path> InvariantRule<T> isEmpty(final MessageFormatter<T> messageFormatter) {
            return satisfiesValue(ComponentUri.Path::isEmpty, messageFormatter).ruleContext(
                  "ComponentUriRules.PathRules.isEmpty");
         }

         @SafeVarargs
         public static <T extends ComponentUri.Path> InvariantRule<T> isEmpty(final String message,
                                                                              final MessageValue<T>... values) {
            return isEmpty(DefaultMessageFormat.of(message, values));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isEmpty() {
            return isEmpty(FastStringFormat.of("'", validatingObject(), "' must be empty"));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isNotEmpty(final MessageFormatter<T> messageFormatter) {
            return notSatisfiesValue(ComponentUri.Path::isEmpty, messageFormatter).ruleContext(
                  "ComponentUriRules.PathRules.isNotEmpty");
         }

         @SafeVarargs
         public static <T extends ComponentUri.Path> InvariantRule<T> isNotEmpty(final String message,
                                                                                 final MessageValue<T>... values) {
            return isNotEmpty(DefaultMessageFormat.of(message, values));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isNotEmpty() {
            return isNotEmpty(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isRoot(final MessageFormatter<T> messageFormatter) {
            return satisfiesValue(ComponentUri.Path::isRoot, messageFormatter).ruleContext(
                  "ComponentUriRules.PathRules.isRoot");
         }

         @SafeVarargs
         public static <T extends ComponentUri.Path> InvariantRule<T> isRoot(final String message,
                                                                             final MessageValue<T>... values) {
            return isRoot(DefaultMessageFormat.of(message, values));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isRoot() {
            return isRoot(FastStringFormat.of("'", validatingObject(), "' must be root"));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isNotRoot(final MessageFormatter<T> messageFormatter) {
            return notSatisfiesValue(ComponentUri.Path::isRoot, messageFormatter).ruleContext(
                  "ComponentUriRules.PathRules.isNotRoot");
         }

         @SafeVarargs
         public static <T extends ComponentUri.Path> InvariantRule<T> isNotRoot(final String message,
                                                                                final MessageValue<T>... values) {
            return isNotRoot(DefaultMessageFormat.of(message, values));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isNotRoot() {
            return isNotRoot(FastStringFormat.of("'", validatingObjectName(), "' must not be root"));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isAbsolute(final MessageFormatter<T> messageFormatter) {
            return satisfiesValue(ComponentUri.Path::isAbsolute, messageFormatter).ruleContext(
                  "ComponentUriRules.PathRules.isAbsolute");
         }

         @SafeVarargs
         public static <T extends ComponentUri.Path> InvariantRule<T> isAbsolute(final String message,
                                                                                 final MessageValue<T>... values) {
            return isAbsolute(DefaultMessageFormat.of(message, values));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isAbsolute() {
            return isAbsolute(FastStringFormat.of("'", validatingObject(), "' must be absolute"));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isRelative(final MessageFormatter<T> messageFormatter) {
            return satisfiesValue(ComponentUri.Path::isAbsolute, messageFormatter).ruleContext(
                  "ComponentUriRules.PathRules.isRelative");
         }

         @SafeVarargs
         public static <T extends ComponentUri.Path> InvariantRule<T> isRelative(final String message,
                                                                                 final MessageValue<T>... values) {
            return isRelative(DefaultMessageFormat.of(message, values));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> isRelative() {
            return isRelative(FastStringFormat.of("'", validatingObject(), "' must be relative"));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> hasNoTraversal(final MessageFormatter<T> messageFormatter) {
            return satisfiesValue(path -> {
               var parsePath = Path.of(path.value());
               for (int i = 0; i < parsePath.getNameCount(); i++) {
                  if (parsePath.getName(i).toString().equals("..")) {
                     return false;
                  }
               }
               return true;
            }, messageFormatter).ruleContext("ComponentUriRules.PathRules.hasNoTraversal");
         }

         @SafeVarargs
         public static <T extends ComponentUri.Path> InvariantRule<T> hasNoTraversal(final String message,
                                                                                     final MessageValue<T>... values) {
            return hasNoTraversal(DefaultMessageFormat.of(message, values));
         }

         public static <T extends ComponentUri.Path> InvariantRule<T> hasNoTraversal() {
            return hasNoTraversal(FastStringFormat.of("'",
                                                      validatingObject(),
                                                      "' must not have traversal paths"));
         }

      }

   }
}
