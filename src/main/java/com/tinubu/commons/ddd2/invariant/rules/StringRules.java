/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.valueformatter.IterableValueFormatter;
import com.tinubu.commons.ddd2.valueformatter.StringValueFormatter;

// FIXME tests : *ignoreCase*, *startWith*, *endWith*, *contain*
// FIXME regroup optionalized rules to <operation>(FLAGS)
public class StringRules {

   private static final IterableValueFormatter VALID_CHARS_VALUE_FORMATTER = new IterableValueFormatter(0);
   private static final StringValueFormatter STRING_FORMATTER = new StringValueFormatter(150);

   private StringRules() {
   }

   public static <T extends CharSequence> InvariantRule<T> isEmpty(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(StringUtils::isEmpty, messageFormatter).ruleContext("StringRules.isEmpty");
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> isEmpty(final String message,
                                                                   final MessageValue<T>... values) {
      return isEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> isEmpty() {
      return isEmpty(FastStringFormat.of("'", validatingObject(), "' must be empty"));
   }

   public static <T extends CharSequence> InvariantRule<T> isNotEmpty(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(StringUtils::isNotEmpty, messageFormatter).ruleContext("StringRules.isNotEmpty");
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> isNotEmpty(final String message,
                                                                      final MessageValue<T>... values) {
      return isNotEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> isNotEmpty() {
      return isNotEmpty(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   /**
    * Blank elements are {@link String} that are empty or containing only blank characters. {@code null}
    * elements are not considered blank.
    */
   public static <T extends CharSequence> InvariantRule<T> isBlank(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(StringUtils::isBlank, messageFormatter).ruleContext("StringRules.isBlank");
   }

   /**
    * Blank elements are {@link String} that are empty or containing only blank characters. {@code null}
    * elements are not considered blank.
    */
   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> isBlank(final String message,
                                                                   final MessageValue<T>... values) {
      return isBlank(DefaultMessageFormat.of(message, values));
   }

   /**
    * Blank elements are {@link String} that are empty or containing only blank characters. {@code null}
    * elements are not considered blank.
    */
   public static <T extends CharSequence> InvariantRule<T> isBlank() {
      return isBlank(FastStringFormat.of("'", validatingObjectName(), "' must be blank"));
   }

   public static <T extends CharSequence> InvariantRule<T> isNotBlank(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(StringUtils::isNotBlank, messageFormatter).ruleContext("StringRules.isNotBlank");
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> isNotBlank(final String message,
                                                                      final MessageValue<T>... values) {
      return isNotBlank(DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> isNotBlank() {
      return isNotBlank(FastStringFormat.of("'", validatingObjectName(), "' must not be blank"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> isEqualToIgnoreCase(final ParameterValue<U> value,
                                                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> StringUtils.equalsIgnoreCase(v,
                                                                                   value.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.isEqualToIgnoreCase", value);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> isEqualToIgnoreCase(final ParameterValue<U> value,
                                                                                                       final String message,
                                                                                                       final MessageValue<T>... values) {
      return isEqualToIgnoreCase(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> isEqualToIgnoreCase(final ParameterValue<U> value) {
      return isEqualToIgnoreCase(value,
                                 FastStringFormat.of("'",
                                                     validatingObject(STRING_FORMATTER),
                                                     "' must be equal to '", parameter(0, STRING_FORMATTER),
                                                     "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> isNotEqualToIgnoreCase(
         final ParameterValue<U> value,
         final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> !StringUtils.equalsIgnoreCase(v,
                                                                                    value.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.isNotEqualToIgnoreCase", value);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> isNotEqualToIgnoreCase(
         final ParameterValue<U> value,
         final String message,
         final MessageValue<T>... values) {
      return isNotEqualToIgnoreCase(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> isNotEqualToIgnoreCase(
         final ParameterValue<U> value) {
      return isNotEqualToIgnoreCase(value,
                                    FastStringFormat.of("'",
                                                        validatingObject(STRING_FORMATTER),
                                                        "' must not be equal to '",
                                                        parameter(0, STRING_FORMATTER),
                                                        "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> startsWith(final ParameterValue<U> prefix,
                                                                                              final MessageFormatter<T> messageFormatter) {
      notNull(prefix, "prefix");

      return satisfiesValue(v -> StringUtils.startsWith(v, prefix.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.startsWith",
                                                                               prefix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> startsWith(final ParameterValue<U> prefix,
                                                                                              final String message,
                                                                                              final MessageValue<T>... values) {
      return startsWith(prefix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> startsWith(final ParameterValue<U> prefix) {
      return startsWith(prefix,
                        FastStringFormat.of("'",
                                            validatingObject(STRING_FORMATTER),
                                            "' must start with '", parameter(0, STRING_FORMATTER),
                                            "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> startsWithIgnoreCase(final ParameterValue<U> prefix,
                                                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(prefix, "prefix");

      return satisfiesValue(v -> StringUtils.startsWithIgnoreCase(v,
                                                                                       prefix.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.startsWithIgnoreCase", prefix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> startsWithIgnoreCase(final ParameterValue<U> prefix,
                                                                                                        final String message,
                                                                                                        final MessageValue<T>... values) {
      return startsWithIgnoreCase(prefix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> startsWithIgnoreCase(final ParameterValue<U> prefix) {
      return startsWithIgnoreCase(prefix,
                                  FastStringFormat.of("'",
                                                      validatingObject(STRING_FORMATTER),
                                                      "' must start with '", parameter(0, STRING_FORMATTER),
                                                      "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> endsWith(final ParameterValue<U> suffix,
                                                                                            final MessageFormatter<T> messageFormatter) {
      notNull(suffix, "suffix");

      return satisfiesValue(v -> StringUtils.endsWith(v, suffix.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.endsWith",
                                                                               suffix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> endsWith(final ParameterValue<U> suffix,
                                                                                            final String message,
                                                                                            final MessageValue<T>... values) {
      return endsWith(suffix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> endsWith(final ParameterValue<U> suffix) {
      return endsWith(suffix,
                      FastStringFormat.of("'",
                                          validatingObject(STRING_FORMATTER),
                                          "' must end with '", parameter(0, STRING_FORMATTER),
                                          "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> endsWithIgnoreCase(final ParameterValue<U> suffix,
                                                                                                      final MessageFormatter<T> messageFormatter) {
      notNull(suffix, "suffix");

      return satisfiesValue(v -> StringUtils.endsWithIgnoreCase(v,
                                                                                     suffix.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.endsWithIgnoreCase", suffix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> endsWithIgnoreCase(final ParameterValue<U> suffix,
                                                                                                      final String message,
                                                                                                      final MessageValue<T>... values) {
      return endsWithIgnoreCase(suffix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> endsWithIgnoreCase(final ParameterValue<U> suffix) {
      return endsWithIgnoreCase(suffix,
                                FastStringFormat.of("'",
                                                    validatingObject(STRING_FORMATTER),
                                                    "' must end with '", parameter(0, STRING_FORMATTER),
                                                    "' (case-insensitive)"));
   }

   public static <T extends CharSequence> InvariantRule<T> containsOnly(final ParameterValue<String> validChars,
                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(validChars, "validChars");

      return satisfiesValue(v -> StringUtils.containsOnly(v,
                                                                               validChars.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.containsOnly",
                                                                               validChars);
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> containsOnly(final ParameterValue<String> validChars,
                                                                        final String message,
                                                                        final MessageValue<T>... values) {
      return containsOnly(validChars, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> containsOnly(final ParameterValue<String> validChars) {
      return containsOnly(validChars,
                          FastStringFormat.of("'",
                                              validatingObject(STRING_FORMATTER),
                                              "' must contain only ",
                                              MessageValue.<String, String, Object>parameter(0,
                                                                                             VALID_CHARS_VALUE_FORMATTER.compose(
                                                                                                   vc -> vc
                                                                                                         .chars()
                                                                                                         .mapToObj(
                                                                                                               c -> (char) c)
                                                                                                         .collect(
                                                                                                               toList()))),
                                              " characters"));
   }

   public static <T extends CharSequence> InvariantRule<T> containsNone(final ParameterValue<String> validChars,
                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(validChars, "validChars");

      return satisfiesValue(v -> StringUtils.containsNone(v,
                                                                               validChars.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.containsNone",
                                                                               validChars);
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> containsNone(final ParameterValue<String> validChars,
                                                                        final String message,
                                                                        final MessageValue<T>... values) {
      return containsNone(validChars, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> containsNone(final ParameterValue<String> validChars) {
      return containsNone(validChars,
                          FastStringFormat.of("'",
                                              validatingObject(STRING_FORMATTER),
                                              "' must not contain ",
                                              MessageValue.<String, String, Object>parameter(0,
                                                                                             VALID_CHARS_VALUE_FORMATTER.compose(
                                                                                                   vc -> vc
                                                                                                         .chars()
                                                                                                         .mapToObj(
                                                                                                               c -> (char) c)
                                                                                                         .collect(
                                                                                                               toList()))),
                                              " characters"));
   }

   public static <T extends CharSequence> InvariantRule<T> containsAny(final ParameterValue<String> validChars,
                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(validChars, "validChars");

      return satisfiesValue(v -> StringUtils.containsAny(v,
                                                                              validChars.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.containsAny",
                                                                               validChars);
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> containsAny(final ParameterValue<String> validChars,
                                                                       final String message,
                                                                       final MessageValue<T>... values) {
      return containsAny(validChars, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> containsAny(final ParameterValue<String> validChars) {
      return containsAny(validChars,
                         FastStringFormat.of("'",
                                             validatingObject(STRING_FORMATTER),
                                             "' must contain any ",
                                             MessageValue.<String, String, Object>parameter(0,
                                                                                            VALID_CHARS_VALUE_FORMATTER.compose(
                                                                                                  vc -> vc
                                                                                                        .chars()
                                                                                                        .mapToObj(
                                                                                                              c -> (char) c)
                                                                                                        .collect(
                                                                                                              toList()))),
                                             " characters"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> contains(final ParameterValue<U> subString,
                                                                                            final MessageFormatter<T> messageFormatter) {
      notNull(subString, "subString");

      return satisfiesValue(v -> StringUtils.contains(v,
                                                                           subString.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.contains",
                                                                               subString);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> contains(final ParameterValue<U> subString,
                                                                                            final String message,
                                                                                            final MessageValue<T>... values) {
      return contains(subString, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> contains(final ParameterValue<U> subString) {
      return contains(subString,
                      FastStringFormat.of("'%1$s' must contain '%2$s'",
                                          validatingObject(STRING_FORMATTER),
                                          parameter(0, STRING_FORMATTER)));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> containsIgnoreCase(final ParameterValue<U> subString,
                                                                                                      final MessageFormatter<T> messageFormatter) {
      notNull(subString, "subString");

      return satisfiesValue(v -> StringUtils.containsIgnoreCase(v,
                                                                                     subString.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.containsIgnoreCase", subString);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> containsIgnoreCase(final ParameterValue<U> subString,
                                                                                                      final String message,
                                                                                                      final MessageValue<T>... values) {
      return containsIgnoreCase(subString, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> containsIgnoreCase(final ParameterValue<U> subString) {
      return containsIgnoreCase(subString,
                                FastStringFormat.of("'",
                                                    validatingObject(STRING_FORMATTER),
                                                    "' must contain '", parameter(0, STRING_FORMATTER),
                                                    "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotStartWith(final ParameterValue<U> prefix,
                                                                                                    final MessageFormatter<T> messageFormatter) {
      notNull(prefix, "prefix");

      return satisfiesValue(v -> !StringUtils.startsWith(v,
                                                                              prefix.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.doesNotStartWith",
                                                                               prefix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotStartWith(final ParameterValue<U> prefix,
                                                                                                    final String message,
                                                                                                    final MessageValue<T>... values) {
      return doesNotStartWith(prefix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotStartWith(final ParameterValue<U> prefix) {
      return doesNotStartWith(prefix,
                              FastStringFormat.of("'",
                                                  validatingObject(STRING_FORMATTER),
                                                  "' must not start with '", parameter(0, STRING_FORMATTER),
                                                  "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotStartWithIgnoreCase(
         final ParameterValue<U> prefix,
         final MessageFormatter<T> messageFormatter) {
      notNull(prefix, "prefix");

      return satisfiesValue(v -> !StringUtils.startsWithIgnoreCase(v,
                                                                                        prefix.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.doesNotStartWithIgnoreCase", prefix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotStartWithIgnoreCase(
         final ParameterValue<U> prefix,
         final String message,
         final MessageValue<T>... values) {
      return doesNotStartWithIgnoreCase(prefix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotStartWithIgnoreCase(
         final ParameterValue<U> prefix) {
      return doesNotStartWithIgnoreCase(prefix,
                                        FastStringFormat.of("'",
                                                            validatingObject(STRING_FORMATTER),
                                                            "' must not start with '",
                                                            parameter(0, STRING_FORMATTER),
                                                            "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotEndWith(final ParameterValue<U> suffix,
                                                                                                  final MessageFormatter<T> messageFormatter) {
      notNull(suffix, "suffix");

      return satisfiesValue(v -> !StringUtils.endsWith(v, suffix.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.doesNotEndWith",
                                                                               suffix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotEndWith(final ParameterValue<U> suffix,
                                                                                                  final String message,
                                                                                                  final MessageValue<T>... values) {
      return doesNotEndWith(suffix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotEndWith(final ParameterValue<U> suffix) {
      return doesNotEndWith(suffix,
                            FastStringFormat.of("'",
                                                validatingObject(STRING_FORMATTER),
                                                "' must not end with '", parameter(0, STRING_FORMATTER),
                                                "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotEndWithIgnoreCase(
         final ParameterValue<U> suffix,
         final MessageFormatter<T> messageFormatter) {
      notNull(suffix, "suffix");

      return satisfiesValue(v -> !StringUtils.endsWithIgnoreCase(v,
                                                                                      suffix.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.doesNotEndWithIgnoreCase", suffix);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotEndWithIgnoreCase(
         final ParameterValue<U> suffix,
         final String message,
         final MessageValue<T>... values) {
      return doesNotEndWithIgnoreCase(suffix, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotEndWithIgnoreCase(
         final ParameterValue<U> suffix) {
      return doesNotEndWithIgnoreCase(suffix,
                                      FastStringFormat.of("'",
                                                          validatingObject(STRING_FORMATTER),
                                                          "' must not end with '",
                                                          parameter(0, STRING_FORMATTER),
                                                          "' (case-insensitive)"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotContain(final ParameterValue<U> subString,
                                                                                                  final MessageFormatter<T> messageFormatter) {
      notNull(subString, "subString");

      return satisfiesValue(v -> !StringUtils.contains(v,
                                                                            subString.requireNonNullValue()),
                                                 messageFormatter).ruleContext("StringRules.doesNotContain",
                                                                               subString);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotContain(final ParameterValue<U> subString,
                                                                                                  final String message,
                                                                                                  final MessageValue<T>... values) {
      return doesNotContain(subString, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotContain(final ParameterValue<U> subString) {
      return doesNotContain(subString,
                            FastStringFormat.of("'",
                                                validatingObject(STRING_FORMATTER),
                                                "' must not contain '", parameter(0, STRING_FORMATTER),
                                                "'"));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotContainIgnoreCase(
         final ParameterValue<U> subString,
         final MessageFormatter<T> messageFormatter) {
      notNull(subString, "subString");

      return satisfiesValue(v -> !StringUtils.containsIgnoreCase(v,
                                                                                      subString.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "StringRules.doesNotContainIgnoreCase", subString);
   }

   @SafeVarargs
   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotContainIgnoreCase(
         final ParameterValue<U> subString,
         final String message,
         final MessageValue<T>... values) {
      return doesNotContainIgnoreCase(subString, DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence, U extends CharSequence> InvariantRule<T> doesNotContainIgnoreCase(
         final ParameterValue<U> subString) {
      return doesNotContainIgnoreCase(subString,
                                      FastStringFormat.of("'",
                                                          validatingObject(STRING_FORMATTER),
                                                          "' must not contain '",
                                                          parameter(0, STRING_FORMATTER),
                                                          "' (case-insensitive)"));
   }

   public static <T extends CharSequence> InvariantRule<T> isNumeric() {
      return satisfiesValue(StringUtils::isNumeric,
                                                 "'%s' must be numerics", validatingObject()).ruleContext(
            "StringRules.isNumeric");
   }

   public static <T extends CharSequence> InvariantRule<T> isAlpha() {
      return satisfiesValue(StringUtils::isAlpha,
                                                 "'%s' must be alphas", validatingObject()).ruleContext(
            "StringRules.isAlpha");
   }

   public static <T extends CharSequence> InvariantRule<T> isAlphanumeric() {
      return satisfiesValue(StringUtils::isAlphanumeric,
                                                 "'%s' must be alphanumerics",
                            validatingObject()).ruleContext("StringRules.isAlphanumeric");
   }

   /**
    * Converts current {@link CharSequence} to {@code cs -> lowercase(uppercase(cs))} for subsequent rules to
    * be case-insensitive. {@link Locale} is not taken into account so this can lead to bad results depending
    * on locale.
    *
    * @param rule rule to chain on case-insensitive value
    * @param <T> rule type
    *
    * @return rule
    */
   public static <T extends CharSequence> InvariantRule<T> caseInsensitive(final InvariantRule<? super CharSequence> rule) {
      return as(cs -> cs.toString().toUpperCase().toLowerCase(), rule);
   }

   /**
    * Character sequence length representing the number of chars (UTF-16) in the sequence.
    *
    * @param rule length rule
    * @param <T> rule type
    *
    * @return rule
    */
   public static <T extends CharSequence> InvariantRule<T> length(final InvariantRule<? super Integer> rule) {
      return property(CharSequence::length, "length", rule);
   }

   /**
    * Character sequence length representing the number of bytes.
    *
    * @param rule length rule
    * @param <T> rule type
    *
    * @return rule
    */
   // FIXME wrong ! depends on internal implementation (utf-8 or 16 ..)
   public static <T extends CharSequence> InvariantRule<T> bytesLength(final InvariantRule<? super Integer> rule) {
      return property(t -> t.length() * 2, "length(bytes)", rule);
   }

   public static <T extends CharSequence> InvariantRule<T> chars(final InvariantRule<? super Collection<Character>> rule) {
      return property(s -> s.chars().mapToObj(c -> (char) c).collect(toList()), "chars", rule);
   }

   public static <T extends String> InvariantRule<T> numerics(InvariantRule<? super Long> rule) {
      return isNumeric().andValue(as(v -> Long.parseLong(String.valueOf(v)), rule));
   }

   public static <T extends String> InvariantRule<T> alphas(InvariantRule<? super String> rule) {
      return isAlpha().andValue(as(identity(), rule));
   }

   public static <T extends String> InvariantRule<T> alphanumerics(InvariantRule<? super String> rule) {
      return isAlphanumeric().andValue(as(identity(), rule));
   }

}
