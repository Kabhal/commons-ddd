/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;

import java.util.function.Function;
import java.util.function.Supplier;

import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

/**
 * Type for {@link MessageFormatter} argument values.
 * All message formatter values must be of that type.
 *
 * @param <T> validating object type
 */
@FunctionalInterface
public interface MessageValue<T> {

   /** Default object name to put in messages when user does not provide a real one. */
   String DEFAULT_OBJECT_NAME = "<object>";
   /** Default parameter name to put in messages when user does not provide a real one. */
   String DEFAULT_PARAMETER_NAME = "<parameter>";

   /**
    * Returns message argument value.
    *
    * @param validatingObject validating object used to return argument value
    *
    * @return message value evaluation
    */
   Object value(ValidatingObject<T> validatingObject);

   /**
    * A lazily evaluated message value of any type.
    *
    * @param value message value supplier. Supplied value can be {@code null}
    * @param <T> validating object type
    * @param <V> message value type
    *
    * @return value
    */
   static <T, V> MessageValue<T> lazyValue(Supplier<V> value) {
      notNull(value, "value");

      return __ -> value.get();
   }

   /** Alias to work-around import conflicts. */
   static <T, V> MessageValue<T> mLazyValue(Supplier<V> value) {
      notNull(value, "value");

      return __ -> value.get();
   }

   /**
    * A simple message value of any type.
    *
    * @param value message value. Value can be {@code null}
    * @param <T> validating object type
    * @param <V> message value type
    *
    * @return value
    */
   static <T, V> MessageValue<T> value(V value) {
      return __ -> value;
   }

   /** Alias to work-around import conflicts. */
   static <T, V> MessageValue<T> mValue(V value) {
      return value(value);
   }

   /**
    * A lazily evaluated message value of any type with an extra value mapper.
    *
    * @param value message value supplier. Supplied value can be {@code null}
    * @param valueMapper valueMapper to apply on value. Input and output value can be {@code null}
    * @param <T> validating value type
    * @param <V> message value type
    *
    * @return mapped value
    */
   static <T, V> MessageValue<T> lazyValue(Supplier<V> value, Function<? super V, ?> valueMapper) {
      notNull(value, "value");
      notNull(valueMapper, "valueMapper");

      return __ -> nullable(value.get()).map(valueMapper).orElse(null);
   }

   /** Alias to work-around import conflicts. */
   static <T, V> MessageValue<T> mLazyValue(Supplier<V> value, Function<? super V, ?> valueMapper) {
      return lazyValue(value, valueMapper);
   }

   /**
    * A simple message value of any type with an extra value mapper.
    *
    * @param value message value. Value can be {@code null}
    * @param valueMapper valueMapper to apply on value. Input and output value can be {@code null}
    * @param <T> validating value type
    * @param <V> message value type
    *
    * @return mapped value
    */
   static <T, V> MessageValue<T> value(V value, Function<? super V, ?> valueMapper) {
      return __ -> nullable(value).map(valueMapper).orElse(null);
   }

   /** Alias to work-around import conflicts. */
   static <T, V> MessageValue<T> mValue(V value, Function<? super V, ?> valueMapper) {
      return value(value, valueMapper);
   }

   /**
    * References actual validating object name. If a {@link InvariantRule#map(Function, Function)} has been
    * applied to invariant, this references the mapped name.
    * The function always return a name or {@value #DEFAULT_OBJECT_NAME}.
    *
    * @param <T> validating object type
    *
    * @return actual validating object name reference
    */
   static <T> MessageValue<T> validatingObjectName() {
      return validatingObject -> validatingObject.name().orElse(DEFAULT_OBJECT_NAME);
   }

   /**
    * References actual validating object name, with an extra name mapper.
    * The function always return a name or {@value #DEFAULT_OBJECT_NAME}.
    *
    * @param nameMapper nameMapper to apply on name. Function argument can be {@code null} if name not
    *       set
    * @param <T> validating object type
    *
    * @return actual validating object name reference
    */
   static <T> MessageValue<T> validatingObjectName(Function<? super String, String> nameMapper) {
      notNull(nameMapper, "nameMapper");

      return validatingObject -> nullable(nameMapper.apply(validatingObject.name().orElse(null))).orElse(
            DEFAULT_OBJECT_NAME);
   }

   /**
    * References initial validating object name, unchanged if a {@link InvariantRule#map(Function, Function)}
    * has been applied to invariant.
    * The function always return a name or {@value #DEFAULT_OBJECT_NAME}.
    * <p>
    * {@code Invariant.of(() -> myOperator,
    * isTrue("'%1$s' operator with '%2$s' value must be unary",
    * validatingObjectInitialName(), validatingObjectInitialValue())
    * .map(operator -> operator.arity == UNARY)) }
    *
    * @param <T> validating object type
    *
    * @return initial validating object initial name reference
    */
   static <T> MessageValue<T> validatingObjectInitialName() {
      return validatingObject -> validatingObject.initialName().orElse(DEFAULT_OBJECT_NAME);
   }

   /**
    * References initial validating object name, unchanged if a {@link InvariantRule#map(Function, Function)}
    * has been applied to invariant.
    * The function always return a name or {@value #DEFAULT_OBJECT_NAME}.
    * <p>
    * {@code Invariant.of(() -> myCriterion, "myCriterion",
    * isTrue("'%1$s' operator with '%2$s' value must be unary",
    * validatingObjectInitialName(propertyNameMapper("operator"),
    * validatingObjectInitialValue(Criterion::operator))
    * .map(criterion -> criterion.operator.arity == UNARY)) }
    *
    * @param nameMapper nameMapper to apply on initial name. Function argument can be {@code null} if
    *       initial name not set
    * @param <T> validating object type
    *
    * @return initial validating object initial name reference
    */
   static <T> MessageValue<T> validatingObjectInitialName(Function<? super String, String> nameMapper) {
      notNull(nameMapper, "nameMapper");

      return validatingObject -> nullable(nameMapper.apply(validatingObject
                                                                 .initialName()
                                                                 .orElse(null))).orElse(DEFAULT_OBJECT_NAME);
   }

   /**
    * References actual validating object value. If a {@link InvariantRule#map(Function)} has been
    * applied to invariant, this references the mapped value.
    *
    * @param <T> validating object type
    *
    * @return actual validating object value reference
    */
   static <T> MessageValue<T> validatingObjectValue() {
      return ValidatingObject::value;
   }

   /**
    * References actual validating object value, with an extra value mapper.
    *
    * @param valueMapper valueMapper to apply on value. Input and output value can be {@code null}
    * @param <T> validating object type
    * @param <V> mapped message value type
    *
    * @return actual validating object value reference
    */
   static <T, V> MessageValue<T> validatingObjectValue(Function<? super T, V> valueMapper) {
      notNull(valueMapper, "valueMapper");

      return validatingObject -> valueMapper.apply(validatingObject.value());
   }

   /**
    * References actual validating object as a compound of object name, if present, and object value.
    *
    * @param <T> validating object type
    *
    * @return actual validating object reference
    */
   static <T> MessageValue<T> validatingObject() {
      return validatingObject -> validatingObject.name().map(name -> name + "=").orElse("")
                                 + validatingObject.value();
   }

   /**
    * References actual validating object as a compound of object name, if present, and object value, with an
    * extra mapper.
    *
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return actual validating object reference
    */
   static <T, V> MessageValue<T> validatingObject(Function<? super T, V> valueMapper) {
      return validatingObject(valueMapper, identity());
   }

   /**
    * References actual validating object as a compound of object name, if present, and object value, with
    * extra mappers for both value and name.
    *
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param nameMapper name mapper. Input and output value can be {@code null}, if output is {@code
    *       null}, no name will be displayed
    * @param <T> validating object type
    *
    * @return actual validating object reference
    */
   static <T, V> MessageValue<T> validatingObject(Function<? super T, V> valueMapper,
                                                  Function<? super String, String> nameMapper) {
      notNull(valueMapper, "valueMapper");

      return validatingObject -> nullable(nameMapper.apply(validatingObject.name().orElse(null)))
                                       .map(name -> name + "=")
                                       .orElse("") + valueMapper.apply(validatingObject.value());
   }

   /**
    * References initial validating object value, unchanged if a {@link InvariantRule#map(Function)} has
    * been applied to invariant.
    * <p>
    * {@code Invariant.of(() -> myOperator,
    * isTrue("'%1$s' operator with '%2$s' value must be unary",
    * validatingObjectInitialName(), validatingObjectInitialValue())
    * .map(operator -> operator.arity == UNARY)) }
    *
    * @param <T> validating object type
    *
    * @return initial validating object initial value reference
    */
   static <T> MessageValue<T> validatingObjectInitialValue() {
      return ValidatingObject::initialValue;
   }

   /**
    * References initial validating object value, unchanged if a {@link InvariantRule#map(Function)} has
    * been applied to invariant.
    * <p>
    * {@code Invariant.of(() -> myCriterion, "myCriterion",
    * isTrue("'%1$s' operator with '%2$s' value must be unary",
    * validatingObjectInitialName(propertyNameMapper("operator"), validatingObjectInitialValue(c ->
    * ((Criterion)c).operator()))
    * .map(criterion -> criterion.operator.arity == UNARY)) }
    *
    * @param valueMapper valueMapper to apply on value. Input and output value can be {@code null}
    * @param <T> validating object type
    * @param <V> message value type
    *
    * @return initial validating object initial value reference
    */
   static <T, V> MessageValue<T> validatingObjectInitialValue(Function<Object, V> valueMapper) {
      notNull(valueMapper, "valueMapper");

      return validatingObject -> valueMapper.apply(validatingObject.initialValue());
   }

   /**
    * Returns a {@link MessageValue} from validation result at specified index.
    *
    * @param i validation result index (start from 0)
    * @param <T> validating object type
    *
    * @return result
    *
    * @throws IndexOutOfBoundsException if not result available at specified index
    */
   static <T> MessageValue<T> validationResult(int i) {
      return validationResult(i, identity());
   }

   /**
    * Returns a {@link MessageValue} from validation result at specified index.
    * Result is cast to specified class.
    *
    * @param i validation result index (start from 0)
    * @param resultMapper resultMapper to apply on result value
    * @param <T> validating object type
    * @param <V0> message value type
    * @param <V> mapped message value type
    *
    * @return mapped result
    *
    * @throws IndexOutOfBoundsException if not result available at specified index
    * @throws ClassCastException if result can't be casted to specified resultMapper input
    */
   static <T, V0, V> MessageValue<T> validationResult(int i, Function<? super V0, V> resultMapper) {
      return validationResult(i, resultMapper, null);
   }

   /**
    * Returns a {@link MessageValue} from validation result at specified index.
    * Result is cast to specified class.
    *
    * @param i validation result index (start from 0)
    * @param resultMapper resultMapper to apply on result value
    * @param <T> validating object type
    * @param <V0> message value type
    * @param <V> mapped message value type
    *
    * @return mapped result
    *
    * @throws IndexOutOfBoundsException if not result available at specified index
    * @throws ClassCastException if result can't be casted to specified resultMapper input
    */
   @SuppressWarnings("unchecked")
   static <T, V0, V> MessageValue<T> validationResult(int i,
                                                      Function<? super V0, V> resultMapper,
                                                      V defaultValue) {
      notNull(resultMapper, "resultMapper");

      return validatingObject -> {
         if (i < 0 || i >= validatingObject.validationResults().size()) {
            throw new IndexOutOfBoundsException(String.format("No validation result for index : %d", i));
         }
         return nullable((V0) validatingObject.validationResults().get(i))
               .map(resultMapper)
               .orElse(defaultValue);
      };
   }

   /**
    * Name mapper to format object name defined as property of current validating object.
    *
    * @param property mapped property name
    *
    * @return function returning validating object name suffixed with '.' and specified property name
    */
   static Function<String, String> propertyNameMapper(String property) {
      notNull(property, "property");

      return name -> nullable(name).map(n -> n + ".").orElse("") + property;
   }

   /**
    * References a parameter name, with an extra name mapper.
    * The function always return a name or {@value #DEFAULT_PARAMETER_NAME}.
    *
    * @param nameMapper name mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter name
    */
   static <T, P> MessageValue<T> parameterName(ParameterValue<P> value,
                                               Function<? super String, String> nameMapper) {
      notNull(value, "value");
      notNull(nameMapper, "nameMapper");

      return validatingObject -> nullable(nameMapper.apply(value.name().orElse(null))).orElse(
            DEFAULT_PARAMETER_NAME);
   }

   /**
    * References a parameter name.
    * The function always return a name or {@value #DEFAULT_PARAMETER_NAME}.
    *
    * @param <T> validating object type
    *
    * @return parameter name
    */
   static <T, P> MessageValue<T> parameterName(ParameterValue<P> value) {
      return parameterName(value, identity());
   }

   /**
    * References a parameter value, with an extra value mapper.
    *
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter value
    */
   static <T, P, V> MessageValue<T> parameterValue(ParameterValue<P> value,
                                                   Function<? super P, V> valueMapper) {
      notNull(value, "value");
      notNull(valueMapper, "valueMapper");

      return validatingObject -> valueMapper.apply(value.value());
   }

   /**
    * References a parameter value.
    *
    * @param <T> validating object type
    *
    * @return parameter value
    */
   static <T, P> MessageValue<T> parameterValue(ParameterValue<P> value) {
      notNull(value, "value");

      return __ -> value.value();
   }

   /**
    * References a parameter as a compound of parameter name, if present, and parameter value, with extra
    * mappers for both value and name.
    *
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param nameMapper name mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter
    */
   static <T, P, V> MessageValue<T> parameter(ParameterValue<P> value,
                                              Function<? super P, V> valueMapper,
                                              Function<? super String, String> nameMapper) {
      notNull(value, "value");
      notNull(valueMapper, "valueMapper");
      notNull(nameMapper, "nameMapper");

      return validatingObject -> nullable(nameMapper.apply(value.name().orElse(null)))
                                       .map(n -> n + "=")
                                       .orElse("") + valueMapper.apply(value.value());
   }

   /**
    * References a parameter as a compound of parameter name, if present, and parameter value, with an extra
    * value mapper.
    *
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter
    */
   static <T, P, V> MessageValue<T> parameter(ParameterValue<P> value, Function<? super P, V> valueMapper) {
      return parameter(value, valueMapper, identity());
   }

   /**
    * References a parameter as a compound of parameter name, if present, and parameter value.
    *
    * @param <T> validating object type
    *
    * @return parameter
    */
   static <T, P> MessageValue<T> parameter(ParameterValue<P> value) {
      return parameter(value, identity(), identity());
   }

   @SuppressWarnings("unchecked")
   /* private */ static <T, P> ParameterValue<P> parameterValueByIndex(int i,
                                                                       ValidatingObject<T> validatingObject) {
      if (i < 0 || i >= validatingObject.ruleParameters().size()) {
         throw new IndexOutOfBoundsException(String.format("No rule parameter for index : %d", i));
      }

      return (ParameterValue<P>) validatingObject.ruleParameters().get(i);
   }

   /**
    * References current rule parameter name by index, with an extra name mapper.
    * As a pre-requisite, parameters must be registered in context by the current rule, using {@link
    * InvariantRule#ruleContext(String, ParameterValue[])}.
    * The function always return a name or {@value #DEFAULT_PARAMETER_NAME}.
    *
    * @param i parameter index starting from 0
    * @param nameMapper name mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter name
    */
   static <T, P> MessageValue<T> parameterName(int i, Function<? super String, String> nameMapper) {
      notNull(nameMapper, "nameMapper");

      return validatingObject -> {
         ParameterValue<P> value = parameterValueByIndex(i, validatingObject);

         return nullable(nameMapper.apply(value.name().orElse(null))).orElse(DEFAULT_PARAMETER_NAME);
      };
   }

   /**
    * References current rule parameter name by index.
    * As a pre-requisite, parameters must be registered in context by the current rule, using {@link
    * InvariantRule#ruleContext(String, ParameterValue[])}.
    * The function always return a name or {@value #DEFAULT_PARAMETER_NAME}.
    *
    * @param i parameter index starting from 0
    * @param <T> validating object type
    *
    * @return parameter name
    */
   static <T, P> MessageValue<T> parameterName(int i) {
      return parameterName(i, identity());
   }

   /**
    * References current rule parameter value by index, with an extra value mapper.
    * As a pre-requisite, parameters must be registered in context by the current rule, using {@link
    * InvariantRule#ruleContext(String, ParameterValue[])}.
    *
    * @param i parameter index starting from 0
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter value
    */
   static <T, P, V> MessageValue<T> parameterValue(int i, Function<? super P, V> valueMapper) {
      notNull(valueMapper, "valueMapper");

      return validatingObject -> {
         ParameterValue<P> value = parameterValueByIndex(i, validatingObject);

         return valueMapper.apply(value.value());
      };
   }

   /**
    * References current rule parameter value by index.
    * As a pre-requisite, parameters must be registered in context by the current rule, using {@link
    * InvariantRule#ruleContext(String, ParameterValue[])}.
    *
    * @param i parameter index starting from 0
    * @param <T> validating object type
    *
    * @return parameter value
    */
   static <T, P> MessageValue<T> parameterValue(int i) {
      return parameterValue(i, identity());
   }

   /**
    * References current rule parameter by index, as a compound of parameter name and parameter value, with
    * extra mappers for both value and name.
    * As a pre-requisite, parameters must be registered in context by the current rule, using {@link
    * InvariantRule#ruleContext(String, ParameterValue[])}.
    *
    * @param i parameter index starting from 0
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param nameMapper name mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter
    */
   static <T, P, V> MessageValue<T> parameter(int i,
                                              Function<? super P, V> valueMapper,
                                              Function<? super String, String> nameMapper) {
      notNull(valueMapper, "valueMapper");
      notNull(nameMapper, "nameMapper");

      return validatingObject -> {
         ParameterValue<P> value = parameterValueByIndex(i, validatingObject);

         return nullable(nameMapper.apply(value.name().orElse(null))).map(n -> n + "=").orElse("")
                + valueMapper.apply(value.value());
      };
   }

   /**
    * References current rule parameter by index, as a compound of parameter name and parameter value, with
    * and extra value mapper.
    * As a pre-requisite, parameters must be registered in context by the current rule, using {@link
    * InvariantRule#ruleContext(String, ParameterValue[])}.
    *
    * @param i parameter index starting from 0
    * @param valueMapper value mapper. Input and output value can be {@code null}
    * @param <T> validating object type
    *
    * @return parameter
    */
   static <T, P, V> MessageValue<T> parameter(int i, Function<? super P, V> valueMapper) {
      return parameter(i, valueMapper, identity());
   }

   /**
    * References current rule parameter by index, as a compound of parameter name and parameter value.
    * As a pre-requisite, parameters must be registered in context by the current rule, using {@link
    * InvariantRule#ruleContext(String, ParameterValue[])}.
    *
    * @param i parameter index starting from 0
    * @param <T> validating object type
    *
    * @return parameter
    */
   static <T> MessageValue<T> parameter(int i) {
      return parameter(i, identity(), identity());
   }

}
