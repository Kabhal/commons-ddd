/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.formatter;

import java.util.stream.Stream;

import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ValidatingObject;

public abstract class AbstractMessageFormatter<T> implements MessageFormatter<T> {

   /**
    * Replaces {@link MessageValue} elements of {@code values} by their evaluated value against
    * validating object.
    *
    * @param values values to evaluate
    * @param validatingObject validating object
    *
    * @return new stream of object with {@link MessageValue} elements evaluated
    */
   protected Object[] messageValues(MessageValue<T>[] values, ValidatingObject<T> validatingObject) {
      return Stream.of(values).map(value -> value.value(validatingObject)).toArray();
   }

}
