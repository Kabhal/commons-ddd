/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;

import java.io.File;
import java.nio.file.Path;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

// FIXME tests
public class FileRules {

   private FileRules() {
   }

   public static <T extends File> InvariantRule<T> doesExist(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(File::exists, messageFormatter).ruleContext("FileRules.doesExist");
   }

   @SafeVarargs
   public static <T extends File> InvariantRule<T> doesExist(final String message,
                                                             final MessageValue<T>... values) {
      return doesExist(DefaultMessageFormat.of(message, values));
   }

   public static <T extends File> InvariantRule<T> doesExist() {
      return doesExist(FastStringFormat.of("'", validatingObject(), "' must exists"));
   }

   public static <T extends File> InvariantRule<T> isFile(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(File::isFile, messageFormatter).ruleContext("FileRules.isFile");
   }

   @SafeVarargs
   public static <T extends File> InvariantRule<T> isFile(final String message,
                                                          final MessageValue<T>... values) {
      return isFile(DefaultMessageFormat.of(message, values));
   }

   public static <T extends File> InvariantRule<T> isFile() {
      return isFile(FastStringFormat.of("'", validatingObject(), "' must be a file"));
   }

   public static <T extends File> InvariantRule<T> isDirectory(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(File::isDirectory, messageFormatter).ruleContext("FileRules.isDirectory");
   }

   @SafeVarargs
   public static <T extends File> InvariantRule<T> isDirectory(final String message,
                                                               final MessageValue<T>... values) {
      return isDirectory(DefaultMessageFormat.of(message, values));
   }

   public static <T extends File> InvariantRule<T> isDirectory() {
      return isDirectory(FastStringFormat.of("'", validatingObject(), "' must be a directory"));
   }

   public static <T extends File> InvariantRule<T> isAbsolute(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(File::isAbsolute, messageFormatter).ruleContext("FileRules.isAbsolute");
   }

   @SafeVarargs
   public static <T extends File> InvariantRule<T> isAbsolute(final String message,
                                                              final MessageValue<T>... values) {
      return isAbsolute(DefaultMessageFormat.of(message, values));
   }

   public static <T extends File> InvariantRule<T> isAbsolute() {
      return isAbsolute(FastStringFormat.of("'", validatingObject(), "' must be absolute path"));
   }

   public static <T extends File> InvariantRule<T> length(final InvariantRule<? super Long> rule) {
      return property(File::length, "length", rule);
   }

   public static <T extends File> InvariantRule<T> path(final InvariantRule<? super Path> rule) {
      return as(File::toPath, rule);
   }

}
