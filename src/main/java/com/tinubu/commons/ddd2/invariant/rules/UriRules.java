/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.uri.Uri;

// FIXME tests
public class UriRules {

   private UriRules() {
   }

   public static <T extends URI> InvariantRule<T> isAbsolute(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isAbsolute, messageFormatter).ruleContext("UriRules.isAbsolute");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isAbsolute(final String message,
                                                             final MessageValue<T>... values) {
      return isAbsolute(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isAbsolute() {
      return isAbsolute(FastStringFormat.of("'", validatingObject(), "' must be absolute"));
   }

   public static <T extends URI> InvariantRule<T> isAbsolute(final ParameterValue<String> scheme,
                                                             final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> Uri.isAbsolute(uri) && schemeIsEqualTo(uri, scheme),
                            messageFormatter).ruleContext("UriRules.isAbsoluteForScheme", scheme);
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isAbsolute(final ParameterValue<String> scheme,
                                                             final String message,
                                                             final MessageValue<T>... values) {
      return isAbsolute(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isAbsolute(final ParameterValue<String> scheme) {
      return isAbsolute(scheme,
                        FastStringFormat.of("'",
                                            validatingObject(),
                                            "' must be absolute, and scheme must be equal to '",
                                            parameter(0),
                                            "'"));
   }

   public static <T extends URI> InvariantRule<T> isRelative(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isRelative, messageFormatter).ruleContext("UriRules.isRelative");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isRelative(final String message,
                                                             final MessageValue<T>... values) {
      return isRelative(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isRelative() {
      return isRelative(FastStringFormat.of("'", validatingObject(), "' must be relative"));
   }

   public static <T extends URI> InvariantRule<T> isOpaque(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isOpaque, messageFormatter).ruleContext("UriRules.isOpaque");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isOpaque(final String message,
                                                           final MessageValue<T>... values) {
      return isOpaque(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isOpaque() {
      return isOpaque(FastStringFormat.of("'", validatingObject(), "' must be opaque"));
   }

   public static <T extends URI> InvariantRule<T> isOpaque(final ParameterValue<String> scheme,
                                                           final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> Uri.isOpaque(uri) && schemeIsEqualTo(uri, scheme),
                            messageFormatter).ruleContext("UriRules.isOpaqueForScheme", scheme);
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isOpaque(final ParameterValue<String> scheme,
                                                           final String message,
                                                           final MessageValue<T>... values) {
      return isOpaque(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isOpaque(final ParameterValue<String> scheme) {
      return isOpaque(scheme,
                      FastStringFormat.of("'",
                                          validatingObject(),
                                          "' must be opaque, and scheme must be equal to '",
                                          parameter(0),
                                          "'"));
   }

   public static <T extends URI> InvariantRule<T> isNotOpaque(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isOpaque, messageFormatter).ruleContext("UriRules.isNotOpaque");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isNotOpaque(final String message,
                                                              final MessageValue<T>... values) {
      return isNotOpaque(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isNotOpaque() {
      return isNotOpaque(FastStringFormat.of("'", validatingObject(), "' must not be opaque"));
   }

   public static <T extends URI> InvariantRule<T> isHierarchical(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isHierarchical, messageFormatter).ruleContext("UriRules.isHierarchical");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isHierarchical(final String message,
                                                                 final MessageValue<T>... values) {
      return isHierarchical(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isHierarchical() {
      return isHierarchical(FastStringFormat.of("'", validatingObject(), "' must be hierarchical"));
   }

   public static <T extends URI> InvariantRule<T> isNotHierarchical(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isHierarchical,
                               messageFormatter).ruleContext("UriRules.isNotHierarchical");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isNotHierarchical(final String message,
                                                                    final MessageValue<T>... values) {
      return isNotHierarchical(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isNotHierarchical() {
      return isNotHierarchical(FastStringFormat.of("'", validatingObject(), "' must not be hierarchical"));
   }

   public static <T extends URI> InvariantRule<T> isEmptyHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isEmptyHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isEmptyHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isEmptyHierarchicalServer(final String message,
                                                                            final MessageValue<T>... values) {
      return isEmptyHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isEmptyHierarchicalServer() {
      return isEmptyHierarchicalServer(FastStringFormat.of("'",
                                                           validatingObject(),
                                                           "' must be empty hierarchical server-based"));
   }

   public static <T extends URI> InvariantRule<T> isNotEmptyHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isEmptyHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isNotEmptyHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isNotEmptyHierarchicalServer(final String message,
                                                                               final MessageValue<T>... values) {
      return isNotEmptyHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isNotEmptyHierarchicalServer() {
      return isNotEmptyHierarchicalServer(FastStringFormat.of("'",
                                                              validatingObject(),
                                                              "' must not be empty hierarchical server-based"));
   }

   public static <T extends URI> InvariantRule<T> isHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isHierarchicalServer(final String message,
                                                                       final MessageValue<T>... values) {
      return isHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isHierarchicalServer() {
      return isHierarchicalServer(FastStringFormat.of("'",
                                                      validatingObject(),
                                                      "' must be hierarchical server-based"));
   }

   public static <T extends URI> InvariantRule<T> isNotHierarchicalServer(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isHierarchicalServer, messageFormatter).ruleContext(
            "UriRules.isNotHierarchicalServer");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isNotHierarchicalServer(final String message,
                                                                          final MessageValue<T>... values) {
      return isNotHierarchicalServer(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isNotHierarchicalServer() {
      return isNotHierarchicalServer(FastStringFormat.of("'",
                                                         validatingObject(),
                                                         "' must not be hierarchical server-based"));
   }

   public static <T extends URI> InvariantRule<T> isHierarchicalRegistry(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Uri::isHierarchicalRegistry, messageFormatter).ruleContext(
            "UriRules.isHierarchicalRegistry");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isHierarchicalRegistry(final String message,
                                                                         final MessageValue<T>... values) {
      return isHierarchicalRegistry(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isHierarchicalRegistry() {
      return isHierarchicalRegistry(FastStringFormat.of("'",
                                                        validatingObject(),
                                                        "' must be hierarchical registry-based"));
   }

   public static <T extends URI> InvariantRule<T> isNotHierarchicalRegistry(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Uri::isHierarchicalRegistry, messageFormatter).ruleContext(
            "UriRules.isNotHierarchicalRegistry");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> isNotHierarchicalRegistry(final String message,
                                                                            final MessageValue<T>... values) {
      return isNotHierarchicalRegistry(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> isNotHierarchicalRegistry() {
      return isNotHierarchicalRegistry(FastStringFormat.of("'",
                                                           validatingObject(),
                                                           "' must not be hierarchical registry-based"));
   }

   public static <T extends URI> InvariantRule<T> hasNoTraversal(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(path -> {
         return nullable(path.getPath()).map(Path::of).map(parsePath -> {
            for (int i = 0; i < parsePath.getNameCount(); i++) {
               if (parsePath.getName(i).toString().equals("..")) {
                  return false;
               }
            }
            return true;
         }).orElse(true);
      }, messageFormatter).ruleContext("UriRules.hasNoTraversal");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoTraversal(final String message,
                                                                 final MessageValue<T>... values) {
      return hasNoTraversal(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoTraversal() {
      return hasNoTraversal(FastStringFormat.of("'", validatingObject(), "' must not have traversal paths"));
   }

   public static <T extends URI, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme,
                                                                                    final MessageFormatter<T> messageFormatter) {
      notNull(scheme, "scheme");

      return isAbsolute().andValue(satisfiesValue(uri -> schemeIsEqualTo(uri, scheme),
                                                  messageFormatter).ruleContext("UriRules.schemeIsEqualTo",
                                                                                scheme));
   }

   @SafeVarargs
   public static <T extends URI, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme,
                                                                                    final String message,
                                                                                    final MessageValue<T>... values) {
      return schemeIsEqualTo(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI, U extends String> InvariantRule<T> schemeIsEqualTo(final ParameterValue<U> scheme) {
      return schemeIsEqualTo(scheme,
                             FastStringFormat.of("'",
                                                 validatingObject(),
                                                 "' scheme must be equal to '",
                                                 parameter(0),
                                                 "'"));
   }

   public static <T extends URI, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme,
                                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(scheme, "scheme");

      return isAbsolute().andValue(satisfiesValue(uri -> !schemeIsEqualTo(uri, scheme),
                                                  messageFormatter).ruleContext("UriRules.schemeIsNotEqualTo",
                                                                                scheme));
   }

   private static <T extends URI, U extends String> boolean schemeIsEqualTo(T uri, ParameterValue<U> scheme) {
      return nullable(uri.getScheme())
            .map(s -> s.equalsIgnoreCase(scheme.requireNonNullValue()))
            .orElse(false);
   }

   @SafeVarargs
   public static <T extends URI, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
      return schemeIsNotEqualTo(scheme, DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI, U extends String> InvariantRule<T> schemeIsNotEqualTo(final ParameterValue<U> scheme) {
      return schemeIsNotEqualTo(scheme,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' scheme must not be equal to '",
                                                    parameter(0),
                                                    "'"));
   }

   public static <T extends URI> InvariantRule<T> hasAuthority(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getAuthority() != null, messageFormatter).ruleContext(
            "UriRules.hasAuthority");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasAuthority(final String message,
                                                               final MessageValue<T>... values) {
      return hasAuthority(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasAuthority() {
      return hasAuthority(FastStringFormat.of("'", validatingObject(), "' must have authority"));
   }

   public static <T extends URI> InvariantRule<T> hasNoAuthority(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(uri -> uri.getAuthority() != null, messageFormatter).ruleContext(
            "UriRules.hasNoAuthority");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoAuthority(final String message,
                                                                 final MessageValue<T>... values) {
      return hasNoAuthority(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoAuthority() {
      return hasNoAuthority(FastStringFormat.of("'", validatingObject(), "' must not have authority"));
   }

   public static <T extends URI> InvariantRule<T> hasUserInfo(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getUserInfo() != null, messageFormatter).ruleContext(
            "UriRules.hasUserInfo");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasUserInfo(final String message,
                                                              final MessageValue<T>... values) {
      return hasUserInfo(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasUserInfo() {
      return hasUserInfo(FastStringFormat.of("'", validatingObject(), "' must have user info"));
   }

   public static <T extends URI> InvariantRule<T> hasNoUserInfo(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getUserInfo() == null, messageFormatter).ruleContext(
            "UriRules.hasNoUserInfo");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoUserInfo(final String message,
                                                                final MessageValue<T>... values) {
      return hasNoUserInfo(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoUserInfo() {
      return hasNoUserInfo(FastStringFormat.of("'", validatingObject(), "' must not have user info"));
   }

   public static <T extends URI> InvariantRule<T> hasPassword(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getUserInfo() != null && uri.getUserInfo().indexOf(':') >= 0,
                            messageFormatter).ruleContext("UriRules.hasPassword");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasPassword(final String message,
                                                              final MessageValue<T>... values) {
      return hasPassword(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasPassword() {
      return hasPassword(FastStringFormat.of("'", validatingObject(), "' must have password"));
   }

   public static <T extends URI> InvariantRule<T> hasNoPassword(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getUserInfo() == null || uri.getUserInfo().indexOf(':') == -1,
                            messageFormatter).ruleContext("UriRules.hasNoPassword");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoPassword(final String message,
                                                                final MessageValue<T>... values) {
      return hasNoPassword(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoPassword() {
      return hasNoPassword(FastStringFormat.of("'", validatingObject(), "' must not have password"));
   }

   public static <T extends URI> InvariantRule<T> hasHost(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getHost() != null, messageFormatter).ruleContext("UriRules.hasHost");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasHost(final String message,
                                                          final MessageValue<T>... values) {
      return hasHost(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasHost() {
      return hasHost(FastStringFormat.of("'", validatingObject(), "' must have a host"));
   }

   public static <T extends URI> InvariantRule<T> hasNoHost(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getHost() == null, messageFormatter).ruleContext("UriRules.hasNoHost");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoHost(final String message,
                                                            final MessageValue<T>... values) {
      return hasNoHost(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoHost() {
      return hasNoHost(FastStringFormat.of("'", validatingObject(), "' must not have a host"));
   }

   public static <T extends URI> InvariantRule<T> hasPort(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getPort() != -1, messageFormatter).ruleContext("UriRules.hasPort");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasPort(final String message,
                                                          final MessageValue<T>... values) {
      return hasPort(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasPort() {
      return hasPort(FastStringFormat.of("'", validatingObject(), "' must have a port"));
   }

   public static <T extends URI> InvariantRule<T> hasNoPort(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getPort() == -1, messageFormatter).ruleContext("UriRules.hasNoPort");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoPort(final String message,
                                                            final MessageValue<T>... values) {
      return hasNoPort(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoPort() {
      return hasNoPort(FastStringFormat.of("'", validatingObject(), "' must not have a port"));
   }

   public static <T extends URI> InvariantRule<T> hasPath(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getPath() != null, messageFormatter).ruleContext("UriRules.hasPath");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasPath(final String message,
                                                          final MessageValue<T>... values) {
      return hasPath(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasPath() {
      return hasPath(FastStringFormat.of("'", validatingObject(), "' must have a path"));
   }

   public static <T extends URI> InvariantRule<T> hasNoPath(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getPath() == null, messageFormatter).ruleContext("UriRules.hasNoPath");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoPath(final String message,
                                                            final MessageValue<T>... values) {
      return hasNoPath(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoPath() {
      return hasNoPath(FastStringFormat.of("'", validatingObject(), "' must not have a path"));
   }

   public static <T extends URI> InvariantRule<T> hasQuery(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getQuery() != null, messageFormatter).ruleContext("UriRules.hasQuery");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasQuery(final String message,
                                                           final MessageValue<T>... values) {
      return hasQuery(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasQuery() {
      return hasQuery(FastStringFormat.of("'", validatingObject(), "' must have a query"));
   }

   public static <T extends URI> InvariantRule<T> hasNoQuery(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getQuery() == null,
                            messageFormatter).ruleContext("UriRules.hasNoQuery");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoQuery(final String message,
                                                             final MessageValue<T>... values) {
      return hasNoQuery(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoQuery() {
      return hasNoQuery(FastStringFormat.of("'", validatingObject(), "' must not have a query"));
   }

   public static <T extends URI> InvariantRule<T> hasFragment(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getFragment() != null, messageFormatter).ruleContext(
            "UriRules.hasFragment");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasFragment(final String message,
                                                              final MessageValue<T>... values) {
      return hasFragment(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasFragment() {
      return hasFragment(FastStringFormat.of("'", validatingObject(), "' must have a fragment"));
   }

   public static <T extends URI> InvariantRule<T> hasNoFragment(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(uri -> uri.getFragment() == null, messageFormatter).ruleContext(
            "UriRules.hasNoFragment");
   }

   @SafeVarargs
   public static <T extends URI> InvariantRule<T> hasNoFragment(final String message,
                                                                final MessageValue<T>... values) {
      return hasNoFragment(DefaultMessageFormat.of(message, values));
   }

   public static <T extends URI> InvariantRule<T> hasNoFragment() {
      return hasNoFragment(FastStringFormat.of("'", validatingObject(), "' must not have a fragment"));
   }

   public static <T extends URI> InvariantRule<T> normalized(final InvariantRule<? super URI> rule) {
      return as(URI::normalize, rule);
   }

   public static <T extends URI> InvariantRule<T> scheme(final InvariantRule<? super String> rule) {
      return property(URI::getScheme, "scheme", rule);
   }

   public static <T extends URI> InvariantRule<T> userInfo(final InvariantRule<? super String> rule) {
      return hasUserInfo().andValue(property(URI::getUserInfo, "userInfo", rule));
   }

   public static <T extends URI> InvariantRule<T> username(final InvariantRule<? super String> rule) {
      return hasUserInfo().andValue(as(URI::getUserInfo, property(UriRules::username, "username", rule)));
   }

   public static <T extends URI> InvariantRule<T> password(final InvariantRule<? super String> rule) {
      return hasUserInfo().andValue(as(URI::getUserInfo,
                                       property(ui -> password(ui).orElse(null), "password", rule)));
   }

   public static <T extends URI> InvariantRule<T> host(final InvariantRule<? super String> rule) {
      return hasHost().andValue(property(URI::getHost, "host", rule));
   }

   public static <T extends URI> InvariantRule<T> port(final InvariantRule<? super Integer> rule) {
      return hasPort().andValue(property(URI::getPort, "port", rule));
   }

   public static <T extends URI> InvariantRule<T> path(final InvariantRule<? super String> rule) {
      return hasPath().andValue(property(URI::getPath, "path", rule));
   }

   public static <T extends URI> InvariantRule<T> query(final InvariantRule<? super String> rule) {
      return hasQuery().andValue(property(URI::getQuery, "query", rule));
   }

   public static <T extends URI> InvariantRule<T> fragment(final InvariantRule<? super String> rule) {
      return hasFragment().andValue(property(URI::getFragment, "fragment", rule));
   }

   static String username(String userInfo) {
      var sepIdx = userInfo.indexOf(':');
      if (sepIdx == -1) {
         return userInfo;
      } else {
         return userInfo.substring(0, sepIdx);
      }
   }

   static Optional<String> password(String userInfo) {
      var sepIdx = userInfo.indexOf(':');
      if (sepIdx == -1) {
         return optional();
      } else {
         return optional(userInfo.substring(sepIdx + 1));
      }
   }

   public static class UrnRules {

      public static <T extends URI> InvariantRule<T> isUrn(final MessageFormatter<T> messageFormatter) {
         return satisfiesValue(UrnRules::isUrn, messageFormatter).ruleContext("UriRules.UrnRules.isUrn");
      }

      @SafeVarargs
      public static <T extends URI> InvariantRule<T> isUrn(final String message,
                                                           final MessageValue<T>... values) {
         return isUrn(DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI> InvariantRule<T> isUrn() {
         return isUrn(FastStringFormat.of("'",
                                          validatingObject(),
                                          "' must be a valid URN : 'urn:<nid>:<nss>'"));
      }

      public static <T extends URI> InvariantRule<T> isNotUrn(final MessageFormatter<T> messageFormatter) {
         return isAbsolute().andValue(satisfiesValue(uri -> !isUrn(uri), messageFormatter).ruleContext(
               "UriRules.UrnRules.isNotUrn"));
      }

      @SafeVarargs
      public static <T extends URI> InvariantRule<T> isNotUrn(final String message,
                                                              final MessageValue<T>... values) {
         return isNotUrn(DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI> InvariantRule<T> isNotUrn() {
         return isNotUrn(FastStringFormat.of("'", validatingObject(), "' must not be a valid URN"));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nidIsEqualTo(final ParameterValue<U> nid,
                                                                                    final MessageFormatter<T> messageFormatter) {
         notNull(nid, "nid");

         return isUrn().andValue(satisfiesValue(uri -> nidIsEqualTo(uri, nid), messageFormatter).ruleContext(
               "UriRules.UrnRules.nidIsEqualTo",
               nid));
      }

      @SafeVarargs
      public static <T extends URI, U extends String> InvariantRule<T> nidIsEqualTo(final ParameterValue<U> nid,
                                                                                    final String message,
                                                                                    final MessageValue<T>... values) {
         return nidIsEqualTo(nid, DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nidIsEqualTo(final ParameterValue<U> nid) {
         return nidIsEqualTo(nid,
                             FastStringFormat.of("'",
                                                 validatingObject(),
                                                 "' URN NID must be equal to '",
                                                 parameter(0),
                                                 "' : 'urn:",
                                                 parameter(0),
                                                 ":<nss>'"));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nidIsNotEqualTo(final ParameterValue<U> nid,
                                                                                       final MessageFormatter<T> messageFormatter) {
         notNull(nid, "nid");

         return isUrn().andValue(satisfiesValue(uri -> !nidIsEqualTo(uri, nid), messageFormatter).ruleContext(
               "UriRules.UrnRules.nidIsNotEqualTo",
               nid));
      }

      @SafeVarargs
      public static <T extends URI, U extends String> InvariantRule<T> nidIsNotEqualTo(final ParameterValue<U> nid,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
         return nidIsNotEqualTo(nid, DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nidIsNotEqualTo(final ParameterValue<U> nid) {
         return nidIsNotEqualTo(nid,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' URN NID must not be equal to '",
                                                    parameter(0),
                                                    "' : 'urn:<nid>:",
                                                    parameter(0),
                                                    "'"));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsEqualTo(final ParameterValue<U> nss,
                                                                                    final MessageFormatter<T> messageFormatter) {
         notNull(nss, "nss");

         return isUrn().andValue(satisfiesValue(uri -> nssIsEqualTo(uri, nss), messageFormatter).ruleContext(
               "UriRules.UrnRules.nssIsEqualTo",
               nss));
      }

      @SafeVarargs
      public static <T extends URI, U extends String> InvariantRule<T> nssIsEqualTo(final ParameterValue<U> nss,
                                                                                    final String message,
                                                                                    final MessageValue<T>... values) {
         return nssIsEqualTo(nss, DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsEqualTo(final ParameterValue<U> nss) {
         return nssIsEqualTo(nss,
                             FastStringFormat.of("'",
                                                 validatingObject(),
                                                 "' URN NSS must be equal to '",
                                                 parameter(0),
                                                 "'"));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsNotEqualTo(final ParameterValue<U> nss,
                                                                                       final MessageFormatter<T> messageFormatter) {
         notNull(nss, "nss");

         return isUrn().andValue(satisfiesValue(uri -> !nssIsEqualTo(uri, nss), messageFormatter).ruleContext(
               "UriRules.UrnRules.nssIsNotEqualTo",
               nss));
      }

      @SafeVarargs
      public static <T extends URI, U extends String> InvariantRule<T> nssIsNotEqualTo(final ParameterValue<U> nss,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
         return nssIsNotEqualTo(nss, DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsNotEqualTo(final ParameterValue<U> nss) {
         return nssIsNotEqualTo(nss,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' URN NSS must not be equal to '",
                                                    parameter(0),
                                                    "'"));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsEqualToIgnoreCase(final ParameterValue<U> nss,
                                                                                              final MessageFormatter<T> messageFormatter) {
         notNull(nss, "nss");

         return isUrn().andValue(satisfiesValue(uri -> nssIsEqualToIgnoreCase(uri, nss),
                                                messageFormatter).ruleContext(
               "UriRules.UrnRules.nssIsEqualToIgnoreCase",
               nss));
      }

      @SafeVarargs
      public static <T extends URI, U extends String> InvariantRule<T> nssIsEqualToIgnoreCase(final ParameterValue<U> nss,
                                                                                              final String message,
                                                                                              final MessageValue<T>... values) {
         return nssIsEqualToIgnoreCase(nss, DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsEqualToIgnoreCase(final ParameterValue<U> nss) {
         return nssIsEqualToIgnoreCase(nss,
                                       FastStringFormat.of("'",
                                                           validatingObject(),
                                                           "' URN NSS must be equal to '",
                                                           parameter(0),
                                                           "' (case-insensitive)"));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsNotEqualToIgnoreCase(final ParameterValue<U> nss,
                                                                                                 final MessageFormatter<T> messageFormatter) {
         notNull(nss, "nss");

         return isUrn().andValue(satisfiesValue(uri -> !nssIsEqualToIgnoreCase(uri, nss),
                                                messageFormatter).ruleContext(
               "UriRules.UrnRules.nssIsNotEqualToIgnoreCase",
               nss));
      }

      @SafeVarargs
      public static <T extends URI, U extends String> InvariantRule<T> nssIsNotEqualToIgnoreCase(final ParameterValue<U> nss,
                                                                                                 final String message,
                                                                                                 final MessageValue<T>... values) {
         return nssIsNotEqualToIgnoreCase(nss, DefaultMessageFormat.of(message, values));
      }

      public static <T extends URI, U extends String> InvariantRule<T> nssIsNotEqualToIgnoreCase(final ParameterValue<U> nss) {
         return nssIsNotEqualToIgnoreCase(nss,
                                          FastStringFormat.of("'",
                                                              validatingObject(),
                                                              "' URN NSS must not be equal to '",
                                                              parameter(0),
                                                              "' (case-insensitive)"));
      }

      public static <T extends URI> InvariantRule<T> nid(final InvariantRule<? super String> rule) {
         return isUrn().andValue(property(uri -> {
            String[] parts = uri.getSchemeSpecificPart().split(":", 2);
            return parts[0].toLowerCase();
         }, "nid", rule));
      }

      public static <T extends URI> InvariantRule<T> nss(final InvariantRule<? super String> rule) {
         return isUrn().andValue(property(uri -> {
            String[] parts = uri.getSchemeSpecificPart().split(":", 2);
            return parts[1];
         }, "nss", rule));
      }

      private static <T extends URI> boolean isUrn(T uri) {
         if (uri.isOpaque() && uri.getScheme().equalsIgnoreCase("urn")) {
            String[] parts = uri.getSchemeSpecificPart().split(":", 2);
            return (parts.length == 2 && parts[0].length() > 0 && parts[1].length() > 0);
         }

         return false;
      }

      private static <T extends URI, U extends String> boolean nidIsEqualTo(T uri, ParameterValue<U> nid) {
         String[] parts = uri.getSchemeSpecificPart().split(":", 2);

         return parts[0].equalsIgnoreCase(nid.requireNonNullValue());
      }

      private static <T extends URI, U extends String> boolean nssIsEqualTo(T uri, ParameterValue<U> nss) {
         String[] parts = uri.getSchemeSpecificPart().split(":", 2);

         return parts[1].contentEquals(nss.requireNonNullValue());
      }

      private static <T extends URI, U extends String> boolean nssIsEqualToIgnoreCase(URI uri,
                                                                                      ParameterValue<U> nss) {
         String[] parts = uri.getSchemeSpecificPart().split(":", 2);

         return parts[1].equalsIgnoreCase(nss.requireNonNullValue());
      }

   }

}
