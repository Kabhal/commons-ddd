/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

public class FloatRules {

   private FloatRules() {
   }

   public static InvariantRule<Float> isFinite(final MessageFormatter<Float> messageFormatter) {
      return isNotNull()
            .andValue(isNotNan())
            .andValue(satisfiesValue(value -> !Float.isInfinite(value), messageFormatter).ruleContext(
                  "FloatRules.isFinite"));
   }

   @SafeVarargs
   public static InvariantRule<Float> isFinite(final String message, final MessageValue<Float>... values) {
      return isFinite(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Float> isFinite() {
      return isFinite(FastStringFormat.of("'", validatingObject(), "' must be finite"));
   }

   public static InvariantRule<Float> isNotNan(final MessageFormatter<Float> messageFormatter) {
      return notSatisfiesValue(value -> Float.isNaN(value), messageFormatter).ruleContext(
            "FloatRules.isNotNan");
   }

   @SafeVarargs
   public static InvariantRule<Float> isNotNan(final String message, final MessageValue<Float>... values) {
      return isNotNan(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Float> isNotNan() {
      return isNotNan(FastStringFormat.of("'", validatingObjectName(), "' must not be NaN"));
   }

}
