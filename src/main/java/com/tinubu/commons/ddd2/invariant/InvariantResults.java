/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.ddd2.invariant.InvariantResult.DEFAULT_LOGGER_LEVEL;
import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.Map.Entry;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.AbstractList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;

import org.slf4j.Logger;

import com.tinubu.commons.ddd2.invariant.InvariantResult.InvariantStatus;
import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.lang.log.ExtendedLogger.Level;
import com.tinubu.commons.lang.util.StreamUtils;
import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * A result set of {@link InvariantResult}.
 *
 * @implSpec Mutable class implementation to support {@link #defer(InvariantResults)}, {@link
 *       InvariantResult#defer(InvariantResults)}} deferring mechanisms.
 */
public class InvariantResults extends AbstractList<InvariantResult<?>> {

   private List<InvariantResult<?>> results;
   private final InvariantContext context;

   private InvariantResults(InvariantContext context, List<InvariantResult<?>> results) {
      this.context = notNull(context, "context");
      this.results = validateResults(results);
   }

   public static InvariantResults empty() {
      return new InvariantResults(InvariantContext.empty(), emptyList());
   }

   public static InvariantResults of(InvariantContext context, List<InvariantResult<?>> results) {
      return new InvariantResults(context, immutable(results));
   }

   public static InvariantResults of(List<InvariantResult<?>> results) {
      return of(InvariantContext.empty(), immutable(results));
   }

   public static InvariantResults of(InvariantContext context, InvariantResult<?>... results) {
      return new InvariantResults(context, immutable(list(results)));
   }

   public static InvariantResults of(InvariantResult<?>... results) {
      return new InvariantResults(InvariantContext.empty(), immutable(list(results)));
   }

   @Override
   public InvariantResult<?> get(int index) {
      return results.get(index);
   }

   @Override
   public int size() {
      return results.size();
   }

   /**
    * Adds a new result to result list.
    *
    * @param invariantResult new result to add to result list
    *
    * @return always {@code true}
    */
   @Override
   public boolean add(InvariantResult<?> invariantResult) {
      notNull(invariantResult, "invariantResult");

      this.results = immutable(validateResults(listConcat(StreamUtils.stream(this.results),
                                                          StreamUtils.stream(invariantResult))));

      return true;
   }

   /**
    * Returns context for these results.
    *
    * @return results context
    */
   public InvariantContext context() {
      return context;
   }

   /**
    * Returns {@code true} if all results are in success.
    *
    * @return {@code true} if all results are in success
    */
   public boolean success() {
      return results.stream().allMatch(InvariantResult::success);
   }

   /**
    * Filters these results with specified predicate.
    *
    * @param resultPredicate filter predicate
    *
    * @return new results with filtered entries
    *
    * @see InvariantResult#invariantNamePredicate(String)
    * @see InvariantResult#invariantStatusPredicate(InvariantStatus)
    * @see InvariantResult#invariantRuleNamePredicate(String)
    */
   public InvariantResults filter(Predicate<? super InvariantResult<?>> resultPredicate) {
      notNull(resultPredicate, "resultPredicate");
      return new InvariantResults(context, immutable(list(results.stream().filter(resultPredicate))));
   }

   /**
    * Generates a formatted error message for this result on a single line.
    * Results in {@link InvariantStatus#SUCCESS} status are filtered out.
    *
    * @param withContext whether to add context to error message
    *
    * @return formatted error message
    */
   public String errorMessage(boolean withPrefix, boolean withContext) {
      String contextMessage = "";

      if (withContext && !context.isEmpty()) {
         contextMessage = "Context "
                          + context.stream().map(Object::toString).collect(joining(",", "[", "]"))
                          + (results.isEmpty() ? "" : " > ");
      }

      String prefixMessage = "";

      if (withPrefix) {
         prefixMessage =
               "Invariant validation error" + (contextMessage.isEmpty() && results.isEmpty() ? "" : " > ");
      }

      return prefixMessage + contextMessage + results
            .stream()
            .filter(result -> !result.success())
            .map(InvariantResult::errorMessage)
            .collect(joining(" | "));
   }

   /**
    * Generates a formatted error message with context for this result on a single line.
    *
    * @return formatted error message with context
    */
   public String errorMessage() {
      return errorMessage(true, true);
   }

   /**
    * Throws specified exception if this result set is not in {@link #success()}.
    *
    * @param validationException custom validation exception generator from validation results
    *
    * @return this for convenience
    *
    * @throws InvariantValidationException if validation fails
    */
   public <E extends RuntimeException> InvariantResults orThrow(Function<? super InvariantResults, E> validationException) {
      notNull(validationException, "validationException");

      if (!success()) {
         throw validationException.apply(this);
      }

      return this;
   }

   /**
    * Throws specified exception if this result set is not in {@link #success()}.
    *
    * @param validationException custom validation exception generator from validation error message
    *
    * @return this for convenience
    *
    * @throws InvariantValidationException if validation fails
    */
   public <E extends RuntimeException> InvariantResults orThrowMessage(Function<String, E> validationException) {
      notNull(validationException, "validationException");

      if (!success()) {
         throw validationException.apply(errorMessage());
      }

      return this;
   }

   /**
    * Throws {@link InvariantValidationException} exception if this result set is not in {@link #success()}.
    *
    * @return this for convenience
    *
    * @throws InvariantValidationException if validation fails
    */
   public InvariantResults orThrow() {
      return orThrow(InvariantValidationException::new);
   }

   /**
    * Logs to specified logger if result status is not {@link InvariantStatus#SUCCESS}.
    *
    * @param logger logger
    * @param level log level
    *
    * @return this for convenience
    *
    * @throws InvariantValidationException if validation fails
    */
   public InvariantResults orLog(Logger logger, Level level) {
      notNull(logger, "logger");

      if (!success()) {
         ExtendedLogger.of(logger).log(level, this::errorMessage);
      }
      return this;
   }

   /**
    * Logs to specified logger/{@link InvariantResult#DEFAULT_LOGGER_LEVEL} if result status is not
    * {@link InvariantStatus#SUCCESS}.
    *
    * @param logger logger
    *
    * @return this for convenience
    *
    * @throws InvariantValidationException if validation fails
    */
   public InvariantResults orLog(Logger logger) {
      return orLog(logger, DEFAULT_LOGGER_LEVEL);
   }

   /**
    * Logs error message to specified writer with an extra {@link System#lineSeparator() line return} if
    * result status is not {@link InvariantStatus#SUCCESS}.
    *
    * @param writer output writer
    *
    * @return this for convenience
    *
    * @throws InvariantValidationException if validation fails
    * @throws UncheckedIOException if an I/O error occurs while writing to writer
    */
   public InvariantResults orLog(Writer writer) {
      notNull(writer, "writer");

      if (!success()) {
         try {
            writer.write(errorMessage());
            writer.write(System.lineSeparator());
         } catch (IOException e) {
            runtimeThrow(e);
         }
      }
      return this;
   }

   /**
    * Logs error message to specified output stream, in UTF-8, with an extra
    * {@link System#lineSeparator() line return} if result status is not {@link InvariantStatus#SUCCESS}.
    *
    * @param output output stream
    *
    * @return this for convenience
    *
    * @throws InvariantValidationException if validation fails
    * @throws UncheckedIOException if an I/O error occurs while writing to output stream
    */
   public InvariantResults orLog(OutputStream output) {
      notNull(output, "output");

      if (!success()) {
         try {
            output.write(errorMessage().getBytes(StandardCharsets.UTF_8));
            output.write(System.lineSeparator().getBytes(StandardCharsets.UTF_8));
         } catch (IOException e) {
            runtimeThrow(e);
         }
      }
      return this;
   }

   /**
    * Defers this result set to specified {@link InvariantResults} for later evaluation.
    * This enables result set concatenation.
    *
    * @return this for convenience
    */
   public InvariantResults defer(InvariantResults results) {
      notNull(results, "results");

      results.addAll(this);
      return this;
   }

   /**
    * Adds new result to this results block with a fluent style.
    *
    * @param result result
    *
    * @return this result set for checking
    */
   @CheckReturnValue
   public InvariantResults and(InvariantResult<?> result) {
      notNull(result, "result");

      add(result);
      return this;
   }

   /**
    * Checks if all results in error status match one of specified rule names.
    *
    * @param ruleNames rule names to match
    *
    * @return {@code true} if all results in error match one of specified rule names
    */
   public boolean allErrorsMatchRule(String... ruleNames) {
      var ruleNamesSet = collection(HashSet::new, ruleNames);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .allMatch(result -> result
                  .validatingObject()
                  .ruleName()
                  .map(ruleNamesSet::contains)
                  .orElse(false));
   }

   /**
    * Checks if all results in error status match one of specified invariant group names.
    *
    * @param groups group names to match
    *
    * @return {@code true} if all results in error match one of specified invariant group names
    */
   public boolean allErrorsMatchInvariantGroup(String... groups) {
      var groupsSet = collection(HashSet::new, groups);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .allMatch(result -> result.invariantGroups().stream().anyMatch(groupsSet::contains));
   }

   /**
    * Checks if all results in error status match one of specified invariant matching group names.
    *
    * @param matchingGroups matching group names to match
    *
    * @return {@code true} if all results in error match one of specified invariant matching group names
    */
   public boolean allErrorsMatchInvariantMatchingGroup(String... matchingGroups) {
      var matchingGroupsSet = collection(HashSet::new, matchingGroups);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .allMatch(result -> result
                  .invariantMatchingGroups()
                  .stream()
                  .anyMatch(matchingGroupsSet::contains));
   }

   /**
    * Checks if any result in error status matches one of specified rule names.
    *
    * @param ruleNames rule names to match
    *
    * @return {@code true} if any result in error matches one of specified rule names
    */
   public boolean anyErrorMatchRule(String... ruleNames) {
      var ruleNamesSet = collection(HashSet::new, ruleNames);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .anyMatch(result -> result
                  .validatingObject()
                  .ruleName()
                  .map(ruleNamesSet::contains)
                  .orElse(false));
   }

   /**
    * Checks if any result in error status matches one of specified invariant group names.
    *
    * @param groups group names to match
    *
    * @return {@code true} if any result in error matches one of specified group names
    */
   public boolean anyErrorMatchInvariantGroup(String... groups) {
      var groupsSet = collection(HashSet::new, groups);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .anyMatch(result -> result.invariantGroups().stream().anyMatch(groupsSet::contains));
   }

   /**
    * Checks if any result in error status matches one of specified invariant matching group names.
    *
    * @param matchingGroups matching group names to match
    *
    * @return {@code true} if any result in error matches one of specified group names
    */
   public boolean anyErrorMatchInvariantMatchingGroup(String... matchingGroups) {
      var matchingGroupsSet = collection(HashSet::new, matchingGroups);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .anyMatch(result -> result
                  .invariantMatchingGroups()
                  .stream()
                  .anyMatch(matchingGroupsSet::contains));
   }

   /**
    * Checks if none result in error status matches one of specified rule names.
    *
    * @param ruleNames rule names to match
    *
    * @return {@code true} if none result in error matches one of specified rule names
    */
   public boolean noneErrorMatchRule(String... ruleNames) {
      var ruleNamesSet = collection(HashSet::new, ruleNames);

      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .noneMatch(result -> result
                  .validatingObject()
                  .ruleName()
                  .map(ruleNamesSet::contains)
                  .orElse(false));
   }

   /**
    * Checks if none result in error status matches one of specified invariant group names.
    *
    * @param groups group names to match
    *
    * @return {@code true} if none result in error matches one of specified group names
    */
   public boolean noneErrorMatchInvariantGroup(String... groups) {
      var groupsSet = collection(HashSet::new, groups);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .noneMatch(result -> result.invariantGroups().stream().anyMatch(groupsSet::contains));
   }

   /**
    * Checks if none result in error status matches one of specified invariant matching group names.
    *
    * @param matchingGroups matching group names to match
    *
    * @return {@code true} if none result in error matches one of specified group names
    */
   public boolean noneErrorMatchInvariantMatchingGroup(String... matchingGroups) {
      var matchingGroupsSet = collection(HashSet::new, matchingGroups);
      return results
            .stream()
            .filter(invariantResult -> !invariantResult.success())
            .noneMatch(result -> result
                  .invariantMatchingGroups()
                  .stream()
                  .anyMatch(matchingGroupsSet::contains));
   }

   @Override
   public String toString() {
      return new StringJoiner(",", InvariantResults.class.getSimpleName() + "[", "]")
            .add("results=" + results)
            .add("context=" + context)
            .toString();
   }

   private List<InvariantResult<?>> validateResults(List<InvariantResult<?>> results) {
      noNullElements(results, "results");

      List<String> invariantNamesDuplicates = results
            .stream()
            .map(InvariantResult::invariantName)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(groupingBy(Function.identity(), counting()))
            .entrySet()
            .stream()
            .filter(e -> e.getValue() > 1)
            .map(Entry::getKey)
            .collect(toList());

      if (invariantNamesDuplicates.size() > 0) {
         throw new IllegalArgumentException(String.format("Found invariant name duplicates in results : %s",
                                                          invariantNamesDuplicates));
      }

      return results;
   }
}