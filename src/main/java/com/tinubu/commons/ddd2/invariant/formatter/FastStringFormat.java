/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.formatter;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ValidatingObject;

/**
 * Basic formatter optimized for performance, mainly for internal rule library.
 * This format takes a list of objects and concatenate them. An object can be any {@link Object} or a {@link
 * MessageValue} to include validation references.
 *
 * @param <T> validating object type
 */
public interface FastStringFormat<T> extends MessageFormatter<T> {

   static <T> FastStringFormat<T> of() {
      return new FastStringFormat0<>();
   }

   static <T> FastStringFormat<T> of(Object value) {
      return new FastStringFormat1<>(value);
   }

   static <T> FastStringFormat<T> of(Object value1, Object value2) {
      return new FastStringFormat2<>(value1, value2);
   }

   static <T> FastStringFormat<T> of(Object value1, Object value2, Object value3) {
      return new FastStringFormat3<>(value1, value2, value3);
   }

   static <T> FastStringFormat<T> of(Object... values) {
      return new FastStringFormatN<>(values);
   }

   /**
    * 0-parameter {@link FastStringFormat} specialization for performance.
    */
   class FastStringFormat0<T> implements FastStringFormat<T> {

      private FastStringFormat0() {
      }

      @Override
      public String format(ValidatingObject<T> validatingObject) {
         return "";
      }

   }

   /**
    * 1-parameter {@link FastStringFormat} specialization.
    */
   class FastStringFormat1<T> extends AbstractMessageFormatter<T> implements FastStringFormat<T> {
      private final Object value;

      private FastStringFormat1(Object value) {
         this.value = notNull(value, "value");
      }

      @Override
      @SuppressWarnings("unchecked")
      public String format(ValidatingObject<T> validatingObject) {
         if (value instanceof MessageValue) {
            return ((MessageValue<T>) value).value(validatingObject).toString();
         } else {
            return value.toString();
         }
      }

   }

   /**
    * 2-parameter {@link FastStringFormat} specialization.
    */
   class FastStringFormat2<T> extends AbstractMessageFormatter<T> implements FastStringFormat<T> {
      private final Object value1;
      private final Object value2;

      private FastStringFormat2(Object value1, Object value2) {
         this.value1 = notNull(value1, "value1");
         this.value2 = notNull(value2, "value2");
      }

      @Override
      @SuppressWarnings("unchecked")
      public String format(ValidatingObject<T> validatingObject) {
         String m1;
         String m2;

         if (value1 instanceof MessageValue) {
            m1 = ((MessageValue<T>) value1).value(validatingObject).toString();
         } else {
            m1 = value1.toString();
         }

         if (value2 instanceof MessageValue) {
            m2 = ((MessageValue<T>) value2).value(validatingObject).toString();
         } else {
            m2 = value2.toString();
         }

         return m1 + m2;
      }

   }

   /**
    * 3-parameter {@link FastStringFormat} specialization.
    */
   class FastStringFormat3<T> extends AbstractMessageFormatter<T> implements FastStringFormat<T> {
      private final Object value1;
      private final Object value2;
      private final Object value3;

      private FastStringFormat3(Object value1, Object value2, Object value3) {
         this.value1 = notNull(value1, "value1");
         this.value2 = notNull(value2, "value2");
         this.value3 = notNull(value3, "value3");
      }

      @Override
      @SuppressWarnings("unchecked")
      public String format(ValidatingObject<T> validatingObject) {
         String m1;
         String m2;
         String m3;

         if (value1 instanceof MessageValue) {
            m1 = ((MessageValue<T>) value1).value(validatingObject).toString();
         } else {
            m1 = value1.toString();
         }

         if (value2 instanceof MessageValue) {
            m2 = ((MessageValue<T>) value2).value(validatingObject).toString();
         } else {
            m2 = value2.toString();
         }

         if (value3 instanceof MessageValue) {
            m3 = ((MessageValue<T>) value3).value(validatingObject).toString();
         } else {
            m3 = value3.toString();
         }

         return m1 + m2 + m3;
      }

   }

   /**
    * N-parameter {@link FastStringFormat} specialization.
    */
   class FastStringFormatN<T> extends AbstractMessageFormatter<T> implements FastStringFormat<T> {
      private final Object[] values;

      private FastStringFormatN(Object[] values) {
         this.values = notNull(values, "values");
      }

      @Override
      @SuppressWarnings("unchecked")
      public String format(ValidatingObject<T> validatingObject) {
         StringBuilder message = new StringBuilder();
         for (Object value : values) {
            if (value instanceof MessageValue) {
               message.append(((MessageValue<T>) value).value(validatingObject));
            } else {
               message.append(value);
            }
         }
         return message.toString();
      }

   }
}
