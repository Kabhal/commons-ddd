/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.InvariantResult.InvariantStatus.ERROR;
import static com.tinubu.commons.ddd2.invariant.InvariantResult.InvariantStatus.SUCCESS;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.slf4j.Logger;

import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.lang.log.ExtendedLogger.Level;
import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * Single invariant result.
 *
 * @param <T> validating object type
 *
 * @implSpec Immutable class implementation.
 */
public class InvariantResult<T> {

   /**
    * Whether to format and include validation message in result on success. Set to {@code false} to improve
    * performance.
    */
   static final boolean VALIDATION_MESSAGE_ON_SUCCESS = false;

   /** Default log level for {@link #orLog(Logger)}. */
   static final Level DEFAULT_LOGGER_LEVEL = Level.WARN;

   /**
    * Whether to display matching groups in {@link #errorMessage()}. Change this behavior with
    * {@link #errorMessageIncludesMatchingGroups()}
    */
   private static boolean errorMessageIncludesMatchingGroups = false;

   /** Optional name of evaluated invariant. */
   private final String invariantName;

   /** Groups of evaluated invariant, can be empty. */
   private final List<String> invariantGroups;

   /** Matching groups of evaluated invariant, can be empty. */
   private final List<String> invariantMatchingGroups;

   /** Validating object. */
   private final ValidatingObject<T> validatingObject;

   /** Result status. */
   private final InvariantStatus status;

   /** Evaluation message. */
   private final String message;

   public InvariantResult(String invariantName,
                          List<String> invariantGroups,
                          List<String> invariantMatchingGroups,
                          ValidatingObject<T> validatingObject,
                          InvariantStatus status,
                          String message) {
      this.invariantName = invariantName == null ? null : notBlank(invariantName, "invariantName");
      this.invariantGroups = immutable(noNullElements(invariantGroups, "invariantGroups"));
      this.invariantMatchingGroups =
            immutable(noNullElements(invariantMatchingGroups, "invariantMatchingGroups"));
      this.validatingObject = notNull(validatingObject, "validatingObject");
      this.status = notNull(status, "status");
      this.message = notNull(message, "message");
   }

   /** Globally updates {@link #errorMessage()} behavior. */
   public static void errorMessageIncludesMatchingGroups(boolean errorMessageIncludesMatchingGroups) {
      InvariantResult.errorMessageIncludesMatchingGroups = errorMessageIncludesMatchingGroups;
   }

   public static <T> InvariantResult<T> success(ValidatingObject<T> validatingObject,
                                                MessageFormatter<T> messageFormatter) {
      return new InvariantResult<>(null,
                                   emptyList(),
                                   emptyList(),
                                   validatingObject,
                                   SUCCESS,
                                   VALIDATION_MESSAGE_ON_SUCCESS
                                   ? messageFormatter.format(validatingObject)
                                   : "");
   }

   public static <T> InvariantResult<T> error(ValidatingObject<T> validatingObject,
                                              MessageFormatter<T> messageFormatter) {
      return new InvariantResult<>(null,
                                   emptyList(),
                                   emptyList(),
                                   validatingObject,
                                   ERROR,
                                   messageFormatter.format(validatingObject));
   }

   public Optional<String> invariantName() {
      return nullable(invariantName);
   }

   public InvariantResult<T> invariantName(String invariantName) {
      return new InvariantResult<>(invariantName,
                                   this.invariantGroups,
                                   this.invariantMatchingGroups,
                                   this.validatingObject,
                                   this.status,
                                   this.message);
   }

   public List<String> invariantGroups() {
      return invariantGroups;
   }

   public InvariantResult<T> invariantGroups(List<String> invariantGroups) {
      return new InvariantResult<>(this.invariantName,
                                   invariantGroups,
                                   this.invariantMatchingGroups,
                                   this.validatingObject,
                                   this.status,
                                   this.message);
   }

   public InvariantResult<T> invariantGroups(String... invariantGroups) {
      return invariantGroups(list(invariantGroups));
   }

   public List<String> invariantMatchingGroups() {
      return invariantMatchingGroups;
   }

   public InvariantResult<T> invariantMatchingGroups(List<String> invariantMatchingGroups) {
      return new InvariantResult<>(this.invariantName,
                                   this.invariantGroups,
                                   invariantMatchingGroups,
                                   this.validatingObject,
                                   this.status,
                                   this.message);
   }

   public InvariantResult<T> invariantMatchingGroups(String... invariantMatchingGroups) {
      return invariantMatchingGroups(list(invariantMatchingGroups));
   }

   public ValidatingObject<T> validatingObject() {
      return validatingObject;
   }

   public InvariantStatus status() {
      return status;
   }

   public String message() {
      return message;
   }

   public boolean success() {
      return status == SUCCESS;
   }

   /**
    * Generates a formatted error message for this result.
    *
    * @return formatted error message
    */
   public String errorMessage() {
      if (errorMessageIncludesMatchingGroups) {
         var matchingGroups = invariantMatchingGroups.isEmpty()
                              ? null
                              : "[" + String.join(",", invariantMatchingGroups) + "]";
         return nullable(invariantName)
                      .map(name -> "{" + nullable(matchingGroups).orElse("") + name + "} ")
                      .orElse(nullable(matchingGroups).map(mg -> "{" + mg + "} ").orElse("")) + message;
      } else {
         return nullable(invariantName).map(name -> "{" + name + "} ").orElse("") + message;
      }
   }

   /**
    * Returns initial validating object's value if result status is {@link InvariantStatus#SUCCESS} or throw
    * specified exception.
    *
    * @param validationException custom validation exception generator from validation result
    *
    * @return validating object initial value
    *
    * @throws E if validation fails
    */
   public <E extends RuntimeException> T orThrow(Function<? super InvariantResult<T>, E> validationException) {
      notNull(validationException, "validationException");

      if (success()) {
         return validatingObject.value();
      } else {
         throw validationException.apply(this);
      }
   }

   /**
    * Returns initial validating object's value if result status is {@link InvariantStatus#SUCCESS} or throw
    * specified exception.
    *
    * @param validationException custom validation exception generator from validation error message
    *
    * @return validating object initial value
    *
    * @throws E if validation fails
    */
   public <E extends RuntimeException> T orThrowMessage(Function<String, E> validationException) {
      notNull(validationException, "validationException");

      if (success()) {
         return validatingObject.value();
      } else {
         throw validationException.apply(errorMessage());
      }
   }

   /**
    * Returns initial validating object's value if result status is {@link InvariantStatus#SUCCESS} or throw
    * a {@link InvariantValidationException} exception.
    *
    * @return validating object initial value
    *
    * @throws InvariantValidationException if validation fails
    */
   public T orThrow() {
      return orThrow(InvariantValidationException::new);
   }

   /**
    * Returns initial validating object's value whatever the result status, and log to
    * specified logger/level.
    *
    * @param logger logger
    * @param level log level
    *
    * @return validating object initial value
    */
   public T orLog(Logger logger, Level level) {
      notNull(logger, "logger");

      if (!success()) {
         ExtendedLogger.of(logger).log(level, this::errorMessage);
      }
      return validatingObject.value();
   }

   /**
    * Returns initial validating object's value whatever the result status, and log to
    * specified logger/{@link #DEFAULT_LOGGER_LEVEL}.
    *
    * @param logger logger
    *
    * @return validating object initial value
    */
   public T orLog(Logger logger) {
      return orLog(logger, DEFAULT_LOGGER_LEVEL);
   }

   /**
    * Returns initial validating object's value whatever the result status, and log
    * error message to specified writer with an extra {@link System#lineSeparator() line return}.
    *
    * @param writer output writer
    *
    * @return validating object initial value
    *
    * @throws UncheckedIOException if an I/O error occurs while writing to writer
    */
   public T orLog(Writer writer) {
      notNull(writer, "writer");

      if (!success()) {
         try {
            writer.write(errorMessage());
            writer.write(System.lineSeparator());
         } catch (IOException e) {
            runtimeThrow(e);
         }
      }
      return validatingObject.value();
   }

   /**
    * Returns initial validating object's value whatever the result status, and log
    * error message to specified output stream, in UTF-8, with an extra
    * {@link System#lineSeparator() line return}.
    *
    * @param output output stream
    *
    * @return validating object initial value
    *
    * @throws UncheckedIOException if an I/O error occurs while writing to output stream
    */
   public T orLog(OutputStream output) {
      notNull(output, "output");

      if (!success()) {
         try {
            output.write(errorMessage().getBytes(StandardCharsets.UTF_8));
            output.write(System.lineSeparator().getBytes(StandardCharsets.UTF_8));
         } catch (IOException e) {
            runtimeThrow(e);
         }
      }
      return validatingObject.value();
   }

   /**
    * Returns initial validating object's value if result status is {@link InvariantStatus#SUCCESS}, or
    * specified value otherwise.
    *
    * @param value alternative value
    *
    * @return validating object initial value, or specified value
    */
   public T orElse(T value) {
      if (!success()) {
         return value;
      } else {
         return validatingObject.value();
      }
   }

   /**
    * Returns initial validating object's value if result status is {@link InvariantStatus#SUCCESS}, or
    * supplied lazy value otherwise.
    *
    * @param value alternative value
    *
    * @return validating object initial value, or specified value
    */
   public T orElseGet(Supplier<? extends T> value) {
      if (!success()) {
         return value.get();
      } else {
         return validatingObject.value();
      }
   }

   /**
    * Returns initial validating object's value, and defer this validation result to specified {@link
    * InvariantResults} for later decision.
    *
    * @return validating object initial value
    */
   public T defer(InvariantResults results) {
      notNull(results, "results");

      results.add(this);
      return validatingObject.value();
   }

   /**
    * Returns a new results block chaining this result and specified one. All invariants will be evaluated and
    * reported once, there's no optimization logic if current result in not in {@link #success()}.
    *
    * @return validation results block
    */
   @CheckReturnValue
   public <U> InvariantResults and(InvariantResult<U> result) {
      notNull(result, "result");

      return InvariantResults.of(this, result);
   }

   /**
    * Returns a new results block chaining this result and specified one. All invariants will be evaluated and
    * reported once, there's no optimization logic if current result in not in {@link #success()}.
    *
    * @return validation results block
    */
   @CheckReturnValue
   public <U> InvariantResults and(U validatingObjectValue,
                                   String validatingObjectName,
                                   InvariantRule<U> invariantRule) {
      return InvariantResults.of(this, validate(validatingObjectValue, validatingObjectName, invariantRule));
   }

   /**
    * Returns a new results block chaining this result and specified one. All invariants will be evaluated and
    * reported once, there's no optimization logic if current result in not in {@link #success()}.
    *
    * @return validation results block
    */
   @CheckReturnValue
   public <U> InvariantResults and(U validatingObjectValue, InvariantRule<U> invariantRule) {
      return InvariantResults.of(this, validate(validatingObjectValue, invariantRule));
   }

   /**
    * Result predicate matching specified invariant name if existing.
    *
    * @param invariantName invariant name to match
    *
    * @return result predicate
    */
   public static Predicate<? super InvariantResult<?>> invariantNamePredicate(String invariantName) {
      notBlank(invariantName, "invariantName");
      return result -> result.invariantName().map(name -> name.equals(invariantName)).orElse(false);
   }

   /**
    * Result predicate matching specified result status.
    *
    * @param status status to match
    *
    * @return result predicate
    */
   public static Predicate<InvariantResult<?>> invariantStatusPredicate(InvariantStatus status) {
      return result -> result.status() == status;
   }

   /**
    * Result predicate matching invariant rule name if existing.
    *
    * @param ruleName invariant rule name to match
    *
    * @return result predicate
    */
   public static Predicate<InvariantResult<?>> invariantRuleNamePredicate(String ruleName) {
      notBlank(ruleName, "ruleName");
      return result -> result.validatingObject().ruleName().map(name -> name.equals(ruleName)).orElse(false);
   }

   /**
    * Result predicate matching invariant validating object name if existing.
    *
    * @param validatingObjectName validating object name to match
    *
    * @return result predicate
    */
   public static Predicate<InvariantResult<?>> validatingObjectNamePredicate(String validatingObjectName) {
      notBlank(validatingObjectName, "validatingObjectName");
      return result -> result
            .validatingObject()
            .name()
            .map(name -> name.equals(validatingObjectName))
            .orElse(false);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      InvariantResult<?> that = (InvariantResult<?>) o;
      return Objects.equals(invariantName, that.invariantName)
             && validatingObject.equals(that.validatingObject)
             && status == that.status
             && Objects.equals(message, that.message);
   }

   @Override
   public int hashCode() {
      return Objects.hash(invariantName, validatingObject, status, message);
   }

   @Override
   public String toString() {
      return new StringJoiner(",", InvariantResult.class.getSimpleName() + "[", "]")
            .add("invariantName=" + invariantName)
            .add("validatingObject=" + validatingObject)
            .add("status=" + status)
            .add("message='" + message + "'")
            .toString();
   }

   /**
    * Possible evaluation result status for an invariant.
    */
   public enum InvariantStatus {
      SUCCESS, ERROR
   }

}