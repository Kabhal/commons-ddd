/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.InvariantResult.error;
import static com.tinubu.commons.ddd2.invariant.InvariantResult.success;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ValidatingObject;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

/**
 * Provides support for third-party validators throwing exceptions on validation error.
 * You can use this rule to delegate validation to Apache commons :
 * <ul>
 *    <li>{@code validate(aString, satisfiesValue(Validate::notNull)); }</li>
 *    <li>{@code validate(aString, satisfiesValue(Validate::notNull, "%s must not be null", v.mappedName())); }</li>
 *    <li>{@code validate(aString, satisfies(v -> Validate.notNull(v.mappedValue(), "%s must not be null", v.mappedName()))); }</li>
 * </ul>
 * <p>
 * The resulting validation error will contain the exception message.
 */
public class ThrowingDelegationRules {

   private ThrowingDelegationRules() {
   }

   public static <T> InvariantRule<T> satisfies(final Consumer<ValidatingObject<T>> validator,
                                                final MessageFormatter<T> messageFormatter) {
      notNull(validator, "validator");

      return satisfiesThrowingValidator(validator, messageFormatter);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> satisfies(final Consumer<ValidatingObject<T>> validator,
                                                final String message,
                                                final MessageValue<T>... values) {
      return satisfies(validator, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> satisfies(Consumer<ValidatingObject<T>> validator) {
      return satisfies(validator, null);
   }

   public static <T> InvariantRule<T> satisfiesValue(final Consumer<T> validator,
                                                     final MessageFormatter<T> messageFormatter) {
      notNull(validator, "validator");

      return satisfiesThrowingValidator(v -> validator.accept(v.value()), messageFormatter);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> satisfiesValue(final Consumer<T> validator,
                                                     final String message,
                                                     final MessageValue<T>... values) {
      return satisfiesValue(validator, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> satisfiesValue(Consumer<T> validator) {
      return satisfiesValue(validator, null);
   }

   private static <T> InvariantRule<T> satisfiesThrowingValidator(final Consumer<ValidatingObject<T>> validator,
                                                                  final MessageFormatter<T> messageFormatter) {
      return v -> {
         try {
            validator.accept(v);
            return success(v, messageFormatter);
         } catch (Exception e) {
            return error(v, messageFormatter != null ? messageFormatter : (__ -> errorMessage(e)));
         }
      };
   }

   private static String errorMessage(Exception e) {
      String errorMessage = e.getMessage();

      if (errorMessage == null || StringUtils.isBlank(errorMessage)) {
         return e.getClass().getSimpleName();
      } else {
         return errorMessage;
      }
   }

}
