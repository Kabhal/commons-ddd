/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.formatter.FixedString;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.invariant.rules.BaseRules;
import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * Represents a domain invariant.
 *
 * @param <T> validating object type
 *
 * @implSpec Immutable class implementation.
 */
public class Invariant<T> {

   /**
    * Default groups to assign to invariant when not explicitly set. It would be possible to define an empty
    * list by default, but it provides more flexibility to assign a single group by default.
    */
   static final List<String> DEFAULT_GROUPS = list("default");

   private final String name;
   private final Supplier<T> object;
   private final String objectName;
   private final InvariantRule<T> rule;
   private final List<String> groups;

   private Invariant(String name,
                     Supplier<T> object,
                     String objectName,
                     InvariantRule<T> rule,
                     List<String> groups) {
      this.name = name == null ? null : notBlank(name, "name");
      this.object = notNull(object, "object");
      this.objectName = objectName == null ? null : notBlank(objectName, "objectName");
      this.rule = notNull(rule, "rule");
      this.groups = immutable(noNullElements(groups, "groups"));
   }

   private Invariant(Supplier<T> object, InvariantRule<T> rule, List<String> groups) {
      this(null, object, null, rule, groups);
   }

   /**
    * Create an invariant for specified object.
    *
    * @param object validating object supplier to check for
    * @param rules a list of rules to check for object. Rules are AND'ed.
    * @param <T> validating object type
    *
    * @return created invariant
    */
   public static <T> Invariant<T> of(Supplier<T> object, List<InvariantRule<T>> rules) {
      return new Invariant<>(object, rulesSequence(list(rules)), DEFAULT_GROUPS);
   }

   /**
    * Create an invariant for specified object.
    *
    * @param object validating object supplier to check for
    * @param rules a list of rules to check for object. Rules are AND'ed.
    * @param <T> validating object type
    *
    * @return created invariant
    */
   @SafeVarargs
   public static <T> Invariant<T> of(Supplier<T> object, InvariantRule<T>... rules) {
      return of(object, list(rules));
   }

   /**
    * Validates this invariant. Matching groups are set to current invariant groups.
    *
    * @return invariant evaluation result
    *
    * @implSpec If any exception is thrown during evaluation, the exception is rethrown. See
    *       {@link BaseRules#throwing(InvariantRule)} collection to catch possible exceptions in rules.
    */
   public InvariantResult<T> validate() {
      return validate(groups);
   }

   /**
    * Validates this invariant with specified matching groups.
    *
    * @param matchingGroups matching groups among invariant groups
    *
    * @return invariant evaluation result
    *
    * @implSpec If any exception is thrown during evaluation, the exception is rethrown. See
    *       {@link BaseRules#throwing(InvariantRule)} collection to catch possible exceptions in rules.
    */
   public InvariantResult<T> validate(List<String> matchingGroups) {
      if (matchingGroups != groups) {
         com.tinubu.commons.lang.validation.Validate.satisfies(noNullElements(matchingGroups,
                                                                              "matchingGroups"),
                                                               groups::containsAll,
                                                               "matchingGroups",
                                                               String.format(
                                                                     "matching '%s' groups must be contained in '%s' invariant groups",
                                                                     matchingGroups,
                                                                     groups));
      }

      ValidatingObject<T> validatingObject = ValidatingObject.validatingObject(object().get(), objectName);

      return rule
            .evaluate(validatingObject)
            .invariantName(name)
            .invariantGroups(groups)
            .invariantMatchingGroups(matchingGroups);
   }

   /**
    * Generates an error message from specified exception.
    *
    * @param e exception to generate error message from
    *
    * @return error message formatter
    */
   public static <T> MessageFormatter<T> errorMessage(Exception e) {
      String errorMessage = e.getMessage();

      if (errorMessage == null || StringUtils.isBlank(errorMessage)) {
         errorMessage = e.getClass().getSimpleName();
      }

      return FixedString.of(errorMessage);
   }

   /**
    * Provides inlined validation of a single invariant rule. You must operate the returned result, either
    * with {@link InvariantResult#orThrow()}, {@link InvariantResult#defer(InvariantResults)} or other, for
    * the validation to be effective.
    *
    * @param <T> validating object type
    * @param validatingObjectValue validating object value to validate
    * @param validatingObjectName validating object name to validate
    * @param invariantRule invariant rule to evaluate
    *
    * @return invariant rule validation result
    */
   @CheckReturnValue
   public static <T> InvariantResult<T> validate(T validatingObjectValue,
                                                 String validatingObjectName,
                                                 InvariantRule<T> invariantRule) {
      ValidatingObject<T> validatingObject =
            ValidatingObject.validatingObject(validatingObjectValue, validatingObjectName);

      try {
         return invariantRule.evaluate(validatingObject);
      } catch (Exception e) {
         return InvariantResult.error(validatingObject, errorMessage(e));
      }
   }

   /**
    * Provides inlined validation of a single invariant rule, without validating object name.
    * You must operate the returned result, either with {@link InvariantResult#orThrow()},
    * {@link InvariantResult#defer(InvariantResults)} or other, for the validation to be effective.
    *
    * @param validatingObjectValue validating object value to validate
    * @param invariantRule invariant rule to evaluate
    * @param <T> validating object type
    *
    * @return invariant rule validation result
    */
   @CheckReturnValue
   public static <T> InvariantResult<T> validate(T validatingObjectValue, InvariantRule<T> invariantRule) {
      return validate(validatingObjectValue, null, invariantRule);
   }

   /**
    * Evaluates invariant rule as a predicate on specified validating object.
    *
    * @param validatingObjectValue validating object value to evaluate
    * @param invariantRule invariant rule to evaluate
    * @param <T> validating object type
    *
    * @return predicate evaluation result
    */
   public static <T> boolean satisfies(T validatingObjectValue, Predicate<T> invariantRule) {
      return invariantRule.test(validatingObjectValue);
   }

   /** Optional invariant name. */
   public Optional<String> name() {
      return nullable(name);
   }

   public Invariant<T> name(String name) {
      return new Invariant<>(name, object, objectName, rule, groups);
   }

   /** Validating object value. */
   public Supplier<T> object() {
      return object;
   }

   /** Optional validating object name. */
   public Optional<String> objectName() {
      return nullable(objectName);
   }

   public Invariant<T> objectName(String objectName) {
      return new Invariant<>(name, object, objectName, rule, groups);
   }

   /** Invariant rule. */
   public InvariantRule<T> rule() {
      return rule;
   }

   /** Groups to put this invariant into. */
   public List<String> groups() {
      return groups;
   }

   public Invariant<T> groups(List<String> groups) {
      return new Invariant<>(name, object, objectName, rule, groups);
   }

   public Invariant<T> groups(String... groups) {
      return groups(list(groups));
   }

   private static <T> InvariantRule<T> rulesSequence(List<InvariantRule<T>> rules) {
      noNullElements(rules, "rules");

      return rules.stream().reduce(InvariantRule::andValue).orElseGet(BaseRules::isAlwaysValid);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Invariant<?> invariant = (Invariant<?>) o;
      return Objects.equals(name, invariant.name)
             && Objects.equals(object, invariant.object)
             && Objects.equals(objectName, invariant.objectName)
             && Objects.equals(rule, invariant.rule)
             && Objects.equals(groups, invariant.groups);
   }

   @Override
   public int hashCode() {
      return Objects.hash(name, object, objectName, rule, groups);
   }

   @Override
   public String toString() {
      return new StringJoiner(",", Invariant.class.getSimpleName() + "[", "]")
            .add("name='" + name + "'")
            .add("object=" + object)
            .add("objectName='" + objectName + "'")
            .add("rule=" + rule)
            .add("groups=" + groups)
            .toString();
   }
}