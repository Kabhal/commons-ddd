/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;

import java.util.Optional;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

// FIXME tests
public class OptionalRules {

   private OptionalRules() {
   }

   public static <T extends Optional<?>> InvariantRule<T> isEmpty(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Optional::isPresent, messageFormatter).ruleContext("OptionalRules.isEmpty");
   }

   @SafeVarargs
   public static <T extends Optional<?>> InvariantRule<T> isEmpty(final String message,
                                                                  final MessageValue<T>... values) {
      return isEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Optional<?>> InvariantRule<T> isEmpty() {
      return isEmpty(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static <T extends Optional<?>> InvariantRule<T> isPresent(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Optional::isPresent, messageFormatter).ruleContext("OptionalRules.isPresent");
   }

   @SafeVarargs
   public static <T extends Optional<?>> InvariantRule<T> isPresent(final String message,
                                                                    final MessageValue<T>... values) {
      return isPresent(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Optional<?>> InvariantRule<T> isPresent() {
      return isPresent(FastStringFormat.of("'", validatingObjectName(), "' must be present"));
   }

   /**
    * Applies an invariant to validating object value that must be present.
    *
    * @param rule optional value invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <V, T extends Optional<V>> InvariantRule<T> requiresValue(final InvariantRule<? super V> rule) {
      return isPresent().andValue(as(Optional::get, rule));
   }

   /**
    * Applies an invariant to validating object value only if present.
    *
    * @param rule optional value invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <V, T extends Optional<V>> InvariantRule<T> optionalValue(final InvariantRule<? super V> rule) {
      return isEmpty().orValue(as(Optional::get, rule));
   }

}
