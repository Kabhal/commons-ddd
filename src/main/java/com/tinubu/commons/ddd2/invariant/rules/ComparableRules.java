/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional ∞rmation
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

public class ComparableRules {

   private ComparableRules() {
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isEqualTo(final ParameterValue<T> value,
                                                                              final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.compareTo(value.requireNonNullValue()) == 0,
                                                 messageFormatter).ruleContext("ComparableRules.isEqualTo",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isEqualTo(final ParameterValue<T> value,
                                                                              final String message,
                                                                              final MessageValue<T>... values) {
      return isEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isEqualTo(final ParameterValue<T> value) {
      return isEqualTo(value,
                       FastStringFormat.of("'",
                                           validatingObject(),
                                           "' must be equal to '",
                                           parameter(0),
                                           "'"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isNotEqualTo(final ParameterValue<T> value,
                                                                                 final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.compareTo(value.requireNonNullValue()) != 0,
                                                 messageFormatter).ruleContext("ComparableRules.isNotEqualTo",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isNotEqualTo(final ParameterValue<T> value,
                                                                                 final String message,
                                                                                 final MessageValue<T>... values) {
      return isNotEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isNotEqualTo(final ParameterValue<T> value) {
      return isNotEqualTo(value,
                          FastStringFormat.of("'",
                                              validatingObject(),
                                              "' must not be equal to '",
                                              parameter(0),
                                              "'"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isGreaterThan(final ParameterValue<T> value,
                                                                                  final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.compareTo(value.requireNonNullValue()) > 0,
                                                 messageFormatter).ruleContext("ComparableRules.isGreaterThan",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isGreaterThan(final ParameterValue<T> value,
                                                                                  final String message,
                                                                                  final MessageValue<T>... values) {
      return isGreaterThan(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isGreaterThan(final ParameterValue<T> value) {
      return isGreaterThan(value,
                           FastStringFormat.of("'",
                                               validatingObject(),
                                               "' must be greater than '",
                                               parameter(0),
                                               "'"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isGreaterThanOrEqualTo(final ParameterValue<T> value,
                                                                                           final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.compareTo(value.requireNonNullValue()) >= 0,
                                                 messageFormatter).ruleContext(
            "ComparableRules.isGreaterThanOrEqualTo", value);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isGreaterThanOrEqualTo(final ParameterValue<T> value,
                                                                                           final String message,
                                                                                           final MessageValue<T>... values) {
      return isGreaterThanOrEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isGreaterThanOrEqualTo(final ParameterValue<T> value) {
      return isGreaterThanOrEqualTo(value,
                                    FastStringFormat.of("'",
                                                        validatingObject(),
                                                        "' must be greater than or equal to '",
                                                        parameter(0),
                                                        "'"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isLessThan(final ParameterValue<T> value,
                                                                               final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.compareTo(value.requireNonNullValue()) < 0,
                                                 messageFormatter).ruleContext("ComparableRules.isLessThan",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isLessThan(final ParameterValue<T> value,
                                                                               final String message,
                                                                               final MessageValue<T>... values) {
      return isLessThan(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isLessThan(final ParameterValue<T> value) {
      return isLessThan(value,
                        FastStringFormat.of("'",
                                            validatingObject(),
                                            "' must be less than '",
                                            parameter(0),
                                            "'"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isLessThanOrEqualTo(final ParameterValue<T> value,
                                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.compareTo(value.requireNonNullValue()) <= 0,
                                                 messageFormatter).ruleContext(
            "ComparableRules.isLessThanOrEqualTo", value);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isLessThanOrEqualTo(final ParameterValue<T> value,
                                                                                        final String message,
                                                                                        final MessageValue<T>... values) {
      return isLessThanOrEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isLessThanOrEqualTo(final ParameterValue<T> value) {
      return isLessThanOrEqualTo(value,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' must be less than or equal to '",
                                                     parameter(0),
                                                     "'"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isInRange(final ParameterValue<T> fromInclusive,
                                                                              final ParameterValue<T> toExclusive,
                                                                              final MessageFormatter<T> messageFormatter) {
      notNull(fromInclusive, "fromInclusive");
      notNull(toExclusive, "toExclusive");

      return satisfiesValue(v -> (fromInclusive.value() == null
                                                       || v.compareTo(fromInclusive.value()) >= 0) && (
                                                            toExclusive.value() == null
                                                            || v.compareTo(toExclusive.value()) < 0),
                                                 messageFormatter).ruleContext("ComparableRules.isInRange",
                                                                               fromInclusive, toExclusive);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isInRange(final ParameterValue<T> fromInclusive,
                                                                              final ParameterValue<T> toExclusive,
                                                                              final String message,
                                                                              final MessageValue<T>... values) {
      return isInRange(fromInclusive, toExclusive, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isInRange(final ParameterValue<T> fromInclusive,
                                                                              final ParameterValue<T> toExclusive) {
      return isInRange(fromInclusive,
                       toExclusive,
                       FastStringFormat.of("'",
                                           validatingObject(),
                                           "' must be in [",
                                           parameter(0, v -> v == null ? "-∞" : v),
                                           ",",
                                           parameter(1, v -> v == null ? "+∞" : v),
                                           "[ range"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isInRangeInclusive(final ParameterValue<T> fromInclusive,
                                                                                       final ParameterValue<T> toInclusive,
                                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(fromInclusive, "fromInclusive");
      notNull(toInclusive, "toInclusive");

      return satisfiesValue(v -> (fromInclusive.value() == null
                                                       || v.compareTo(fromInclusive.value()) >= 0) && (
                                                            toInclusive.value() == null
                                                            || v.compareTo(toInclusive.value()) <= 0),
                                                 messageFormatter).ruleContext(
            "ComparableRules.isInRangeInclusive",
            fromInclusive, toInclusive);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isInRangeInclusive(final ParameterValue<T> fromInclusive,
                                                                                       final ParameterValue<T> toInclusive,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
      return isInRangeInclusive(fromInclusive, toInclusive, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isInRangeInclusive(final ParameterValue<T> fromInclusive,
                                                                                       final ParameterValue<T> toInclusive) {
      return isInRangeInclusive(fromInclusive,
                                toInclusive,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' must be in [",
                                                    parameter(0, v -> v == null ? "-∞" : v),
                                                    ",",
                                                    parameter(1, v -> v == null ? "+∞" : v),
                                                    "] range"));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isInRangeExclusive(final ParameterValue<T> fromExclusive,
                                                                                       final ParameterValue<T> toExclusive,
                                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(fromExclusive, "fromExclusive");
      notNull(toExclusive, "toExclusive");

      return satisfiesValue(v -> (fromExclusive.value() == null
                                                       || v.compareTo(fromExclusive.value()) > 0) && (
                                                            toExclusive.value() == null
                                                            || v.compareTo(toExclusive.value()) < 0),
                                                 messageFormatter).ruleContext(
            "ComparableRules.isInRangeExclusive",
            fromExclusive, toExclusive);
   }

   @SafeVarargs
   public static <T extends Comparable<? super T>> InvariantRule<T> isInRangeExclusive(final ParameterValue<T> fromExclusive,
                                                                                       final ParameterValue<T> toExclusive,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
      return isInRangeExclusive(fromExclusive, toExclusive, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Comparable<? super T>> InvariantRule<T> isInRangeExclusive(final ParameterValue<T> fromExclusive,
                                                                                       final ParameterValue<T> toExclusive) {
      return isInRangeExclusive(fromExclusive,
                                toExclusive,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' must be in ]",
                                                    parameter(0, v -> v == null ? "-∞" : v),
                                                    ",",
                                                    parameter(1, v -> v == null ? "+∞" : v),
                                                    "[ range"));
   }

}
