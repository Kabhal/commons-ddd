/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.formatter;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ValidatingObject;

/**
 * Delegates formatting to default message formatter configured in {@link InvariantConfiguration}.
 */
public class DefaultMessageFormat<T> extends AbstractMessageFormatter<T> implements MessageFormatter<T> {
   private final String pattern;
   private final MessageValue<T>[] values;

   private DefaultMessageFormat(String pattern, MessageValue<T>[] values) {
      this.pattern = notNull(pattern, "pattern");
      this.values = notNull(values, "values");
   }

   @SafeVarargs
   public static <T> DefaultMessageFormat<T> of(String pattern, MessageValue<T>... values) {
      return new DefaultMessageFormat<>(pattern, values);
   }

   @Override
   public String format(ValidatingObject<T> validatingObject) {
      return defaultMessageFormatter(pattern, values).format(validatingObject);
   }

   private static <T> MessageFormatter<T> defaultMessageFormatter(String message, MessageValue<T>[] values) {
      return InvariantConfiguration.instance().<T>defaultMessageFormatter().apply(message, values);
   }

}
