/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * Holds a set of {@link Invariant} to validate later.
 * This holder can also manage a supplementary context for validation.
 *
 * @implSpec Immutable class implementation.
 * @see Invariant for single invariant entry point
 */
public class Invariants {

   private final List<Invariant<?>> invariants;
   private final InvariantContext context;

   private Invariants(List<Invariant<?>> invariants, InvariantContext context) {
      this.invariants = noNullElements(invariants, "invariants");
      this.context = notNull(context, "context");
   }

   private Invariants(List<Invariant<?>> invariants) {
      this(invariants, InvariantContext.empty());
   }

   public static Invariants empty() {
      return new Invariants(emptyList());
   }

   public static Invariants of(List<Invariant<?>> invariants) {
      return new Invariants(invariants);
   }

   public static Invariants of(Invariant<?>... invariants) {
      return of(list(invariants));
   }

   public static Invariants of(Invariant<?> invariant) {
      return new Invariants(list(invariant));
   }

   /**
    * Validate invariants for specified groups.
    *
    * @param groups groups to validate, or empty list to match all groups. Groups starting with
    *       '{@code !}' are excluded from validation
    *
    * @return invariant rule validation result
    */
   @CheckReturnValue
   public InvariantResults validate(List<String> groups) {
      noNullElements(groups, "groups");

      return InvariantResults.of(context,
                                 this.invariants
                                       .stream()
                                       .map(invariant -> Pair.of(invariant,
                                                                 matchingGroups(invariant, groups)))
                                       .filter(p -> !p.getRight().isEmpty())
                                       .map(st -> st.getLeft().validate(st.getRight()))
                                       .collect(toList()));
   }

   /**
    * Validate invariants for specified groups.
    *
    * @param groups groups to validate, or empty list to match all groups. Groups starting with
    *       '{@code !}' are excluded from validation
    *
    * @return invariant rule validation result
    */
   @CheckReturnValue
   public InvariantResults validate(String... groups) {
      return validate(list(groups));
   }

   /**
    * Validate all invariants, having a group or not.
    *
    * @return invariant rule validation result
    */
   @CheckReturnValue
   public InvariantResults validate() {
      return validate(emptyList());
   }

   /**
    * Adds supplementary invariants to current list.
    *
    * @param invariants invariants to add
    *
    * @return this
    */
   public Invariants withInvariants(Invariants invariants) {
      notNull(invariants, "invariants");

      List<Invariant<?>> newInvariants = new ArrayList<>(this.invariants);
      newInvariants.addAll(invariants.invariants);

      return new Invariants(newInvariants, context);
   }

   /**
    * Adds supplementary invariants to current list.
    *
    * @param invariants invariants to add
    *
    * @return this
    */
   public Invariants withInvariants(Invariant<?>... invariants) {
      return withInvariants(Invariants.of(invariants));
   }

   public Invariants context(InvariantContext context) {
      return new Invariants(invariants, context);
   }

   public Invariants context(Object... contextObjects) {
      notNull(contextObjects, "contextObjects");

      return context(InvariantContext.of(contextObjects));
   }

   public List<Invariant<?>> invariants() {
      return invariants;
   }

   public InvariantContext context() {
      return context;
   }

   public Invariants filter(Predicate<? super Invariant<?>> predicate) {
      return new Invariants(invariants.stream().filter(predicate).collect(toList()));
   }

   /**
    * Predicate intersecting invariant groups against specified groups.
    *
    * @param groups groups to match, or empty list to match all groups. Groups starting with
    *       '{@code !}' are excluded from match
    * @param <T> Introduced generic because Invariant is a raw type in this class
    *
    * @return predicate matching specified groups against invariant groups
    */
   private <T extends Invariant<?>> Predicate<T> filterInvariantByGroups(List<String> groups) {
      return invariant -> !matchingGroups(invariant, groups).isEmpty();
   }

   /**
    * Returns list of intersecting invariant groups against specified groups.
    * If any excluded group matches, returns an empty list, so that only matching "inclusion" groups are
    * returned.
    *
    * @param invariant invariant to match
    * @param groups groups to match, or empty list to match all groups. Groups starting with
    *       '{@code !}' are excluded from match
    *
    * @return matching specified "inclusion" groups against invariant groups
    */
   private List<String> matchingGroups(Invariant<?> invariant, List<String> groups) {
      if (groups.isEmpty()) {
         return invariant.groups();
      } else {
         List<String> includeGroups = list();
         List<String> excludeGroups = list();

         for (String group : groups) {
            if (group.startsWith("!")) {
               excludeGroups.add(group.substring(1));
            } else {
               includeGroups.add(group);
            }
         }

         if (!invariant
               .groups()
               .stream()
               .noneMatch(invariantGroup -> (!excludeGroups.isEmpty()
                                             && excludeGroups.contains(invariantGroup)))) {
            return list();
         }

         return list(invariant
                           .groups()
                           .stream()
                           .filter(invariantGroup -> (includeGroups.isEmpty() || includeGroups.contains(
                                 invariantGroup))));
      }
   }
}

