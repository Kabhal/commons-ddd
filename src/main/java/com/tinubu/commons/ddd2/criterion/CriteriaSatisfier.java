package com.tinubu.commons.ddd2.criterion;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Helps to construct a {@link Criteria#satisfiedBy(Object)} result by satisfying multiple criterion.
 */
public class CriteriaSatisfier {

   boolean satisfied = true;

   /**
    * Checks if any specified value is satisfied by specified criterion and updates current builder state. If
    * specified criterion is {@code null}, the operation is neutral.
    *
    * @param criterion criterion to satisfy or {@code null}
    * @param values supplier of any values to satisfy, supplied values can be {@code null}
    * @param <U> criterion value type
    *
    * @return this builder
    */
   public <U extends Comparable<? super U>> CriteriaSatisfier satisfiedByAny(Criterion<U> criterion,
                                                                             Supplier<Stream<U>> values) {
      notNull(values, "values");

      satisfied &= (criterion == null || criterion.satisfiedByAny(values.get()));

      return this;
   }

   /**
    * Checks if any specified value is satisfied by specified criterion and updates current builder state. If
    * specified criterion is {@code null}, the operation is neutral.
    *
    * @param criterion criterion to satisfy or {@code null}
    * @param values supplier of any values to satisfy. {@link Optional#empty()} values are satisfied as
    *       {@code null} values
    * @param <U> criterion value type
    *
    * @return this builder
    */
   public <U extends Comparable<? super U>> CriteriaSatisfier satisfiedByAnyOptional(Criterion<U> criterion,
                                                                                     Supplier<Stream<Optional<U>>> values) {
      notNull(values, "values");

      return satisfiedByAny(criterion, () -> values.get().map(value -> value.orElse(null)));
   }

   /**
    * Checks if specified value is satisfied by specified criterion and updates current builder state. If
    * specified criterion is {@code null}, the operation is neutral.
    *
    * @param criterion criterion to satisfy or {@code null}
    * @param value supplier of value to satisfy, supplied value can be {@code null}
    * @param <U> criterion value type
    *
    * @return this builder
    */
   public <U extends Comparable<? super U>> CriteriaSatisfier satisfiedBy(Criterion<U> criterion,
                                                                          Supplier<U> value) {
      notNull(value, "value");

      satisfied &= (criterion == null || criterion.satisfiedBy(value.get()));

      return this;
   }

   /**
    * Checks if specified value is satisfied by specified criterion and updates current builder state. If
    * specified criterion is {@code null}, the operation is neutral.
    *
    * @param criterion criterion to satisfy or {@code null}
    * @param value supplied of value to satisfy. {@link Optional#empty()} value is satisfied as {@code
    *       null} value
    * @param <U> criterion value type
    *
    * @return this builder
    */
   public <U extends Comparable<? super U>> CriteriaSatisfier satisfiedByOptional(Criterion<U> criterion,
                                                                                  Supplier<Optional<U>> value) {
      notNull(value, "value");

      return satisfiedBy(criterion, () -> value.get().orElse(null));
   }

   public boolean isSatisfied() {
      return satisfied;
   }

   /**
    * @return result
    *
    * @deprecated Use {@link #isSatisfied()} instead
    */
   @Deprecated
   public boolean build() {
      return isSatisfied();
   }
}
