package com.tinubu.commons.ddd2.criterion;

import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.type.Value;

/**
 * Defines a common interface for criteria objects. A Criteria should be defined as a set of {@link
 * Criterion}.
 *
 * @param <T> Domain object type satisfied by this criteria
 */
public interface Criteria<T> extends CompositeSpecification<T>, Value {

}
