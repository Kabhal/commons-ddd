package com.tinubu.commons.ddd2.criterion;

import static com.tinubu.commons.ddd2.criterion.Criterion.Flag.NONE;
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.propertyNameMapper;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Function;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * A criterion is an operation definition, defined by an operator and some operands, that can be applied or
 * transformed later. Any {@link OperatorArity operator arity} is supported.
 * <p>
 * Supported operators are defined in {@link CriterionOperator}.
 *
 * @param <T> operands type
 */
public class Criterion<T extends Comparable<? super T>> extends AbstractValue
      implements CompositeSpecification<T> {

   protected final CriterionOperator operator;
   protected final List<T> operands;
   protected final Flags flags;

   protected Criterion(CriterionBuilder<T> builder) {
      operands = immutable(list(builder.operands));
      operator = defaultOperator(builder.operator, operands);
      flags = builder.flags;
   }

   @Override
   protected Fields<? extends Criterion<T>> defineDomainFields() {
      return Fields
            .<Criterion<T>>builder()
            .field("operator", v -> v.operator, isNotNull())
            .field("operands", v -> v.operands, hasNoNullElements())
            .field("flags", v -> v.flags, isNotNull())
            .invariants(validOperatorArity())
            .build();
   }

   /**
    * Maps criterion operands with specified mapper
    *
    * @param mapper function to apply to each operand
    * @param <U> mapped type
    *
    * @return new criterion with mapped operands
    */
   public <U extends Comparable<? super U>> Criterion<U> map(Function<? super T, ? extends U> mapper) {
      return new CriterionBuilder<U>()
            .operator(operator)
            .operands(operands.stream().map(mapper).collect(toList()))
            .flags(flags)
            .build();
   }

   @Override
   public boolean satisfiedBy(T operand) {
      // FIXME throw new UnsupportedOperationException("You should wrap this criterion in InvariantCriterion or other specialized criterion");
      return InvariantCriterion.of(this).satisfiedBy(operand);
   }

   public boolean hasFlag(Flag flag) {
      return flags.hasFlag(flag);
   }

   /**
    * Criterion operator.
    * <p>
    * Default operator rules :
    * <ul>
    * <li>{@link CriterionOperator#DEFINED} if {@link #operands} is empty</li>
    * <li>{@link CriterionOperator#EQUAL} if {@link #operands} has one operand</li>
    * <li>{@link CriterionOperator#IN} if {@link #operands} has more than one operand</li>
    * </ul>
    *
    * @return criterion operator
    */
   @Getter
   public CriterionOperator operator() {
      return operator;
   }

   /**
    * Criterion operands, list can contain {@code null} values.
    *
    * @return criterion operand, never {@code null}
    */
   @Getter
   public List<T> operands() {
      return operands;
   }

   /**
    * Returns the nth operand.
    *
    * @param position operand position, starting from 0
    *
    * @return the nth operand
    *
    * @throws IndexOutOfBoundsException if the position is out of range
    */
   public T operand(int position) {
      return operands.get(position);
   }

   /**
    * Returns the first operand. Equivalent to call {@link #operand(int)} with 0.
    *
    * @return the first operand
    *
    * @throws IndexOutOfBoundsException if the position is out of range
    */
   public T operand() {
      return operand(0);
   }

   /**
    * Returns flags.
    *
    * @return flags, never {@code null}
    */
   @Getter
   public Flags flags() {
      return flags;
   }

   private Invariant<Criterion<T>> validOperatorArity() {
      MessageValue<Criterion<T>> operator =
            validatingObject(Criterion::operator, propertyNameMapper("operator"));

      return Invariant.of(() -> this,
                          satisfies(c -> c.operator.arity != OperatorArity.UNARY || c.operands.size() == 0,
                                    "'%1$s' unary operator must not have extra operands",
                                    operator),
                          satisfies(c -> c.operator.arity != OperatorArity.BINARY || c.operands.size() == 1,
                                    "'%1$s' binary operator must have exactly one extra operand",
                                    operator),
                          satisfies(c -> c.operator.arity != OperatorArity.TERNARY || c.operands.size() == 2,
                                    "'%1$s' ternary operator must have exactly two extra operand",
                                    operator));
   }

   /**
    * Defines the best-fitting default operator based on available parameters.
    *
    * @param operator operator
    * @param operands operands
    *
    * @return default operator
    */
   private CriterionOperator defaultOperator(CriterionOperator operator, List<T> operands) {
      CriterionOperator defaultOperator;

      if (operator != null) defaultOperator = operator;
      else if (operands.size() == 0) defaultOperator = CriterionOperator.DEFINED;
      else if (operands.size() == 1) defaultOperator = CriterionOperator.EQUAL;
      else defaultOperator = CriterionOperator.IN;

      return defaultOperator;
   }

   public static <T extends Comparable<? super T>> CriterionBuilder<T> reconstituteBuilder() {
      return new CriterionBuilder<T>().reconstitute();
   }

   /** Operator flag. Flag value is used to compute a bitset and must always be a power of 2. */
   public enum Flag {
      /** Neutral flag. */
      NONE(0b0),

      /** Ignore case comparison when applicable. */
      IGNORE_CASE(0b1);

      private final int value;

      Flag(int value) {
         this.value = value;
      }

      /**
       * @return flag bit set value
       */
      public int value() {
         return value;
      }

      @Override
      public String toString() {
         return name();
      }
   }

   /**
    * Operator flags list and logic.
    *
    * @implSpec Immutable class implementation.
    */
   public static class Flags {

      /** Flags list is stored as a bitfield. */
      private final int value;

      private Flags(List<Flag> flags) {
         validate(flags, "flags", hasNoNullElements()).orThrow();

         this.value = value(flags);
      }

      /**
       * Creates new instance of specified flags.
       *
       * @param flags flags
       *
       * @return new instance
       */
      public static Flags of(List<Flag> flags) {
         validate(flags, "flags", isNotNull()).orThrow();

         return new Flags(flags);
      }

      /**
       * Creates new instance of specified flags.
       *
       * @param flags flags
       *
       * @return new instance
       */
      public static Flags of(Flag... flags) {
         validate(flags, "flags", isNotNull()).orThrow();

         return new Flags(Arrays.asList(flags));
      }

      /**
       * Creates new instance with no flags.
       *
       * @return new instance
       */
      public static Flags ofNone() {
         return new Flags(emptyList());
      }

      /**
       * Checks if specified flag is defined or not.
       *
       * @param flag flag to check
       *
       * @return {@code true} if flag is defined
       */
      public boolean hasFlag(Flag flag) {
         return (value & flag.value) != NONE.value;
      }

      /**
       * Returns unique flag list.
       *
       * @return flag list
       */
      public List<Flag> flags() {
         return Arrays.stream(Flag.values()).filter(this::hasFlag).collect(toList());
      }

      private int value(List<Flag> flags) {
         return flags.stream().mapToInt(flag -> flag.value).reduce(NONE.value, (a, b) -> a | b);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Flags flags = (Flags) o;
         return value == flags.value;
      }

      @Override
      public int hashCode() {
         return Objects.hash(value);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Flags.class.getSimpleName() + "[", "]")
               .add(flags().stream().map(Flag::name).collect(joining(",")))
               .toString();
      }

      public String toString2() {
         return new ToStringBuilder(this)
               .appendToString(flags().stream().map(Flag::name).collect(joining(",")))
               .toString();
      }
   }

   /**
    * Arity for the operators.
    */
   public enum OperatorArity {
      /** Operator with no extra operand. */
      UNARY,
      /** Operator with exactly 1 extra operands. */
      BINARY,
      /** Operator with exactly 2 extra operands. */
      TERNARY,
      /** Operator with 0 to n extra operands. */
      N_ARY
   }

   /**
    * Criterion operators.
    */
   // FIXME create an empty CriterionOperator interface so that Criterion implementations can add new operators
   public enum CriterionOperator {
      /**
       * Always {@code false} operand.
       */
      FALSE(OperatorArity.UNARY),
      /**
       * Always {@code false} operand.
       */
      TRUE(OperatorArity.UNARY),
      /**
       * Undefined operand.
       */
      UNDEFINED(OperatorArity.UNARY),
      /**
       * Defined operand.
       */
      DEFINED(OperatorArity.UNARY),
      /**
       * Operand equality.
       */
      EQUAL(OperatorArity.BINARY),
      /**
       * Operand inequality.
       */
      NOT_EQUAL(OperatorArity.BINARY),
      /**
       * Operand is strictly greater than second operand.
       */
      GREATER(OperatorArity.BINARY),
      /**
       * Operand is greater or equal than second operand.
       */
      GREATER_OR_EQUAL(OperatorArity.BINARY),
      /**
       * Operand is strictly lesser than second operand.
       */
      LESSER(OperatorArity.BINARY),
      /**
       * Operand is lesser or equal than second operand.
       */
      LESSER_OR_EQUAL(OperatorArity.BINARY),
      /**
       * Operand is between second operand (inclusive) and third operand (exclusive).
       */
      BETWEEN(OperatorArity.TERNARY),
      /**
       * Operand is between second operand (inclusive) and third operand (inclusive).
       */
      BETWEEN_INCLUSIVE(OperatorArity.TERNARY),
      /**
       * Operand is between second operand (exclusive) and third operand (exclusive).
       */
      BETWEEN_EXCLUSIVE(OperatorArity.TERNARY),
      /**
       * Operand equals to any other operand.
       */
      IN(OperatorArity.N_ARY),
      /**
       * Operand not equals to any other operand.
       */
      NOT_IN(OperatorArity.N_ARY),
      /**
       * Operand matches second operand. {@code *} matches any characters, {@code ?} matches one character.
       * Special characters can be escaped with {@code \}, including the escape character itself.
       */
      MATCH(OperatorArity.BINARY),
      /**
       * Operand does not match second operand. {@code *} matches any characters, {@code ?} matches one
       * character. Special characters can be escaped with {@code \}, including the escape character itself.
       */
      NOT_MATCH(OperatorArity.BINARY),
      /**
       * Operand starts with second operand.
       */
      START_WITH(OperatorArity.BINARY),
      /**
       * Operand does not start with second operand.
       */
      NOT_START_WITH(OperatorArity.BINARY),
      /**
       * Operand ends with second operand.
       */
      END_WITH(OperatorArity.BINARY),
      /**
       * Operand does not end with second operand.
       */
      NOT_END_WITH(OperatorArity.BINARY),
      /**
       * Operand includes second operand.
       */
      CONTAIN(OperatorArity.BINARY),

      /**
       * Operand does not includes second operand.
       */
      NOT_CONTAIN(OperatorArity.BINARY);

      private final OperatorArity arity;

      CriterionOperator(OperatorArity arity) {
         this.arity = arity;
      }

      /**
       * Returns operator arity.
       *
       * @return operator arity
       */
      public OperatorArity arity() {
         return arity;
      }
   }

   /**
    * {@link Criterion} builder.
    *
    * @param <T> criterion operands type
    */
   public static class CriterionBuilder<T extends Comparable<? super T>> extends DomainBuilder<Criterion<T>> {
      private CriterionOperator operator;
      private List<T> operands;
      private Flags flags = Flags.ofNone();

      /**
       * Creates new builder from existing {@link Criterion}.
       *
       * @param criterion existing criterion
       * @param <T> criterion type
       *
       * @return new builder
       */
      public static <T extends Comparable<? super T>> CriterionBuilder<T> from(Criterion<T> criterion) {
         return new CriterionBuilder<T>()
               .<CriterionBuilder<T>>reconstitute()
               .operator(criterion.operator)
               .operands(criterion.operands)
               .flags(criterion.flags);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#FALSE} operator.
       *
       * @param flags criterion flags
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> alwaysFalse(Flags flags) {
         return new CriterionBuilder<T>().operator(CriterionOperator.FALSE).flags(flags).build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#FALSE} operator.
       *
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> alwaysFalse() {
         return alwaysFalse(Flags.ofNone());
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#TRUE} operator.
       *
       * @param flags criterion flags
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> alwaysTrue(Flags flags) {
         return new CriterionBuilder<T>().operator(CriterionOperator.TRUE).flags(flags).build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#TRUE} operator.
       *
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> alwaysTrue() {
         return alwaysTrue(Flags.ofNone());
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#DEFINED} operator.
       *
       * @param flags criterion flags
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> defined(Flags flags) {
         return new CriterionBuilder<T>().operator(CriterionOperator.DEFINED).flags(flags).build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#DEFINED} operator.
       *
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> defined() {
         return defined(Flags.ofNone());
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#UNDEFINED} operator.
       *
       * @param flags criterion flags
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> undefined(Flags flags) {
         return new CriterionBuilder<T>().operator(CriterionOperator.UNDEFINED).flags(flags).build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#UNDEFINED} operator.
       *
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> undefined() {
         return undefined(Flags.ofNone());
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#EQUAL} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      @SuppressWarnings("squid:S1221" /* Methods should not be named equal */)
      public static <T extends Comparable<? super T>> Criterion<T> equal(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.EQUAL)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#EQUAL} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      @SuppressWarnings("squid:S1221" /* Methods should not be named equal */)
      public static <T extends Comparable<? super T>> Criterion<T> equal(T operand) {
         return equal(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_EQUAL} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notEqual(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.NOT_EQUAL)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_EQUAL} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notEqual(T operand) {
         return notEqual(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#LESSER} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> lesser(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.LESSER)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#LESSER} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> lesser(T operand) {
         return lesser(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#LESSER_OR_EQUAL} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> lesserOrEqual(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.LESSER_OR_EQUAL)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#LESSER_OR_EQUAL} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> lesserOrEqual(T operand) {
         return lesserOrEqual(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#GREATER} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> greater(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.GREATER)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#GREATER} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> greater(T operand) {
         return greater(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#GREATER_OR_EQUAL} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> greaterOrEqual(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.GREATER_OR_EQUAL)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#GREATER_OR_EQUAL} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> greaterOrEqual(T operand) {
         return greaterOrEqual(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#BETWEEN} operator.
       *
       * @param flags criterion flags
       * @param fromInclusive from operand
       * @param toExclusive to operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> between(Flags flags,
                                                                           T fromInclusive,
                                                                           T toExclusive) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.BETWEEN)
               .operands(fromInclusive, toExclusive)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#BETWEEN} operator.
       *
       * @param fromInclusive from operand
       * @param toExclusive to operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> between(T fromInclusive, T toExclusive) {
         return between(Flags.ofNone(), fromInclusive, toExclusive);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#BETWEEN_INCLUSIVE} operator.
       *
       * @param flags criterion flags
       * @param fromInclusive from operand
       * @param toInclusive to operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> betweenInclusive(Flags flags,
                                                                                    T fromInclusive,
                                                                                    T toInclusive) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.BETWEEN_INCLUSIVE)
               .operands(fromInclusive, toInclusive)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#BETWEEN_INCLUSIVE} operator.
       *
       * @param fromInclusive from operand
       * @param toInclusive to operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> betweenInclusive(T fromInclusive,
                                                                                    T toInclusive) {
         return betweenInclusive(Flags.ofNone(), fromInclusive, toInclusive);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#BETWEEN_EXCLUSIVE} operator.
       *
       * @param flags criterion flags
       * @param fromExclusive from operand
       * @param toExclusive to operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> betweenExclusive(Flags flags,
                                                                                    T fromExclusive,
                                                                                    T toExclusive) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.BETWEEN_EXCLUSIVE)
               .operands(fromExclusive, toExclusive)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#BETWEEN_EXCLUSIVE} operator.
       *
       * @param fromExclusive from operand
       * @param toExclusive to operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> betweenExclusive(T fromExclusive,
                                                                                    T toExclusive) {
         return betweenExclusive(Flags.ofNone(), fromExclusive, toExclusive);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#IN} operator.
       *
       * @param flags criterion flags
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> in(Flags flags, List<? extends T> operands) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.IN)
               .operands((List<T>) operands)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#IN} operator.
       *
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> in(List<? extends T> operands) {
         return in(Flags.ofNone(), operands);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#IN} operator.
       *
       * @param flags criterion flags
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      @SafeVarargs
      public static <T extends Comparable<? super T>> Criterion<T> in(Flags flags, T... operands) {
         return in(flags, asList(operands));
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#IN} operator.
       *
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      @SafeVarargs
      public static <T extends Comparable<? super T>> Criterion<T> in(T... operands) {
         return in(Flags.ofNone(), operands);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_IN} operator.
       *
       * @param flags criterion flags
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notIn(Flags flags, List<T> operands) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.NOT_IN)
               .operands(operands)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_IN} operator.
       *
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notIn(List<T> operands) {
         return notIn(Flags.ofNone(), operands);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_IN} operator.
       *
       * @param flags criterion flags
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      @SafeVarargs
      public static <T extends Comparable<? super T>> Criterion<T> notIn(Flags flags, T... operands) {
         return notIn(flags, asList(operands));
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_IN} operator.
       *
       * @param operands operands
       * @param <T> criterion type
       *
       * @return new criterion
       */
      @SafeVarargs
      public static <T extends Comparable<? super T>> Criterion<T> notIn(T... operands) {
         return notIn(Flags.ofNone(), operands);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#MATCH} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> match(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.MATCH)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#MATCH} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> match(T operand) {
         return match(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_MATCH} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notMatch(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.NOT_MATCH)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_MATCH} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notMatch(T operand) {
         return notMatch(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#START_WITH} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> startWith(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.START_WITH)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#START_WITH} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> startWith(T operand) {
         return startWith(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_START_WITH} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notStartWith(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.NOT_START_WITH)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_START_WITH} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notStartWith(T operand) {
         return notStartWith(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#END_WITH} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> endWith(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.END_WITH)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#END_WITH} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> endWith(T operand) {
         return endWith(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_END_WITH} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notEndWith(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.NOT_END_WITH)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_END_WITH} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notEndWith(T operand) {
         return notEndWith(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#CONTAIN} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> contain(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.CONTAIN)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#CONTAIN} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> contain(T operand) {
         return contain(Flags.ofNone(), operand);
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_CONTAIN} operator.
       *
       * @param flags criterion flags
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notContain(Flags flags, T operand) {
         return new CriterionBuilder<T>()
               .operator(CriterionOperator.NOT_CONTAIN)
               .operands(operand)
               .flags(flags)
               .build();
      }

      /**
       * Creates {@link Criterion} with {@link CriterionOperator#NOT_CONTAIN} operator.
       *
       * @param operand operand
       * @param <T> criterion type
       *
       * @return new criterion
       */
      public static <T extends Comparable<? super T>> Criterion<T> notContain(T operand) {
         return notContain(Flags.ofNone(), operand);
      }

      /**
       * Sets criterion operator.
       *
       * @param operator operator
       *
       * @return this builder
       */
      @Setter
      public CriterionBuilder<T> operator(CriterionOperator operator) {
         this.operator = operator;
         return this;
      }

      /**
       * Sets criterion operands.
       *
       * @param operands operands
       *
       * @return this builder
       */
      @Setter
      public CriterionBuilder<T> operands(List<T> operands) {
         this.operands = operands;
         return this;
      }

      /**
       * Sets criterion operands.
       *
       * @param operands operands
       *
       * @return this builder
       */
      @SafeVarargs
      public final CriterionBuilder<T> operands(T... operands) {
         return operands(Arrays.asList(operands));
      }

      /**
       * Sets criterion flags.
       *
       * @param flags flags
       *
       * @return this builder
       */
      @Setter
      public CriterionBuilder<T> flags(Flags flags) {
         this.flags = flags;
         return this;
      }

      /**
       * Sets criterion flags.
       *
       * @param flags flags
       *
       * @return this builder
       */
      public CriterionBuilder<T> flags(Flag... flags) {
         return flags(Flags.of(flags));
      }

      @Override
      public Criterion<T> buildDomainObject() {
         return new Criterion<>(this);
      }
   }

}

