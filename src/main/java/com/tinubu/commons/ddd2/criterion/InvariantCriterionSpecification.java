package com.tinubu.commons.ddd2.criterion;

import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.BETWEEN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.BETWEEN_EXCLUSIVE;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.BETWEEN_INCLUSIVE;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.CONTAIN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.DEFINED;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.END_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.FALSE;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.GREATER;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.GREATER_OR_EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.IN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.LESSER;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.LESSER_OR_EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.MATCH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_CONTAIN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_END_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_IN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_MATCH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_START_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.START_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.TRUE;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.UNDEFINED;
import static com.tinubu.commons.ddd2.criterion.Criterion.Flag.IGNORE_CASE;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isInRange;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isInRangeExclusive;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isInRangeInclusive;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isNotEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isNotIn;
import static com.tinubu.commons.ddd2.invariant.rules.MatchingRules.doesNotMatchGlob;
import static com.tinubu.commons.ddd2.invariant.rules.MatchingRules.doesNotMatchGlobIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.MatchingRules.matchesGlob;
import static com.tinubu.commons.ddd2.invariant.rules.MatchingRules.matchesGlobIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.contains;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.containsIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotContain;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotContainIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotEndWith;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotEndWithIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotStartWith;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotStartWithIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.endsWith;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.endsWithIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isEqualToIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotEqualToIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.startsWith;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.startsWithIgnoreCase;
import static java.util.Optional.ofNullable;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.ComparableRules;

/**
 * {@link Specification} implementation of a {@link Criterion} implemented with invariant framework.
 *
 * @apiNote {@code null} values will never be matched by any operator except {@link
 *       CriterionOperator#UNDEFINED} (e.g.: {@link
 *       CriterionOperator#NOT_EQUAL} will never match a {@code null} value). To match a {@code
 *       null} value you can create a composite specification,
 *       e.g.: {@code CriterionBuilder.undefined().or(CriterionBuilder.notEqual("value")); }
 * @apiNote Criterion operand type can mismatch operator-required type at runtime and throw cast
 *       exception.
 * @deprecated Use {@link InvariantCriterion} instead
 */
@Deprecated
// FIXME consistency of rules unapplicatble to criterion type (e.g.: IGNORE_CASE & no string -> ignore ignore_case, should throw instead)
public class InvariantCriterionSpecification<T extends Comparable<? super T>> implements Specification<T> {

   /**
    * Evaluators for known criterion operators.
    */
   private final Map<CriterionOperator, Function<Criterion<T>, Specification<T>>> operatorSpecifications =
         new HashMap<CriterionOperator, Function<Criterion<T>, Specification<T>>>() {{
            put(FALSE, InvariantCriterionSpecification.this::alwaysFalseSpecification);
            put(TRUE, InvariantCriterionSpecification.this::alwaysTrueSpecification);
            put(UNDEFINED, InvariantCriterionSpecification.this::undefinedSpecification);
            put(DEFINED, InvariantCriterionSpecification.this::definedSpecification);
            put(EQUAL, InvariantCriterionSpecification.this::equalSpecification);
            put(NOT_EQUAL, InvariantCriterionSpecification.this::notEqualSpecification);
            put(GREATER, InvariantCriterionSpecification.this::greaterSpecification);
            put(GREATER_OR_EQUAL, InvariantCriterionSpecification.this::greaterOrEqualSpecification);
            put(LESSER, InvariantCriterionSpecification.this::lesserSpecification);
            put(LESSER_OR_EQUAL, InvariantCriterionSpecification.this::lesserOrEqualSpecification);
            put(BETWEEN, InvariantCriterionSpecification.this::betweenSpecification);
            put(BETWEEN_INCLUSIVE, InvariantCriterionSpecification.this::betweenInclusiveSpecification);
            put(BETWEEN_EXCLUSIVE, InvariantCriterionSpecification.this::betweenExclusiveSpecification);
            put(IN, InvariantCriterionSpecification.this::inSpecification);
            put(NOT_IN, InvariantCriterionSpecification.this::notInSpecification);
            put(MATCH, InvariantCriterionSpecification.this::matchSpecification);
            put(NOT_MATCH, InvariantCriterionSpecification.this::notMatchSpecification);
            put(START_WITH, InvariantCriterionSpecification.this::startWithSpecification);
            put(NOT_START_WITH, InvariantCriterionSpecification.this::notStartWithSpecification);
            put(END_WITH, InvariantCriterionSpecification.this::endWithSpecification);
            put(NOT_END_WITH, InvariantCriterionSpecification.this::notEndWithSpecification);
            put(CONTAIN, InvariantCriterionSpecification.this::containSpecification);
            put(NOT_CONTAIN, InvariantCriterionSpecification.this::notContainSpecification);
         }};

   private final Criterion<T> criterion;

   /**
    * New instance for specified criterion.
    *
    * @param criterion criterion
    */
   public InvariantCriterionSpecification(Criterion<T> criterion) {
      this.criterion = criterion;
   }

   /**
    * Applies the criterion on the specified value.
    *
    * @param t value to apply the criterion on
    *
    * @return {@code true} is criterion is satisfied for the specified value
    *
    * @implNote {@link NullPointerException} or {@link ClassCastException} can be thrown by some
    *       operators if a criterion operand value is {@code null} but  should not be or if an ignore case
    *       flag has been set on a type not implementing {@link CharSequence}. This validation can't be
    *       made earlier and depend on each operator rationale. In these cases, the specification is not
    *       considered as satisfied.
    */
   @Override
   public boolean satisfiedBy(T t) {
      return operatorSpecification(criterion).satisfiedBy(t);
   }

   /**
    * Filters specified stream by applying criterion to each item.
    *
    * @param stream stream to filter
    *
    * @return filtered list
    */
   public Stream<T> filter(Stream<T> stream) {
      return stream.filter(operatorSpecification(criterion));
   }

   /**
    * Returns an evaluator based on specified criterion.
    *
    * @param criterion criterion to evaluate
    *
    * @return criterion evaluator
    */
   private Specification<T> operatorSpecification(Criterion<T> criterion) {
      return ofNullable(operatorSpecifications
                              .get(criterion.operator())
                              .apply(criterion)).orElseThrow(() -> new IllegalStateException(String.format(
            "No evaluator found for '%s' operator",
            criterion.operator())));
   }

   private Specification<T> alwaysFalseSpecification(Criterion<T> criterion) {
      return __ -> false;
   }

   private Specification<T> alwaysTrueSpecification(Criterion<T> criterion) {
      return __ -> true;
   }

   private Specification<T> undefinedSpecification(Criterion<T> criterion) {
      return isNull();
   }

   private Specification<T> definedSpecification(Criterion<T> criterion) {
      return isNotNull();
   }

   private Specification<T> equalSpecification(Criterion<T> criterion) {
      if (criterion.hasFlag(IGNORE_CASE) && criterion.operand(0) instanceof CharSequence) {
         return isEqualToIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return isEqualTo(value(criterion.operand(0)));
      }
   }

   private Specification<T> notEqualSpecification(Criterion<T> criterion) {
      if (criterion.hasFlag(IGNORE_CASE) && criterion.operand(0) instanceof CharSequence) {
         return isNotEqualToIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return isNotEqualTo(value(criterion.operand(0)));
      }
   }

   private Specification<T> greaterSpecification(Criterion<T> criterion) {
      return ComparableRules.isGreaterThan(value(criterion.operand(0)));
   }

   private Specification<T> greaterOrEqualSpecification(Criterion<T> criterion) {
      return ComparableRules.isGreaterThanOrEqualTo(value(criterion.operand(0)));
   }

   private Specification<T> lesserSpecification(Criterion<T> criterion) {
      return ComparableRules.isLessThan(value(criterion.operand(0)));
   }

   private Specification<T> lesserOrEqualSpecification(Criterion<T> criterion) {
      return ComparableRules.isLessThanOrEqualTo(value(criterion.operand(0)));
   }

   private Specification<T> betweenSpecification(Criterion<T> criterion) {
      return isInRange(value(criterion.operand(0)), value(criterion.operand(1)));
   }

   private Specification<T> betweenInclusiveSpecification(Criterion<T> criterion) {
      return isInRangeInclusive(value(criterion.operand(0)), value(criterion.operand(1)));
   }

   private Specification<T> betweenExclusiveSpecification(Criterion<T> criterion) {
      return isInRangeExclusive(value(criterion.operand(0)), value(criterion.operand(1)));
   }

   private Specification<T> inSpecification(Criterion<T> criterion) {
      return isIn(value(criterion.operands()));
   }

   private Specification<T> notInSpecification(Criterion<T> criterion) {
      return isNotIn(value(criterion.operands()));
   }

   private Specification<T> matchSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return matchesGlobIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return matchesGlob(value((CharSequence) criterion.operand(0))).cast();
      }
   }

   private Specification<T> notMatchSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return doesNotMatchGlobIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return doesNotMatchGlob(value((CharSequence) criterion.operand(0))).cast();
      }
   }

   private Specification<T> startWithSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return startsWithIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return startsWith(value((CharSequence) criterion.operand(0))).cast();
      }
   }

   private Specification<T> notStartWithSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return doesNotStartWithIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return doesNotStartWith(value((CharSequence) criterion.operand(0))).cast();
      }
   }

   private Specification<T> endWithSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return endsWithIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return endsWith(value((CharSequence) criterion.operand(0))).cast();
      }
   }

   private Specification<T> notEndWithSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return doesNotEndWithIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return doesNotEndWith(value((CharSequence) criterion.operand(0))).cast();
      }
   }

   private Specification<T> containSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return containsIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return contains(value((CharSequence) criterion.operand(0))).cast();
      }
   }

   private Specification<T> notContainSpecification(Criterion<T> criterion) {
      if (!(criterion.operand(0) instanceof CharSequence)) {
         return isNotNull();
      } else if (criterion.hasFlag(IGNORE_CASE)) {
         return doesNotContainIgnoreCase(value((CharSequence) criterion.operand(0))).cast();
      } else {
         return doesNotContain(value((CharSequence) criterion.operand(0))).cast();
      }
   }

}
