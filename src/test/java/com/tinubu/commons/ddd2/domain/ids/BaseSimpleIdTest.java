/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.ids;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIf;

import com.tinubu.commons.ddd2.domain.type.SimpleId;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

public abstract class BaseSimpleIdTest<I extends SimpleId<?>, T extends Comparable<? super T>> {

   protected boolean supportsUninitializedValue() {
      return true;
   }

   protected abstract I newInstanceForNewObject(T value);

   protected abstract I newInstanceForNewObject(URI urn);

   protected abstract I newInstanceForNewObject();

   protected abstract I newInstance(T value);

   protected abstract I newInstance(URI urn);

   protected abstract T neutralValue();

   protected abstract String urnIdType();

   @Test
   public void testNewInstanceWhenNominal() {
      I id = newInstance(neutralValue());

      assertThat(id.value()).isEqualTo(neutralValue());
      assertThat(id.newObject()).isFalse();
   }

   @Test
   @EnabledIf("supportsUninitializedValue")
   public void testNewInstanceWhenUninitialized() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance((T) null))
            .withMessageMatching(
                  "Invariant validation error > Context \\[.*\\[value=<null>.*]] > \\{newObject} 'newObject' must be true if value is uninitialized");
   }

   @Test
   public void testNewInstanceForUrnWhenNominal() {
      I id = newInstance(uri("urn:id:%s:%s", urnIdType(), neutralValue()));

      assertThat(id.value()).isEqualTo(neutralValue());
      assertThat(id.newObject()).isFalse();
   }

   @Test
   public void testNewInstanceForUrnWhenCaseSensitive() {
      I id = newInstance(uri("UrN:Id:%s:%s", urnIdType(), neutralValue()));

      assertThat(id.value()).isEqualTo(neutralValue());
      assertThat(id.newObject()).isFalse();
   }

   @Test
   @EnabledIf("supportsUninitializedValue")
   public void testNewInstanceForUrnWhenUninitialized() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance((URI) null))
            .withMessageMatching(
                  "Invariant validation error > Context \\[.*\\[value=<null>.*]] > \\{newObject} 'newObject' must be true if value is uninitialized");
   }

   @Test
   public void testNewInstanceForUrnWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("bad-prefix:id:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=bad-prefix:id:%1$s:%2$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:bad-id:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:bad-id:%1$s:%2$s' URN NID must be equal to 'id' : 'urn:id:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:id:bad-type:%s", neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn.nss=bad-type:%2$s' must start with '%1$s:'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:id:%s", urnIdType())))
            .withMessage(String.format("Invariant validation error > 'urn.nss=%1$s' must start with '%1$s:'",
                                       urnIdType()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:id")))
            .withMessage("Invariant validation error > 'urn=urn:id' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn")))
            .withMessage("Invariant validation error > 'urn=urn' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("")))
            .withMessage("Invariant validation error > 'urn=' must be a valid URN : 'urn:<nid>:<nss>'");
   }

   @Test
   public void testNewInstanceForNewObjectWhenNominal() {
      I id = newInstanceForNewObject(neutralValue());

      assertThat(id.value()).isEqualTo(neutralValue());
      assertThat(id.newObject()).isTrue();
   }

   @Test
   public void testNewInstanceForNewObjectUrnWhenNominal() {
      I id = newInstanceForNewObject(uri("urn:id:%s:%s", urnIdType(), neutralValue()));

      assertThat(id.value()).isEqualTo(neutralValue());
      assertThat(id.newObject()).isTrue();
   }

   @Test
   public void testNewInstanceForNewObjectUrnWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("bad-prefix:id:%s:%s",
                                                          urnIdType(),
                                                          neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=bad-prefix:id:%1$s:%2$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:bad-id:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:bad-id:%1$s:%2$s' URN NID must be equal to 'id' : 'urn:id:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:id:bad-type:%s", neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn.nss=bad-type:%2$s' must start with '%1$s:'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:id:%s", urnIdType())))
            .withMessage(String.format("Invariant validation error > 'urn.nss=%1$s' must start with '%1$s:'",
                                       urnIdType()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:id")))
            .withMessage("Invariant validation error > 'urn=urn:id' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn")))
            .withMessage("Invariant validation error > 'urn=urn' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("")))
            .withMessage("Invariant validation error > 'urn=' must be a valid URN : 'urn:<nid>:<nss>'");
   }

   @Test
   @EnabledIf("supportsUninitializedValue")
   public void testNewInstanceForNewObjectWhenUninitialized() {
      I id = newInstanceForNewObject();

      assertThat(id.value()).isNull();
      assertThat(id.newObject()).isTrue();

      I id2 = newInstanceForNewObject((T) null);

      assertThat(id2.value()).isNull();
      assertThat(id2.newObject()).isTrue();

      I id3 = newInstanceForNewObject((URI) null);

      assertThat(id3.value()).isNull();
      assertThat(id3.newObject()).isTrue();
   }

   @Test
   public void testUrnValue() {
      I id = newInstance(neutralValue());

      assertThat(newInstance(id.urnValue())).isEqualTo(id);
   }

   protected static URI uri(String uri, Object... args) {
      try {
         return new URI(String.format(uri, args));
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

}
