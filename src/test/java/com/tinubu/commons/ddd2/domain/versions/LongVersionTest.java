/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.LongId;
import com.tinubu.commons.ddd2.domain.type.AbstractEntity;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.VersionedEntity;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

// FIXME BaseVersionTest + other *Version tests
public class LongVersionTest {

   @Test
   public void testLongVersionWhenNominal() {
      LongVersion version = LongVersion.of(3L);

      assertThat(version.value()).isEqualTo(3L);
   }

   @Test
   public void testLongVersionWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class).isThrownBy(() -> LongVersion.of(null))
            .withMessage(
                  "Invariant validation error > Context [LongVersion[value=<null>]] > {value} 'value' must not be null");
   }

   @Test
   public void testFirstVersionWhenNominal() {
      assertThat(LongVersion.initialVersion().value()).isEqualTo(0L);
   }

   @Test
   public void testNextVersionWhenNominal() {
      LongVersion version = LongVersion.of(0L);

      version = version.nextVersion(new MyEntity("a"));
      assertThat(version).isEqualTo(LongVersion.of(1L));
   }

   @Test
   public void testNextVersionWhenNullDomainObject() {
      LongVersion version = LongVersion.of(0L);

      assertThatNullPointerException().isThrownBy(() -> version.nextVersion(null)).withMessage("'domainObject' must not be null");
   }

   @Test
   public void testNextVersionWhenOverflow() {
      LongVersion version = LongVersion.of(Long.MAX_VALUE);

      assertThatIllegalStateException().isThrownBy(() -> version.nextVersion(new MyEntity("a"))).withMessage("Version overflow : 9223372036854775807");
   }

   @Test
   public void testSameValueAsWhenNominal() {
      LongVersion version1 = LongVersion.of(3L);
      LongVersion version2 = LongVersion.of(3L);
      LongVersion version3 = LongVersion.of(1L);

      assertThat(version1.sameValueAs(version2)).isTrue();
      assertThat(version1.sameValueAs(version3)).isFalse();
   }

   @Test
   public void testCompareTo() {
      LongVersion version1 = LongVersion.of(3L);
      LongVersion version2 = LongVersion.of(3L);
      LongVersion version3 = LongVersion.of(1L);

      assertThat(version1).isEqualByComparingTo(version2);
      assertThat(version1).isGreaterThan(version3);
      assertThat(version3).isLessThan(version1);
   }

   public static class MyEntity extends AbstractEntity<LongId> implements VersionedEntity<LongId, LongVersion> {

      private final String value;
      private final int constant = 32;

      public MyEntity(String value) {
         this.value = value;
      }

      @Override
      public LongId id() {
         return LongId.of(0L);
      }

      @Override
      public LongVersion version() {
         return LongVersion.initialVersion();
      }

      @Override
      @SuppressWarnings("Convert2MethodRef")
      public Fields<MyEntity> defineDomainFields() {
         return Fields
               .<MyEntity>builder()
               .field("id", v -> v.id(), isNotNull())
               .field("value", v -> v.value)
               .technicalField("constant", v -> v.constant)
               .build();
      }
   }

}
