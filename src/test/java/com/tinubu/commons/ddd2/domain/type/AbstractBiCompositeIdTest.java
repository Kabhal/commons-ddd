/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.net.URI;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

class AbstractBiCompositeIdTest {

   @Test
   public void testAbstractBiCompositeIdWhenNewObject() {
      TestBiCompositeId id = new TestBiCompositeId("a", 0, true);

      assertThat(id.value1()).isEqualTo("a");
      assertThat(id.value2()).isEqualTo(0);
      assertThat(id.newObject()).isTrue();
   }

   @Test
   public void testAbstractBiCompositeIdWhenReconstituteObject() {
      TestBiCompositeId id = new TestBiCompositeId("a", 0, false);

      assertThat(id.value1()).isEqualTo("a");
      assertThat(id.value2()).isEqualTo(0);
      assertThat(id.newObject()).isFalse();

      TestBiCompositeId id2 = new TestBiCompositeId("a", 0);

      assertThat(id2.value1()).isEqualTo("a");
      assertThat(id2.value2()).isEqualTo(0);
      assertThat(id2.newObject()).isFalse();
   }

   @Test
   public void testAbstractBiCompositeIdWhenUninitializedValues() {
      TestBiCompositeId id = new TestBiCompositeId();

      assertThat(id.value1()).isNull();
      assertThat(id.value2()).isNull();
      assertThat(id.newObject()).isTrue();
   }

   @Test
   public void testAbstractBiCompositeIdWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new TestBiCompositeId(null, null, false))
            .withMessage(
                  "Invariant validation error > Context [TestBiCompositeId[value1=<null>,value2=<null>]] > {newObject} 'newObject' must be true if at least one value is uninitialized");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new TestBiCompositeId("a", null, false))
            .withMessage(
                  "Invariant validation error > Context [TestBiCompositeId[value1=a,value2=<null>]] > {newObject} 'newObject' must be true if at least one value is uninitialized");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new TestBiCompositeId(null, 0, false))
            .withMessage(
                  "Invariant validation error > Context [TestBiCompositeId[value1=<null>,value2=0]] > {newObject} 'newObject' must be true if at least one value is uninitialized");
   }

   @Test
   public void testCompareToWhenNominal() {
      assertThat(new TestBiCompositeId("a", 0)).isEqualByComparingTo(new TestBiCompositeId("a", 0));
      assertThat(new TestBiCompositeId("b", 0)).isGreaterThan(new TestBiCompositeId("a", 0));
      assertThat(new TestBiCompositeId("a", 1)).isGreaterThan(new TestBiCompositeId("a", 0));
   }

   @Test
   @SuppressWarnings("ResultOfMethodCallIgnored")
   public void testCompareToWhenUninitializedValues() {
      assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> new TestBiCompositeId(null, 0, true).compareTo(new TestBiCompositeId("a",
                                                                                                   0,
                                                                                                   true)))
            .withMessage("Can't compare uninitialized ids");
      assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> new TestBiCompositeId("a", null, true).compareTo(new TestBiCompositeId("a",
                                                                                                     0,
                                                                                                     true)))
            .withMessage("Can't compare uninitialized ids");
      assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> new TestBiCompositeId("a", 0, true).compareTo(new TestBiCompositeId(null,
                                                                                                  0,
                                                                                                  true)))
            .withMessage("Can't compare uninitialized ids");
      assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> new TestBiCompositeId("a", 0, true).compareTo(new TestBiCompositeId("a",
                                                                                                  null,
                                                                                                  true)))
            .withMessage("Can't compare uninitialized ids");
      assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> new TestBiCompositeId(null, null, true).compareTo(new TestBiCompositeId(null,
                                                                                                      null,
                                                                                                      true)))
            .withMessage("Can't compare uninitialized ids");

   }

   @Test
   public void testSameValueAsWhenNominal() {
      assertThat(new TestBiCompositeId("a", 0).sameValueAs(new TestBiCompositeId("a", 0))).isTrue();
      assertThat(new TestBiCompositeId("a", 0).sameValueAs(new TestBiCompositeId("b", 0))).isFalse();
      assertThat(new TestBiCompositeId("a", 0).sameValueAs(new TestBiCompositeId("a", 1))).isFalse();
   }

   @Test
   public void testSameValueAsWhenUninitializedValues() {
      assertThat(new TestBiCompositeId(null, 0, true).sameValueAs(new TestBiCompositeId("a",
                                                                                        0,
                                                                                        true))).isFalse();
      assertThat(new TestBiCompositeId("a", null, true).sameValueAs(new TestBiCompositeId("a",
                                                                                          0,
                                                                                          true))).isFalse();
      assertThat(new TestBiCompositeId("a", 0, true).sameValueAs(new TestBiCompositeId(null,
                                                                                       0,
                                                                                       true))).isFalse();
      assertThat(new TestBiCompositeId("a", 0, true).sameValueAs(new TestBiCompositeId("a",
                                                                                       null,
                                                                                       true))).isFalse();
      assertThat(new TestBiCompositeId(null, 0, true).sameValueAs(new TestBiCompositeId(null,
                                                                                        0,
                                                                                        true))).isFalse();
      assertThat(new TestBiCompositeId("a", null, true).sameValueAs(new TestBiCompositeId("a",
                                                                                          null,
                                                                                          true))).isFalse();
      assertThat(new TestBiCompositeId(null, null, true).sameValueAs(new TestBiCompositeId(null,
                                                                                           null,
                                                                                           true))).isFalse();
   }

   public static class TestBiCompositeId extends AbstractBiCompositeId<String, Integer> {

      public TestBiCompositeId(String value1, Integer value2, boolean newObject) {
         super(value1, value2, newObject);
         checkInvariants(this);
      }

      public TestBiCompositeId(String value1, Integer value2) {
         super(value1, value2);
         checkInvariants(this);
      }

      public TestBiCompositeId() {
         checkInvariants(this);
      }

      @Override
      public URI urnValue() {
         throw new UnsupportedOperationException();
      }
   }

}