/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.StringId;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

class EntityValueTest {

   @Test
   public void testEntityValueWhenNominal() {
      EntityValue<StringId, MyValue> entityValue = EntityValue.newInstance(StringId.of("id"), new MyValue());

      assertThat(entityValue.id()).isEqualTo(StringId.of("id"));
      assertThat(entityValue.value()).isEqualTo(new MyValue());
      assertThat(entityValue).isEqualTo(EntityValue.newInstance(StringId.of("id"), new MyValue()));
   }

   @Test
   public void testEntityValueWhenNullId() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> EntityValue.newInstance(null, new MyValue()))
            .withMessage(
                  "Invariant validation error > Context [EntityValue[id=<null>,value=MyValue[]]] > {id} 'id' must not be null");
   }

   @Test
   public void testEntityValueWhenNullValue() {
      EntityValue<StringId, MyValue> entityValue = EntityValue.newInstance(StringId.of("id"), null);

      assertThat(entityValue.id()).isEqualTo(StringId.of("id"));
      assertThat(entityValue.value()).isEqualTo(null);
      assertThat(entityValue).isEqualTo(EntityValue.newInstance(StringId.of("id"), null));
   }

   @Test
   public void testInheritedEntityValueWhenNominal() {
      MyEntityValue entityValue = new MyEntityValue(StringId.of("id"), new MyValue());

      assertThat(entityValue.id()).isEqualTo(StringId.of("id"));
      assertThat(entityValue.value()).isEqualTo(new MyValue());
      assertThat(entityValue).isEqualTo(new MyEntityValue(StringId.of("id"), new MyValue()));
   }

   public static class MyValue extends AbstractValue {}

   public static class MyEntityValue extends EntityValue<StringId, MyValue> {
      public MyEntityValue(StringId id, MyValue value) {
         super(id, value);
      }
   }

}