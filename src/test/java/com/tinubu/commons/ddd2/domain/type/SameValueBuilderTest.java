package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.StringId;

// FIXME[hdr] add checks when some values are null in appends (not only in constructor)
// FIXME[hdr]  Object downcast tests + assert correct append called during tests (as it can be tricky some time)
class SameValueBuilderTest {

   @Test
   public void testSameValueBuilderWhenHoldersHaveNull() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "yyy");

      AssertionsForClassTypes.assertThat(new SameValueBuilder(null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, null).sameValue()).isFalse();
      assertThat(new SameValueBuilder(null, value2).sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenHoldersAreEquals() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "yyy");

      assertThat(new SameValueBuilder(value1, value1).append(3, 4).sameValue())
            .as("Holders having the same pointer must not be the same if extra appends are not the same")
            .isFalse();
      assertThat(new SameValueBuilder(value1, value2).sameValue())
            .as("Different holders require some extra append method to check their values are the same or not")
            .isTrue();
   }

   @Test
   public void testSameValueBuilderWhenHoldersAreEnums() {
      MyEnum value1 = MyEnum.A;
      MyEnum value2 = MyEnum.B;

      assertThat(new SameValueBuilder(value1, value1).append(3, 4).sameValue())
            .as("Same enums must not be the same if extra append are not the same")
            .isFalse();
      assertThat(new SameValueBuilder(value1, value2).sameValue())
            .as("Different enums must not be the same")
            .isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendEnums() {
      MyValue value1 = new MyValue("Any val", "xxx");
      MyValue value2 = new MyValue("Any other val", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append(MyEnum.A, MyEnum.A).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2).append(MyEnum.A, MyEnum.B).sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append((MyEnum) null, (MyEnum) null)
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2).append(MyEnum.A, (MyEnum) null).sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendValue() {
      MyValue value1 = new MyValue("Any val", "xxx");
      MyValue value2 = new MyValue("Any other val", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((MyValue) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new MyValue("A", "xxx"), new MyValue("A", "yyy"))
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new MyValue("A", "xxx"), new MyValue("B", "xxx"))
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new MyValue("B", "xxx"))
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new MyValue("A", "xxx"), null)
                       .sameValue()).isFalse();

      MyValue value = new MyValue("A", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append(value, value).sameValue()).isTrue();
   }

   @Test
   public void testSameValueBuilderWhenAppendEntity() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "yyy");

      assertThat(new SameValueBuilder(value1, value2).append((MyEntity) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new MyEntity(StringId.of("A"), "xxx"), new MyEntity(StringId.of("A"), "yyy"))
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new MyEntity(StringId.of("A"), "xxx"), new MyEntity(StringId.of("B"), "xxx"))
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2).append(null, new MyEntity(StringId.of("B"), "xxx"))
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2).append(new MyEntity(StringId.of("A"), "xxx"), null)
                       .sameValue()).isFalse();

      MyEntity value = new MyEntity(StringId.of("A"), "xxx");

      assertThat(new SameValueBuilder(value1, value2).append(value, value).sameValue()).isTrue();
   }

   @Test
   public void testSameValueBuilderWhenAppendObject() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "yyy");

      assertThat(new SameValueBuilder(value1, value2).append((Object) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2).append("A", "A").sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2).append("A", "B").sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2).append(null, "B").sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2).append("A", null).sameValue()).isFalse();

      String value = "A";

      assertThat(new SameValueBuilder(value1, value2).append(value, value).sameValue()).isTrue();
   }

   @Test
   public void testSameValueBuilderWhenInheritingValue() {
      MyValue value = new MyValue("A", "xxx");
      InheritingMyValue inheritingValue = new InheritingMyValue("A", "xxx", "I");

      assertThat(value.sameValueAs(value)).isTrue();
      assertThat(value.sameValueAs(inheritingValue)).isFalse();
      assertThat(inheritingValue.sameValueAs(inheritingValue)).isTrue();
      assertThat(inheritingValue.sameValueAs(value)).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendObjectArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((Integer[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new Integer[] { 4, 3 }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new Integer[] { 4, 3 })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new Integer[] { 4, 3 }, new Integer[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new Integer[] { 4, 3 }, new Integer[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendNestedObjectArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((Integer[][]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new Integer[][] { new Integer[] { 1, 2 }, new Integer[] { 3, 4 } }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new Integer[][] { new Integer[] { 1, 2 }, new Integer[] { 3, 4 } })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new Integer[][] { new Integer[] { 1, 2 }, new Integer[] { 3, 4 } },
                               new Integer[][] { new Integer[] { 1, 2 }, new Integer[] { 3, 4 } })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new Integer[][] { new Integer[] { 1, 2 }, new Integer[] { 3, 4 } },
                               new Integer[][] { new Integer[] { 1, 2 }, new Integer[] { 4, 3 } })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendByteArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((byte[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new byte[] { 4, 3 }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new byte[] { 4, 3 })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new byte[] { 4, 3 }, new byte[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new byte[] { 4, 3 }, new byte[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendIntArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((int[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2).append(new int[] { 4, 3 }, null).sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2).append(null, new int[] { 4, 3 }).sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new int[] { 4, 3 }, new int[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new int[] { 4, 3 }, new int[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendLongArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((long[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new long[] { 4, 3 }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new long[] { 4, 3 })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new long[] { 4, 3 }, new long[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new long[] { 4, 3 }, new long[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendDoubleArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((double[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new double[] { 4, 3 }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new double[] { 4, 3 })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new double[] { 4, 3 }, new double[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new double[] { 4, 3 }, new double[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendFloatArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((float[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new float[] { 4, 3 }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new float[] { 4, 3 })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new float[] { 4, 3 }, new float[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new float[] { 4, 3 }, new float[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendBooleanArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((boolean[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new boolean[] { true, false }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new boolean[] { true, false })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new boolean[] { true, false }, new boolean[] { true, false })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new boolean[] { true, false }, new boolean[] { false, true })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendCharArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((char[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new char[] { 4, 3 }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new char[] { 4, 3 })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new char[] { 4, 3 }, new char[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new char[] { 4, 3 }, new char[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   @Test
   public void testSameValueBuilderWhenAppendShortArrays() {
      MyValue value1 = new MyValue("A", "xxx");
      MyValue value2 = new MyValue("B", "xxx");

      assertThat(new SameValueBuilder(value1, value2).append((short[]) null, null).sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new short[] { 4, 3 }, null)
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(null, new short[] { 4, 3 })
                       .sameValue()).isFalse();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new short[] { 4, 3 }, new short[] { 4, 3 })
                       .sameValue()).isTrue();
      assertThat(new SameValueBuilder(value1, value2)
                       .append(new short[] { 4, 3 }, new short[] { 3, 4 })
                       .sameValue()).isFalse();
   }

   static class MyValue extends AbstractValue {
      private final String s;
      /** Other, not relevant, field. */
      private final String meta;

      MyValue(String s, String meta) {
         this.s = s;
         this.meta = meta;
      }

      @Override
      protected Fields<? extends MyValue> defineDomainFields() {
         return Fields.<MyValue>builder().field("s", v -> v.s).build();
      }
   }

   static class InheritingMyValue extends MyValue {
      private final String is;

      InheritingMyValue(String s, String meta, String is) {
         super(s, meta);

         this.is = is;
      }
   }

   static class MyEntity extends AbstractEntity<StringId> {
      private final StringId id;
      /** Other, not relevant, field. */
      private final String meta;

      MyEntity(StringId id, String meta) {
         this.id = id;
         this.meta = meta;
      }

      @Override
      public StringId id() {
         return this.id;
      }

      @Override
      protected Fields<? extends MyEntity> defineDomainFields() {
         return Fields.<MyEntity>builder().field("id", v -> v.id, isNotNull()).build();
      }

   }

   enum MyEnum implements Value {
      A, B;
   }

}