/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.LongId;
import com.tinubu.commons.ddd2.domain.versions.LongVersion;

public class VersionedEntityTest {

   @Test
   public void testVersionedEntityWhenNominal() {
      MyEntity entity = new MyEntity("a");

      assertThat(entity.version()).isEqualTo(LongVersion.of(0L));
   }

   @Test
   public void testNextVersionWhenNominal() {
      MyEntity entity = new MyEntity("a");

      assertThat(entity.version()).isEqualTo(LongVersion.of(0L));
      assertThat(entity.nextVersion()).isEqualTo(LongVersion.of(1L));
   }

   @Test
   public void testSameVersionAsWhenNominal() {
      assertThat(new MyEntity("a", LongVersion.of(0L)).sameValueAs(new MyEntity("a",
                                                                                LongVersion.of(0L)))).isTrue();
      assertThat(new MyEntity("a", LongVersion.of(0L)).sameValueAs(new MyEntity("a",
                                                                                LongVersion.of(1L)))).isFalse();
   }

   @Test
   public void testSameVersionAsWhenDifferentClasses() {
      assertThat(new MyEntity("a", LongVersion.of(0L)).sameValueAs(new MyEntity("a",
                                                                                LongVersion.of(0L)))).isTrue();
      assertThat(new MyEntity("a", LongVersion.of(0L)).sameValueAs(new MyEntity2("a",
                                                                                 LongVersion.of(0L)))).isFalse();
      assertThat(new MyEntity("a", LongVersion.of(0L)).sameValueAs(new MyChildEntity("a",
                                                                                     LongVersion.of(0L)))).isFalse();
   }

   public static class MyEntity extends AbstractEntity<LongId>
         implements VersionedEntity<LongId, LongVersion> {

      private final String value;
      private final LongVersion version;
      private final int constant = 32;

      public MyEntity(String value, LongVersion version) {
         this.value = value;
         this.version = version;
      }

      public MyEntity(String value) {
         this(value, LongVersion.initialVersion());
      }

      @Override
      public LongId id() {
         return LongId.of(0L);
      }

      @Override
      public LongVersion version() {
         return version;
      }

      @Override
      @SuppressWarnings("Convert2MethodRef")
      public Fields<MyEntity> defineDomainFields() {
         return Fields
               .<MyEntity>builder()
               .field("id", v -> v.id(), isNotNull())
               .field("value", v -> v.value)
               .field("version", v -> v.version)
               .technicalField("constant", v -> v.constant)
               .build();
      }
   }

   public static class MyChildEntity extends MyEntity {

      public MyChildEntity(String value, LongVersion version) {
         super(value, version);
      }

      public MyChildEntity(String value) {
         super(value);
      }
   }

   public static class MyEntity2 extends AbstractEntity<LongId>
         implements VersionedEntity<LongId, LongVersion> {

      private final String value;
      private final LongVersion version;
      private final int constant = 32;

      public MyEntity2(String value, LongVersion version) {
         this.value = value;
         this.version = version;
      }

      public MyEntity2(String value) {
         this(value, LongVersion.initialVersion());
      }

      @Override
      public LongId id() {
         return LongId.of(0L);
      }

      @Override
      public LongVersion version() {
         return version;
      }

      @Override
      @SuppressWarnings("Convert2MethodRef")
      public Fields<MyEntity> defineDomainFields() {
         return Fields
               .<MyEntity>builder()
               .field("id", v -> v.id(), isNotNull())
               .field("value", v -> v.value)
               .field("version", v -> v.version)
               .technicalField("constant", v -> v.constant)
               .build();
      }
   }

}
