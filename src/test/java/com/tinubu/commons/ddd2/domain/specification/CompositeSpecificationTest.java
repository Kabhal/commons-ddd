package com.tinubu.commons.ddd2.domain.specification;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.Objects;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class CompositeSpecificationTest {

   @Test
   public void testOfSpecificationWhenNominal() {
      CompositeSpecification<MyClass> spec = CompositeSpecification.of(__ -> true);

      assertThat(spec).isNotNull();
   }

   @Test
   public void testOfSpecificationWhenCompositeSpecification() {
      Specification<MyClass> value1 = new MyClassSpecification("value1");
      CompositeSpecification<MyClass> spec = CompositeSpecification.of(value1);

      assertThat(spec).isNotNull();
      assertThat(spec).as("Composite tree should not introduce arbitrary lambdas").isEqualTo(value1);
   }

   @Test
   public void testOfSpecificationWhenNull() {
      assertThatNullPointerException()
            .isThrownBy(() -> CompositeSpecification.of(null))
            .withMessage("'specification' must not be null");
   }

   @Test
   public void testCompositeSpecification() {
      Specification<MyClass> spec = new MyClassSpecification("value3")
            .not()
            .and(new MyClassSpecification("value1").or(new MyClassSpecification("value2")).not());

      assertThat(spec.satisfiedBy(new MyClass("value1"))).isFalse();
      assertThat(spec.satisfiedBy(new MyClass("value2"))).isFalse();
      assertThat(spec.satisfiedBy(new MyClass("value3"))).isFalse();
      assertThat(spec.satisfiedBy(new MyClass("value4"))).isTrue();
   }

   @Test
   public void testCompositeSpecificationWhenDifferentSpecifications() {
      Specification<MyClass> spec = new MyClassSpecification("value3")
            .not()
            .and(new MyClassSpecification("value1").or(new MyClassSpecification2("value2")).not());

      assertThat(spec.satisfiedBy(new MyClass("value1"))).isFalse();
      assertThat(spec.satisfiedBy(new MyClass("value2"))).isFalse();
      assertThat(spec.satisfiedBy(new MyClass("value3"))).isFalse();
      assertThat(spec.satisfiedBy(new MyClass("value4"))).isTrue();
   }

   @Test
   public void testCompositeSpecificationTree() {
      CompositeSpecification<MyClass> spec =
            new MyClassSpecification("value1").or(list(new MyClassSpecification2("value2"))).not();

      assertThat(spec).isInstanceOf(NotSpecification.class);
      NotSpecification<MyClass> notSpec = (NotSpecification<MyClass>) spec;
      assertThat(notSpec.specification()).isInstanceOf(OrSpecification.class);
      OrSpecification<MyClass> orSpec = (OrSpecification<MyClass>) notSpec.specification();
      assertThat(orSpec.specifications().get(0)).isInstanceOf(MyClassSpecification.class);
      assertThat(orSpec.specifications().get(1)).isInstanceOf(MyClassSpecification2.class);
   }

   @Test
   public void testCompositeSpecificationWhen0aryAnd() {
      CompositeSpecification<MyClass> spec = new AndSpecification<>(emptyList());

      assertThat(spec).isInstanceOf(AndSpecification.class);
      AndSpecification<MyClass> andSpec = (AndSpecification<MyClass>) spec;
      assertThat(andSpec.specifications()).isEmpty();
   }

   @Test
   public void testCompositeSpecificationWhen1aryAnd() {
      CompositeSpecification<MyClass> spec =
            new AndSpecification<>(list((new MyClassSpecification("value1"))));

      assertThat(spec).isInstanceOf(AndSpecification.class);
      AndSpecification<MyClass> andSpec = (AndSpecification<MyClass>) spec;
      assertThat(andSpec.specifications()).containsExactly(new MyClassSpecification("value1"));
   }

   @Test
   public void testCompositeSpecificationWhenNaryAnd() {
      CompositeSpecification<MyClass> spec =
            new MyClassSpecification("value1").and(list(new MyClassSpecification("value2"),
                                                        new MyClassSpecification("value3")));

      assertThat(spec).isInstanceOf(AndSpecification.class);
      AndSpecification<MyClass> andSpec = (AndSpecification<MyClass>) spec;
      assertThat(andSpec.specifications()).containsExactly(new MyClassSpecification("value1"),
                                                           new MyClassSpecification("value2"),
                                                           new MyClassSpecification("value3"));
   }

   @Test
   public void testCompositeSpecificationWhenAssociativeAndWithPureOperators() {
      CompositeSpecification<MyClass> spec = new MyClassSpecification("value1")
            .and(new MyClassSpecification("value2"))
            .and(new MyClassSpecification("value3"));

      assertThat(spec).isEqualTo(new AndSpecification<>(list(new MyClassSpecification("value1"),
                                                             new MyClassSpecification("value2"),
                                                             new MyClassSpecification("value3"))));
   }

   @Test
   public void testCompositeSpecificationWhenAssociativeAndWithMixedOperators() {
      CompositeSpecification<MyClass> spec = new MyClassSpecification("value1")
            .or(new MyClassSpecification("value2"))
            .and(new MyClassSpecification("value3"))
            .and(new MyClassSpecification("value4"));

      assertThat(spec).isEqualTo(new AndSpecification<>(list(new OrSpecification<>(list(new MyClassSpecification(
                                                                   "value1"), new MyClassSpecification("value2"))),
                                                             new MyClassSpecification("value3"),
                                                             new MyClassSpecification("value4"))));
   }

   @Test
   public void testCompositeSpecificationSatisfiedByWhen0aryAnd() {
      CompositeSpecification<MyClass> spec = new AndSpecification<>(list());

      assertThat(spec.satisfiedBy(new MyClass("value1"))).isTrue();
   }

   @Test
   public void testCompositeSpecificationSatisfiedByWhen1aryAnd() {
      CompositeSpecification<MyClass> spec = new AndSpecification<>(list(new MyClassSpecification("value1")));

      assertThat(spec.satisfiedBy(new MyClass("value1"))).isTrue();
      assertThat(spec.satisfiedBy(new MyClass("value2"))).isFalse();
   }

   @Test
   public void testCompositeSpecificationSatisfiedByWhenNaryAnd() {
      CompositeSpecification<MyClass> spec1 = new AndSpecification<>(list(new MyClassSpecification("value1"),
                                                                          new MyClassSpecification("value2")));

      assertThat(spec1.satisfiedBy(new MyClass("value1"))).isFalse();

      CompositeSpecification<MyClass> spec2 = new AndSpecification<>(list(new MyClassSpecification("valueX"),
                                                                          new MyClassSpecification("valueY")));

      assertThat(spec2.satisfiedBy(new MyClass("value1"))).isFalse();

      CompositeSpecification<MyClass> spec3 = new AndSpecification<>(list(new MyClassSpecification("value1"),
                                                                          new MyClassSpecification("value1")));

      assertThat(spec3.satisfiedBy(new MyClass("value1"))).isTrue();
   }

   @Test
   public void testCompositeSpecificationWhen0aryOr() {
      CompositeSpecification<MyClass> spec = new OrSpecification<>(list());

      assertThat(spec).isInstanceOf(OrSpecification.class);
      OrSpecification<MyClass> orSpec = (OrSpecification<MyClass>) spec;
      assertThat(orSpec.specifications()).isEmpty();
   }

   @Test
   public void testCompositeSpecificationWhen1aryOr() {
      CompositeSpecification<MyClass> spec = new OrSpecification<>(list(new MyClassSpecification("value1")));

      assertThat(spec).isInstanceOf(OrSpecification.class);
      OrSpecification<MyClass> orSpec = (OrSpecification<MyClass>) spec;
      assertThat(orSpec.specifications()).containsExactly(new MyClassSpecification("value1"));
   }

   @Test
   public void testCompositeSpecificationWhenNaryOr() {
      CompositeSpecification<MyClass> spec =
            new MyClassSpecification("value1").or(list(new MyClassSpecification("value2"),
                                                       new MyClassSpecification("value3")));

      assertThat(spec).isInstanceOf(OrSpecification.class);
      OrSpecification<MyClass> orSpec = (OrSpecification<MyClass>) spec;
      assertThat(orSpec.specifications()).containsExactly(new MyClassSpecification("value1"),
                                                          new MyClassSpecification("value2"),
                                                          new MyClassSpecification("value3"));
   }

   @Test
   public void testCompositeSpecificationWhenAssociativeOrWithPureOperators() {
      CompositeSpecification<MyClass> spec = new MyClassSpecification("value1")
            .or(new MyClassSpecification("value2"))
            .or(new MyClassSpecification("value3"));

      assertThat(spec).isEqualTo(new OrSpecification<>(list(new MyClassSpecification("value1"),
                                                            new MyClassSpecification("value2"),
                                                            new MyClassSpecification("value3"))));
   }

   @Test
   public void testCompositeSpecificationWhenAssociativeOrWithMixedOperators() {
      CompositeSpecification<MyClass> spec = new MyClassSpecification("value1")
            .and(new MyClassSpecification("value2"))
            .or(new MyClassSpecification("value3"))
            .or(new MyClassSpecification("value4"));

      assertThat(spec).isEqualTo(new OrSpecification<>(list(new AndSpecification<>(list(new MyClassSpecification(
                                                                  "value1"), new MyClassSpecification("value2"))),
                                                            new MyClassSpecification("value3"),
                                                            new MyClassSpecification("value4"))));
   }

   @Test
   public void testCompositeSpecificationSatisfiedByWhen0aryOr() {
      CompositeSpecification<MyClass> spec = new OrSpecification<>(list());

      assertThat(spec.satisfiedBy(new MyClass("value1"))).isFalse();
   }

   @Test
   public void testCompositeSpecificationSatisfiedByWhen1aryOr() {
      CompositeSpecification<MyClass> spec = new OrSpecification<>(list(new MyClassSpecification("value1")));

      assertThat(spec.satisfiedBy(new MyClass("value1"))).isTrue();
      assertThat(spec.satisfiedBy(new MyClass("value2"))).isFalse();
   }

   @Test
   public void testCompositeSpecificationSatisfiedByWhenNaryOr() {
      CompositeSpecification<MyClass> spec1 = new OrSpecification<>(list(new MyClassSpecification("value1"),
                                                                         new MyClassSpecification("value2")));

      assertThat(spec1.satisfiedBy(new MyClass("value1"))).isTrue();

      CompositeSpecification<MyClass> spec2 = new OrSpecification<>(list(new MyClassSpecification("valueX"),
                                                                         new MyClassSpecification("valueY")));

      assertThat(spec2.satisfiedBy(new MyClass("value1"))).isFalse();

      CompositeSpecification<MyClass> spec3 = new OrSpecification<>(list(new MyClassSpecification("value1"),
                                                                         new MyClassSpecification("value1")));

      assertThat(spec3.satisfiedBy(new MyClass("value1"))).isTrue();
   }

   @Test
   public void testCompositeSpecificationSatisfiedByWhenNot() {
      CompositeSpecification<MyClass> spec1 = new MyClassSpecification("value1").not();

      assertThat(spec1.satisfiedBy(new MyClass("value1"))).isFalse();
      assertThat(spec1.satisfiedBy(new MyClass("value2"))).isTrue();
   }

   @Test
   public void testCompositeSpecificationVisitor() {
      CompositeSpecification<MyClass> spec =
            new MyClassSpecification("value1").or(new MyClassSpecification2("value2")).not();

      Assertions
            .assertThat(spec.accept(new MyClassSpecificationVisitor()))
            .isEqualTo("NOT (MyClassSpecification OR MyClassSpecification2)");
   }

   @Test
   public void testIsPureWhenNominal() {
      CompositeSpecification<MyClass> specification = new MyClassSpecification("a");

      assertThat(specification.isPure(MyClassSpecification.class)).isTrue();
      assertThat(specification.isPure(MyClassSpecification2.class)).isFalse();
   }

   @Test
   public void testIsPureWhenNull() {
      CompositeSpecification<MyClass> specification = new MyClassSpecification("a");

      assertThatNullPointerException()
            .isThrownBy(() -> specification.isPure(null))
            .withMessage("'specificationClass' must not be null");
   }

   @Test
   public void testIsPureWhenSubclass() {
      CompositeSpecification<MyClass> childSpecification = new MyClassChildSpecification("a");

      assertThat(childSpecification.isPure(MyClassSpecification.class)).isTrue();

      CompositeSpecification<MyClass> specification = new MyClassSpecification("a");

      assertThat(specification.isPure(MyClassChildSpecification.class)).isFalse();
   }

   @Test
   public void testIsPureWhenCompositeNot() {
      CompositeSpecification<MyClass> specification = new MyClassSpecification("a").not();

      assertThat(specification.isPure(MyClassSpecification.class)).isTrue();
      assertThat(specification.isPure(MyClassSpecification2.class)).isFalse();
   }

   @Test
   public void testIsPureWhenCompositeOr() {
      CompositeSpecification<MyClass> specification =
            new MyClassSpecification("a").or(new MyClassSpecification("a"));

      assertThat(specification.isPure(MyClassSpecification.class)).isTrue();
      assertThat(specification.isPure(MyClassSpecification2.class)).isFalse();

      assertThat(specification
                       .or(new MyClassSpecification2("a"))
                       .isPure(MyClassSpecification.class)).isFalse();
   }

   @Test
   public void testIsPureWhenCompositeAnd() {
      CompositeSpecification<MyClass> specification =
            new MyClassSpecification("a").and(new MyClassSpecification("a"));

      assertThat(specification.isPure(MyClassSpecification.class)).isTrue();
      assertThat(specification.isPure(MyClassSpecification2.class)).isFalse();

      assertThat(specification
                       .and(new MyClassSpecification2("a"))
                       .isPure(MyClassSpecification.class)).isFalse();
   }

   public static class MyClassSpecificationVisitor implements CompositeSpecificationVisitor<MyClass, String> {

      @Override
      public String visit(NotSpecification<MyClass> specification) {
         return "NOT " + specification.specification().accept(this);
      }

      @Override
      public String visit(AndSpecification<MyClass> specification) {
         StringBuilder and = new StringBuilder();

         for (Specification<MyClass> s : specification.specifications()) {
            if (and.length() > 0) {
               and.append(" AND ");
            }
            and.append(s.accept(this));
         }

         return "(" + and + ")";
      }

      @Override
      public String visit(OrSpecification<MyClass> specification) {
         StringBuilder or = new StringBuilder();

         for (Specification<MyClass> s : specification.specifications()) {
            if (or.length() > 0) {
               or.append(" OR ");
            }
            or.append(s.accept(this));
         }

         return "(" + or + ")";
      }

      @Override
      public String visit(Specification<MyClass> specification) {
         return specification.getClass().getSimpleName();
      }
   }

   public static class MyClass {

      private final String myString;

      public MyClass(String myString) {
         this.myString = myString;
      }

      public String getMyString() {
         return myString;
      }
   }

   public static class MyClassSpecification implements CompositeSpecification<MyClass> {

      private final String value;

      public MyClassSpecification(String value) {
         this.value = value;
      }

      @Override
      public boolean satisfiedBy(MyClass myClass) {
         return value != null && value.equals(myClass.getMyString());
      }

      @Override
      public <V> V accept(CompositeSpecificationVisitor<MyClass, V> visitor) {
         return visitor.visit(this);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         MyClassSpecification that = (MyClassSpecification) o;
         return Objects.equals(value, that.value);
      }

      @Override
      public int hashCode() {
         return Objects.hash(value);
      }
   }

   public static class MyClassSpecification2 implements CompositeSpecification<MyClass> {

      private final String value;

      public MyClassSpecification2(String value) {
         this.value = value;
      }

      @Override
      public boolean satisfiedBy(MyClass myClass) {
         return value != null && value.equals(myClass.getMyString());
      }

      @Override
      public <V> V accept(CompositeSpecificationVisitor<MyClass, V> visitor) {
         return visitor.visit(this);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         MyClassSpecification2 that = (MyClassSpecification2) o;
         return Objects.equals(value, that.value);
      }

      @Override
      public int hashCode() {
         return Objects.hash(value);
      }
   }

   public static class MyClassChildSpecification extends MyClassSpecification {
      public MyClassChildSpecification(String value) {
         super(value);
      }
   }
}
