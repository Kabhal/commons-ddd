/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.ids;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.net.URI;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

public class UuIdTest extends BaseSimpleIdTest<Uuid, String> {

   @Override
   protected Uuid newInstanceForNewObject(String value) {
      return Uuid.ofNewObject(value);
   }

   @Override
   protected Uuid newInstanceForNewObject(URI urn) {
      return Uuid.ofNewObject(urn);
   }

   @Override
   protected Uuid newInstanceForNewObject() {
      return Uuid.ofNewObject();
   }

   @Override
   protected Uuid newInstance(String value) {
      return Uuid.of(value);
   }

   @Override
   protected Uuid newInstance(URI urn) {
      return Uuid.of(urn);
   }

   @Override
   protected String neutralValue() {
      return "b068931c-c450-342b-a3f5-b3d276ea4297";
   }

   @Override
   protected String urnIdType() {
      return "uuid";
   }

   @Test
   public void testReconstituteObjectIdWhenBlank() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> Uuid.of(""))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=]] > {value} 'value' must not be blank");
   }

   @Test
   public void testOfNewObjectWhenBlank() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> Uuid.ofNewObject(""))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=,newObject=true]] > {value} 'value' must not be blank");
   }

   @Test
   public void testReconstituteObjectIdWhenUppercase() {
      Uuid uuid = Uuid.of("b068931c-c450-342b-a3f5-b3d276ea4297".toUpperCase());

      assertThat(uuid).isNotNull();
      assertThat(uuid.value()).isEqualTo("b068931c-c450-342b-a3f5-b3d276ea4297");
   }

   @Test
   public void testOfNewObjecWhenUppercase() {
      Uuid uuid = Uuid.ofNewObject("b068931c-c450-342b-a3f5-b3d276ea4297".toUpperCase());

      assertThat(uuid).isNotNull();
      assertThat(uuid.value()).isEqualTo("b068931c-c450-342b-a3f5-b3d276ea4297");
   }

   @Test
   public void testReconstituteObjectIdWhenBadFormat() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> Uuid.of("invalid uuid"))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=invalid uuid]] > {value} 'value=invalid uuid' UUID must be valid");
   }

   @Test
   public void testOfNewObjectWhenBadFormat() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> Uuid.ofNewObject("invalid uuid"))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=invalid uuid,newObject=true]] > {value} 'value=invalid uuid' UUID must be valid");
   }

   @Test
   void testNilUuidWhenNominal() {
      assertThat(Uuid.nilUuid()).isNotNull();
      assertThat(Uuid.nilUuid().stringValue()).isEqualTo("00000000-0000-0000-0000-000000000000");
      assertThat(Uuid.nilUuid())
            .as("Nil UUID should return the same instance for optimization")
            .isSameAs(Uuid.nilUuid());
   }

   @Test
   void testTimeBasedUuidWhenNominal() {
      assertThat(Uuid.newUuidV1()).isNotNull();
      assertThat(Uuid.newTimeBasedUuid()).isNotNull();
   }

   @Test
   public void testNameBasedMd5UuidWhenNominal() {
      assertThat(Uuid.newUuidV3("name").value()).isEqualTo("b068931c-c450-342b-a3f5-b3d276ea4297");
      assertThat(Uuid.newNameBasedMd5Uuid("name").value()).isEqualTo("b068931c-c450-342b-a3f5-b3d276ea4297");
      assertThat(Uuid.newUuidV3("name".getBytes(StandardCharsets.UTF_8)).value()).isEqualTo(
            "b068931c-c450-342b-a3f5-b3d276ea4297");
      assertThat(Uuid.newNameBasedMd5Uuid("name".getBytes(StandardCharsets.UTF_8)).value()).isEqualTo(
            "b068931c-c450-342b-a3f5-b3d276ea4297");

      assertThat(Uuid.newUuidV3("name1").value()).isEqualTo(Uuid.newUuidV3("name1").value());
      assertThat(Uuid.newNameBasedMd5Uuid("name1").value()).isEqualTo(Uuid
                                                                            .newNameBasedMd5Uuid("name1")
                                                                            .value());
   }

   @Test
   public void testNameBasedMd5UuidWhenBlank() {
      assertThatNullPointerException()
            .isThrownBy(() -> Uuid.newNameBasedMd5Uuid((byte[]) null))
            .withMessage("'name' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> Uuid.newNameBasedMd5Uuid((String) null))
            .withMessage("'name' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> Uuid.newNameBasedMd5Uuid(""))
            .withMessage("'name' must not be blank");
   }

   @Test
   void testRandomUuidWhenNominal() {
      Uuid uuid = Uuid.newRandomUuid();

      assertThat(uuid).isNotNull();
   }

   @Test
   public void testNameBasedSha1UuidWhenNominal() {
      assertThat(Uuid.newUuidV5("name").value()).isEqualTo("6ae99955-2a0d-5dca-94d6-2e2bc8b764d3");
      assertThat(Uuid.newNameBasedSha1Uuid("name").value()).isEqualTo("6ae99955-2a0d-5dca-94d6-2e2bc8b764d3");
      assertThat(Uuid.newUuidV5("name".getBytes(StandardCharsets.UTF_8)).value()).isEqualTo(
            "6ae99955-2a0d-5dca-94d6-2e2bc8b764d3");
      assertThat(Uuid.newNameBasedSha1Uuid("name".getBytes(StandardCharsets.UTF_8)).value()).isEqualTo(
            "6ae99955-2a0d-5dca-94d6-2e2bc8b764d3");

      assertThat(Uuid.newUuidV5("name1").value()).isEqualTo(Uuid.newUuidV5("name1").value());
      assertThat(Uuid.newNameBasedSha1Uuid("name1").value()).isEqualTo(Uuid
                                                                             .newNameBasedSha1Uuid("name1")
                                                                             .value());
   }

   @Test
   public void testNameBasedSha1UuidWhenBlank() {
      assertThatNullPointerException()
            .isThrownBy(() -> Uuid.newNameBasedSha1Uuid((byte[]) null))
            .withMessage("'name' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> Uuid.newNameBasedSha1Uuid((String) null))
            .withMessage("'name' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> Uuid.newNameBasedSha1Uuid(""))
            .withMessage("'name' must not be blank");
   }

   @Test
   public void testUrnValueWhenNominal() {
      Uuid uuid = Uuid.newRandomUuid();
      assertThat(uuid.urnValue().toString()).isEqualTo("urn:uuid:" + uuid.stringValue().toLowerCase());
   }

   @Test
   @Override
   public void testNewInstanceForUrnWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance((URI) null))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=<null>]] > {newObject} 'newObject' must be true if value is uninitialized");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:id:%s:bad-uuid", urnIdType())))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=bad-uuid]] > {value} 'value=bad-uuid' UUID must be valid");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("bad-prefix:id:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=bad-prefix:id:%1$s:%2$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:bad-id:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:bad-id:%1$s:%2$s' URN NID must be equal to '%1$s' : 'urn:%1$s:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:id:%s", urnIdType())))
            .withMessage(String.format("Invariant validation error > 'urn.nss=%1$s' must start with '%1$s:'",
                                       urnIdType()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:id")))
            .withMessage("Invariant validation error > 'urn=urn:id' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn")))
            .withMessage("Invariant validation error > 'urn=urn' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("")))
            .withMessage("Invariant validation error > 'urn=' must be a valid URN : 'urn:<nid>:<nss>'");
   }

   @Test
   public void testNewInstanceForOfficialUrn() {
      Uuid id = newInstance(uri("urn:uuid:%s", neutralValue()));

      assertThat(id.value()).isEqualTo(neutralValue());
      assertThat(id.newObject()).isFalse();
   }

   @Test
   public void testNewInstanceForOfficialUrnWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance((URI) null))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=<null>]] > {newObject} 'newObject' must be true if value is uninitialized");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:%s:bad-uuid", urnIdType())))
            .withMessage(
                  "Invariant validation error > Context [Uuid[value=bad-uuid]] > {value} 'value=bad-uuid' UUID must be valid");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("bad-prefix:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=bad-prefix:%1$s:%2$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:bad-id:%s", neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:bad-id:%2$s' URN NID must be equal to '%1$s' : 'urn:%1$s:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn:%s", urnIdType())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:%1$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("urn")))
            .withMessage("Invariant validation error > 'urn=urn' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstance(uri("")))
            .withMessage("Invariant validation error > 'urn=' must be a valid URN : 'urn:<nid>:<nss>'");
   }

   @Test
   @Override
   public void testNewInstanceForNewObjectUrnWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("bad-prefix:id:%s:%s",
                                                          urnIdType(),
                                                          neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=bad-prefix:id:%1$s:%2$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:bad-id:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:bad-id:%1$s:%2$s' URN NID must be equal to '%1$s' : 'urn:%1$s:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:id:%s", urnIdType())))
            .withMessage(String.format("Invariant validation error > 'urn.nss=%1$s' must start with '%1$s:'",
                                       urnIdType()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:id")))
            .withMessage("Invariant validation error > 'urn=urn:id' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn")))
            .withMessage("Invariant validation error > 'urn=urn' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("")))
            .withMessage("Invariant validation error > 'urn=' must be a valid URN : 'urn:<nid>:<nss>'");
   }

   @Test
   public void testNewInstanceForNewObjectOfficialUrn() {
      Uuid id = newInstanceForNewObject(uri("urn:uuid:%s", neutralValue()));

      assertThat(id.value()).isEqualTo(neutralValue());
      assertThat(id.newObject()).isTrue();
   }

   @Test
   public void testNewInstanceForNewObjectOfficialUrnWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("bad-prefix:%s:%s", urnIdType(), neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=bad-prefix:%1$s:%2$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:bad-id:%s", neutralValue())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:bad-id:%2$s' URN NID must be equal to '%1$s' : 'urn:%1$s:<nss>'",
                  urnIdType(),
                  neutralValue()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn:%s", urnIdType())))
            .withMessage(String.format(
                  "Invariant validation error > 'urn=urn:%1$s' must be a valid URN : 'urn:<nid>:<nss>'",
                  urnIdType()));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("urn")))
            .withMessage("Invariant validation error > 'urn=urn' must be a valid URN : 'urn:<nid>:<nss>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newInstanceForNewObject(uri("")))
            .withMessage("Invariant validation error > 'urn=' must be a valid URN : 'urn:<nid>:<nss>'");
   }

}
