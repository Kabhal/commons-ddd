/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.ids;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.net.URI;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

class StringIdTest extends BaseSimpleIdTest<StringId, String> {

   protected StringId newInstanceForNewObject(String value) {
      return StringId.ofNewObject(value);
   }

   @Override
   protected StringId newInstanceForNewObject(URI urn) {
      return StringId.ofNewObject(urn);
   }

   protected StringId newInstanceForNewObject() {
      return StringId.ofNewObject();
   }

   protected StringId newInstance(String value) {
      return StringId.of(value);
   }

   @Override
   protected StringId newInstance(URI urn) {
      return StringId.of(urn);
   }

   @Override
   protected String neutralValue() {
      return "a";
   }

   @Override
   protected String urnIdType() {
      return "string";
   }

   @Test
   public void testStringIdWhenBadParameter() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> StringId.of(""))
            .withMessage(
                  "Invariant validation error > Context [StringId[value=]] > {value} 'value' must not be blank");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> StringId.of(" "))
            .withMessage(
                  "Invariant validation error > Context [StringId[value= ]] > {value} 'value' must not be blank");
   }

}