/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.test.uri;

import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.PartAssert.assertThatPart;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static org.assertj.core.api.InstanceOfAssertFactories.type;

import java.net.URI;
import java.util.function.Consumer;
import java.util.function.Function;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;

import com.tinubu.commons.ddd2.uri.AbstractComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.Authority;
import com.tinubu.commons.ddd2.uri.ComponentUri.Fragment;
import com.tinubu.commons.ddd2.uri.ComponentUri.Host;
import com.tinubu.commons.ddd2.uri.ComponentUri.Part;
import com.tinubu.commons.ddd2.uri.ComponentUri.Path;
import com.tinubu.commons.ddd2.uri.ComponentUri.Port;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;
import com.tinubu.commons.ddd2.uri.ComponentUri.Scheme;
import com.tinubu.commons.ddd2.uri.ComponentUri.SchemeSpecific;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimpleHost;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleRegistryAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleSchemeSpecific;
import com.tinubu.commons.ddd2.uri.parts.SimpleServerAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleUserInfo;
import com.tinubu.commons.lang.util.StreamUtils;

public class ComponentUriAssert<T extends AbstractComponentUri> extends AbstractUriAssert<ComponentUriAssert<T>, T> {

   private final Components components = new Components();

   public ComponentUriAssert(T actual) {
      super(actual);
   }

   @SuppressWarnings("unchecked")
   public static <T extends AbstractComponentUri> ComponentUriAssert<T> assertThat(String actual) {
      return assertThat((T) ComponentUri.ofUri(actual));
   }

   @SuppressWarnings("unchecked")
   public static <T extends AbstractComponentUri> ComponentUriAssert<T> assertThat(URI actual) {
      return assertThat((T) ComponentUri.ofUri(actual));
   }

   public static <T extends AbstractComponentUri> ComponentUriAssert<T> assertThat(T actual) {
      return new ComponentUriAssert<>(actual);
   }

   @SuppressWarnings("unchecked")
   public static <T extends AbstractComponentUri> ComponentUriAssert<T> assertThat(Uri actual) {
      return new ComponentUriAssert<>((T) ComponentUri.ofUri(actual));
   }

   public Components components() {
      return components;
   }

   public static SimpleServerAuthority serverAuthority(String authority) {
      return SimpleServerAuthority.of(authority);
   }

   public static SimpleServerAuthority serverAuthority(UserInfo userInfo, Host host, Port port) {
      return SimpleServerAuthority.of(userInfo, host, port);
   }

   public static SimpleServerAuthority serverAuthority(UserInfo userInfo, Host host) {
      return SimpleServerAuthority.of(userInfo, host);
   }

   public static SimpleServerAuthority serverAuthority(Host host, Port port) {
      return SimpleServerAuthority.of(host, port);
   }

   public static SimpleServerAuthority serverAuthority(Host host) {
      return SimpleServerAuthority.of(host);
   }

   public static SimpleServerAuthority serverAuthority() {
      return SimpleServerAuthority.ofEmpty();
   }

   public static SimpleRegistryAuthority registryAuthority(String authority) {
      return SimpleRegistryAuthority.of(authority);
   }

   public static SimpleFragment fragment(String fragment) {
      return SimpleFragment.of(fragment);
   }

   public static SimpleQuery query(String query) {
      return SimpleQuery.of(query);
   }

   public static SimplePath path(String path) {
      return SimplePath.of(path);
   }

   public static SimplePort port(int port) {
      return SimplePort.of(port);
   }

   public static SimpleHost host(String host) {
      return SimpleHost.of(host);
   }

   public static SimpleUserInfo userInfo(String userInfo) {
      return SimpleUserInfo.of(userInfo);
   }

   public static SimpleSchemeSpecific schemeSpecific(String schemeSpecific) {
      return SimpleSchemeSpecific.of(schemeSpecific);
   }

   public static SimpleScheme scheme(String scheme) {
      return SimpleScheme.of(scheme);
   }

   public class Components {

      public ComponentUriAssert<T> and() {
         return ComponentUriAssert.this;
      }

      @SafeVarargs
      @SuppressWarnings("unchecked")
      public final <P extends Part> ComponentUriAssert<T>.Components hasPartSatisfying(Class<P> partType,
                                                                                       Consumer<? super P>... requirements) {

         Function<? super AbstractComponentUri, ? extends P> mapper;

         if (Scheme.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().scheme().orElse(null);
         } else if (SchemeSpecific.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().schemeSpecific().orElse(null);
         } else if (Authority.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().authority().orElse(null);
         } else if (UserInfo.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().userInfo().orElse(null);
         } else if (Host.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().host().orElse(null);
         } else if (Port.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().port().orElse(null);
         } else if (Path.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().path().orElse(null);
         } else if (Query.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().query().orElse(null);
         } else if (Fragment.class.isAssignableFrom(partType)) {
            mapper = u -> (P) u.component().fragment().orElse(null);
         } else {
            throw new IllegalStateException();
         }

         return hasPartSatisfying(partType, mapper, requirements);
      }

      @SafeVarargs
      protected final <P extends Part> ComponentUriAssert<T>.Components hasPartSatisfying(Class<P> partType,
                                                                                          Function<? super AbstractComponentUri, ? extends P> mapper,
                                                                                          Consumer<? super P>... requirements) {
         notNull(mapper);

         assertThat(actual)
               .extracting(mapper, type(partType))
               .isNotNull()
               .satisfies(requirements)
               .satisfies(trackMatchedPart());
         return this;
      }

      /**
       * Matches part value and encoded value. Part class must be equal to specified part class.
       */
      public ComponentUriAssert<T>.Components hasPart(Part expected) {
         Part part;

         if (expected instanceof Scheme) {
            part = actual.component().scheme().orElse(null);
         } else if (expected instanceof SchemeSpecific) {
            part = actual.component().schemeSpecific().orElse(null);
         } else if (expected instanceof Authority) {
            part = actual.component().authority().orElse(null);
         } else if (expected instanceof UserInfo) {
            part = actual.component().userInfo().orElse(null);
         } else if (expected instanceof Host) {
            part = actual.component().host().orElse(null);
         } else if (expected instanceof Port) {
            part = actual.component().port().orElse(null);
         } else if (expected instanceof Path) {
            part = actual.component().path().orElse(null);
         } else if (expected instanceof Query) {
            part = actual.component().query().orElse(null);
         } else if (expected instanceof Fragment) {
            part = actual.component().fragment().orElse(null);
         } else {
            throw new IllegalStateException();
         }

         return hasPart(part, expected);
      }

      protected ComponentUriAssert<T>.Components hasPart(Part part, Part expected) {
         assertThatPart(part)
               .isNotNull()
               .isExactlyInstanceOf(expected.getClass())
               .matchValues(expected)
               .isEqualTo(expected)
               .satisfies(trackMatchedPart());
         return this;
      }

      public ComponentUriAssert<T>.Components hasParts(Part... expectedParts) {
         StreamUtils.stream(expectedParts).forEach(this::hasPart);
         return this;
      }

      public ComponentUriAssert<T>.Components hasNoMoreParts() {
         ComponentUriAssert.super.hasNoMoreParts();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasSchemeSatisfying(Consumer<? super Scheme>... requirements) {
         return hasPartSatisfying(Scheme.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasScheme(Scheme scheme) {
         return hasPart(scheme);
      }

      public ComponentUriAssert<T>.Components hasScheme(String scheme) {
         return hasScheme(SimpleScheme.of(scheme));
      }

      public ComponentUriAssert<T>.Components hasNoScheme() {
         Assertions.assertThat(actual.component().scheme()).isEmpty();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasSchemeSpecificSatisfying(Consumer<? super SchemeSpecific>... requirements) {
         return hasPartSatisfying(SchemeSpecific.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasSchemeSpecific(SchemeSpecific schemeSpecific) {
         return hasPart(schemeSpecific);
      }

      public ComponentUriAssert<T>.Components hasSchemeSpecific(String schemeSpecific) {
         return hasSchemeSpecific(SimpleSchemeSpecific.of(schemeSpecific));
      }

      public ComponentUriAssert<T>.Components hasNoSchemeSpecific() {
         Assertions.assertThat(actual.component().schemeSpecific()).isEmpty();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasAuthoritySatisfying(Consumer<? super Authority>... requirements) {
         return hasPartSatisfying(Authority.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasAuthority(Authority authority) {
         return hasPart(authority);
      }

      public ComponentUriAssert<T>.Components hasNoAuthority() {
         Assertions.assertThat(actual.component().authority()).isEmpty();
         return this;
      }

      public ComponentUriAssert<T>.Components hasServerAuthority(String authority) {
         return hasAuthority(SimpleServerAuthority.of(authority));
      }

      public ComponentUriAssert<T>.Components hasRegistryAuthority(String authority) {
         return hasAuthority(SimpleRegistryAuthority.of(authority));
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasUserInfoSatisfying(Consumer<? super UserInfo>... requirements) {
         return hasPartSatisfying(UserInfo.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasUserInfo(UserInfo userInfo) {
         return hasPart(userInfo);
      }

      public ComponentUriAssert<T>.Components hasUserInfo(String userInfo) {
         return hasUserInfo(SimpleUserInfo.of(userInfo));
      }

      public ComponentUriAssert<T>.Components hasNoUserInfo() {
         Assertions.assertThat(actual.component().userInfo()).isEmpty();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasHostSatisfying(Consumer<? super Host>... requirements) {
         return hasPartSatisfying(Host.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasHost(Host host) {
         return hasPart(host);
      }

      public ComponentUriAssert<T>.Components hasHost(String host) {
         return hasHost(SimpleHost.of(host));
      }

      public ComponentUriAssert<T>.Components hasNoHost() {
         Assertions.assertThat(actual.component().host()).isEmpty();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasPortSatisfying(Consumer<? super Port>... requirements) {
         return hasPartSatisfying(Port.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasPort(Port port) {
         return hasPart(port);
      }

      public ComponentUriAssert<T>.Components hasPort(int port) {
         return hasPort(SimplePort.of(port));
      }

      public ComponentUriAssert<T>.Components hasNoPort() {
         Assertions.assertThat(actual.component().port()).isEmpty();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasPathSatisfying(Consumer<? super Path>... requirements) {
         return hasPartSatisfying(Path.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasPath(Path path) {
         return hasPart(path);
      }

      public ComponentUriAssert<T>.Components hasPath(String path) {
         return hasPath(SimplePath.of(path));
      }

      public ComponentUriAssert<T>.Components hasNoPath() {
         Assertions.assertThat(actual.component().path()).isEmpty();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasQuerySatisfying(Consumer<? super Query>... requirements) {
         return hasPartSatisfying(Query.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasQuery(Query query) {
         return hasPart(query);
      }

      public ComponentUriAssert<T>.Components hasQuery(String query) {
         return hasQuery(SimpleQuery.of(query));
      }

      public ComponentUriAssert<T>.Components hasNoQuery() {
         Assertions.assertThat(actual.component().query()).isEmpty();
         return this;
      }

      @SafeVarargs
      public final ComponentUriAssert<T>.Components hasFragmentSatisfying(Consumer<? super Fragment>... requirements) {
         return hasPartSatisfying(Fragment.class, requirements);
      }

      public ComponentUriAssert<T>.Components hasFragment(Fragment fragment) {
         return hasPart(fragment);
      }

      public ComponentUriAssert<T>.Components hasFragment(String fragment) {
         return hasFragment(SimpleFragment.of(fragment));
      }

      public ComponentUriAssert<T>.Components hasNoFragment() {
         Assertions.assertThat(actual.component().fragment()).isEmpty();
         return this;
      }

   }

   public static class PartAssert extends AbstractAssert<PartAssert, Part> {
      public PartAssert(Part actual) {
         super(actual, PartAssert.class);
      }

      public static PartAssert assertThatPart(Part actual) {
         return new PartAssert(actual);
      }

      /**
       * Matches part value and encoded value. Part type must be an instance of specified part class, but
       * not necessarily exactly the same class.
       */
      public PartAssert matchValues(Part part) {
         notNull(part);

         return isNotNull()
               .isInstanceOf(part.getClass())
               .hasValue(part.value())
               .hasEncodedValue(part.encodedValue());
      }

      public PartAssert hasValue(String value) {
         notNull(value);

         isNotNull();
         if (!actual.value().equals(value)) {
            failWithMessage("Expected '%s' to have '%s' value, but was '%s'", actual, value, actual.value());
         }
         return this;
      }

      public PartAssert hasEncodedValue(String encodedValue) {
         notNull(encodedValue);

         isNotNull();
         if (!actual.encodedValue().equals(encodedValue)) {
            failWithMessage("Expected '%s' to have '%s' encoded value, but was '%s'",
                            actual,
                            encodedValue,
                            actual.value());
         }
         return this;
      }
   }

}
