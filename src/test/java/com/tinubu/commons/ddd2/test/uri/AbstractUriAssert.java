/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.test.uri;

import static com.tinubu.commons.lang.util.CollectionUtils.collectionSubtraction;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowingConsumer;

import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.Part;
import com.tinubu.commons.ddd2.uri.ComponentUri.ServerAuthority;
import com.tinubu.commons.ddd2.uri.DefaultUri;
import com.tinubu.commons.ddd2.uri.Uri;

public abstract class AbstractUriAssert<SELF extends AbstractUriAssert<SELF, ACTUAL>, ACTUAL extends Uri>
      extends AbstractAssert<SELF, ACTUAL> {

   private final Set<Part> matchedParts = new HashSet<>();

   public AbstractUriAssert(ACTUAL actual) {
      super(actual, AbstractUriAssert.class);
   }

   /**
    * Checks {@link DefaultUri#value()} and {@link DefaultUri#stringValue()} values.
    *
    * @param value expected value
    *
    * @return this assert
    */
   @SuppressWarnings("unchecked")
   public SELF hasValue(String value) {
      isNotNull();

      var toValue = actual.value();
      if (!(toValue.equals(value) && actual.stringValue().equals(value))) {
         failWithMessage("Expected Uri value to be equal to '%s' but was '%s'", value, toValue);
      }
      return (SELF) this;
   }

   /**
    * Checks {@link DefaultUri#toURI()} value.
    *
    * @param uri expected value
    *
    * @return this assert
    */
   @SuppressWarnings("unchecked")
   public SELF toURIEqualTo(URI uri) {
      isNotNull();

      var toURI = actual.toURI();
      if (!toURI.equals(uri)) {
         failWithMessage("Expected Uri toURI result to be equal to '%s' but was '%s'", uri, toURI);
      }
      return (SELF)this;
   }

   /**
    * Checks {@link DefaultUri#urnValue()} value.
    *
    * @param urn expected value
    *
    * @return this assert
    */
   @SuppressWarnings("unchecked")
   public SELF hasUrn(URI urn) {
      isNotNull();

      var toUrn = actual.urnValue();
      if (!toUrn.equals(urn)) {
         failWithMessage("Expected Uri URN to be equal to '%s' but was '%s'", urn, toUrn);
      }
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF isAbsolute() {
      isNotNull();
      if (!actual.isAbsolute()) {
         failWithMessage("Expected Uri to be absolute but was not");
      }
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF isRelative() {
      isNotNull();
      if (!actual.isRelative()) {
         failWithMessage("Expected Uri to be relative but was not");
      }
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF isOpaque() {
      isNotNull();
      if (!actual.isOpaque()) {
         failWithMessage("Expected Uri to be opaque but was not");
      }
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF isHierarchical() {
      isNotNull();
      if (!actual.isHierarchical()) {
         failWithMessage("Expected Uri to be hierarchical but was not");
      }
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF isHierarchicalServer() {
      isNotNull();
      if (!actual.isHierarchicalServer()) {
         failWithMessage("Expected Uri to be hierarchical server but was not");
      }
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF isHierarchicalRegistry() {
      isNotNull();
      if (!actual.isHierarchicalRegistry()) {
         failWithMessage("Expected Uri to be hierarchical registry but was not");
      }
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   protected <T> SELF hasPartSatisfying(Function<? super T, ? extends Part> partFactory,
                                                                   T part,
                                                                   Consumer<? super T>... requirements) {
      assertThat(part)
            .isNotNull()
            .satisfies(requirements)
            .extracting(partFactory)
            .satisfies(trackMatchedPart());
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   protected <T> SELF hasPart(Function<? super T, ? extends Part> partFactory,
                                                         T part,
                                                         T expected) {
      assertThat(part).isNotNull().isEqualTo(expected).extracting(partFactory).satisfies(trackMatchedPart());
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoMoreParts() {
      var actualParts = list();
      instanceOf(actual, ComponentUri.class)
            .flatMap(cactual -> cactual.component().scheme())
            .or(() -> actual.scheme().map(ComponentUriAssert::scheme))
            .map(actualParts::add);
      instanceOf(actual, ComponentUri.class)
            .flatMap(cactual -> cactual.component().schemeSpecific())
            .or(() -> actual.schemeSpecific(false).map(ComponentUriAssert::schemeSpecific))
            .map(actualParts::add);
      instanceOf(actual, ComponentUri.class)
            .flatMap(cactual -> cactual.component().authority())
            .or(() -> actual.serverAuthority(false).map(ComponentUriAssert::serverAuthority))
            .or(() -> actual.registryAuthority(false).map(ComponentUriAssert::registryAuthority))
            .map(actualParts::add);
      instanceOf(actual, ComponentUri.class)
            .flatMap(cactual -> cactual.component().path())
            .or(() -> actual.path(false).map(ComponentUriAssert::path))
            .map(actualParts::add);
      instanceOf(actual, ComponentUri.class)
            .flatMap(cactual -> cactual.component().query())
            .or(() -> actual.query(false).map(ComponentUriAssert::query))
            .map(actualParts::add);
      instanceOf(actual, ComponentUri.class)
            .flatMap(cactual -> cactual.component().fragment())
            .or(() -> actual.fragment(false).map(ComponentUriAssert::fragment))
            .map(actualParts::add);

      var unmatchedParts = collectionSubtraction(HashSet::new, actualParts, matchedParts);

      Assertions
            .assertThat(unmatchedParts)
            .withFailMessage("'%s' URI is expected to have no more unchecked parts, but have %s",
                             actual,
                             unmatchedParts.stream().map(Object::toString).collect(joining(",", "[", "]")))
            .isEmpty();

      return (SELF)this;
   }

   protected <T extends Part> ThrowingConsumer<T> trackMatchedPart() {
      return part -> {
         matchedParts.add(part);
         if (part instanceof ServerAuthority) {
            var serverAuthority = (ServerAuthority) part;
            serverAuthority.userInfo().map(matchedParts::add);
            serverAuthority.host().map(matchedParts::add);
            serverAuthority.port().map(matchedParts::add);
         }
      };
   }

   @SuppressWarnings("unchecked")
   public SELF hasSchemeSatisfying(Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::scheme, actual.scheme().orElse(null), requirements);
   }

   public SELF hasScheme(String scheme) {
      return hasPart(ComponentUriAssert::scheme, actual.scheme().orElse(null), scheme);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoScheme() {
      assertThat(actual.scheme()).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasSchemeSpecificSatisfying(boolean encoded,
                                                                      Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::schemeSpecific,
                               actual.schemeSpecific(encoded).orElse(null),
                               requirements);
   }

   public SELF hasSchemeSpecific(boolean encoded, String schemeSpecific) {
      return hasPart(ComponentUriAssert::schemeSpecific,
                     actual.schemeSpecific(encoded).orElse(null),
                     schemeSpecific);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoSchemeSpecific() {
      assertThat(actual.schemeSpecific(false)).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasAuthoritySatisfying(boolean encoded,
                                                                 Consumer<? super String>... requirements) {
      return hasPartSatisfying(actual.isHierarchicalServer()
                               ? ComponentUriAssert::serverAuthority
                               : ComponentUriAssert::registryAuthority,
                               actual.authority(encoded).orElse(null),
                               requirements);
   }

   public SELF hasAuthority(boolean encoded, String authority) {
      return hasPart(actual.isHierarchicalServer()
                     ? ComponentUriAssert::serverAuthority
                     : ComponentUriAssert::registryAuthority,
                     actual.authority(encoded).orElse(null),
                     authority);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoAuthority() {
      assertThat(actual.authority(false)).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasServerAuthoritySatisfying(boolean encoded,
                                                                       Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::serverAuthority,
                               actual.serverAuthority(encoded).orElse(null),
                               requirements);
   }

   public SELF hasServerAuthority(boolean encoded, String authority) {
      return hasPart(ComponentUriAssert::serverAuthority,
                     actual.serverAuthority(encoded).orElse(null),
                     authority);
   }

   @SuppressWarnings("unchecked")
   public SELF hasRegistryAuthoritySatisfying(boolean encoded,
                                                                         Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::registryAuthority,
                               actual.registryAuthority(encoded).orElse(null),
                               requirements);
   }

   public SELF hasRegistryAuthority(boolean encoded, String authority) {
      return hasPart(ComponentUriAssert::registryAuthority,
                     actual.registryAuthority(encoded).orElse(null),
                     authority);
   }

   @SuppressWarnings("unchecked")
   public SELF hasUserInfoSatisfying(boolean encoded,
                                                                Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::userInfo,
                               actual.userInfo(encoded).orElse(null),
                               requirements);
   }

   public SELF hasUserInfo(boolean encoded, String userInfo) {
      return hasPart(ComponentUriAssert::userInfo, actual.userInfo(encoded).orElse(null), userInfo);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoUserInfo() {
      assertThat(actual.userInfo(false)).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasHostSatisfying(Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::host, actual.host().orElse(null), requirements);
   }

   public SELF hasHost(String host) {
      return hasPart(ComponentUriAssert::host, actual.host().orElse(null), host);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoHost() {
      assertThat(actual.host()).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasPortSatisfying(Consumer<? super Integer>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::port, actual.port().orElse(null), requirements);
   }

   public SELF hasPort(Integer port) {
      return hasPart(ComponentUriAssert::port, actual.port().orElse(null), port);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoPort() {
      assertThat(actual.port()).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasPathSatisfying(boolean encoded,
                                                            Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::path, actual.path(encoded).orElse(null), requirements);
   }

   public SELF hasPath(boolean encoded, String path) {
      return hasPart(ComponentUriAssert::path, actual.path(encoded).orElse(null), path);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoPath() {
      assertThat(actual.path(false)).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasQuerySatisfying(boolean encoded,
                                                             Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::query, actual.query(encoded).orElse(null), requirements);
   }

   public SELF hasQuery(boolean encoded, String query) {
      return hasPart(ComponentUriAssert::query, actual.query(encoded).orElse(null), query);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoQuery() {
      assertThat(actual.query(false)).isEmpty();
      return (SELF)this;
   }

   @SuppressWarnings("unchecked")
   public SELF hasFragmentSatisfying(boolean encoded,
                                                                Consumer<? super String>... requirements) {
      return hasPartSatisfying(ComponentUriAssert::fragment,
                               actual.fragment(encoded).orElse(null),
                               requirements);
   }

   public SELF hasFragment(boolean encoded, String fragment) {
      return hasPart(ComponentUriAssert::fragment, actual.fragment(encoded).orElse(null), fragment);
   }

   @SuppressWarnings("unchecked")
   public SELF hasNoFragment() {
      assertThat(actual.fragment(false)).isEmpty();
      return (SELF)this;
   }

}
