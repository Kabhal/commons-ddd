/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.test.uri;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;

import java.net.URI;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultComponentUri;
import com.tinubu.commons.ddd2.uri.DefaultUri;
import com.tinubu.commons.ddd2.uri.DefaultUriRestrictions;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.UriRestrictions;

public class UriProvider implements ArgumentsProvider {

   @Override
   public Stream<? extends Arguments> provideArguments(ExtensionContext context) {

      List<Stream<?>> argumentsList = list();

      var testUri = context.getRequiredTestMethod().getAnnotation(TestUri.class);
      if (testUri != null) {
         var ofUri = streamConcat(stream(testUri.value()).map(DefaultUri::ofUri), stream("Uri.ofUri(Uri)"));
         var ofURI = streamConcat(stream(testUri.value()).map(uri -> DefaultUri.ofUri(URI.create(uri))),
                                  stream("Uri.ofUri(URI)"));
         var ofComponents = streamConcat(stream(testUri.value()).map(uri -> uriByComponents(uri,
                                                                                            DefaultUriRestrictions.ofDefault())),
                                         stream("Uri.ofComponents(...)"));
         argumentsList.addAll(list(ofUri, ofURI, ofComponents));
      }

      var componentTestUri = context.getRequiredTestMethod().getAnnotation(ComponentTestUri.class);
      if (componentTestUri != null) {
         if (componentTestUri.provideUri()) {
            var ofUri = streamConcat(stream(componentTestUri.value()).map(DefaultUri::ofUri),
                                     stream("Uri.ofUri(Uri)"));
            var ofURI =
                  streamConcat(stream(componentTestUri.value()).map(uri -> DefaultUri.ofUri(URI.create(uri))),
                               stream("Uri.ofUri(URI)"));
            var ofComponents = streamConcat(stream(componentTestUri.value()).map(uri -> uriByComponents(uri,
                                                                                                        DefaultUriRestrictions.ofDefault())),
                                            stream("Uri.ofComponents(...)"));
            argumentsList.addAll(list(ofUri, ofURI, ofComponents));
         }
         var ofUri = streamConcat(stream(componentTestUri.value()).map(DefaultComponentUri::ofUri),
                                  stream("ComponentUri.ofUri(Uri)"));
         var ofURI =
               streamConcat(stream(componentTestUri.value()).map(uri -> DefaultComponentUri.ofUri(URI.create(
                     uri))), stream("ComponentUri.ofUri(URI)"));
         var ofComponents = streamConcat(stream(componentTestUri.value()).map(uri -> componentUriByComponents(
               uri,
               DefaultUriRestrictions.ofDefault())), stream("ComponentUri.ofComponents(...)"));
         argumentsList.addAll(list(ofUri, ofURI, ofComponents));
      }

      return stream(argumentsList).map(arguments -> Arguments.of(arguments.toArray()));

   }

   /**
    * Generates a {@link Uri} by components to use it as SUT.
    *
    * @implSpec Forces URI creation by components.
    */
   public static DefaultUri uriByComponents(String uri, UriRestrictions restrictions) {
      var u = Uri.ofUri(uri, restrictions);

      if (u.isRelative()) {
         return relative(u).restrictions(restrictions);
      } else if (u.isOpaque()) {
         return opaque(u).restrictions(restrictions);
      } else if (u.isHierarchicalServer()) {
         return hierarchicalServer(u).restrictions(restrictions);
      } else if (u.isHierarchicalRegistry()) {
         return hierarchicalRegistry(u).restrictions(restrictions);
      } else if (u.isHierarchical()) {
         return hierarchical(u).restrictions(restrictions);
      } else {
         throw new IllegalStateException();
      }
   }

   public static DefaultComponentUri componentUriByComponents(String uri, UriRestrictions restrictions) {
      var u = DefaultComponentUri.ofUri(uri, restrictions);

      if (u.isRelative()) {
         return relative(u).restrictions(restrictions);
      } else if (u.isOpaque()) {
         return opaque(u).restrictions(restrictions);
      } else if (u.isHierarchicalServer()) {
         return hierarchicalServer(u).restrictions(restrictions);
      } else if (u.isHierarchicalRegistry()) {
         return hierarchicalRegistry(u).restrictions(restrictions);
      } else if (u.isHierarchical()) {
         return hierarchical(u).restrictions(restrictions);
      } else {
         throw new IllegalStateException();
      }

   }

   public static DefaultComponentUri hierarchical(ComponentUri uri) {
      return DefaultComponentUri.hierarchical(uri.component().scheme().orElse(null),
                                              uri.component().path().orElse(null),
                                              uri.component().query().orElse(null),
                                              uri.component().fragment().orElse(null));
   }

   public static DefaultUri hierarchical(Uri uri) {
      return DefaultUri.hierarchical(true,
                                     uri.scheme().orElse(null),
                                     uri.path(true).orElse(null),
                                     uri.query(true).orElse(null),
                                     uri.fragment(true).orElse(null));
   }

   public static DefaultComponentUri hierarchicalServer(ComponentUri uri) {
      return DefaultComponentUri.hierarchical(uri.component().scheme().orElse(null),
                                              uri.component().serverAuthority().orElse(null),
                                              uri.component().path().orElse(null),
                                              uri.component().query().orElse(null),
                                              uri.component().fragment().orElse(null));
   }

   public static DefaultUri hierarchicalServer(Uri uri) {
      return DefaultUri.hierarchicalServer(true,
                                           uri.scheme().orElse(null),
                                           uri.serverAuthority(true).orElse(null),
                                           uri.path(true).orElse(null),
                                           uri.query(true).orElse(null),
                                           uri.fragment(true).orElse(null));
   }

   public static DefaultComponentUri hierarchicalRegistry(ComponentUri uri) {
      return DefaultComponentUri.hierarchical(uri.component().scheme().orElse(null),
                                              uri.component().registryAuthority().orElse(null),
                                              uri.component().path().orElse(null),
                                              uri.component().query().orElse(null),
                                              uri.component().fragment().orElse(null));
   }

   public static DefaultUri hierarchicalRegistry(Uri uri) {
      return DefaultUri.hierarchicalRegistry(true,
                                             uri.scheme().orElse(null),
                                             uri.registryAuthority(true).orElse(null),
                                             uri.path(true).orElse(null),
                                             uri.query(true).orElse(null),
                                             uri.fragment(true).orElse(null));
   }

   public static DefaultComponentUri opaque(ComponentUri uri) {
      return DefaultComponentUri.opaque(uri.component().scheme().orElse(null),
                                        uri.component().schemeSpecific().orElse(null),
                                        uri.component().fragment().orElse(null));
   }

   public static DefaultUri opaque(Uri uri) {
      return DefaultUri.opaque(true,
                               uri.scheme().orElse(null),
                               uri.schemeSpecific(true).orElse(null),
                               uri.fragment(true).orElse(null));
   }

   public static DefaultComponentUri relative(ComponentUri uri) {
      return DefaultComponentUri.relative(uri.component().authority().orElse(null),
                                          uri.component().path().orElse(null),
                                          uri.component().query().orElse(null),
                                          uri.component().fragment().orElse(null));
   }

   public static DefaultUri relative(Uri uri) {
      return DefaultUri.relative(true,
                                 uri.authority(true).orElse(null),
                                 uri.path(true).orElse(null),
                                 uri.query(true).orElse(null),
                                 uri.fragment(true).orElse(null));
   }

}
