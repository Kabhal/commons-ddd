/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.test.uri;

import java.net.URI;

import com.tinubu.commons.ddd2.uri.Uri;

public class UriAssert<T extends Uri> extends AbstractUriAssert<UriAssert<T>, T> {

   public UriAssert(T actual) {
      super(actual);
   }

   @SuppressWarnings("unchecked")
   public static <T extends Uri> UriAssert<T> assertThat(String actual) {
      return assertThat((T) Uri.ofUri(actual));
   }

   @SuppressWarnings("unchecked")
   public static <T extends Uri> UriAssert<T> assertThat(URI actual) {
      return assertThat((T) Uri.ofUri(actual));
   }

   public static <T extends Uri> UriAssert<T> assertThat(T actual) {
      return new UriAssert<>(actual);
   }

   public static String serverAuthority(String userInfo, String host, int port) {
      StringBuilder builder = new StringBuilder();

      if (userInfo != null) {
         builder.append(userInfo).append('@');
      }
      if (host != null) {
         builder.append(host);
      }
      if (port != -1) {
         builder.append(':').append(port);
      }

      return builder.toString();
   }

   public static String serverAuthority(String userInfo, String host) {
      return serverAuthority(userInfo, host, -1);
   }

   public static String serverAuthority(String host, int port) {
      return serverAuthority(null, host, port);
   }

   public static String serverAuthority(String host) {
      return serverAuthority(null, host, -1);
   }

}
