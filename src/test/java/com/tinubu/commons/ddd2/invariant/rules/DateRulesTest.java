/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

public class DateRulesTest extends BaseInvariantTest {

   private static final Instant INSTANT1 =
         OffsetDateTime.of(2021, 3, 1, 0, 0, 0, 0, ZoneOffset.UTC).toInstant();
   private static final Instant INSTANT2 =
         OffsetDateTime.of(2021, 3, 2, 0, 0, 0, 0, ZoneOffset.UTC).toInstant();

   @Test
   public void testItsInstant() {
      assertThatSuccess(date(INSTANT1),
                        DateRules.instant(TemporalRules.isEqualTo(value(INSTANT1))),
                        "'2021-03-01T00:00:00Z' must be equal to '2021-03-01T00:00:00Z'");
      assertThatError(date(INSTANT1),
                      "field",
                      DateRules.instant(TemporalRules.isEqualTo(value(INSTANT2))),
                      "'field=2021-03-01T00:00:00Z' must be equal to '2021-03-02T00:00:00Z'");
   }

   private Date date(Instant instant) {
      return new Date(instant.toEpochMilli());
   }

}