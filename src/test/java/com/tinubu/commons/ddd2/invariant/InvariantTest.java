/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.InvariantResult.error;
import static com.tinubu.commons.ddd2.invariant.InvariantResult.success;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfies;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyInts;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.formatter.FixedString;

public class InvariantTest extends BaseInvariantTest {

   private static final int PERFORMANCE_ITERATIONS = 10_000_000;

   @Test
   public void testAssignmentValidationWhenSuccess() {
      assertThat(validate(32, "name", isNotNull()).orThrow()).isEqualTo(32);
   }

   @Test
   public void testAssignmentValidationWhenThrow() {
      assertThatThrownBy(() -> validate(null, "name", isNotNull()).orThrow())
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'name' must not be null");
      assertThatThrownBy(() -> validate(null, isNotNull()).orThrow())
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > '<object>' must not be null");
   }

   @Test
   public void testAssignmentValidationWhenThrowCustomException() {
      assertThatThrownBy(() -> validate(null, "name", isNotNull()).orThrow(r -> new IllegalStateException(
            String.format("Validation '%s' error : name=%s, value=%s",
                          r.status(),
                          r.validatingObject().name().orElse("<object>"),
                          r.validatingObject().value()))))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("Validation 'ERROR' error : name=name, value=null");
   }

   @Test
   public void testAssignmentValidationWhenDefer() {
      InvariantResults results = InvariantResults.empty();
      assertThat(validate(32, "name1", isNotNull()).defer(results)).isEqualTo(32);
      assertThat(validate(null, "name2", isNotNull()).defer(results)).isEqualTo(null);
      assertThat(results).containsExactly(InvariantResult.success(validatingObject(32, "name1").ruleName(
                                                "BaseRules.isNotNull"), FixedString.of("'name1' must not be null")),
                                          InvariantResult.error(validatingObject(null, "name2").ruleName(
                                                                      "BaseRules.isNotNull"),
                                                                FixedString.of("'name2' must not be null")));
      assertThatThrownBy(() -> results.orThrow()).isInstanceOf(InvariantValidationException.class);
   }

   @Test
   public void testInvariantValidation() {
      assertThat(validate(null, "name", isNotNull())).isEqualTo(error(validatingObject(null, "name").ruleName(
            "BaseRules.isNotNull"), FixedString.of("'name' must not be null")));
      assertThat(validate(null, isNotNull())).isEqualTo(error(validatingObject(null).ruleName(
            "BaseRules.isNotNull"), FixedString.of("'<object>' must not be null")));
      assertThat(validate("value", "name", isNotNull())).isEqualTo(success(validatingObject("value",
                                                                                            "name").ruleName(
            "BaseRules.isNotNull"), FixedString.of("'name' must not be null")));
      assertThat(validate("value", isNotNull())).isEqualTo(success(validatingObject("value").ruleName(
            "BaseRules.isNotNull"), FixedString.of("'<object>' must not be null")));
   }

   @Test
   public void testInvariantWhenInvariantName() {
      assertThat(Invariant
                       .of(() -> null, isNotNull())
                       .name("invariant")
                       .objectName("name")
                       .validate()).isEqualTo(error(validatingObject(null, "name").ruleName(
            "BaseRules.isNotNull"), FixedString.of("'name' must not be null")).invariantName("invariant"));
   }

   @Test
   public void testInvariantValidationWhenRuleException() {
      assertThat(validate(43, "name", satisfies(v -> {
         throw new IllegalArgumentException();
      }, FixedString.of("validation error")))).isEqualTo(error(validatingObject(43, "name"),
                                                               FixedString.of("IllegalArgumentException")));

      assertThat(validate(43, "name", satisfies(v -> {
         throw new IllegalArgumentException("illegal argument");
      }, FixedString.of("validation error")))).isEqualTo(error(validatingObject(43, "name"),
                                                               FixedString.of("illegal argument")));

      assertThat(validate(43, "name", satisfies(v -> {
         throw new NullPointerException();
      }, FixedString.of("validation error")))).isEqualTo(error(validatingObject(43, "name"),
                                                               FixedString.of("NullPointerException")));

      assertThat(validate(43, "name", satisfies(v -> {
         throw new NullPointerException("null pointer exception");
      }, FixedString.of("validation error")))).isEqualTo(error(validatingObject(43, "name"),
                                                               FixedString.of("null pointer exception")));

      assertThat(validate(43, "name", satisfies(v -> {
         throw new InvariantValidationException(InvariantResults.empty());
      }, FixedString.of("validation error")))).isEqualTo(error(validatingObject(43, "name"),
                                                               FixedString.of("Invariant validation error")));

      assertThat(validate(43, "name", satisfies(v -> {
         throw new IllegalArgumentException();
      }, FixedString.of("validation error")).ruleContext("test1")))
            .as("rule name must be set even in case of exception")
            .isEqualTo(error(validatingObject(43, "name").ruleName("test1"),
                             FixedString.of("IllegalArgumentException")));

   }

   @Test
   @Disabled("Manual performance test")
   public void testInvariantValidationPerformance() {

      // Warmup JVM

      for (int i = 0; i < PERFORMANCE_ITERATIONS; i++) {
         invariantValidationPerformanceOperation();
         invariantPresetValidationPerformanceOperation();
         apacheCommonsPerformanceOperation();
      }

      StopWatch watch = new StopWatch();

      watch.start();
      for (int i = 0; i < PERFORMANCE_ITERATIONS; i++) {
         invariantValidationPerformanceOperation();
      }
      watch.stop();

      System.out.printf("Invariant validation mean duration = %dns%n",
                        watch.getTime(TimeUnit.NANOSECONDS) / PERFORMANCE_ITERATIONS);

      watch.reset();

      watch.start();
      for (int i = 0; i < PERFORMANCE_ITERATIONS; i++) {
         invariantPresetValidationPerformanceOperation();
      }
      watch.stop();

      System.out.printf("Invariant preset validation mean duration = %dns%n",
                        watch.getTime(TimeUnit.NANOSECONDS) / PERFORMANCE_ITERATIONS);

      watch.reset();

      watch.start();
      for (int i = 0; i < PERFORMANCE_ITERATIONS; i++) {
         apacheCommonsPerformanceOperation();
      }
      watch.stop();

      System.out.printf("Apache commons validation mean duration = %dns%n",
                        watch.getTime(TimeUnit.NANOSECONDS) / PERFORMANCE_ITERATIONS);
   }

   @Test
   @Disabled("Manual performance test for Profiling")
   public void testInvariantValidationProfiling() {
      for (int i = 0; i < PERFORMANCE_ITERATIONS; i++) {
         invariantValidationPerformanceOperation();
      }
   }

   private static void apacheCommonsPerformanceOperation() {
      org.apache.commons.lang3.Validate.notBlank("32");
      org.apache.commons.lang3.Validate.notEmpty(new Integer[] { 32 });
   }

   private static void invariantValidationPerformanceOperation() {
      validate("32", "name", isNotBlank()).orThrow();
      validate(new int[] { 32 }, "name", isNotEmptyInts()).orThrow();
   }

   private static void invariantPresetValidationPerformanceOperation() {
      Validate.notBlank("32", "name");
      Validate.notEmpty(new int[] { 32 }, "name");
   }

}