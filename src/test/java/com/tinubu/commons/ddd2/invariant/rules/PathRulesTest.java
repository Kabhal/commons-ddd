/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;

import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

class PathRulesTest extends BaseInvariantTest {

   @Test
   public void testIsEmpty() {
      assertThatSuccess(Paths.get(""), "path", PathRules.isEmpty(), "'path=' must be empty");
      assertThatError(Paths.get(" "), "path", PathRules.isEmpty(), "'path= ' must be empty");
      assertThatError(Paths.get("/"), "path", PathRules.isEmpty(), "'path=/' must be empty");
      assertThatError(Paths.get("path"), "path", PathRules.isEmpty(), "'path=path' must be empty");
      assertThatError(Paths.get("/path"), "path", PathRules.isEmpty(), "'path=/path' must be empty");
   }

   @Test
   public void testNotIsEmpty() {
      assertThatError(Paths.get(""), "path", PathRules.isNotEmpty(), "'path' must not be empty");
      assertThatSuccess(Paths.get(" "), "path", PathRules.isNotEmpty(), "'path' must not be empty");
      assertThatSuccess(Paths.get("/"), "path", PathRules.isNotEmpty(), "'path' must not be empty");
      assertThatSuccess(Paths.get("path"), "path", PathRules.isNotEmpty(), "'path' must not be empty");
      assertThatSuccess(Paths.get("/path"), "path", PathRules.isNotEmpty(), "'path' must not be empty");
   }

   @Test
   public void testHasNoTraversal() {
      assertThatSuccess(Paths.get("/path", "fileName.pdf"),
                        PathRules.hasNoTraversal(),
                        "'/path/fileName.pdf' must not have traversal paths");
      assertThatSuccess(Paths.get(".", "fileName.pdf"),
                        PathRules.hasNoTraversal(),
                        "'./fileName.pdf' must not have traversal paths");
      assertThatError(Paths.get("..", "fileName.pdf"),
                      PathRules.hasNoTraversal(),
                      "'../fileName.pdf' must not have traversal paths");
      assertThatError(Paths.get("path", "..", "fileName.pdf"),
                      PathRules.hasNoTraversal(),
                      "'path/../fileName.pdf' must not have traversal paths");
   }

   @Test
   public void testContains() {
      assertThatSuccess(Paths.get("/some/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("path"))),
                        "'/some/path/fileName.pdf' must contain 'path'");
      assertThatSuccess(Paths.get("/some/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("/"))),
                        "'/some/path/fileName.pdf' must contain '/'");
      assertThatSuccess(Paths.get("/"), PathRules.contains(value(Paths.get("/"))), "'/' must contain '/'");
      assertThatError(Paths.get(""), PathRules.contains(value(Paths.get("/"))), "'' must contain '/'");
      assertThatError(Paths.get("some/path", "fileName.pdf"),
                      PathRules.contains(value(Paths.get("/"))),
                      "'some/path/fileName.pdf' must contain '/'");
      assertThatSuccess(Paths.get("/some/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("some"))),
                        "'/some/path/fileName.pdf' must contain 'some'");
      assertThatSuccess(Paths.get("/some/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("some/path"))),
                        "'/some/path/fileName.pdf' must contain 'some/path'");
      assertThatSuccess(Paths.get("/some/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("path/fileName.pdf"))),
                        "'/some/path/fileName.pdf' must contain 'path/fileName.pdf'");
      assertThatSuccess(Paths.get("/some/other/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("other/path"))),
                        "'/some/other/path/fileName.pdf' must contain 'other/path'");
      /*assertThatSuccess(Paths.get("/other/other/other/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("other/other/path"))),
                        "'/some/other/other/path/fileName.pdf' must contain 'other/path'"); FIXME */
      assertThatError(Paths.get("/some/path", "fileName.pdf"),
                      PathRules.contains(value(Paths.get("some/path/other"))),
                      "'/some/path/fileName.pdf' must contain 'some/path/other'");
      assertThatSuccess(Paths.get("/some/path", "fileName.pdf"),
                        PathRules.contains(value(Paths.get("fileName.pdf"))),
                        "'/some/path/fileName.pdf' must contain 'fileName.pdf'");
      assertThatError(Paths.get("/some/path", "fileName.pdf"),
                      PathRules.contains(value(Paths.get("other"))),
                      "'/some/path/fileName.pdf' must contain 'other'");
      assertThatError(Paths.get("/some/path", "fileName.pdf"),
                      PathRules.contains(value(Paths.get(""))),
                      "'/some/path/fileName.pdf' must contain ''");
      assertThatSuccess(Paths.get(""), PathRules.contains(value(Paths.get(""))), "'' must contain ''");
      assertThatError(Paths.get("/some/path", "fileName.pdf"),
                      PathRules.contains(value(Paths.get("SOME"))),
                      "'/some/path/fileName.pdf' must contain 'SOME'");
      assertThatError(Paths.get("/SOME/path", "fileName.pdf"),
                      PathRules.contains(value(Paths.get("some"))),
                      "'/SOME/path/fileName.pdf' must contain 'some'");
   }

   @Test
   public void testIsEqualTo() {
      assertThatSuccess(Paths.get("/some/path/fileName.pdf"),
                        PathRules.isEqualTo(value(Paths.get("/some/path/fileName.pdf"))),
                        "'/some/path/fileName.pdf' must be equal to '/some/path/fileName.pdf'");
      assertThatSuccess(Paths.get("some/path/fileName.pdf"),
                        PathRules.isEqualTo(value(Paths.get("some/path/fileName.pdf"))),
                        "'some/path/fileName.pdf' must be equal to 'some/path/fileName.pdf'");
      assertThatError(Paths.get("some/path/fileName.pdf"),
                      PathRules.isEqualTo(value(Paths.get("some/PATH/fileName.pdf"))),
                      "'some/path/fileName.pdf' must be equal to 'some/PATH/fileName.pdf'");
      assertThatError(Paths.get("some/PATH/fileName.pdf"),
                      PathRules.isEqualTo(value(Paths.get("some/path/fileName.pdf"))),
                      "'some/PATH/fileName.pdf' must be equal to 'some/path/fileName.pdf'");
      assertThatSuccess(Paths.get("/"),
                        PathRules.isEqualTo(value(Paths.get("/"))),
                        "'/' must be equal to '/'");
      assertThatError(Paths.get("/"),
                      PathRules.isEqualTo(value(Paths.get("/some"))),
                      "'/' must be equal to '/some'");
      assertThatSuccess(Paths.get(""), PathRules.isEqualTo(value(Paths.get(""))), "'' must be equal to ''");
      assertThatError(Paths.get(""),
                      PathRules.isEqualTo(value(Paths.get("some"))),
                      "'' must be equal to 'some'");
      assertThatError(Paths.get("some/path/fileName.pdf"),
                      PathRules.isEqualTo(value(Paths.get("some/path"))),
                      "'some/path/fileName.pdf' must be equal to 'some/path'");
      assertThatError(Paths.get("some/path/fileName.pdf"),
                      PathRules.isEqualTo(value(Paths.get("fileName.pdf"))),
                      "'some/path/fileName.pdf' must be equal to 'fileName.pdf'");
   }

   @Test
   public void testIsEqualToIgnoreCase() {
      assertThatSuccess(Paths.get("some/path/fileName.pdf"),
                        PathRules.isEqualToIgnoreCase(value(Paths.get("some/PATH/fileName.pdf"))),
                        "'some/path/fileName.pdf' must be equal to 'some/PATH/fileName.pdf' (case-insensitive)");
      assertThatSuccess(Paths.get("some/PATH/fileName.pdf"),
                        PathRules.isEqualToIgnoreCase(value(Paths.get("some/path/fileName.pdf"))),
                        "'some/PATH/fileName.pdf' must be equal to 'some/path/fileName.pdf' (case-insensitive)");
   }

   @Test
   public void testContainsIgnoreCase() {
      assertThatSuccess(Paths.get("/some/path", "fileName.pdf"),
                        PathRules.containsIgnoreCase(value(Paths.get("SOME"))),
                        "'/some/path/fileName.pdf' must contain 'SOME' (case-insensitive)");
      assertThatSuccess(Paths.get("/SOME/path", "fileName.pdf"),
                        PathRules.containsIgnoreCase(value(Paths.get("some"))),
                        "'/SOME/path/fileName.pdf' must contain 'some' (case-insensitive)");
   }

   @Test
   public void testStartsWith() {
      assertThatSuccess(Paths.get("/path", "fileName.pdf"),
                        PathRules.startsWith(value(Paths.get("/"))),
                        "'/path/fileName.pdf' must start with '/'");
      assertThatError(Paths.get("path", "fileName.pdf"),
                      PathRules.startsWith(value(Paths.get("/"))),
                      "'path/fileName.pdf' must start with '/'");
      assertThatSuccess(Paths.get("/path", "fileName.pdf"),
                        PathRules.startsWith(value(Paths.get("/path"))),
                        "'/path/fileName.pdf' must start with '/path'");
      assertThatError(Paths.get("/path", "fileName.pdf"),
                      PathRules.startsWith(value(Paths.get("/otherPath"))),
                      "'/path/fileName.pdf' must start with '/otherPath'");
      assertThatError(Paths.get("fileName.pdf"),
                      PathRules.startsWith(value(Paths.get("/fileName.pdf"))),
                      "'fileName.pdf' must start with '/fileName.pdf'");
      assertThatError(Paths.get("/fileName.pdf"),
                      PathRules.startsWith(value(Paths.get("fileName.pdf"))),
                      "'/fileName.pdf' must start with 'fileName.pdf'");
      assertThatError(Paths.get("some/other"),
                      PathRules.startsWith(value(Paths.get("some/other/path"))),
                      "'some/other' must start with 'some/other/path'");
   }

   @Test
   public void testEndsWith() {
      assertThatSuccess(Paths.get("/path", "fileName.pdf"),
                        PathRules.endsWith(value(Paths.get("fileName.pdf"))),
                        "'/path/fileName.pdf' must end with 'fileName.pdf'");
      assertThatError(Paths.get("/some/path", "fileName.pdf"),
                      PathRules.endsWith(value(Paths.get("some/path"))),
                      "'/some/path/fileName.pdf' must end with 'some/path'");
      assertThatSuccess(Paths.get("/path", "fileName.pdf"),
                        PathRules.endsWith(value(Paths.get("/path/fileName.pdf"))),
                        "'/path/fileName.pdf' must end with '/path/fileName.pdf'");
      assertThatSuccess(Paths.get("/path", "fileName.pdf"),
                        PathRules.endsWith(value(Paths.get("path/fileName.pdf"))),
                        "'/path/fileName.pdf' must end with 'path/fileName.pdf'");
      assertThatError(Paths.get("/path", "fileName.pdf"),
                      PathRules.endsWith(value(Paths.get("/otherPath"))),
                      "'/path/fileName.pdf' must end with '/otherPath'");
      assertThatError(Paths.get("fileName.pdf"),
                      PathRules.endsWith(value(Paths.get("/fileName.pdf"))),
                      "'fileName.pdf' must end with '/fileName.pdf'");
      assertThatSuccess(Paths.get("/"), PathRules.endsWith(value(Paths.get("/"))), "'/' must end with '/'");
   }

}