/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.anySatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoDuplicates;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.noneSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThan;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThan;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.Collections.singletonList;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.InvariantResult;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.FixedString;

public class CollectionRulesTest extends BaseInvariantTest {

   @Test
   // FIXME Create real tests for *Satisfies
   public void testSatisfies() {
      System.out.println(validate(Arrays.asList(1, 2),
                                  "myList",
                                  isNotNull().andValue(allSatisfies(isNotNull()))));
      System.out.println(validate(Arrays.asList(), "myList", allSatisfies(isNotNull())));
      System.out.println(validate(Arrays.asList(1, 2), "myList", allSatisfies(isNull())));
      System.out.println(validate(Arrays.asList(), "myList", anySatisfies(isNotNull())));
      System.out.println(validate(Arrays.asList(1, 2), "myList", anySatisfies(isNotNull())));
      System.out.println(validate(Arrays.asList(1, 2), "myList", anySatisfies(isGreaterThan(value(1)))));
      System.out.println(validate(Arrays.asList(1, 2), "myList", anySatisfies(isNull())));
      System.out.println(validate(Arrays.asList(1, 2), "myList", noneSatisfies(isNotNull())));
      System.out.println(validate(Arrays.asList(), "myList", noneSatisfies(isNotNull())));
      System.out.println(validate(Arrays.asList(1, 2), "myList", noneSatisfies(isNull())));
      System.out.println(validate(Arrays.asList(1, 2),
                                  "myList",
                                  isNull().orValue(size(isLessThan(value(3))))));
      System.out.println(validate(Arrays.asList(1, 2),
                                  "myList",
                                  isNotNull().andValue(allSatisfies(isPositive()))));
   }

   @Test
   public void testIsNotEmpty() {
      assertThatSuccess(Arrays.asList(1, 2), CollectionRules.isNotEmpty(), "'<object>' must not be empty");
      assertThatError(Arrays.asList(), "field", CollectionRules.isNotEmpty(), "'field' must not be empty");
      assertThatError(null, "field", CollectionRules.isNotEmpty(), "'field' must not be null");
   }

   @Test
   public void testHasNoNullElements() {
      assertThatSuccess(Arrays.asList(1, 2),
                        "field",
                        CollectionRules.hasNoNullElements(),
                        "'field=[1,2]' > all elements must satisfy rule",
                        validatingObject(Arrays.asList(1, 2), "field")
                              .ruleName("CollectionRules.allSatisfies")
                              .validationResults(Arrays.asList(-1, null)));
      assertThatError(null, "field", CollectionRules.hasNoNullElements(), "'field' must not be null");
      assertThatError(Arrays.asList(1, null, 2),
                      "field",
                      CollectionRules.hasNoNullElements(),
                      "'field=[1,null,2]' > 'field[1]' must not be null",
                      validatingObject(Arrays.asList(1, null, 2), "field")
                            .ruleName("CollectionRules.allSatisfies")
                            .validationResults(Arrays.asList(1,
                                                             InvariantResult.error(validatingObject(null,
                                                                                                    "field[1]")
                                                                                         .ruleName(
                                                                                               "BaseRules.isNotNull")
                                                                                         .validationResults(
                                                                                               Arrays.asList()),
                                                                                   FixedString.of(
                                                                                         "'field[1]' must not be null")))));
   }

   @Test
   public void testHasNoNullElementsWhenCustomMessage() {
      assertThatError(Arrays.asList(1, null, 2),
                      "field",
                      CollectionRules.hasNoNullElements("this '%1$s' must not have null items at index : %2$d",
                                                        MessageValue.validatingObjectName(),
                                                        MessageValue.validationResult(0)),
                      "this 'field' must not have null items at index : 1",
                      validatingObject(Arrays.asList(1, null, 2), "field")
                            .ruleName("CollectionRules.allSatisfies")
                            .validationResults(Arrays.asList(1,
                                                             InvariantResult.error(validatingObject(null,
                                                                                                    "field[1]")
                                                                                         .ruleName(
                                                                                               "BaseRules.isNotNull")
                                                                                         .validationResults(
                                                                                               Arrays.asList()),
                                                                                   FixedString.of(
                                                                                         "'field[1]' must not be null")))));
   }

   @Test
   public void testHasNoBlankElements() {
      assertThatSuccess(Arrays.asList("1", "2"),
                        "field",
                        CollectionRules.hasNoBlankElements(),
                        "'field=[1,2]' > all elements must satisfy rule",
                        validatingObject(Arrays.asList("1", "2"), "field")
                              .ruleName("CollectionRules.allSatisfies")
                              .validationResults(Arrays.asList(-1, null)));
      assertThatError(null, "field", CollectionRules.hasNoBlankElements(), "'field' must not be null");
      assertThatError(Arrays.asList("1", null, "2"),
                      "field",
                      CollectionRules.hasNoBlankElements(),
                      "'field=[1,null,2]' > 'field[1]' must not be null",
                      validatingObject(Arrays.asList("1", null, "2"), "field")
                            .ruleName("CollectionRules.allSatisfies")
                            .validationResults(Arrays.asList(1,
                                                             InvariantResult.error(validatingObject(null,
                                                                                                    "field[1]")
                                                                                         .ruleName(
                                                                                               "StringRules.isNotBlank")
                                                                                         .validationResults(
                                                                                               Arrays.asList()),
                                                                                   FixedString.of(
                                                                                         "'field[1]' must not be null")))));
      assertThatError(Arrays.asList("1", "", "2"),
                      "field",
                      CollectionRules.hasNoBlankElements(),
                      "'field=[1,,2]' > 'field[1]' must not be blank",
                      validatingObject(Arrays.asList("1", "", "2"), "field")
                            .ruleName("CollectionRules.allSatisfies")
                            .validationResults(Arrays.asList(1,
                                                             InvariantResult.error(validatingObject("",
                                                                                                    "field[1]")
                                                                                         .ruleName(
                                                                                               "StringRules.isNotBlank")
                                                                                         .validationResults(
                                                                                               Arrays.asList()),
                                                                                   FixedString.of(
                                                                                         "'field[1]' must not be blank")))));
      assertThatError(Arrays.asList("1", " ", "2"),
                      "field",
                      CollectionRules.hasNoBlankElements(),
                      "'field=[1, ,2]' > 'field[1]' must not be blank",
                      validatingObject(Arrays.asList("1", " ", "2"), "field")
                            .ruleName("CollectionRules.allSatisfies")
                            .validationResults(Arrays.asList(1,
                                                             InvariantResult.error(validatingObject(" ",
                                                                                                    "field[1]")
                                                                                         .ruleName(
                                                                                               "StringRules.isNotBlank")
                                                                                         .validationResults(
                                                                                               Arrays.asList()),
                                                                                   FixedString.of(
                                                                                         "'field[1]' must not be blank")))));
   }

   @Test
   public void testHasNoBlankElementsWhenCustomMessage() {
      assertThatError(Arrays.asList("1", "", "2"),
                      "field",
                      CollectionRules.hasNoBlankElements(
                            "this '%1$s' must not have blank items at index : %2$d",
                            MessageValue.validatingObjectName(),
                            MessageValue.validationResult(0)),
                      "this 'field' must not have blank items at index : 1",
                      validatingObject(Arrays.asList("1", "", "2"), "field")
                            .ruleName("CollectionRules.allSatisfies")
                            .validationResults(Arrays.asList(1,
                                                             InvariantResult.error(validatingObject("",
                                                                                                    "field[1]")
                                                                                         .ruleName(
                                                                                               "StringRules.isNotBlank")
                                                                                         .validationResults(
                                                                                               Arrays.asList()),
                                                                                   FixedString.of(
                                                                                         "'field[1]' must not be blank")))));
   }

   @Test
   public void testContainsAll() {
      assertThatSuccess(Arrays.asList(1, 2),
                        CollectionRules.containsAll(value(Arrays.asList(1))),
                        "'[1,2]' must contain [1]");
      assertThatSuccess(Arrays.asList(),
                        CollectionRules.containsAll(value(Arrays.asList())),
                        "'[]' must contain []");
      assertThatSuccess(Arrays.asList(1, 2),
                        CollectionRules.containsAll(value(Arrays.asList(1, 2))),
                        "'[1,2]' must contain [1,2]");
      assertThatSuccess(Arrays.asList(1, null, 2),
                        CollectionRules.containsAll(value(singletonList(null))),
                        "'[1,null,2]' must contain [null]");
      assertThatSuccess(singletonList(null),
                        CollectionRules.containsAll(value(singletonList(null))),
                        "'[null]' must contain [null]");
      assertThatThrown(Arrays.asList(1, 2), "field", () -> CollectionRules.containsAll(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'items' must not be null");
      assertThatError(null,
                      "field",
                      CollectionRules.containsAll(value(Arrays.asList(1))),
                      "'field' must not be null");
      assertThatError(Arrays.asList(1, 2),
                      "field",
                      CollectionRules.containsAll(value(Arrays.asList(1, 3))),
                      "'field=[1,2]' must contain [1,3]");
   }

   @Test
   public void testContains() {
      assertThatSuccess(Arrays.asList(1, 2), CollectionRules.contains(value(1)), "'[1,2]' must contain '1'");
      assertThatError(Arrays.asList(), CollectionRules.contains(value(1)), "'[]' must contain '1'");
      assertThatSuccess(Arrays.asList(1, null, 2),
                        CollectionRules.contains(value(null)),
                        "'[1,null,2]' must contain 'null'");
      assertThatSuccess(singletonList(null),
                        CollectionRules.contains(value(null)),
                        "'[null]' must contain 'null'");
      assertThatError(null,
                      "field",
                      CollectionRules.contains(value(Arrays.asList(1))),
                      "'field' must not be null");
      assertThatError(Arrays.asList(1, 2),
                      "field",
                      CollectionRules.contains(value(3)),
                      "'field=[1,2]' must contain '3'");
   }

   @Test
   public void testContainsExactly() {
      assertThatError(Arrays.asList(1, 2),
                      "field",
                      CollectionRules.containsExactly(value(Arrays.asList(1))),
                      "'field=[1,2]' must contain exactly [1]");
      assertThatSuccess(Arrays.asList(),
                        "field",
                        CollectionRules.containsExactly(value(Arrays.asList())),
                        "'field=[]' must contain exactly []");
      assertThatSuccess(Arrays.asList(1, 2),
                        CollectionRules.containsExactly(value(Arrays.asList(1, 2))),
                        "'[1,2]' must contain exactly [1,2]");
      assertThatError(Arrays.asList(1, 2),
                      "field",
                      CollectionRules.containsExactly(value(Arrays.asList(1, 2, 3))),
                      "'field=[1,2]' must contain exactly [1,2,3]");
      assertThatError(Arrays.asList(1, 2),
                      "field",
                      CollectionRules.containsExactly(value(Arrays.asList(2, 1))),
                      "'field=[1,2]' must contain exactly [2,1]");
      assertThatError(Arrays.asList(1, null, 2),
                      "field",
                      CollectionRules.containsExactly(value(singletonList(null))),
                      "'field=[1,null,2]' must contain exactly [null]");
      assertThatSuccess(Arrays.asList(1, null, 2),
                        CollectionRules.containsExactly(value(Arrays.asList(1, null, 2))),
                        "'[1,null,2]' must contain exactly [1,null,2]");
      assertThatSuccess(singletonList(null),
                        CollectionRules.containsExactly(value(singletonList(null))),
                        "'[null]' must contain exactly [null]");
      assertThatThrown(Arrays.asList(1, 2), "field", () -> CollectionRules.containsExactly(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'items' must not be null");
   }

   @Test
   public void testItsSize() {
      assertThatSuccess(Arrays.asList(1, 2), size(isEqualTo(value(2))), "'size=2' must be equal to '2'");
      assertThatError(Arrays.asList(1, 2),
                      "field",
                      size(isEqualTo(value(5))),
                      "'field.size=2' must be equal to '5'");
      assertThatError(null, "field", size(isEqualTo(value(3))), "'field' must not be null");

      assertThatError(Arrays.asList(1, 2),
                      "field",
                      size(isEqualTo(value(5),
                                     "this '%1$s' value must be = 5 : %2$s (%3$s)",
                                     MessageValue.validatingObjectName(),
                                     MessageValue.validatingObjectValue(),
                                     MessageValue.validatingObjectInitialValue())),
                      "this 'field.size' value must be = 5 : 2 ([1, 2])");
   }

   @Test
   public void testHasNoDuplicates() {
      assertThatSuccess(list(1, 2), hasNoDuplicates(), "'[1, 2]' has duplicates : {}");
      assertThatSuccess(list("value1", "value2"),
                        hasNoDuplicates(),
                        "'[value1, value2]' has duplicates : {}");
      assertThatError(list(1, 1, 2), hasNoDuplicates(), "'[1, 1, 2]' has duplicates : {1->[1,1]}");
      assertThatError(list("value1", "value1", "value2"),
                      hasNoDuplicates(),
                      "'[value1, value1, value2]' has duplicates : {value1->[value1,value1]}");
   }

}