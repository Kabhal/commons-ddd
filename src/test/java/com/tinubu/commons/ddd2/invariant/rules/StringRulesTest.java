/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.contains;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.chars;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.length;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.MessageValue;

public class StringRulesTest extends BaseInvariantTest {

   @Test
   public void testIsEmpty() {
      assertThatError("abc", StringRules.isEmpty(), "'abc' must be empty");

      assertThatSuccess("", "field", StringRules.isEmpty(), "'field=' must be empty");
      assertThatError(null, "field", StringRules.isEmpty(), "'field' must not be null");

      assertThatSuccess("",
                        "field",
                        StringRules.isEmpty("this '%1$s' value must be empty : %2$s",
                                            MessageValue.validatingObjectName(),
                                            MessageValue.validatingObjectValue()),
                        "this 'field' value must be empty : ");
   }

   @Test
   public void testIsNotEmpty() {
      assertThatSuccess("abc", StringRules.isNotEmpty(), "'<object>' must not be empty");

      assertThatError("", "field", StringRules.isNotEmpty(), "'field' must not be empty");
      assertThatError(null, "field", StringRules.isNotEmpty(), "'field' must not be null");

      assertThatError("",
                      "field",
                      StringRules.isNotEmpty("this '%1$s' value must not be empty : %2$s",
                                             MessageValue.validatingObjectName(),
                                             MessageValue.validatingObjectValue()),
                      "this 'field' value must not be empty : ");
   }

   @Test
   public void testIsBlank() {
      assertThatError("abc", StringRules.isBlank(), "'<object>' must be blank");
      assertThatSuccess("\t", "field", StringRules.isBlank(), "'field' must be blank");
      assertThatSuccess(" ", "field", StringRules.isBlank(), "'field' must be blank");
      assertThatSuccess("", "field", StringRules.isBlank(), "'field' must be blank");
      assertThatError(null, "field", StringRules.isBlank(), "'field' must not be null");

      assertThatSuccess("",
                        "field",
                        StringRules.isBlank("this '%1$s' value must be blank : %2$s",
                                            MessageValue.validatingObjectName(),
                                            MessageValue.validatingObjectValue()),
                        "this 'field' value must be blank : ");
   }

   @Test
   public void testIsNotBlank() {
      assertThatSuccess("abc", StringRules.isNotBlank(), "'<object>' must not be blank");
      assertThatError("\t", "field", StringRules.isNotBlank(), "'field' must not be blank");
      assertThatError(" ", "field", StringRules.isNotBlank(), "'field' must not be blank");
      assertThatError("", "field", StringRules.isNotBlank(), "'field' must not be blank");
      assertThatError(null, "field", StringRules.isNotBlank(), "'field' must not be null");

      assertThatError("",
                      "field",
                      StringRules.isNotBlank("this '%1$s' value must not be blank : %2$s",
                                             MessageValue.validatingObjectName(),
                                             MessageValue.validatingObjectValue()),
                      "this 'field' value must not be blank : ");
   }

   @Test
   public void testContainsOnly() {
      assertThatSuccess("abc",
                        StringRules.containsOnly(value("abc")),
                        "'abc' must contain only [a,b,c] characters");
      assertThatError("abc",
                      StringRules.containsOnly(value("ab")),
                      "'abc' must contain only [a,b] characters");
      assertThatError("abc", StringRules.containsOnly(value("z")), "'abc' must contain only [z] characters");
      assertThatError(null, "field", StringRules.containsOnly(value("abc")), "'field' must not be null");

      assertThatError("abc",
                      "field",
                      StringRules.containsOnly(value("ab"),
                                               "this '%1$s' value must contain only specified characters : %2$s",
                                               MessageValue.validatingObjectName(),
                                               MessageValue.validatingObjectValue()),
                      "this 'field' value must contain only specified characters : abc");
   }

   @Test
   public void testContainsNone() {
      assertThatSuccess("abc", StringRules.containsNone(value("z")), "'abc' must not contain [z] characters");
      assertThatError("abc",
                      StringRules.containsNone(value("ab")),
                      "'abc' must not contain [a,b] characters");
      assertThatError("abc", StringRules.containsNone(value("a")), "'abc' must not contain [a] characters");
      assertThatError(null, "field", StringRules.containsNone(value("abc")), "'field' must not be null");

      assertThatError("abc",
                      "field",
                      StringRules.containsNone(value("ab"),
                                               "this '%1$s' value must not contain specified characters : %2$s",
                                               MessageValue.validatingObjectName(),
                                               MessageValue.validatingObjectValue()),
                      "this 'field' value must not contain specified characters : abc");
   }

   @Test
   public void testContainsAny() {
      assertThatSuccess("abc", StringRules.containsAny(value("a")), "'abc' must contain any [a] characters");
      assertThatSuccess("abc",
                        StringRules.containsAny(value("ab")),
                        "'abc' must contain any [a,b] characters");
      assertThatError("abc", StringRules.containsAny(value("z")), "'abc' must contain any [z] characters");
      assertThatError(null, "field", StringRules.containsAny(value("abc")), "'field' must not be null");

      assertThatError("abc",
                      "field",
                      StringRules.containsAny(value("z"),
                                              "this '%1$s' value must contain any specified characters : %2$s",
                                              MessageValue.validatingObjectName(),
                                              MessageValue.validatingObjectValue()),
                      "this 'field' value must contain any specified characters : abc");
   }

   @Test
   public void testLength() {
      assertThatSuccess("abc", length(isEqualTo(value(3))), "'length=3' must be equal to '3'");
      assertThatError("abc", "field", length(isEqualTo(value(5))), "'field.length=3' must be equal to '5'");
      assertThatError(null, "field", length(isEqualTo(value(3))), "'field' must not be null");
   }

   @Test
   public void testChars() {
      assertThatSuccess("abc", chars(contains(value('b'))), "'chars=[a,b,c]' must contain 'b'");
      assertThatError("abc", "field", chars(contains(value('z'))), "'field.chars=[a,b,c]' must contain 'z'");
      assertThatError(null, "field", chars(contains(value('b'))), "'field' must not be null");
   }

}