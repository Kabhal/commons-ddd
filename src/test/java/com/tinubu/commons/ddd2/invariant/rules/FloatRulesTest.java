/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

public class FloatRulesTest extends BaseInvariantTest {

   @Test
   public void testisFinite() {
      assertThatSuccess(3f, FloatRules.isFinite(), "'3.0' must be finite");
      assertThatError(Float.NEGATIVE_INFINITY,
                      "field",
                      FloatRules.isFinite(),
                      "'field=-Infinity' must be finite");
      assertThatError(Float.POSITIVE_INFINITY,
                      "field",
                      FloatRules.isFinite(),
                      "'field=Infinity' must be finite");
   }

   @Test
   public void testIsNotNan() {
      assertThatSuccess(3f, FloatRules.isNotNan(), "'<object>' must not be NaN");
      assertThatError(Float.NaN, "field", FloatRules.isNotNan(), "'field' must not be NaN");
   }

}