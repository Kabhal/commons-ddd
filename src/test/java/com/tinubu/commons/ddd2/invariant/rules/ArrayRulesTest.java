/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.booleanList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.byteList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.charList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.collection;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.doubleList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.floatList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.intList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.list;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.longList;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.shortList;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.containsExactly;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThan;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.lang.util.CollectionUtils;

public class ArrayRulesTest extends BaseInvariantTest {

   @Test
   public void testListWhenNominal() {
      assertThatSuccess(new Short[] { 1, 2 },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Integer[] { 1, 2 },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Long[] { 1L, 2L },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Float[] { 1f, 2f },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Double[] { 1d, 2d },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Character[] { '1', '2' },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Byte[] { 1, 2 },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Boolean[] { true, false },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new String[] { "a", "b" },
                        "array",
                        list(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testListWhenNull() {
      assertThatError(null, "array", list(size(isGreaterThan(value(0)))), "'array' must not be null");
   }

   @Test
   public void testListWhenSubTypeRuleParameter() {
      assertThatSuccess(new CharSequence[] { "a", "b" },
                        "array",
                        list(containsExactly(value(CollectionUtils.list("a", "b")))),
                        "'array=[a,b]' must contain exactly [a,b]");
   }

   @Test
   public void testListWhenSuperTypeRule() {
      assertThatSuccess(new String[] { "a", "b" },
                        "array",
                        list(allSatisfies(isNotBlank())),
                        "'array=[a,b]' > all elements must satisfy rule");
   }

   @Test
   public void testShortPrimitiveList() {

      assertThatSuccess(new short[] { 1, 2 },
                        "array",
                        shortList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testIntPrimitiveList() {
      assertThatSuccess(new int[] { 1, 2 },
                        "array",
                        intList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testLongPrimitiveList() {
      assertThatSuccess(new long[] { 1L, 2L },
                        "array",
                        longList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testFloatPrimitiveList() {
      assertThatSuccess(new float[] { 1f, 2f },
                        "array",
                        floatList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testDoublePrimitiveList() {
      assertThatSuccess(new double[] { 1d, 2d },
                        "array",
                        doubleList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testCharPrimitiveList() {
      assertThatSuccess(new char[] { '1', '2' },
                        "array",
                        charList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testBytePrimitiveList() {
      assertThatSuccess(new byte[] { 1, 2 },
                        "array",
                        byteList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testBooleanPrimitiveList() {
      assertThatSuccess(new boolean[] { true, false },
                        "array",
                        booleanList(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
   }

   @Test
   public void testCollection() {
      assertThatError(null, "array", collection(size(isGreaterThan(value(0)))), "'array' must not be null");
      assertThatSuccess(new short[] { 1, 2 },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new int[] { 1, 2 },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new long[] { 1L, 2L },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new float[] { 1f, 2f },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new double[] { 1d, 2d },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new char[] { '1', '2' },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new byte[] { 1, 2 },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new boolean[] { true, false },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Short[] { 1, 2 },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Integer[] { 1, 2 },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Long[] { 1L, 2L },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Float[] { 1f, 2f },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Double[] { 1d, 2d },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Character[] { '1', '2' },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Byte[] { 1, 2 },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new Boolean[] { true, false },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatSuccess(new String[] { "a", "b" },
                        "array",
                        collection(size(isGreaterThan(value(0)))),
                        "'array.size=2' must be greater than '0'");
      assertThatThrown("a", "object", () -> collection(size(isGreaterThan(value(0)))))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("value is not an array : a");
   }

}

