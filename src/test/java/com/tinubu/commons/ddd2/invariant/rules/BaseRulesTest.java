/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.propertyNameMapper;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isFalse;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isMutuallyExclusive;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isMutuallyInclusive;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isTrue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.throwing;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;

public class BaseRulesTest extends BaseInvariantTest {

   @Test
   public void testIsNull() {
      Object object = new Object() {
         @Override
         public String toString() {
            return "[an object]";
         }
      };

      assertThatError("", "field", isNull(), "'field=' must be null");
      assertThatError(true, "field", isNull(), "'field=true' must be null");
      assertThatError(object, "field", isNull(), "'field=[an object]' must be null");

      assertThatSuccess(null, isNull(), "'null' must be null");
      assertThatError(object,
                      "field",
                      isNull("this '%1$s' value must be null : %2$s",
                             validatingObjectName(),
                             validatingObjectValue()),
                      "this 'field' value must be null : [an object]");
   }

   @Test
   public void testIsNotNull() {
      assertThatSuccess("", isNotNull(), "'<object>' must not be null");
      assertThatSuccess(true, isNotNull(), "'<object>' must not be null");
      assertThatSuccess(new Object(), isNotNull(), "'<object>' must not be null");
      assertThatError(null, "field", isNotNull(), "'field' must not be null");
      assertThatError(null,
                      "field",
                      isNotNull("this '%1$s' value must not be null : %2$s",
                                validatingObjectName(),
                                validatingObjectValue()),
                      "this 'field' value must not be null : null");
   }

   @Test
   public void testIsTrueFalse() {
      assertThatSuccess(true, isTrue(), "'<object>' must be true");
      assertThatSuccess(2 == 2, isTrue(), "'<object>' must be true");
      assertThatError(false, "field", isTrue(), "'field' must be true");
      assertThatError(null, "field", isTrue(), "'field' must not be null");

      assertThatSuccess(false, isFalse(), "'<object>' must be false");
      assertThatSuccess(2 == 3, isFalse(), "'<object>' must be false");
      assertThatError(true, "field", isFalse(), "'field' must be false");
      assertThatError(null, "field", isFalse(), "'field' must not be null");
   }

   @Test
   public void testIsMutuallyExclusive() {
      assertThatSuccess(true,
                        isMutuallyExclusive(value(null, "param")),
                        "'true' and 'param=null' must be mutually exclusive");
      assertThatSuccess(null,
                        isMutuallyExclusive(value(true, "param")),
                        "'null' and 'param=true' must be mutually exclusive");
      assertThatError(true,
                      "field",
                      isMutuallyExclusive(value(true, "param")),
                      "'field=true' and 'param=true' must be mutually exclusive");
      assertThatError(null,
                      "field",
                      isMutuallyExclusive(value(null, "param")),
                      "'field=null' and 'param=null' must be mutually exclusive");

      assertThatSuccess(true,
                        isMutuallyExclusive(value(null, "param1")).andValue(isMutuallyExclusive(value(null,
                                                                                                      "param2"))),
                        "'true' and 'param2=null' must be mutually exclusive");
      assertThatError(true,
                      isMutuallyExclusive(value(null, "param1")).andValue(isMutuallyExclusive(value(true,
                                                                                                    "param2"))),
                      "'true' and 'param2=true' must be mutually exclusive");
   }

   @Test
   public void testIsMutuallyInclusive() {
      assertThatSuccess(true,
                        isMutuallyInclusive(value(true, "param")),
                        "'true' and 'param=true' must be mutually inclusive");
      assertThatSuccess(null,
                        isMutuallyInclusive(value(null, "param")),
                        "'null' and 'param=null' must be mutually inclusive");
      assertThatError(true,
                      "field",
                      isMutuallyInclusive(value(null, "param")),
                      "'field=true' and 'param=null' must be mutually inclusive");
      assertThatError(null,
                      "field",
                      isMutuallyInclusive(value(true, "param")),
                      "'field=null' and 'param=true' must be mutually inclusive");

      assertThatSuccess(true,
                        isMutuallyInclusive(value(true, "param1")).andValue(isMutuallyInclusive(value(true,
                                                                                                      "param2"))),
                        "'true' and 'param2=true' must be mutually inclusive");
      assertThatError(true,
                      isMutuallyInclusive(value(true, "param1")).andValue(isMutuallyInclusive(value(null,
                                                                                                    "param2"))),
                      "'true' and 'param2=null' must be mutually inclusive");
   }

   @Test
   public void testAs() {
      assertThatSuccess(MyLocale.FRANCE,
                        "locale",
                        as(MyLocale::country, propertyNameMapper("country"), isNotBlank()),
                        "'locale.country' must not be blank");
      assertThatThrown(MyLocale.FRANCE, () -> as(null, propertyNameMapper("country"), isNotBlank()))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'valueMapper' must not be null");
      assertThatThrown(MyLocale.FRANCE, () -> as(MyLocale::country, null, isNotBlank()))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'nameMapper' must not be null");
      assertThatThrown(MyLocale.FRANCE, () -> as(MyLocale::country, propertyNameMapper("country"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'rule' must not be null");
      assertThatSuccess(MyLocale.FRANCE,
                        "locale",
                        as(MyLocale::country, propertyNameMapper("doesNotExist"), isNotBlank()),
                        "'locale.doesNotExist' must not be blank");
   }

   @Test
   public void testProperty() {
      assertThatSuccess(MyLocale.FRANCE,
                        "locale",
                        property(MyLocale::country, "country", isNotBlank()),
                        "'locale.country' must not be blank");
      assertThatThrown(MyLocale.FRANCE, () -> property(null, "country", isNotBlank()))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'valueMapper' must not be null");
      assertThatThrown(MyLocale.FRANCE, () -> property(MyLocale::country, null, isNotBlank()))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'propertyName' must not be null");
      assertThatThrown(MyLocale.FRANCE, () -> property(MyLocale::country, "country", null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'rule' must not be null");
      assertThatSuccess(MyLocale.FRANCE,
                        "locale",
                        property(MyLocale::country, "doesNotExist", isNotBlank()),
                        "'locale.doesNotExist' must not be blank");

      assertThatSuccess(MyLocale.FRANCE,
                        "locale",
                        property("country", isNotBlank()),
                        "'locale.country' must not be blank");
      assertThatThrown(MyLocale.FRANCE, () -> property(null, isNotBlank()))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'propertyName' must not be null");
      assertThatThrown(MyLocale.FRANCE, () -> property("country", null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'rule' must not be null");
      assertThatThrown(MyLocale.FRANCE, () -> property("doesNotExist", isNotBlank()))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage(
                  "Unknown 'doesNotExist' property in 'com.tinubu.commons.ddd2.invariant.rules.MyLocale'");
   }

   @Test
   public void testThrowingWhenNominal() {
      assertThatSuccess(true, "field", throwing(isNotNull()), "'field' must not be null");
      assertThatSuccess(true, "field", throwing(isNotNull(), "throwing message"), "'field' must not be null");
   }

   @Test
   public void testThrowingWhenException() {
      assertThatError(true, "field", throwing(__ -> {
         throw new IllegalStateException("error");
      }), "error");
      assertThatError(true,
                      "field",
                      throwing(__ -> {throw new IllegalStateException("error");}, "throwing message"),
                      "throwing message");
   }

   @Test
   public void testThrowingWhenExceptionWithNoMessage() {
      assertThatError(true, "field", throwing(__ -> {
         throw new IllegalStateException();
      }), "IllegalStateException");
   }

   @Test
   public void testThrowingWhenCustomMessage() {
      assertThatError(true,
                      "field",
                      throwing(__ -> {
                         throw new IllegalStateException("error");
                      }, (Exception e) -> e.getClass().getSimpleName() + ": " + e.getMessage()),
                      "IllegalStateException: error");
      assertThatError(true,
                      "field",
                      throwing(__ -> {
                                  throw new IllegalStateException("error");
                               },
                               (Exception e) -> FastStringFormat.of("'",
                                                                    validatingObjectName(),
                                                                    "' ",
                                                                    e.getMessage())),
                      "'field' error");
   }

   @Test
   public void testThrowingWhenSelectException() {
      assertThatError(true, "field", throwing(__ -> {
         throw new ChildException("error");
      }, ParentException.class), "error");

      assertThatError(true, "field", throwing(__ -> {
         throw new ChildException("error");
      }, ParentException.class, (Exception e) -> e.getClass().getSimpleName()), "ChildException");

      assertThatThrownBy(() -> assertThatError(true, "field", throwing(__ -> {
         throw new ChildException("error");
      }, IllegalStateException.class), "error")).isInstanceOf(ChildException.class).hasMessage("error");

      assertThatThrownBy(() -> assertThatError(true, "field", throwing(__ -> {
         throw new IllegalStateException("error");
      }, ChildException.class), "error")).isInstanceOf(IllegalStateException.class).hasMessage("error");
   }

}

class MyLocale {

   private String country;
   private String language;

   public MyLocale(String country, String language) {
      this.country = country;
      this.language = language;
   }

   public static MyLocale FRANCE = new MyLocale("FR", "fr");

   public String country() {
      return country;
   }

   public String language() {
      return language;
   }

}

class ParentException extends RuntimeException {

   public ParentException(String message) {
      super(message);
   }
}

class ChildException extends ParentException {

   public ChildException(String message) {
      super(message);
   }
}