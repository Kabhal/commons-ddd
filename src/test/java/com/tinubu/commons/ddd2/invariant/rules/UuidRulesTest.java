/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.isVersion1;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.isVersion3;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.isVersion4;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.isVersion5;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.uuid;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

public class UuidRulesTest extends BaseInvariantTest {

   @Test
   public void testIsUuidWhenNominal() {
      assertThatSuccess(uuidV1(), UuidRules.isUuid(), Pattern.compile("'[^']+' UUID must be valid"));
      assertThatSuccess(uuidV3("name"),
                        UuidRules.isUuid(),
                        "'b068931c-c450-342b-a3f5-b3d276ea4297' UUID must be valid");
      assertThatSuccess(uuidV4(), UuidRules.isUuid(), Pattern.compile("'[^']+' UUID must be valid"));
      assertThatSuccess(uuidV5("name"),
                        UuidRules.isUuid(),
                        "'6ae99955-2a0d-5dca-94d6-2e2bc8b764d3' UUID must be valid");
   }

   @Test
   public void testIsUuidWhenNull() {
      assertThatError(null, UuidRules.isUuid(), "'<object>' must not be null");
   }

   @Test
   public void testIsUuidWhenBlank() {
      assertThatError("", UuidRules.isUuid(), "'<object>' must not be blank");
      assertThatError(" ", UuidRules.isUuid(), "'<object>' must not be blank");
   }

   @Test
   public void testUuidWhenBadParameters() {
      assertThatError(null, UuidRules.uuid(isNotNull()), "'<object>' must not be null");
      assertThatError(" ", UuidRules.uuid(isNotNull()), "'<object>' must not be blank");
      assertThatError("", UuidRules.uuid(isNotNull()), "'<object>' must not be blank");
      assertThatError("invalid uuid", UuidRules.uuid(isNotNull()), "'invalid uuid' UUID must be valid");
   }

   @Test
   public void testUuidIsVersion1WhenNominal() {
      assertThatSuccess(uuidV1(),
                        uuid(isVersion1()),
                        Pattern.compile("'[^']+' UUID version must be 1 \\(time-based\\)"));
      assertThatError(uuidV3("name"),
                      uuid(isVersion1()),
                      Pattern.compile("'[^']+' UUID version must be 1 \\(time-based\\)"));
   }

   @Test
   public void testUuidIsVersion3WhenNominal() {
      assertThatSuccess(uuidV3("name"),
                        uuid(isVersion3()),
                        Pattern.compile("'[^']+' UUID version must be 3 \\(name-based MD5\\)"));
      assertThatError(uuidV1(),
                      uuid(isVersion3()),
                      Pattern.compile("'[^']+' UUID version must be 3 \\(name-based MD5\\)"));
   }

   @Test
   public void testUuidIsVersion4WhenNominal() {
      assertThatSuccess(uuidV4(),
                        uuid(isVersion4()),
                        Pattern.compile("'[^']+' UUID version must be 4 \\(random\\)"));
      assertThatError(uuidV1(),
                      uuid(isVersion4()),
                      Pattern.compile("'[^']+' UUID version must be 4 \\(random\\)"));
   }

   @Test
   public void testUuidIsVersion5WhenNominal() {
      assertThatSuccess(uuidV5("name"),
                        uuid(isVersion5()),
                        Pattern.compile("'[^']+' UUID version must be 5 \\(name-based SHA-1\\)"));
      assertThatError(uuidV1(),
                      uuid(isVersion5()),
                      Pattern.compile("'[^']+' UUID version must be 5 \\(name-based SHA-1\\)"));
   }

   @Test
   public void testUuidVersionWhenNominal() {
      assertThatSuccess(uuidV1(),
                        UuidRules.uuid(UuidRules.version(isEqualTo(value(1)))),
                        "'1' must be equal to '1'");
   }

   @Test
   public void testUuidVersionWhenBadParameters() {
      assertThatError(null,
                      UuidRules.uuid(UuidRules.version(isEqualTo(value(1)))),
                      "'<object>' must not be null");
      assertThatError(" ",
                      UuidRules.uuid(UuidRules.version(isEqualTo(value(1)))),
                      "'<object>' must not be blank");
      assertThatError("",
                      UuidRules.uuid(UuidRules.version(isEqualTo(value(1)))),
                      "'<object>' must not be blank");
      assertThatError("invalid uuid",
                      UuidRules.uuid(UuidRules.version(isEqualTo(value(1)))),
                      "'invalid uuid' UUID must be valid");
   }

   @Test
   public void testUuidTimestampWhenNominal() {
      assertThatSuccess(uuidV1(),
                        UuidRules.uuid(UuidRules.timestamp(isPositive())),
                        Pattern.compile("'[^']+' must be positive"));
   }

   @Test
   public void testUuidTimestampWhenBadParameters() {
      assertThatError(null, UuidRules.uuid(UuidRules.timestamp(isPositive())), "'<object>' must not be null");
      assertThatError(" ", UuidRules.uuid(UuidRules.timestamp(isPositive())), "'<object>' must not be blank");
      assertThatError("", UuidRules.uuid(UuidRules.timestamp(isPositive())), "'<object>' must not be blank");
      assertThatError("invalid uuid",
                      UuidRules.uuid(UuidRules.timestamp(isPositive())),
                      "'invalid uuid' UUID must be valid");
      assertThatError(uuidV4(),
                      UuidRules.uuid(UuidRules.timestamp(isPositive())),
                      Pattern.compile("'[^']+' UUID version must be 1 \\(time-based\\)"));
   }

   @Test
   public void testUuidClockSequenceWhenNominal() {
      assertThatSuccess(uuidV1(),
                        UuidRules.uuid(UuidRules.clockSequence(isPositive())),
                        Pattern.compile("'[^']+' must be positive"));
   }

   @Test
   public void testUuidClockSequenceWhenBadParameters() {
      assertThatError(null,
                      UuidRules.uuid(UuidRules.clockSequence(isPositive())),
                      "'<object>' must not be null");
      assertThatError(" ",
                      UuidRules.uuid(UuidRules.clockSequence(isPositive())),
                      "'<object>' must not be blank");
      assertThatError("",
                      UuidRules.uuid(UuidRules.clockSequence(isPositive())),
                      "'<object>' must not be blank");
      assertThatError("invalid uuid",
                      UuidRules.uuid(UuidRules.clockSequence(isPositive())),
                      "'invalid uuid' UUID must be valid");
      assertThatError(uuidV4(),
                      UuidRules.uuid(UuidRules.clockSequence(isPositive())),
                      Pattern.compile("'[^']+' UUID version must be 1 \\(time-based\\)"));
   }

   @Test
   public void testUuidNodeWhenNominal() {
      assertThatSuccess(uuidV1(),
                        UuidRules.uuid(UuidRules.node(isPositive())),
                        Pattern.compile("'[^']+' must be positive"));
   }

   @Test
   public void testUuidNodeWhenBadParameters() {
      assertThatError(null, UuidRules.uuid(UuidRules.node(isPositive())), "'<object>' must not be null");
      assertThatError(" ", UuidRules.uuid(UuidRules.node(isPositive())), "'<object>' must not be blank");
      assertThatError("", UuidRules.uuid(UuidRules.node(isPositive())), "'<object>' must not be blank");
      assertThatError("invalid uuid",
                      UuidRules.uuid(UuidRules.node(isPositive())),
                      "'invalid uuid' UUID must be valid");
      assertThatError(uuidV4(),
                      UuidRules.uuid(UuidRules.node(isPositive())),
                      Pattern.compile("'[^']+' UUID version must be 1 \\(time-based\\)"));
   }

   private String uuidV1() {
      return Uuid.newUuidV1().stringValue();
   }

   private String uuidV3(String name) {
      return Uuid.newUuidV3(name).stringValue();
   }

   private String uuidV4() {
      return Uuid.newUuidV4().stringValue();
   }

   private String uuidV5(String name) {
      return Uuid.newUuidV5(name).stringValue();
   }

}