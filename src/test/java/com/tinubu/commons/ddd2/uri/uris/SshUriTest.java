/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.FORCE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_PORT;
import static com.tinubu.commons.ddd2.uri.uris.SshUri.SshUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.uris.SshUri.SshUriRestrictions.ofDefault;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.DefaultUri;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;

public class SshUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void sshUriWhenNominal() {
      assertThat(SshUri.ofUri("ssh://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
   }

   @Test
   public void sshUriWhenDefaultUri() {
      assertThat(SshUri.ofUri(DefaultUri.ofUri("ssh://user@host/path"))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
   }

   @Test
   public void sshUriWhenArbitraryUri() {
      assertThat(SshUri.ofUri(ComponentUri
                                    .ofUri("ssh://user@host/path")
                                    .component()
                                    .userInfo(ui -> new UserInfo() {
                                       @Override
                                       public Optional<? extends UserInfo> recreate(URI uri) {
                                          return optional(this);
                                       }

                                       @Override
                                       public String value() {
                                          return "user";
                                       }

                                       @Override
                                       public StringBuilder appendEncodedValue(StringBuilder builder) {
                                          builder.append("user@");
                                          return builder;
                                       }
                                    }))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
   }

   @Test
   public void sshUriWhenBadArguments() {
      assertThatThrownBy(() -> SshUri.ofUri((String) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> SshUri.ofUri((URI) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> SshUri.ofUri((Uri) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Test
   public void sshUriWhenDefaultPortAndNoRestrictions() {
      assertThat(SshUri.ofUri("ssh://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SshUri.ofUri("ssh://user@host:22/path")).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host:22/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(22);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SshUri.ofUri("ssh://user@host:42/path")).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void sshUriWhenDefaultPortAndRemovePortRestriction() {
      assertThat(SshUri.ofUri("ssh://user@host/path",
                              ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SshUri.ofUri("ssh://user@host:22/path", ofDefault().portRestriction(REMOVE_PORT))).satisfies(
            uri -> {
               assertThat(uri).hasValue("ssh://user@host/path");
               assertThat(uri).hasUserInfo(false, "user");
               assertThat(uri).hasHost("host");
               assertThat(uri).hasNoPort().components().hasNoPort();
               assertThat(uri).hasPath(false, "/path");
               assertThat(uri).hasNoQuery();
               Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
            });
      assertThat(SshUri.ofUri("ssh://user@host:42/path", ofDefault().portRestriction(REMOVE_PORT))).satisfies(
            uri -> {
               assertThat(uri).hasValue("ssh://user@host/path");
               assertThat(uri).hasUserInfo(false, "user");
               assertThat(uri).hasHost("host");
               assertThat(uri).hasNoPort().components().hasNoPort();
               assertThat(uri).hasPath(false, "/path");
               assertThat(uri).hasNoQuery();
               Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
            });
   }

   @Test
   public void sshUriWhenDefaultPortAndRemoveMappedPortRestriction() {
      assertThat(SshUri.ofUri("ssh://user@host/path",
                              ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SshUri.ofUri("ssh://user@host:22/path",
                              ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SshUri.ofUri("ssh://user@host:42/path",
                              ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void sshUriWhenDefaultPortAndForceMappedPortRestriction() {
      assertThat(SshUri.ofUri("ssh://user@host/path",
                              ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host:22/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(22).components().hasPort(SimplePort.of(22));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SshUri.ofUri("ssh://user@host:22/path",
                              ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host:22/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(22).components().hasPort(SimplePort.of(22));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SshUri.ofUri("ssh://user@host:42/path",
                              ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("ssh://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42).components().hasPort(SimplePort.of(42));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void sshUriWhenPath() {
      assertThat(SshUri.ofUri("ssh://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void sshUriWhenDoubleSlashedPath() {
      assertThat(SshUri.ofUri("ssh://user@host//path")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "//path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void sshUriWhenQuery() {
      assertThatThrownBy(() -> SshUri.ofUri("ssh://user@host/path?query"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'ssh://user@host/path?query' URI: 'SshUri[value=ssh://user@host/path?query,restrictions=SshUriRestrictions[emptyPath=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='ssh'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path?query', encodedSchemeSpecific='//user@host/path?query'],authority=SimpleServerAuthority[userInfo='UsernamePasswordUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path, encodedPath=/path],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
      assertThat(SshUri.ofUri("ssh://user@host/path?query", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasQuery(false, "query");
      });
   }

   @Test
   public void sshUriWhenFragment() {
      assertThatThrownBy(() -> SshUri.ofUri("ssh://user@host/path#fragment"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'ssh://user@host/path#fragment' URI: 'SshUri[value=ssh://user@host/path#fragment,restrictions=SshUriRestrictions[emptyPath=false, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='ssh'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path', encodedSchemeSpecific='//user@host/path'],authority=SimpleServerAuthority[userInfo='UsernamePasswordUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      assertThat(SshUri.ofUri("ssh://user@host/path#fragment", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasFragment(false, "fragment");
      });
   }

   @Test
   public void sshUriWhenRootPath() {
      assertThat(SshUri.ofUri("ssh://user@host/")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void sshUriWhenEmptyPath() {
      assertThat(SshUri.ofUri("ssh://user@host")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "");
         assertThat(uri).hasNoQuery();
      });
      assertThatThrownBy(() -> SshUri.ofUri("ssh://user@host", ofDefault().emptyPath(true)))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'ssh://user@host' URI: 'path' must not be empty");
   }

   @Test
   public void sshUriWhenNoUserInfo() {
      assertThat(SshUri.ofUri("ssh://host/path")).satisfies(uri -> {
         assertThat(uri).hasNoUserInfo();
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void sshUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> SshUri.ofUri("other://host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'other://host/path' URI:",
                                     "scheme must be equal to 'ssh'");
      assertThatThrownBy(() -> SshUri.ofUri("ssh:/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'ssh:/path' URI:", "must be hierarchical server-based");
      assertThatThrownBy(() -> SshUri.ofUri("ssh:///path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'ssh:///path' URI:",
                                     "must not be empty hierarchical server-based");
      assertThatThrownBy(() -> SshUri.ofUri("//host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible '//host/path' URI:", "must be absolute");
   }

   @Test
   public void sshUriWhenInvalidUri() {
      assertThatThrownBy(() -> SshUri.ofUri("ssh:"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'ssh:' URI: Expected scheme-specific part at index 4: ssh:");
      assertThatThrownBy(() -> SshUri.ofUri("ssh://"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'ssh://' URI: Expected authority at index 6: ssh://");
      assertThatThrownBy(() -> SshUri.ofUri("//"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '//' URI: Expected authority at index 2: //");
   }

   @Test
   public void normalizeWhenNominal() {
      assertThat(SshUri.ofUri("ssh://user@host/path/./").normalize()).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void normalizeWhenDoubleSlashedPath() {
      assertThat(SshUri.ofUri("ssh://user@host//path/./").normalize()).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path/");
         assertThat(uri).hasNoQuery();
      });
   }

}