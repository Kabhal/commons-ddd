/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.FORCE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_PORT;
import static com.tinubu.commons.ddd2.uri.uris.HttpUri.DEFAULT_HTTPS_PORT;
import static com.tinubu.commons.ddd2.uri.uris.HttpUri.DEFAULT_HTTP_PORT;
import static com.tinubu.commons.ddd2.uri.uris.HttpUri.HttpUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.uris.HttpUri.HttpUriRestrictions.ofDefault;
import static com.tinubu.commons.ddd2.uri.uris.HttpUri.HttpUriRestrictions.ofRestrictions;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.type;

import java.net.URI;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.DefaultUri;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;

public class HttpUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void ofUriWhenNominal() {
      assertThat(HttpUri.ofUri("http://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
   }

   @Test
   public void ofUriWhenDefaultUri() {
      assertThat(HttpUri.ofUri(DefaultUri.ofUri("http://user@host/path"))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
   }

   @Test
   public void ofUriWhenArbitraryUri() {
      assertThat(HttpUri.ofUri(ComponentUri
                                     .ofUri("http://user@host/path")
                                     .component()
                                     .userInfo(ui -> new UserInfo() {
                                        public Optional<? extends UserInfo> recreate(URI uri) {
                                           return optional(this);
                                        }

                                        public String value() {
                                           return "user";
                                        }

                                        public StringBuilder appendEncodedValue(StringBuilder builder) {
                                           builder.append("user@");
                                           return builder;
                                        }
                                     }))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri(ComponentUri.ofUri("http://host/path"))).satisfies(uri -> {
         assertThat(uri).hasValue("http://host/path");
         assertThat(uri).hasNoUserInfo();
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
   }

   @Test
   public void ofUriWhenRestricted() {
      assertThatThrownBy(() -> HttpUri.ofUri("http://user@host/path",
                                             ofRestrictions(true, false, false, false)))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage("Incompatible 'http://user@host/path' URI: 'scheme=http' must be in [https]");
      assertThatThrownBy(() -> HttpUri.ofUri("https://user@host/path",
                                             ofRestrictions(false, true, false, false)))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage("Incompatible 'https://user@host/path' URI: 'scheme=https' must be in [http]");
   }

   @Test
   public void ofUriWhenBadArguments() {
      assertThatThrownBy(() -> HttpUri.ofUri((String) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> HttpUri.ofUri((URI) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> HttpUri.ofUri((Uri) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Test
   public void ofUriWhenDefaultPortAndNoRestrictions() {
      assertThat(HttpUri.ofUri("http://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("http://user@host:80/path")).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host:80/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_HTTP_PORT);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("http://user@host:42/path")).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });

      assertThat(HttpUri.ofUri("https://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host:443/path")).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host:443/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_HTTPS_PORT);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host:42/path")).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndRemovePortRestriction() {
      assertThat(HttpUri.ofUri("http://user@host/path", ofDefault().portRestriction(REMOVE_PORT))).satisfies(
            uri -> {
               assertThat(uri).hasValue("http://user@host/path");
               assertThat(uri).hasUserInfo(false, "user");
               assertThat(uri).hasHost("host");
               assertThat(uri).hasNoPort().components().hasNoPort();
               assertThat(uri).hasPath(false, "/path");
               assertThat(uri).hasNoQuery();
               Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
            });
      assertThat(HttpUri.ofUri("http://user@host:80/path",
                               ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("http://user@host:42/path",
                               ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });

      assertThat(HttpUri.ofUri("https://user@host/path", ofDefault().portRestriction(REMOVE_PORT))).satisfies(
            uri -> {
               assertThat(uri).hasValue("https://user@host/path");
               assertThat(uri).hasUserInfo(false, "user");
               assertThat(uri).hasHost("host");
               assertThat(uri).hasNoPort().components().hasNoPort();
               assertThat(uri).hasPath(false, "/path");
               assertThat(uri).hasNoQuery();
               Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
            });
      assertThat(HttpUri.ofUri("https://user@host:443/path",
                               ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host:42/path",
                               ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndRemoveMappedPortRestriction() {
      assertThat(HttpUri.ofUri("http://user@host/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("http://user@host:80/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("http://user@host:42/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
      assertThat(HttpUri.ofUri("https://user@host/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host:443/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host:42/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndForceMappedPortRestriction() {
      assertThat(HttpUri.ofUri("http://user@host/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host:80/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_HTTP_PORT).components().hasPort(SimplePort.of(DEFAULT_HTTP_PORT));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("http://user@host:80/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host:80/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_HTTP_PORT).components().hasPort(SimplePort.of(DEFAULT_HTTP_PORT));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTP_PORT);
      });
      assertThat(HttpUri.ofUri("http://user@host:42/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("http://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42).components().hasPort(SimplePort.of(42));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
      assertThat(HttpUri.ofUri("https://user@host/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host:443/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_HTTPS_PORT).components().hasPort(SimplePort.of(DEFAULT_HTTPS_PORT));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host:443/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host:443/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(DEFAULT_HTTPS_PORT).components().hasPort(SimplePort.of(DEFAULT_HTTPS_PORT));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(DEFAULT_HTTPS_PORT);
      });
      assertThat(HttpUri.ofUri("https://user@host:42/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("https://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42).components().hasPort(SimplePort.of(42));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenPath() {
      assertThat(HttpUri.ofUri("http://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenQuery() {
      assertThatThrownBy(() -> HttpUri.ofUri("http://user@host/path?query", ofRestrictions(true, false)))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'http://user@host/path?query' URI: 'HttpUri[value=http://user@host/path?query,restrictions=HttpUriRestrictions[http=false, https=false, portRestriction=NONE, query=true, fragment=false],scheme=SimpleScheme[scheme='http'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path?query', encodedSchemeSpecific='//user@host/path?query'],authority=SimpleServerAuthority[userInfo='UsernamePasswordUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path, encodedPath=/path],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
      assertThat(HttpUri.ofUri("http://user@host/path?query", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasQuery(false, "query");
      });
   }

   @Test
   public void ofUriWhenFragment() {
      assertThatThrownBy(() -> HttpUri.ofUri("http://user@host/path#fragment", ofRestrictions(false, true)))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'http://user@host/path#fragment' URI: 'HttpUri[value=http://user@host/path#fragment,restrictions=HttpUriRestrictions[http=false, https=false, portRestriction=NONE, query=false, fragment=true],scheme=SimpleScheme[scheme='http'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path', encodedSchemeSpecific='//user@host/path'],authority=SimpleServerAuthority[userInfo='UsernamePasswordUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      assertThat(HttpUri.ofUri("http://user@host/path#fragment", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasFragment(false, "fragment");
      });
   }

   @Test
   public void ofUriWhenRootPath() {
      assertThat(HttpUri.ofUri("http://user@host/")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenEmptyPath() {
      assertThat(HttpUri.ofUri("http://user@host")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenNoUserInfo() {
      assertThat(HttpUri.ofUri("http://host/path")).satisfies(uri -> {
         assertThat(uri).hasNoUserInfo();
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> HttpUri.ofUri("other://host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage("Incompatible 'other://host/path' URI: 'scheme=other' must be in [http,https]");
      assertThatThrownBy(() -> HttpUri.ofUri("http:/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'http:/path' URI:", "must be hierarchical server-based");
      assertThatThrownBy(() -> HttpUri.ofUri("http:///path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'http:///path' URI:",
                                     "must not be empty hierarchical server-based");
      assertThatThrownBy(() -> HttpUri.ofUri("//host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible '//host/path' URI:", "must be absolute");
   }

   @Test
   public void ofUriWhenInvalidUri() {
      assertThatThrownBy(() -> HttpUri.ofUri("http:"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'http:' URI: Expected scheme-specific part at index 5: http:");
      assertThatThrownBy(() -> HttpUri.ofUri("http://"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'http://' URI: Expected authority at index 7: http://");
      assertThatThrownBy(() -> HttpUri.ofUri("//"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '//' URI: Expected authority at index 2: //");
   }

   @Test
   public void normalizeWhenNominal() {
      assertThat(HttpUri.ofUri("http://user@host/path/./").normalize()).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void defaultPortWhenNominal() {
      assertThat(HttpUri.ofUri("http://user@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isTrue();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(80);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(80));
      });
      assertThat(HttpUri.ofUri("http://user@host:80/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isTrue();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(80);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(80));
      });
      assertThat(HttpUri.ofUri("https://user@host:443/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isTrue();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(443);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(443));
      });
      assertThat(HttpUri.ofUri("http://user@host:42/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isFalse();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(42));
      });
      assertThat(HttpUri.ofUri("https://user@host:42/path")).satisfies(uri -> {
         Assertions.assertThat(uri.defaultPort()).isFalse();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
         Assertions.assertThat(uri.component().portOrDefault()).isEqualTo(SimplePort.of(42));
      });
   }

   @Test
   public void usernamePasswordWhenNominal() {
      assertThat(HttpUri.ofUri("http://user:pass@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("user");
         Assertions.assertThat(uri.password()).hasValue("pass");
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("user");
               Assertions.assertThat(upui.password(false)).hasValue("pass");
            });
         });
      });
   }

   @Test
   public void usernamePasswordWhenNoUserInfo() {
      assertThat(HttpUri.ofUri("http://host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).isEmpty();
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasNoUserInfo();
      });
   }

   @Test
   public void usernamePasswordWhenPartial() {
      assertThat(HttpUri.ofUri("http://user@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("user");
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("user");
               Assertions.assertThat(upui.password(false)).isEmpty();
            });
         });
      });
      assertThat(HttpUri.ofUri("http://@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("");
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("");
               Assertions.assertThat(upui.password(false)).isEmpty();
            });
         });
      });
      assertThat(HttpUri.ofUri("http://@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.username()).hasValue("");
         Assertions.assertThat(uri.password()).isEmpty();
         assertThat(uri).components().hasUserInfoSatisfying(ui -> {
            Assertions.assertThat(ui).asInstanceOf(type(UsernamePasswordUserInfo.class)).satisfies(upui -> {
               Assertions.assertThat(upui.username(false)).isEqualTo("");
               Assertions.assertThat(upui.password(false)).isEmpty();
            });
         });
      });
   }

}