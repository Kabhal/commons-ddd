/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.FORCE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_MAPPED_PORT;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.PortRestriction.REMOVE_PORT;
import static com.tinubu.commons.ddd2.uri.uris.SftpUri.SftpUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.uris.SftpUri.SftpUriRestrictions.ofDefault;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.ComponentUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.UserInfo;
import com.tinubu.commons.ddd2.uri.DefaultUri;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.uris.SftpUri.SftpPath;

public class SftpUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void ofUriWhenNominal() {
      assertThat(SftpUri.ofUri("sftp://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
   }

   @Test
   public void ofUriWhenDefaultUri() {
      assertThat(SftpUri.ofUri(DefaultUri.ofUri("sftp://user@host/path"))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
   }

   @Test
   public void ofUriWhenArbitraryUri() {
      assertThat(SftpUri.ofUri(ComponentUri
                                     .ofUri("sftp://user@host/path")
                                     .component()
                                     .userInfo(ui -> new UserInfo() {
                                        @Override
                                        public Optional<? extends UserInfo> recreate(URI uri) {
                                           return optional(this);
                                        }

                                        @Override
                                        public String value() {
                                           return "user";
                                        }

                                        @Override
                                        public StringBuilder appendEncodedValue(StringBuilder builder) {
                                           builder.append("user@");
                                           return builder;
                                        }
                                     }))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri(DefaultUri.ofUri("sftp://host/path"))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://host/path");
         assertThat(uri).hasNoUserInfo();
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
   }

   @Test
   public void ofUriWhenBadArguments() {
      assertThatThrownBy(() -> SftpUri.ofUri((String) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> SftpUri.ofUri((URI) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> SftpUri.ofUri((Uri) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Test
   public void ofUriWhenDefaultPortAndNoRestrictions() {
      assertThat(SftpUri.ofUri("sftp://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri("sftp://user@host:22/path")).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host:22/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(22);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri("sftp://user@host:42/path")).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndRemovePortRestriction() {
      assertThat(SftpUri.ofUri("sftp://user@host/path", ofDefault().portRestriction(REMOVE_PORT))).satisfies(
            uri -> {
               assertThat(uri).hasValue("sftp://user@host/path");
               assertThat(uri).hasUserInfo(false, "user");
               assertThat(uri).hasHost("host");
               assertThat(uri).hasNoPort().components().hasNoPort();
               assertThat(uri).hasPath(false, "/path");
               assertThat(uri).hasNoQuery();
               Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
            });
      assertThat(SftpUri.ofUri("sftp://user@host:22/path",
                               ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri("sftp://user@host:42/path",
                               ofDefault().portRestriction(REMOVE_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndRemoveMappedPortRestriction() {
      assertThat(SftpUri.ofUri("sftp://user@host/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri("sftp://user@host:22/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasNoPort().components().hasNoPort();
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri("sftp://user@host:42/path",
                               ofDefault().portRestriction(REMOVE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42);
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenDefaultPortAndForceMappedPortRestriction() {
      assertThat(SftpUri.ofUri("sftp://user@host/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host:22/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(22).components().hasPort(SimplePort.of(22));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri("sftp://user@host:22/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host:22/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(22).components().hasPort(SimplePort.of(22));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(22);
      });
      assertThat(SftpUri.ofUri("sftp://user@host:42/path",
                               ofDefault().portRestriction(FORCE_MAPPED_PORT))).satisfies(uri -> {
         assertThat(uri).hasValue("sftp://user@host:42/path");
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPort(42).components().hasPort(SimplePort.of(42));
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.portOrDefault()).isEqualTo(42);
      });
   }

   @Test
   public void ofUriWhenRelativePath() {
      assertThat(SftpUri.ofUri("sftp://user@host/path")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenAbsolutePath() {
      assertThat(SftpUri.ofUri("sftp://user@host//path")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "//path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenQuery() {
      assertThatThrownBy(() -> SftpUri.ofUri("sftp://user@host/path?query"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'sftp://user@host/path?query' URI: 'SftpUri[value=sftp://user@host/path?query,restrictions=SftpUriRestrictions[emptyPath=true, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='sftp'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path?query', encodedSchemeSpecific='//user@host/path?query'],authority=SimpleServerAuthority[userInfo='UsernamePasswordUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SftpPath[path=/path, encodedPath=/path],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
      assertThat(SftpUri.ofUri("sftp://user@host/path?query", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasQuery(false, "query");
      });
   }

   @Test
   public void ofUriWhenFragment() {
      assertThatThrownBy(() -> SftpUri.ofUri("sftp://user@host/path#fragment"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'sftp://user@host/path#fragment' URI: 'SftpUri[value=sftp://user@host/path#fragment,restrictions=SftpUriRestrictions[emptyPath=true, portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='sftp'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='//user@host/path', encodedSchemeSpecific='//user@host/path'],authority=SimpleServerAuthority[userInfo='UsernamePasswordUserInfo[userInfo='user', encodedUserInfo='user']', host='SimpleHost[host='host']', port='null'],path=SftpPath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
      assertThat(SftpUri.ofUri("sftp://user@host/path#fragment", noRestrictions())).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasFragment(false, "fragment");
      });
   }

   @Test
   public void ofUriWhenRootPath() {
      assertThat(SftpUri.ofUri("sftp://user@host/")).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenEmptyPath() {
      assertThatThrownBy(() -> SftpUri.ofUri("sftp://user@host"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'sftp://user@host' URI: 'path' must not be empty");
      assertThat(SftpUri.ofUri("sftp://user@host", ofDefault().emptyPath(false))).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenNoUserInfo() {
      assertThat(SftpUri.ofUri("sftp://host/path")).satisfies(uri -> {
         assertThat(uri).hasNoUserInfo();
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void ofUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> SftpUri.ofUri("other://host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'other://host/path' URI:",
                                     "scheme must be equal to 'sftp'");
      assertThatThrownBy(() -> SftpUri.ofUri("sftp:/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'sftp:/path' URI:", "must be hierarchical server-based");
      assertThatThrownBy(() -> SftpUri.ofUri("sftp:///path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'sftp:///path' URI:",
                                     "must not be empty hierarchical server-based");
      assertThatThrownBy(() -> SftpUri.ofUri("//host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible '//host/path' URI:", "must be absolute");
      assertThatThrownBy(() -> SftpUri.ofUri("path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage(
                  "Incompatible 'path' URI: Invariant validation error > 'path=path' must be absolute or empty");
   }

   @Test
   public void ofUriWhenInvalidUri() {
      assertThatThrownBy(() -> SftpUri.ofUri("sftp:"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'sftp:' URI: Expected scheme-specific part at index 5: sftp:");
      assertThatThrownBy(() -> SftpUri.ofUri("sftp://"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'sftp://' URI: Expected authority at index 7: sftp://");
      assertThatThrownBy(() -> SftpUri.ofUri("//"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '//' URI: Expected authority at index 2: //");
   }

   @Test
   public void sftpPathWhenAbsolutePath() {
      assertThat(SftpUri.ofUri("sftp://user@host//path")).satisfies(uri -> {
         Assertions.assertThat(uri.sftpPath(false)).isEqualTo("/path");
         Assertions.assertThat(uri.sftpPathValue()).isEqualTo(Path.of("/path"));
      });
   }

   @Test
   public void sftpPathWhenRelativePath() {
      assertThat(SftpUri.ofUri("sftp://user@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.sftpPath(false)).isEqualTo("path");
         Assertions.assertThat(uri.sftpPathValue()).isEqualTo(Path.of("path"));
      });
   }

   @Test
   public void updateSftpPathWhenAbsolutePath() {
      assertThat(SftpUri.ofUri("sftp://user@host//path")).satisfies(uri -> {
         Assertions.assertThat(uri.sftpPath(false, sftpPath -> {
            Assertions.assertThat(sftpPath).isEqualTo("/path");
            return sftpPath;
         }).sftpPath(false)).isEqualTo("/path");
      });
   }

   @Test
   public void updateSftpPathWhenRelativePath() {
      assertThat(SftpUri.ofUri("sftp://user@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.sftpPath(false, sftpPath -> {
            Assertions.assertThat(sftpPath).isEqualTo("path");
            return sftpPath;
         }).sftpPath(false)).isEqualTo("path");
      });
   }

   @Test
   public void normalizeWhenRelativeSftpPath() {
      assertThat(SftpUri.ofUri("sftp://user@host/path/./").normalize()).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "/path/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Test
   public void normalizeWhenAbsoluteSftpPath() {
      assertThat(SftpUri.ofUri("sftp://user@host//path/./").normalize()).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "//path/");
         assertThat(uri).hasNoQuery();
      });

      assertThat(SftpUri.ofUri("sftp://user@host///path/./").normalize()).satisfies(uri -> {
         assertThat(uri).hasUserInfo(false, "user");
         assertThat(uri).hasHost("host");
         assertThat(uri).hasPath(false, "//path/");
         assertThat(uri).hasNoQuery();
      });
   }

   @Nested
   class SftpPathTest {

      @Test
      public void sftpPathWhenInvalidPath() {
         Assertions
               .assertThatThrownBy(() -> SftpPath.of("path"))
               .isExactlyInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
      }

      @Test
      public void sftpPathWhenAbsoluteSftpPath() {
         stream(SftpPath.of("//path"),
                SftpPath.ofEncoded("//path"),
                SftpPath.ofSftpPath("/path"),
                SftpPath.ofEncodedSftpPath("/path"),
                SftpPath.ofSftpPath(Path.of("/path"))).forEach(sftpPath -> {
            Assertions.assertThat(sftpPath).satisfies(path -> {
               Assertions.assertThat(path.value()).isEqualTo("//path");
               Assertions.assertThat(path.encodedValue()).isEqualTo("//path");
               Assertions.assertThat(path.pathValue()).isEqualTo(Path.of("//path"));

               Assertions.assertThat(path.sftpValue()).isEqualTo("/path");
               Assertions.assertThat(path.encodedSftpValue()).isEqualTo("/path");
               Assertions.assertThat(path.sftpPathValue()).isEqualTo(Path.of("/path"));
            });
         });
      }

      @Test
      public void sftpPathWhenAbsoluteRootSftpPath() {
         stream(SftpPath.of("//"),
                SftpPath.ofEncoded("//"),
                SftpPath.ofSftpPath("/"),
                SftpPath.ofEncodedSftpPath("/"),
                SftpPath.ofSftpPath(Path.of("/"))).forEach(sftpPath -> {
            Assertions.assertThat(sftpPath).satisfies(path -> {
               Assertions.assertThat(path.value()).isEqualTo("//");
               Assertions.assertThat(path.encodedValue()).isEqualTo("//");
               Assertions.assertThat(path.pathValue()).isEqualTo(Path.of("//"));

               Assertions.assertThat(path.sftpValue()).isEqualTo("/");
               Assertions.assertThat(path.encodedSftpValue()).isEqualTo("/");
               Assertions.assertThat(path.sftpPathValue()).isEqualTo(Path.of("/"));
            });
         });
      }

      @Test
      public void sftpPathWhenRelativeSftpPath() {
         stream(SftpPath.of("/path"),
                SftpPath.ofEncoded("/path"),
                SftpPath.of(Path.of("/path")),
                SftpPath.ofSftpPath("path"),
                SftpPath.ofEncodedSftpPath("path"),
                SftpPath.ofSftpPath(Path.of("path"))).forEach(sftpPath -> {
            Assertions.assertThat(sftpPath).satisfies(path -> {
               Assertions.assertThat(path.value()).isEqualTo("/path");
               Assertions.assertThat(path.encodedValue()).isEqualTo("/path");
               Assertions.assertThat(path.pathValue()).isEqualTo(Path.of("/path"));

               Assertions.assertThat(path.sftpValue()).isEqualTo("path");
               Assertions.assertThat(path.encodedSftpValue()).isEqualTo("path");
               Assertions.assertThat(path.sftpPathValue()).isEqualTo(Path.of("path"));
            });
         });
      }

      @Test
      public void sftpPathWhenRelativeRootSftpPath() {
         stream(SftpPath.of("/"), SftpPath.ofEncoded("/"), SftpPath.of(Path.of("/"))).forEach(sftpPath -> {
            Assertions.assertThat(sftpPath).satisfies(path -> {
               Assertions.assertThat(path.value()).isEqualTo("/");
               Assertions.assertThat(path.encodedValue()).isEqualTo("/");
               Assertions.assertThat(path.pathValue()).isEqualTo(Path.of("/"));

               Assertions.assertThat(path.sftpValue()).isEqualTo("");
               Assertions.assertThat(path.encodedSftpValue()).isEqualTo("");
               Assertions.assertThat(path.sftpPathValue()).isEqualTo(Path.of(""));
            });
         });

         stream(SftpPath.ofSftpPath(""),
                SftpPath.ofEncodedSftpPath(""),
                SftpPath.ofSftpPath(Path.of(""))).forEach(sftpPath -> {
            Assertions.assertThat(sftpPath).satisfies(path -> {
               Assertions.assertThat(path.value()).isEqualTo("");
               Assertions.assertThat(path.encodedValue()).isEqualTo("");
               Assertions.assertThat(path.pathValue()).isEqualTo(Path.of(""));

               Assertions.assertThat(path.sftpValue()).isEqualTo("");
               Assertions.assertThat(path.encodedSftpValue()).isEqualTo("");
               Assertions.assertThat(path.sftpPathValue()).isEqualTo(Path.of(""));
            });
         });
      }

      @Test
      public void sftpPathWhenEmptyPath() {
         stream(SftpPath.of(""),
                SftpPath.ofEncoded(""),
                SftpPath.of(Path.of("")),
                SftpPath.ofSftpPath(""),
                SftpPath.ofEncodedSftpPath(""),
                SftpPath.ofSftpPath(Path.of(""))).forEach(sftpPath -> {
            Assertions.assertThat(sftpPath).satisfies(path -> {
               Assertions.assertThat(path.value()).isEqualTo("");
               Assertions.assertThat(path.encodedValue()).isEqualTo("");
               Assertions.assertThat(path.pathValue()).isEqualTo(Path.of(""));

               Assertions.assertThat(path.sftpValue()).isEqualTo("");
               Assertions.assertThat(path.encodedSftpValue()).isEqualTo("");
               Assertions.assertThat(path.sftpPathValue()).isEqualTo(Path.of(""));
            });
         });
      }

      @Test
      public void updateSftpPathWhenAbsolute() {
         Assertions.assertThat(SftpPath.of("/other").value("//path").value()).isEqualTo("//path");
         Assertions.assertThat(SftpPath.of("/other").encodedValue("//path").value()).isEqualTo("//path");
         Assertions.assertThat(SftpPath.of("/other").sftpValue("/path").value()).isEqualTo("//path");
         Assertions.assertThat(SftpPath.of("/other").encodedSftpValue("/path").value()).isEqualTo("//path");
      }

      @Test
      public void updateSftpPathWhenRelative() {
         Assertions.assertThat(SftpPath.of("/other").value("/path").value()).isEqualTo("/path");
         Assertions.assertThat(SftpPath.of("/other").encodedValue("/path").value()).isEqualTo("/path");
         Assertions.assertThat(SftpPath.of("/other").sftpValue("path").value()).isEqualTo("/path");
         Assertions.assertThat(SftpPath.of("/other").encodedSftpValue("path").value()).isEqualTo("/path");
      }

      @Test
      public void updateSftpPathWhenEmpty() {
         Assertions.assertThat(SftpPath.of("/other").value("").value()).isEqualTo("");
         Assertions.assertThat(SftpPath.of("/other").encodedValue("").value()).isEqualTo("");
         Assertions.assertThat(SftpPath.of("/other").sftpValue("").value()).isEqualTo("");
         Assertions.assertThat(SftpPath.of("/other").encodedSftpValue("").value()).isEqualTo("");
      }

      @Test
      public void normalizeWhenRelativeSftpPath() {
         Assertions.assertThat(SftpPath.of("/path/./").normalize().value()).isEqualTo("/path/");
      }

      @Test
      public void normalizeWhenAbsoluteSftpPath() {
         Assertions.assertThat(SftpPath.of("//path/./").normalize().value()).isEqualTo("//path/");
         Assertions.assertThat(SftpPath.of("///path/./").normalize().value()).isEqualTo("//path/");
      }

   }
}