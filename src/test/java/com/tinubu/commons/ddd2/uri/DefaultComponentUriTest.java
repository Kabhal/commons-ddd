/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.assertThat;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.fragment;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.host;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.path;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.port;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.query;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.registryAuthority;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.scheme;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.schemeSpecific;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.serverAuthority;
import static com.tinubu.commons.ddd2.test.uri.ComponentUriAssert.userInfo;
import static com.tinubu.commons.ddd2.uri.DefaultComponentUri.hierarchical;
import static com.tinubu.commons.ddd2.uri.DefaultComponentUri.ofUri;
import static com.tinubu.commons.ddd2.uri.DefaultComponentUri.ofUrn;
import static com.tinubu.commons.ddd2.uri.DefaultComponentUri.opaque;
import static com.tinubu.commons.ddd2.uri.DefaultComponentUri.relative;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encode;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.type;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.BaseSimpleIdTest;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.test.uri.ComponentTestUri;
import com.tinubu.commons.ddd2.test.uri.TestUri;
import com.tinubu.commons.ddd2.uri.ComponentUri.Fragment;
import com.tinubu.commons.ddd2.uri.ComponentUri.Query;
import com.tinubu.commons.ddd2.uri.parts.KeyValueQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleFragment;
import com.tinubu.commons.ddd2.uri.parts.SimpleHost;
import com.tinubu.commons.ddd2.uri.parts.SimplePath;
import com.tinubu.commons.ddd2.uri.parts.SimplePort;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleRegistryAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleScheme;
import com.tinubu.commons.ddd2.uri.parts.SimpleServerAuthority;
import com.tinubu.commons.ddd2.uri.parts.SimpleUserInfo;
import com.tinubu.commons.ddd2.uri.parts.UsernamePasswordUserInfo;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.StreamUtils;

public class DefaultComponentUriTest extends BaseSimpleIdTest<DefaultComponentUri, String> {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Override
   protected boolean supportsUninitializedValue() {
      return false;
   }

   protected DefaultComponentUri newInstanceForNewObject(String value) {
      return ofUri(value).asNewObject();
   }

   @Override
   protected DefaultComponentUri newInstanceForNewObject(URI urn) {
      return ofUrn(urn).asNewObject();
   }

   protected DefaultComponentUri newInstanceForNewObject() {
      throw new UnsupportedOperationException();
   }

   protected DefaultComponentUri newInstance(String value) {
      return ofUri(value);
   }

   @Override
   protected DefaultComponentUri newInstance(URI urn) {
      return ofUrn(urn);
   }

   @Override
   protected String neutralValue() {
      return "scheme:/";
   }

   @Override
   protected String urnIdType() {
      return "uri";
   }

   @Nested
   class OfUri {

      @Nested
      class Hierarchical {

         @TestUri("scheme:/path?query#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("/path?query"),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @TestUri("scheme:/")
         public void ofUriWhenRootPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:/")
                  .components()
                  .hasParts(scheme("scheme"), schemeSpecific("/"), path("/"))
                  .hasNoMoreParts();
         }

         @Test
         public void hierarchicalWhenInvalidUri() {
            assertThatThrownBy(() -> hierarchical(scheme("&"), path("/path"), (Fragment) null))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '&:/path' URI: Illegal character in scheme name at index 0: &:/path");
         }

         @Test
         public void hierarchicalWhenNotAbsolute() {
            assertThatThrownBy(() -> hierarchical(null, path("/path"), query("query"), fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void hierarchicalWhenEmptyScheme() {
            assertThatThrownBy(() -> hierarchical(scheme(""),
                                                  path("/path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void hierarchicalWhenNoPath() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  null,
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void hierarchicalWhenNotAbsolutePath() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  path("path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > 'path=SimplePath[path=path, encodedPath=path]' must be absolute");
         }

      }

      @Nested
      class HierarchicalServer {

         @ComponentTestUri("scheme://user:pass@host:80/path?query#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user:pass@host:80/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//user:pass@host:80/path?query"),
                            serverAuthority(userInfo("user:pass"), host("host"), port(80)),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://user:pass@host/path?query#fragment")
         public void ofUriWhenNoPort(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user:pass@host/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//user:pass@host/path?query"),
                            serverAuthority(userInfo("user:pass"), host("host")),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://user:@host/path?query#fragment")
         public void ofUriWhenUserInfoEmptyPassword(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user:@host/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//user:@host/path?query"),
                            serverAuthority(userInfo("user:"), host("host")),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://user@host/path?query#fragment")
         public void ofUriWhenUserInfoNoPassword(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user@host/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//user@host/path?query"),
                            serverAuthority(userInfo("user"), host("host")),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://:@host/path?query#fragment")
         public void ofUriWhenUserInfoEmptyUsernamePassword(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://:@host/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//:@host/path?query"),
                            serverAuthority(userInfo(":"), host("host")),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://@host/path?query#fragment")
         public void ofUriWhenUserInfoEmptyUsername(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://@host/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//@host/path?query"),
                            serverAuthority(userInfo(""), host("host")),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://host/path?query#fragment")
         public void ofUriWhenUserInfoNoUsername(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/path?query"),
                            serverAuthority(host("host")),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://host/path?query#")
         public void ofUriWhenEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?query#")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/path?query"),
                            serverAuthority(host("host")),
                            path("/path"),
                            query("query"),
                            fragment(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://host/path?query")
         public void ofUriWhenNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?query")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/path?query"),
                            serverAuthority(host("host")),
                            path("/path"),
                            query("query"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://host/path?#fragment")
         public void ofUriWhenEmptyQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/path?"),
                            serverAuthority(host("host")),
                            path("/path"),
                            query(""),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://host/path#fragment")
         public void ofUriWhenNoQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/path"),
                            serverAuthority(host("host")),
                            path("/path"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://host/path")
         public void ofUriWhenNoQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/path"),
                            serverAuthority(host("host")),
                            path("/path"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri({ "scheme://host/", "scheme://host/?query#fragment" })
         public void ofUriWhenRootPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/"),
                            serverAuthority(host("host")),
                            path("/"))
                  .hasNoMoreParts();

            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host/?query"),
                            serverAuthority(host("host")),
                            path("/"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri({ "scheme://host", "scheme://host?query#fragment" })
         public void ofUriWhenEmptyPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host"),
                            serverAuthority(host("host")),
                            path(""))
                  .hasNoMoreParts();

            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//host?query"),
                            serverAuthority(host("host")),
                            path(""),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri({ "scheme:///", "scheme:///path", "scheme://?query" })
         public void ofUriWhenNoHost(Uri rootPath, Uri path, Uri emptyPath, String description) {
            assertThat(rootPath)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:///")
                  .components()
                  .hasSchemeSpecific(schemeSpecific("///"))
                  .hasParts(scheme("scheme"), serverAuthority(), path("/"))
                  .hasNoMoreParts();
            assertThat(path)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:///path")
                  .components()
                  .hasSchemeSpecific(schemeSpecific("///path"))
                  .hasParts(scheme("scheme"), serverAuthority(), path("/path"))
                  .hasNoMoreParts();
            assertThat(emptyPath)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme://?query")
                  .components()
                  .hasSchemeSpecific(schemeSpecific("//?query"))
                  .hasParts(scheme("scheme"), serverAuthority(), path(""), query("query"))
                  .hasNoMoreParts();
         }

         @Test
         public void hierarchicalServerWhenInvalidUri() {
            assertThatThrownBy(() -> hierarchical(scheme("&"), serverAuthority(host("host")), path("/path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '&://host/path' URI: Illegal character in scheme name at index 0: &://host/path");
         }

         @Test
         public void hierarchicalServerWhenNotAbsolute() {
            assertThatThrownBy(() -> hierarchical(null,
                                                  serverAuthority(userInfo("user:pass"),
                                                                  host("host"),
                                                                  port(80)),
                                                  path("/path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void hierarchicalServerWhenEmptyScheme() {
            assertThatThrownBy(() -> hierarchical(scheme(""),
                                                  serverAuthority(userInfo("user:pass"),
                                                                  host("host"),
                                                                  port(80)),
                                                  path("/path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void hierarchicalServerWhenNoHost() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  serverAuthority(userInfo("user:pass"), null, port(80)),
                                                  path("/path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'host' must not be null");
         }

         @Test
         public void hierarchicalServerWhenEmptyHost() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  serverAuthority(userInfo("user:pass"), host(""), port(80)),
                                                  path("/path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'host' must not be blank");
         }

         @Test
         public void hierarchicalUriWhenNoPath() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  serverAuthority("user:pass@host:80"),
                                                  null,
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void hierarchicalServerWhenNotAbsoluteOrEmptyPath() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  serverAuthority("user:pass@host:80"),
                                                  path("path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > 'path=SimplePath[path=path, encodedPath=path]' must be absolute or empty");
         }

      }

      @Nested
      class HierarchicalRegistry {

         @ComponentTestUri("scheme://r:egistry/path?query#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry/path?query"),
                            registryAuthority("r:egistry"),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://r:egistry/path?query#")
         public void ofUriWhenEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?query#")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry/path?query"),
                            registryAuthority("r:egistry"),
                            path("/path"),
                            query("query"),
                            fragment(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://r:egistry/path?query")
         public void ofUriWhenNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?query")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry/path?query"),
                            registryAuthority("r:egistry"),
                            path("/path"),
                            query("query"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://r:egistry/path?#fragment")
         public void ofUriWhenEmptyQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry/path?"),
                            registryAuthority("r:egistry"),
                            path("/path"),
                            query(""),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme://r:egistry/path#fragment")
         public void ofUriWhenNoQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry/path"),
                            registryAuthority("r:egistry"),
                            path("/path"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri({ "scheme://r:egistry/", "scheme://r:egistry/?query#fragment" })
         public void ofUriWhenRootPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry/"),
                            registryAuthority("r:egistry"),
                            path("/"))
                  .hasNoMoreParts();
            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry/?query"),
                            registryAuthority("r:egistry"),
                            path("/"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri({ "scheme://r:egistry", "scheme://r:egistry?query#fragment" })
         public void ofUriWhenEmptyPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry"),
                            registryAuthority("r:egistry"),
                            path(""))
                  .hasNoMoreParts();
            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry?query#fragment")
                  .components()
                  .hasParts(scheme("scheme"),
                            schemeSpecific("//r:egistry?query"),
                            registryAuthority("r:egistry"),
                            path(""),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @Test
         public void hierarchicalRegistryWhenInvalidUri() {
            assertThatThrownBy(() -> hierarchical(scheme("&"), registryAuthority("r:egistry"), path("/path")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '&://r:egistry/path' URI: Illegal character in scheme name at index 0: &://r:egistry/path");
         }

         @Test
         public void hierarchicalRegistryWhenNotAbsolute() {
            assertThatThrownBy(() -> hierarchical(null,
                                                  registryAuthority("r:egistry"),
                                                  path("/path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void hierarchicalRegistryWhenEmptyScheme() {
            assertThatThrownBy(() -> hierarchical(scheme(""),
                                                  registryAuthority("r:egistry"),
                                                  path("/path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void hierarchicalRegistryWhenNoPath() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  registryAuthority("r:egistry"),
                                                  null,
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void hierarchicalRegistryWhenNotAbsoluteOrEmptyPath() {
            assertThatThrownBy(() -> hierarchical(scheme("scheme"),
                                                  registryAuthority("r:egistry"),
                                                  path("path"),
                                                  query("query"),
                                                  fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > 'path=SimplePath[path=path, encodedPath=path]' must be absolute or empty");
         }

      }

      @Nested
      class Opaque {

         @ComponentTestUri("scheme:opaque#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isOpaque()
                  .hasValue("scheme:opaque#fragment")
                  .components()
                  .hasParts(scheme("scheme"), schemeSpecific("opaque"), fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("scheme:opaque")
         public void ofUriWhenNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isOpaque()
                  .hasValue("scheme:opaque")
                  .components()
                  .hasParts(scheme("scheme"), schemeSpecific("opaque"))
                  .hasNoMoreParts();
         }

         @Test
         public void opaqueWhenInvalidUri() {
            assertThatThrownBy(() -> opaque(scheme("&"), schemeSpecific("opaque"), fragment("fragment")))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '&:opaque#fragment' URI: Illegal character in scheme name at index 0: &:opaque#fragment");
         }

         @Test
         public void opaqueWhenNotAbsolute() {
            assertThatThrownBy(() -> opaque(null, schemeSpecific("opaque"), fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void opaqueWhenEmptyScheme() {
            assertThatThrownBy(() -> opaque(scheme(""), schemeSpecific("opaque"), fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void opaqueWhenNoSchemeSpecific() {
            assertThatThrownBy(() -> opaque(scheme("scheme"), null, fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'schemeSpecific' must not be null");
         }

         @Test
         public void opaqueWhenEmptySchemeSpecific() {
            assertThatThrownBy(() -> opaque(scheme("scheme"), schemeSpecific(""), fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'schemeSpecific' must not be blank");
         }

         @Test
         public void opaqueWhenAbsoluteOpaque() {
            assertThatThrownBy(() -> opaque(scheme("scheme"),
                                            schemeSpecific("/opaque"),
                                            fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'schemeSpecific=/opaque' must not start with '/'");
         }

      }

      @Nested
      class Relative {

         @ComponentTestUri("//user:pass@host:80/path?query#fragment")
         public void ofUriWhenServerAuthorityNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//user:pass@host:80/path?query#fragment")
                  .components()
                  .hasParts(serverAuthority(userInfo("user:pass"), host("host"), port(80)),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("//user:pass@host/path?query#")
         public void ofUriWhenServerAuthorityEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//user:pass@host/path?query#")
                  .components()
                  .hasParts(serverAuthority(userInfo("user:pass"), host("host")),
                            path("/path"),
                            query("query"),
                            fragment(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("//user:pass@host/path?#fragment")
         public void ofUriWhenServerAuthorityEmptyQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//user:pass@host/path?#fragment")
                  .components()
                  .hasParts(serverAuthority(userInfo("user:pass"), host("host")),
                            path("/path"),
                            query(""),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("//host/")
         public void ofUriWhenServerAuthorityRootPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//host/")
                  .components()
                  .hasParts(serverAuthority(host("host")), path("/"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("//host")
         public void ofUriWhenServerAuthorityEmptyPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//host")
                  .components()
                  .hasParts(serverAuthority(host("host")), path(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri({ "///", "///path", "//?query" })
         public void ofUriWhenNoHost(Uri rootPath, Uri path, Uri emptyPath, String description) {
            assertThat(rootPath)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("///")
                  .components()
                  .hasParts(serverAuthority(), path("/"))
                  .hasNoMoreParts();
            assertThat(path)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("///path")
                  .components()
                  .hasParts(serverAuthority(), path("/path"))
                  .hasNoMoreParts();
            assertThat(emptyPath)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("//?query")
                  .components()
                  .hasParts(serverAuthority(), path(""), query("query"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("//r:egistry/path?query#fragment")
         public void ofUriWhenRegistryAuthorityNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalRegistry()
                  .hasValue("//r:egistry/path?query#fragment")
                  .components()
                  .hasParts(registryAuthority("r:egistry"),
                            path("/path"),
                            query("query"),
                            fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("/path?query#fragment")
         public void ofUriWhenPathNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/path?query#fragment")
                  .components()
                  .hasParts(path("/path"), query("query"), fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("/path?#")
         public void ofUriWhenPathEmptyQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/path?#")
                  .components()
                  .hasParts(path("/path"), query(""), fragment(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("/path")
         public void ofUriWhenPathNoQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/path")
                  .components()
                  .hasParts(path("/path"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("path?query#fragment")
         public void ofUriWhenRelativePathNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path?query#fragment")
                  .components()
                  .hasParts(path("path"), query("query"), fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("path?#")
         public void ofUriWhenRelativePathEmptyQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path?#")
                  .components()
                  .hasParts(path("path"), query(""), fragment(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("path#fragment")
         public void ofUriWhenRelativePathNoQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path#fragment")
                  .components()
                  .hasParts(path("path"), fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("path?query")
         public void ofUriWhenRelativePathNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path?query")
                  .components()
                  .hasParts(path("path"), query("query"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("path")
         public void ofUriWhenRelativePathNoQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path")
                  .components()
                  .hasParts(path("path"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("/")
         public void ofUriWhenRootPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/")
                  .components()
                  .hasParts(path("/"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("")
         public void ofUriWhenEmptyPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("")
                  .components()
                  .hasParts(path(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("?query#fragment")
         public void ofUriWhenQueryNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?query#fragment")
                  .components()
                  .hasParts(path(""), query("query"), fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("?query#")
         public void ofUriWhenQueryEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?query#")
                  .components()
                  .hasParts(path(""), query("query"), fragment(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("?query")
         public void ofUriWhenQueryNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?query")
                  .components()
                  .hasParts(path(""), query("query"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("?")
         public void ofUriWhenQueryEmpty(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?")
                  .components()
                  .hasParts(path(""), query(""))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("#fragment")
         public void ofUriWhenFragmentNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("#fragment")
                  .components()
                  .hasParts(path(""), fragment("fragment"))
                  .hasNoMoreParts();
         }

         @ComponentTestUri("#")
         public void ofUriWhenFragmentEmpty(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("#")
                  .components()
                  .hasParts(path(""), fragment(""))
                  .hasNoMoreParts();
         }

         @Test
         public void relativeWhenNoParts() {
            assertThatThrownBy(() -> relative(null, null, null, null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > At least one part must be set | 'path' must not be null");
         }

         @Test
         public void relativeWhenBlankAuthority() {
            assertThat(relative(serverAuthority(""), path("/"), null, null))
                  .isHierarchicalServer()
                  .components()
                  .hasAuthority(serverAuthority());
         }

         @Test
         public void relativeWhenNoPath() {
            assertThatThrownBy(() -> relative(serverAuthority(host("host")), null, null, null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
            assertThatThrownBy(() -> relative(null, null, query("query"), null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
            assertThatThrownBy(() -> relative(null, null, null, fragment("fragment")))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void relativeWhenAuthorityAndNotAbsoluteOrEmptyPath() {
            assertThatThrownBy(() -> relative(serverAuthority(host("host")), path("path"), null, null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > 'path=SimplePath[path=path, encodedPath=path]' must be absolute or empty");
         }

      }

      @Nested
      class WhenRestrictions {

         @Test
         public void ofUriWhenQueryRestriction() {
            assertThatThrownBy(() -> DefaultComponentUri.ofUri("scheme:/path?query",
                                                               DefaultUriRestrictions.ofRestrictions(true,
                                                                                                     true)))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'scheme:/path?query' URI: 'DefaultComponentUri[value=scheme:/path?query,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='scheme'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path?query', encodedSchemeSpecific='/path?query'],authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         }

         @Test
         public void ofUriWhenFragmentRestriction() {
            assertThatThrownBy(() -> DefaultComponentUri.ofUri("scheme:/path#fragment",
                                                               DefaultUriRestrictions.ofRestrictions(true,
                                                                                                     true)))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'scheme:/path#fragment' URI: 'DefaultComponentUri[value=scheme:/path#fragment,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true],scheme=SimpleScheme[scheme='scheme'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path', encodedSchemeSpecific='/path'],authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         }

      }

   }

   @Nested
   class Update {

      @Test
      public void updateScheme() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .scheme(__ -> scheme("s")))
               .isHierarchicalServer()
               .hasValue("s://user:pass@host:80/path?query#fragment")
               .hasScheme("s");
         assertThat(ofUri("scheme://r:egistry/path?query#fragment").component().scheme(__ -> scheme("s")))
               .isHierarchicalRegistry()
               .hasValue("s://r:egistry/path?query#fragment")
               .hasScheme("s");
         assertThat(ofUri("scheme:opaque#fragment").component().scheme(__ -> scheme("s")))
               .isOpaque()
               .hasValue("s:opaque#fragment")
               .hasScheme("s");
      }

      @Test
      public void updateSchemeWhenNotAbsolute() {
         assertThatThrownBy(() -> ofUri("/path").component().scheme(__ -> scheme("s")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be absolute");
      }

      @Test
      public void updateSchemeWhenBlank() {
         assertThatThrownBy(() -> ofUri("scheme:/path").component().scheme(null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'schemeMapper' must not be null");
         assertThatThrownBy(() -> ofUri("scheme:/path").component().scheme(__ -> scheme("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'scheme' must not be blank");
      }

      @Test
      public void updateSchemeType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .scheme(SimpleScheme::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasScheme(SimpleScheme.of("scheme"));
      }

      @Test
      public void updateServerAuthority() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .serverAuthority(__ -> serverAuthority("u:p@h:81")))
               .isHierarchicalServer()
               .hasValue("scheme://u:p@h:81/path?query#fragment")
               .components()
               .hasAuthority(serverAuthority("u:p@h:81"))
               .hasSchemeSpecific(schemeSpecific("//u:p@h:81/path?query"));
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .serverAuthority(__ -> serverAuthority("h")))
               .isHierarchicalServer()
               .hasValue("scheme://h/path?query#fragment")
               .hasServerAuthority(false, "h");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment")
                          .component()
                          .serverAuthority(__ -> serverAuthority("u:p@h:81")))
               .isHierarchicalServer()
               .hasValue("//u:p@h:81/path?query#fragment")
               .hasServerAuthority(false, "u:p@h:81");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment")
                          .component()
                          .serverAuthority(__ -> serverAuthority("h")))
               .isHierarchicalServer()
               .hasValue("//h/path?query#fragment")
               .hasServerAuthority(false, "h");
      }

      @Test
      public void updateServerAuthorityType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .serverAuthority(SimpleServerAuthority::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasAuthority(SimpleServerAuthority.of("user:pass@host:80"));
      }

      @Test
      public void updateServerAuthorityWhenMapperReturnNull() {
         assertThatThrownBy(() -> ofUri("scheme://user:pass@host/path")
               .component()
               .serverAuthority(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'authority' must not be null");
         assertThatThrownBy(() -> ofUri("/path").component().serverAuthority(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updateServerAuthorityWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> ofUri("/path")
               .component()
               .serverAuthority(__ -> serverAuthority(host("host"))))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updateRegistryAuthority() {
         assertThat(ofUri("scheme://r:egistry/path?query#fragment")
                          .component()
                          .registryAuthority(__ -> registryAuthority("r:e")))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:e/path?query#fragment")
               .hasRegistryAuthority(false, "r:e")
               .hasSchemeSpecific(false, "//r:e/path?query");
         assertThat(ofUri("//r:egistry/path?query#fragment")
                          .component()
                          .registryAuthority(__ -> registryAuthority("r:e")))
               .isHierarchicalRegistry()
               .hasValue("//r:e/path?query#fragment")
               .hasRegistryAuthority(false, "r:e");
      }

      @Test
      public void updateRegistryAuthorityWhenMapperReturnNull() {
         assertThatThrownBy(() -> ofUri("scheme://r:egistry/path").component().registryAuthority(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'authority' must not be null");
         assertThatThrownBy(() -> ofUri("/path").component().registryAuthority(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical registry-based");
      }

      @Test
      public void updateRegistryAuthorityWhenNotHierarchicalRegistry() {
         assertThatThrownBy(() -> ofUri("/path")
               .component()
               .registryAuthority(__ -> registryAuthority("r:egistry")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical registry-based");
      }

      @Test
      public void updateRegistryAuthorityType() {
         assertThat(ofUri("scheme://r:egistry/path?query#fragment")
                          .component()
                          .registryAuthority(SimpleRegistryAuthority::of))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path?query#fragment")
               .components()
               .hasAuthority(SimpleRegistryAuthority.of("r:egistry"));
      }

      @Test
      public void updateUserInfo() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .userInfo(__ -> userInfo("u:p")))
               .isHierarchicalServer()
               .hasValue("scheme://u:p@host:80/path?query#fragment")
               .hasUserInfo(false, "u:p")
               .hasServerAuthority(false, "u:p@host:80")
               .hasSchemeSpecific(false, "//u:p@host:80/path?query");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment")
                          .component()
                          .userInfo(__ -> userInfo("u:p")))
               .isHierarchicalServer()
               .hasValue("//u:p@host:80/path?query#fragment")
               .hasUserInfo(false, "u:p");
      }

      @Test
      public void updateUserInfoWhenMapperReturnNull() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().userInfo(__ -> null))
               .isHierarchicalServer()
               .hasValue("scheme://host:80/path?query#fragment")
               .hasNoUserInfo();
         assertThatThrownBy(() -> ofUri("/path").component().userInfo(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updateUserInfoWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> ofUri("/path").component().userInfo(__ -> userInfo("u:p")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updateUserInfoType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .userInfo(UsernamePasswordUserInfo::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasUserInfo(UsernamePasswordUserInfo.of("user", "pass"));
      }

      @Test
      public void updateHost() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().host(__ -> host("h")))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@h:80/path?query#fragment")
               .hasHost("h")
               .hasServerAuthority(false, "user:pass@h:80")
               .hasSchemeSpecific(false, "//user:pass@h:80/path?query");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment").component().host(__ -> host("h")))
               .isHierarchicalServer()
               .hasValue("//user:pass@h:80/path?query#fragment")
               .hasHost("h");
      }

      @Test
      public void updateHostWhenMapperReturnBlank() {
         assertThatThrownBy(() -> ofUri("scheme://user:pass@host:80/path?query#fragment")
               .component()
               .host(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'host' must not be null");
         assertThatThrownBy(() -> ofUri("scheme://user:pass@host:80/path?query#fragment")
               .component()
               .host(__ -> host("")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'host' must not be blank");
         assertThatThrownBy(() -> ofUri("/path").component().host(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updateHostWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> ofUri("/path").component().host(__ -> host("h")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updateHostType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().host(SimpleHost::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasHost(SimpleHost.of("host"));
      }

      @Test
      public void updatePort() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().port(__ -> port(81)))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:81/path?query#fragment")
               .hasPort(81)
               .hasServerAuthority(false, "user:pass@host:81")
               .hasSchemeSpecific(false, "//user:pass@host:81/path?query");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment").component().port(__ -> port(81)))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:81/path?query#fragment")
               .hasPort(81);
      }

      @Test
      public void updatePortWhenMapperReturnNull() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().port(__ -> null))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host/path?query#fragment")
               .hasNoPort();
         assertThat(ofUri("//user:pass@host:80/path?query#fragment").component().port(__ -> null))
               .isHierarchicalServer()
               .hasValue("//user:pass@host/path?query#fragment")
               .hasNoPort();
         assertThatThrownBy(() -> ofUri("/path").component().port(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updatePortWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> ofUri("/path").component().port(__ -> port(81)))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=<null>]' must be hierarchical server-based");
      }

      @Test
      public void updatePortType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().port(SimplePort::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasPort(SimplePort.of(80));
      }

      @Test
      public void updatePath() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().path(__ -> path("/")))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/?query#fragment")
               .hasPath(false, "/")
               .hasSchemeSpecific(false, "//user:pass@host:80/?query");
         assertThat(ofUri("scheme://r:egistry/path?query#fragment").component().path(__ -> path("/")))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/?query#fragment")
               .hasPath(false, "/");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment").component().path(__ -> path("/")))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/?query#fragment")
               .hasPath(false, "/");
         assertThat(ofUri("//r:egistry/path?query#fragment").component().path(__ -> path("/")))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/?query#fragment")
               .hasPath(false, "/");
         assertThat(ofUri("/path?query#fragment").component().path(__ -> path("/")))
               .isHierarchical()
               .hasValue("/?query#fragment")
               .hasPath(false, "/");
         assertThat(ofUri("/path?query#fragment").component().path(__ -> path("path")))
               .isHierarchical()
               .hasValue("path?query#fragment")
               .hasPath(false, "path");
         assertThat(ofUri("/path?query#fragment").component().path(__ -> path("")))
               .isHierarchical()
               .hasValue("?query#fragment")
               .hasPath(false, "");
      }

      @Test
      public void updatePathWhenMapperReturnNull() {
         assertThatThrownBy(() -> ofUri("scheme://user:pass@host:80/path?query#fragment")
               .component()
               .path(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path' must not be null");
         assertThatThrownBy(() -> ofUri("scheme:/path?query#fragment").component().path(__ -> null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path' must not be null");
      }

      @Test
      public void updatePathWhenNotAbsolute() {
         assertThatThrownBy(() -> ofUri("scheme://host/path").path(false, "path"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
         assertThatThrownBy(() -> ofUri("//host/path").path(false, "path"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
         assertThatThrownBy(() -> ofUri("scheme:/path").path(false, "path"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path=path' must be absolute");
      }

      @Test
      public void updatePathWhenNotHierarchical() {
         assertThatThrownBy(() -> ofUri("scheme:opaque").component().path(__ -> path("p")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=scheme:opaque,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=SimpleScheme[scheme='scheme'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='opaque', encodedSchemeSpecific='opaque'],authority=<null>,path=<null>,query=<null>,fragment=<null>]' must be hierarchical");
      }

      @Test
      public void updatePathType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().path(SimplePath::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasPath(SimplePath.of("/path"));
      }

      @Test
      public void updateQuery() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .query(__ -> query("q")))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?q#fragment")
               .hasQuery(false, "q")
               .hasSchemeSpecific(false, "//user:pass@host:80/path?q");
         assertThat(ofUri("scheme://r:egistry/path?query#fragment").component().query(__ -> query("q")))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment").component().query(__ -> query("q")))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(ofUri("//r:egistry/path?query#fragment").component().query(__ -> query("q")))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(ofUri("/path?query#fragment").component().query(__ -> query("q")))
               .isHierarchical()
               .hasValue("/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(ofUri("/path?query#fragment").component().query(__ -> query("")))
               .isHierarchical()
               .hasValue("/path?#fragment")
               .hasQuery(false, "");
         assertThat(ofUri("?query#fragment").component().query(__ -> query("q")))
               .isHierarchical()
               .hasValue("?q#fragment")
               .hasQuery(false, "q");
         assertThat(ofUri("?query#fragment").component().query(__ -> query("")))
               .isHierarchical()
               .hasValue("?#fragment")
               .hasQuery(false, "");
      }

      @Test
      public void updateQueryWhenMapperReturnNull() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().query(__ -> null))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path#fragment")
               .hasNoQuery();
         assertThat(ofUri("scheme://r:egistry/path?query#fragment").component().query(__ -> null))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path#fragment")
               .hasNoQuery();
         assertThat(ofUri("scheme:/path?query#fragment").component().query(__ -> null))
               .isHierarchical()
               .hasValue("scheme:/path#fragment")
               .hasNoQuery();
         assertThat(ofUri("//user:pass@host:80/path?query#fragment").component().query(__ -> null))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/path#fragment")
               .hasNoQuery();
         assertThat(ofUri("//r:egistry/path?query#fragment").component().query(__ -> null))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/path#fragment")
               .hasNoQuery();
         assertThat(ofUri("/path?query#fragment").component().query(__ -> null))
               .isHierarchical()
               .hasValue("/path#fragment")
               .hasNoQuery();
         assertThat(ofUri("?query#fragment").component().query(__ -> null))
               .isHierarchical()
               .hasValue("#fragment")
               .hasNoQuery();
         assertThat(ofUri("?query").component().query(__ -> null)).isHierarchical().hasValue("").hasNoQuery();
      }

      @Test
      public void updateQueryWhenNotHierarchical() {
         assertThatThrownBy(() -> ofUri("scheme:opaque").component().query(__ -> query("q")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultComponentUri[value=scheme:opaque,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false],scheme=SimpleScheme[scheme='scheme'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='opaque', encodedSchemeSpecific='opaque'],authority=<null>,path=<null>,query=<null>,fragment=<null>]' must be hierarchical");
      }

      @Test
      public void updateQueryType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .query(KeyValueQuery::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasQuery(KeyValueQuery.of("query"));
      }

      @Test
      public void updateQueryWhenEmptyAndEnabledNoQueryIfEmpty() {
         list(SimpleQuery.of("").noQueryIfEmpty(true),
              SimpleQuery.ofEncoded("").noQueryIfEmpty(true),
              SimpleQuery.ofEmpty().noQueryIfEmpty(true)).forEach(query -> {
            var updatedQuery = query.recreate(uri("")).orElseThrow();
            assertThat(testUri(updatedQuery))
                  .hasValue("scheme:/")
                  .hasNoQuery()
                  .components()
                  .hasQuerySatisfying(updatedQueryComponent -> Assertions
                        .assertThat(updatedQueryComponent)
                        .isSameAs(query));
         });
      }

      @Test
      public void updateFragment() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .fragment(__ -> fragment("f")))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#f")
               .hasFragment(false, "f");
         assertThat(ofUri("scheme://r:egistry/path?query#fragment").component().fragment(__ -> fragment("f")))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path?query#f")
               .hasFragment(false, "f");
         assertThat(ofUri("scheme:opaque#fragment").component().fragment(__ -> fragment("f")))
               .isOpaque()
               .hasValue("scheme:opaque#f")
               .hasFragment(false, "f");
         assertThat(ofUri("//user:pass@host:80/path?query#fragment")
                          .component()
                          .fragment(__ -> fragment("f")))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/path?query#f")
               .hasFragment(false, "f");
         assertThat(ofUri("//r:egistry/path?query#fragment").component().fragment(__ -> fragment("f")))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/path?query#f")
               .hasFragment(false, "f");
         assertThat(ofUri("/path?query#fragment").component().fragment(__ -> fragment("f")))
               .isHierarchical()
               .hasValue("/path?query#f")
               .hasFragment(false, "f");
         assertThat(ofUri("/path?query#fragment").component().fragment(__ -> fragment("")))
               .isHierarchical()
               .hasValue("/path?query#")
               .hasFragment(false, "");
         assertThat(ofUri("#fragment").component().fragment(__ -> fragment("f")))
               .isHierarchical()
               .hasValue("#f")
               .hasFragment(false, "f");
         assertThat(ofUri("#fragment").component().fragment(__ -> fragment("")))
               .isHierarchical()
               .hasValue("#")
               .hasFragment(false, "");
      }

      @Test
      public void updateFragmentWhenMapperReturnNull() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment").component().fragment(__ -> null))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query")
               .hasNoFragment();
         assertThat(ofUri("scheme://r:egistry/path?query#fragment").component().fragment(__ -> null))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path?query")
               .hasNoFragment();
         assertThat(ofUri("scheme:/path?query#fragment").component().fragment(__ -> null))
               .isHierarchical()
               .hasValue("scheme:/path?query")
               .hasNoFragment();
         assertThat(ofUri("scheme:opaque#fragment").component().fragment(__ -> null))
               .isOpaque()
               .hasValue("scheme:opaque")
               .hasNoFragment();
         assertThat(ofUri("//user:pass@host:80/path?query#fragment").component().fragment(__ -> null))
               .isHierarchical()
               .hasValue("//user:pass@host:80/path?query")
               .hasNoFragment();
         assertThat(ofUri("//r:egistry/path?query#fragment").component().fragment(__ -> null))
               .isHierarchical()
               .hasValue("//r:egistry/path?query")
               .hasNoFragment();
         assertThat(ofUri("/path?query#fragment").component().fragment(__ -> null))
               .isHierarchical()
               .hasValue("/path?query")
               .hasNoFragment();
         assertThat(ofUri("?query#fragment").component().fragment(__ -> null))
               .isHierarchical()
               .hasValue("?query")
               .hasNoFragment();
         assertThat(ofUri("#fragment").component().fragment(__ -> null))
               .isHierarchical()
               .hasValue("")
               .hasNoFragment();
      }

      @Test
      public void updateFragmentType() {
         assertThat(ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .component()
                          .fragment(SimpleFragment::of))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#fragment")
               .components()
               .hasFragment(SimpleFragment.of("fragment"));
      }

      private DefaultComponentUri testUri(Query query) {
         return hierarchical(SimpleScheme.of("scheme"), SimplePath.rootPath(), query);
      }

   }

   @Nested
   class Encoding {

      /* Common encoding : @ %40 | % %25 | # %23 | : %3A | / %2F | ? %3F | & %26 | = %3D */

      @Test
      public void ofUriWhenEncoded() {
         var uri = ofUri("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment");

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true, "q%23uery")
               .hasQuery(false, "q#uery")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void hierarchicalServerWhenEncoding() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(userInfo("u#ser:p#ass"), host("host")),
                                path("/p#ath"),
                                query("q#uery"),
                                fragment("f#ragment"));

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true, "q%23uery")
               .hasQuery(false, "q#uery")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void hierarchicalServerUpdateWhenEncoding() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(userInfo("u#ser:p#ass"), host("host")),
                                path("/p#ath"),
                                query("q#uery"),
                                fragment("f#ragment"));

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true, "q%23uery")
               .hasQuery(false, "q#uery")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void encodingHostWhenContainArobase() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(host("ho@st")),
                                path("/path"),
                                query("query"),
                                fragment("fragment"));

         assertThat(uri)
               .hasValue("scheme://ho@st/path?query#fragment")
               .isHierarchicalServer()
               .hasUserInfo(false, "ho")
               .hasHost("st")
               .hasQuery(false, "query")
               .hasFragment(false, "fragment");

         var uri2 = hierarchical(scheme("scheme"),
                                 serverAuthority(userInfo("user:pass"), host("ho@st")),
                                 path("/path"),
                                 query("query"),
                                 fragment("fragment"));

         assertThat(uri2)
               .hasValue("scheme://user:pass@ho@st/path?query#fragment")
               .isHierarchicalRegistry()
               .hasNoUserInfo()
               .hasNoHost()
               .hasRegistryAuthority(true, "user:pass@ho@st")
               .hasQuery(true, "query")
               .hasFragment(true, "fragment");
      }

      @Test
      public void encodingUserInfoWhenContainArobase() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(userInfo("u@ser"), host("host")),
                                path("/path"),
                                query("query"),
                                fragment("fragment"));

         assertThat(uri)
               .hasValue("scheme://u%40ser@host/path?query#fragment")
               .isHierarchicalServer()
               .hasUserInfo(true, "u%40ser")
               .hasServerAuthority(true, "u%40ser@host")
               .hasQuery(true, "query")
               .hasFragment(true, "fragment");
      }

      @Test
      public void encodingQueryUri() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(host("host")),
                                path("/path"),
                                query("uri1="
                                      + "http://query1?k1a=v1a&k1b=v1b#f1"
                                      + "&uri2="
                                      + "http://query2?k2a=v2a&k2b=v2b#f2"),
                                fragment("fragment"));

         assertThat(uri)
               .hasValue(
                     "scheme://host/path?uri1=http://query1?k1a=v1a&k1b=v1b%23f1&uri2=http://query2?k2a=v2a&k2b=v2b%23f2#fragment")
               .hasQuery(true,
                         "uri1=http://query1?k1a=v1a&k1b=v1b%23f1&uri2=http://query2?k2a=v2a&k2b=v2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "fragment");
      }

      @Test
      public void preserveEncodingOnUpdate() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(SimpleUserInfo.ofEncoded("u%23ser:p%23ass"), host("host")),
                                SimplePath.ofEncoded("/p%23ath"),
                                SimpleQuery.ofEncoded("uri1="
                                                      + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                                      + "&uri2="
                                                      + encode("http://query2?k2a=v2a&k2b=v2b#f2")),
                                SimpleFragment.ofEncoded("f%23ragment"));

         uri = uri.scheme("other");

         assertThat(uri)
               .hasValue(
                     "other://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#f%23ragment")
               .hasScheme("other")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void preserveEncodingOnNormalize() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(SimpleUserInfo.ofEncoded("u%23ser:p%23ass"), host("host")),
                                SimplePath.ofEncoded("/p%23ath"),
                                SimpleQuery.ofEncoded("uri1="
                                                      + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                                      + "&uri2="
                                                      + encode("http://query2?k2a=v2a&k2b=v2b#f2")),
                                SimpleFragment.ofEncoded("f%23ragment")).normalize();

         assertThat(uri)
               .hasValue(
                     "scheme://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void preserveEncodingOnResolveFull() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(SimpleUserInfo.ofEncoded("u%23ser:p%23ass"), host("host")),
                                SimplePath.ofEncoded("/p%23ath"),
                                SimpleQuery.ofEncoded("uri1="
                                                      + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                                      + "&uri2="
                                                      + encode("http://query2?k2a=v2a&k2b=v2b#f2")),
                                SimpleFragment.ofEncoded("f%23ragment"));

         uri = uri.resolve(uri);

         assertThat(uri)
               .hasValue(
                     "scheme://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void preserveEncodingOnResolvePartial() {
         var uri = hierarchical(scheme("scheme"),
                                serverAuthority(SimpleUserInfo.ofEncoded("u%23ser:p%23ass"), host("host")),
                                SimplePath.ofEncoded("/p%23ath"),
                                SimpleQuery.ofEncoded("uri1="
                                                      + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                                      + "&uri2="
                                                      + encode("http://query2?k2a=v2a&k2b=v2b#f2")),
                                SimpleFragment.ofEncoded("f%23ragment"));

         uri = uri.resolve(ofUri("#fragment"));

         assertThat(uri)
               .hasValue(
                     "scheme://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#fragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "fragment")
               .hasFragment(false, "fragment");
      }

      @Test
      public void ofUriWhenEncodedQueryUri() {
         var uri =
               ofUri("scheme://host?uri1=" + encode("http://query1?k1a=v1a&k1b=v1b#f1") + "&uri2=" + encode(
                     "http://query2?k2a=v2a&k2b=v2b#f2") + "#fragment");

         assertThat(uri)
               .hasValue(
                     "scheme://host?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#fragment")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "fragment");
      }

   }

   @Nested
   class TestUsernamePasswordUserInfo {

      @Test
      public void usernamePasswordInfoWhenNominal() {
         var uri = testUri(UsernamePasswordUserInfo.of("user:pass"));

         assertThat(uri)
               .hasValue("scheme://user:pass@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("user:pass");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("user:pass");
                        Assertions.assertThat(upui.username(false)).isEqualTo("user");
                        Assertions.assertThat(upui.password(false)).hasValue("pass");
                     }));
      }

      @Test
      public void usernamePasswordInfoWhenEmptyPassword() {
         var uri = testUri(UsernamePasswordUserInfo.of("user:"));

         assertThat(uri)
               .hasValue("scheme://user:@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("user:");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("user:");
                        Assertions.assertThat(upui.username(false)).isEqualTo("user");
                        Assertions.assertThat(upui.password(false)).hasValue("");
                     }));
      }

      @Test
      public void usernamePasswordInfoWhenNoPassword() {
         var uri = testUri(UsernamePasswordUserInfo.of("user"));

         assertThat(uri)
               .hasValue("scheme://user@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("user");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("user");
                        Assertions.assertThat(upui.username(false)).isEqualTo("user");
                        Assertions.assertThat(upui.password(false)).isEmpty();
                     }));
      }

      @Test
      public void usernamePasswordInfoWhenNoUsername() {
         var uri = testUri(UsernamePasswordUserInfo.of(""));

         assertThat(uri)
               .hasValue("scheme://@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("");
                        Assertions.assertThat(upui.username(false)).isEqualTo("");
                        Assertions.assertThat(upui.password(false)).isEmpty();
                     }));
      }

      @Test
      public void usernamePasswordInfoWhenEncoding() {
         var uri = testUri(UsernamePasswordUserInfo.of("u#ser:p#ass"));

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("u#ser:p#ass");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("u%23ser:p%23ass");
                        Assertions.assertThat(upui.username(false)).isEqualTo("u#ser");
                        Assertions.assertThat(upui.password(false)).hasValue("p#ass");
                        Assertions.assertThat(upui.username(true)).isEqualTo("u%23ser");
                        Assertions.assertThat(upui.password(true)).hasValue("p%23ass");
                     }));
      }

      @Test
      public void usernamePasswordInfoWhenEncoded() {
         var uri = testUri(UsernamePasswordUserInfo.ofEncoded("u%23ser:p%23ass"));

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("u#ser:p#ass");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("u%23ser:p%23ass");
                        Assertions.assertThat(upui.username(false)).isEqualTo("u#ser");
                        Assertions.assertThat(upui.password(false)).hasValue("p#ass");
                        Assertions.assertThat(upui.username(true)).isEqualTo("u%23ser");
                        Assertions.assertThat(upui.password(true)).hasValue("p%23ass");
                     }));
      }

      @Test
      public void usernamePasswordInfoWhenColonEncoded() {
         var uri = testUri(UsernamePasswordUserInfo.ofEncoded("u%23ser%3Ap%23ass"));

         assertThat(uri)
               .hasValue("scheme://u%23ser%3Ap%23ass@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("u#ser:p#ass");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("u%23ser%3Ap%23ass");
                        Assertions.assertThat(upui.username(false)).isEqualTo("u#ser:p#ass");
                        Assertions.assertThat(upui.password(false)).isEmpty();
                        Assertions.assertThat(upui.username(true)).isEqualTo("u%23ser%3Ap%23ass");
                        Assertions.assertThat(upui.password(true)).isEmpty();
                     }));
      }

      @Test
      public void usernamePasswordInfoWhenSeveralDecodedColon() {
         var uri = testUri(UsernamePasswordUserInfo.of("user:pa:ss"));

         assertThat(uri)
               .hasValue("scheme://user:pa:ss@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("user:pa:ss");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("user:pa:ss");
                        Assertions.assertThat(upui.username(false)).isEqualTo("user");
                        Assertions.assertThat(upui.password(false)).hasValue("pa:ss");
                        Assertions.assertThat(upui.username(true)).isEqualTo("user");
                        Assertions.assertThat(upui.password(true)).hasValue("pa:ss");
                     }));
      }

      @Test
      public void usernamePasswordInfoBySeparatedValuesWhenDecodedColon() {
         var uri = testUri(UsernamePasswordUserInfo.of("us:er", "pa:ss"));

         assertThat(uri)
               .hasValue("scheme://us%3Aer:pa%3Ass@host")
               .components()
               .hasUserInfoSatisfying(ui -> Assertions
                     .assertThat(ui)
                     .asInstanceOf(type(UsernamePasswordUserInfo.class))
                     .satisfies(upui -> {
                        Assertions.assertThat(upui.value()).isEqualTo("us:er:pa:ss");
                        Assertions.assertThat(upui.encodedValue()).isEqualTo("us%3Aer:pa%3Ass");
                        Assertions.assertThat(upui.username(false)).isEqualTo("us:er");
                        Assertions.assertThat(upui.password(false)).hasValue("pa:ss");
                        Assertions.assertThat(upui.username(true)).isEqualTo("us%3Aer");
                        Assertions.assertThat(upui.password(true)).hasValue("pa%3Ass");
                     }));
      }

      private DefaultComponentUri testUri(UsernamePasswordUserInfo userInfo) {
         return hierarchical(SimpleScheme.of("scheme"),
                             SimpleServerAuthority.of(userInfo, SimpleHost.of("host")),
                             SimplePath.emptyPath());
      }

   }

   @Nested
   class TestKeyValueQuery {

      @Test
      public void keyValueQueryWhenNominal() {
         var map = map(LinkedHashMap::new, Pair.of("k1", list("v1")), Pair.of("k2", list("v2")));

         list(testUri(KeyValueQuery.of("k1=v1&k2=v2")),
              testUri(KeyValueQuery.ofEncoded("k1=v1&k2=v2")),
              testUri(KeyValueQuery.of(map)),
              testUri(KeyValueQuery.ofEncoded(map))).forEach(uri -> {

            assertThat(uri)
                  .hasValue("scheme:/?k1=v1&k2=v2")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2");
                           Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k2=v2");
                           Assertions
                                 .assertThat(kvq.query())
                                 .containsExactly(Pair.of("k1", list("v1")), Pair.of("k2", list("v2")));
                           Assertions
                                 .assertThat(kvq.encodedQuery())
                                 .containsExactly(Pair.of("k1", list("v1")), Pair.of("k2", list("v2")));
                           Assertions.assertThat(kvq.query("k1")).containsExactly("v1");
                           Assertions.assertThat(kvq.query("k2")).containsExactly("v2");
                           Assertions.assertThat(kvq.query("other")).isEmpty();
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenEmptyQuery() {
         list(testUri(KeyValueQuery.of("")),
              testUri(KeyValueQuery.ofEncoded("")),
              testUri(KeyValueQuery.of(map())),
              testUri(KeyValueQuery.ofEncoded(map()))).forEach(uri -> {

            assertThat(uri)
                  .hasValue("scheme:/?")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions.assertThat(kvq.value()).isEqualTo("");
                           Assertions.assertThat(kvq.encodedValue()).isEqualTo("");
                           Assertions.assertThat(kvq.query()).isEmpty();
                           Assertions.assertThat(kvq.encodedQuery()).isEmpty();
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenNoParametersAndEnabledNoQueryIfEmpty() {
         list(testUri(KeyValueQuery.ofEmpty().noQueryIfEmpty(true)),
              testUri(KeyValueQuery.of(map()).noQueryIfEmpty(true)),
              testUri(KeyValueQuery.ofEncoded(map()).noQueryIfEmpty(true))).forEach(uri -> {

            assertThat(uri).hasValue("scheme:/").hasNoQuery();
         });
      }

      @Test
      public void keyValueQueryWhenNoParametersAndDisabledNoQueryIfEmpty() {
         list(testUri(KeyValueQuery.ofEmpty().noQueryIfEmpty(false)),
              testUri(KeyValueQuery.of(map()).noQueryIfEmpty(false)),
              testUri(KeyValueQuery.ofEncoded(map()).noQueryIfEmpty(false))).forEach(uri -> {

            assertThat(uri).hasValue("scheme:/?").hasQuery(false, "");
         });
      }

      @Test
      public void keyValueQueryWhenEmptyParameterValuesAndEnabledNoQueryIfEmpty() {
         LinkedHashMap<String, List<String>> map = map(LinkedHashMap::new, Pair.of("k1", list()));

         list(testUri(KeyValueQuery.of(map).noQueryIfEmpty(true)),
              testUri(KeyValueQuery.ofEncoded(map).noQueryIfEmpty(true)),
              testUri(KeyValueQuery.ofEmpty().noQueryIfEmpty(true))).forEach(uri -> {
            assertThat(uri).hasValue("scheme:/").hasNoQuery();
         });
      }

      @Test
      public void updateKeyValueQueryWhenEmptyAndEnabledNoQueryIfEmpty() {
         list(KeyValueQuery.of(map()).noQueryIfEmpty(true),
              KeyValueQuery.ofEncoded(map()).noQueryIfEmpty(true),
              KeyValueQuery.ofEmpty().noQueryIfEmpty(true)).forEach(query -> {
            var updatedQuery = query.recreate(uri("")).orElseThrow();
            assertThat(testUri(updatedQuery))
                  .hasValue("scheme:/")
                  .hasNoQuery()
                  .components()
                  .hasQuerySatisfying(updatedQueryComponent -> Assertions
                        .assertThat(updatedQueryComponent)
                        .isSameAs(query));
         });
      }

      @Test
      public void keyValueQueryWhenEmptyParameterValuesAndDisabledNoQueryIfEmpty() {
         LinkedHashMap<String, List<String>> map = map(LinkedHashMap::new, Pair.of("k1", list()));

         list(testUri(KeyValueQuery.of(map).noQueryIfEmpty(false)),
              testUri(KeyValueQuery.ofEncoded(map).noQueryIfEmpty(false))).forEach(uri -> {
            assertThat(uri).hasValue("scheme:/?").hasQuery(false, "");
         });
      }

      @Test
      public void keyValueQueryWhenEmptyOrUnsetValue() {
         var map = map(LinkedHashMap::new, Pair.of("k1", list("")), Pair.of("k2", list((String) null)));

         list(testUri(KeyValueQuery.of("k1=&k2")),
              testUri(KeyValueQuery.ofEncoded("k1=&k2")),
              testUri(KeyValueQuery.of(map)),
              testUri(KeyValueQuery.ofEncoded(map))).forEach(uri -> {

            assertThat(uri)
                  .hasValue("scheme:/?k1=&k2")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions.assertThat(kvq.value()).isEqualTo("k1=&k2");
                           Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=&k2");
                           Assertions
                                 .assertThat(kvq.query())
                                 .containsExactly(Pair.of("k1", list("")),
                                                  Pair.of("k2", list((String) null)));
                           Assertions
                                 .assertThat(kvq.encodedQuery())
                                 .containsExactly(Pair.of("k1", list("")),
                                                  Pair.of("k2", list((String) null)));
                           Assertions.assertThat(kvq.query("k1")).containsExactly("");
                           Assertions.assertThat(kvq.query("k2")).containsExactly((String) null);
                           Assertions.assertThat(kvq.encodedQuery("k1")).containsExactly("");
                           Assertions.assertThat(kvq.encodedQuery("k2")).containsExactly((String) null);
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenMultipleEmptyOrUnsetValue() {
         var map = map(LinkedHashMap::new,
                       Pair.of("k1", list("", "")),
                       Pair.of("k2", list((String) null, (String) null)));

         list(testUri(KeyValueQuery.of("k1=&k1=&k2&k2")),
              testUri(KeyValueQuery.ofEncoded("k1=&k1=&k2&k2")),
              testUri(KeyValueQuery.of(map)),
              testUri(KeyValueQuery.ofEncoded(map))).forEach(uri -> {

            assertThat(uri)
                  .hasValue("scheme:/?k1=&k1=&k2&k2")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions.assertThat(kvq.value()).isEqualTo("k1=&k1=&k2&k2");
                           Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=&k1=&k2&k2");
                           Assertions
                                 .assertThat(kvq.query())
                                 .containsExactly(Pair.of("k1", list("", "")),
                                                  Pair.of("k2", list((String) null, (String) null)));
                           Assertions
                                 .assertThat(kvq.encodedQuery())
                                 .containsExactly(Pair.of("k1", list("", "")),
                                                  Pair.of("k2", list((String) null, (String) null)));
                           Assertions.assertThat(kvq.query("k1")).containsExactly("", "");
                           Assertions
                                 .assertThat(kvq.query("k2"))
                                 .containsExactly((String) null, (String) null);
                           Assertions.assertThat(kvq.encodedQuery("k1")).containsExactly("", "");
                           Assertions
                                 .assertThat(kvq.encodedQuery("k2"))
                                 .containsExactly((String) null, (String) null);
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenMultiValuedKeyFromQueryString() {
         list(testUri(KeyValueQuery.of("k1=v1&k2=v2&k1=v3&k1&k1=&k1")),
              testUri(KeyValueQuery.ofEncoded("k1=v1&k2=v2&k1=v3&k1&k1=&k1"))).forEach(uri -> {

            assertThat(uri)
                  .hasValue("scheme:/?k1=v1&k2=v2&k1=v3&k1&k1=&k1")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k1=v3&k1&k1=&k1");
                           Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k2=v2&k1=v3&k1&k1=&k1");
                           Assertions
                                 .assertThat(kvq.query())
                                 .containsExactly(Pair.of("k1", list("v1", "v3", null, "", null)),
                                                  Pair.of("k2", list("v2")));
                           Assertions
                                 .assertThat(kvq.encodedQuery())
                                 .containsExactly(Pair.of("k1", list("v1", "v3", null, "", null)),
                                                  Pair.of("k2", list("v2")));
                           Assertions.assertThat(kvq.query("k1")).containsExactly("v1", "v3", null, "", null);
                           Assertions.assertThat(kvq.query("k2")).containsExactly("v2");
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenMultiValuedKeyFromQueryMap() {
         var map = map(LinkedHashMap::new,
                       Pair.of("k1", list("v1", "v3", null, "", null)),
                       Pair.of("k2", list("v2")));

         list(testUri(KeyValueQuery.of(map)), testUri(KeyValueQuery.ofEncoded(map))).forEach(uri -> {

            assertThat(uri)
                  .hasValue("scheme:/?k1=v1&k1=v3&k1&k1=&k1&k2=v2")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k1=v3&k1&k1=&k1&k2=v2");
                           Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k1=v3&k1&k1=&k1&k2=v2");
                           Assertions
                                 .assertThat(kvq.query())
                                 .containsExactly(Pair.of("k1", list("v1", "v3", null, "", null)),
                                                  Pair.of("k2", list("v2")));
                           Assertions
                                 .assertThat(kvq.encodedQuery())
                                 .containsExactly(Pair.of("k1", list("v1", "v3", null, "", null)),
                                                  Pair.of("k2", list("v2")));
                           Assertions.assertThat(kvq.query("k1")).containsExactly("v1", "v3", null, "", null);
                           Assertions.assertThat(kvq.query("k2")).containsExactly("v2");
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenEncodingFromQueryString() {
         list(testUri(KeyValueQuery.of(
               "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2"))).forEach(uri -> {

            assertThat(uri)
                  .hasValue(
                        "scheme:/?uri1=http://query1?k1a=v1a&k1b=v1b%23f1&uri2=http://query2?k2a=v2a&k2b=v2b%23f2")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions
                                 .assertThat(kvq.value())
                                 .isEqualTo(
                                       "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2");
                           Assertions
                                 .assertThat(kvq.encodedValue())
                                 .isEqualTo(
                                       "uri1=http://query1?k1a=v1a&k1b=v1b%23f1&uri2=http://query2?k2a=v2a&k2b=v2b%23f2");
                           Assertions
                                 .assertThat(kvq.query())
                                 .containsExactly(Pair.of("uri1", list("http://query1?k1a=v1a")),
                                                  Pair.of("k1b", list("v1b#f1")),
                                                  Pair.of("uri2", list("http://query2?k2a=v2a")),
                                                  Pair.of("k2b", list("v2b#f2")));
                           Assertions
                                 .assertThat(kvq.encodedQuery())
                                 .containsExactly(Pair.of("uri1", list("http://query1?k1a=v1a")),
                                                  Pair.of("k1b", list("v1b%23f1")),
                                                  Pair.of("uri2", list("http://query2?k2a=v2a")),
                                                  Pair.of("k2b", list("v2b%23f2")));
                           Assertions.assertThat(kvq.query("uri1")).containsExactly("http://query1?k1a=v1a");
                           Assertions.assertThat(kvq.query("k1b")).containsExactly("v1b#f1");
                           Assertions.assertThat(kvq.query("uri2")).containsExactly("http://query2?k2a=v2a");
                           Assertions.assertThat(kvq.query("k2b")).containsExactly("v2b#f2");
                           Assertions
                                 .assertThat(kvq.encodedQuery("uri1"))
                                 .containsExactly("http://query1?k1a=v1a");
                           Assertions.assertThat(kvq.encodedQuery("k1b")).containsExactly("v1b%23f1");
                           Assertions
                                 .assertThat(kvq.encodedQuery("uri2"))
                                 .containsExactly("http://query2?k2a=v2a");
                           Assertions.assertThat(kvq.encodedQuery("k2b")).containsExactly("v2b%23f2");
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenEncodingFromQueryMap() {
         list(testUri(KeyValueQuery.of(map(() -> new LinkedHashMap<>(),
                                           Pair.of("uri1", list("http://query1?k1a=v1a&k1b=v1b#f1")),
                                           Pair.of("uri2",
                                                   list("http://query2?k2a=v2a&k2b=v2b#f2")))))).forEach(uri -> {

            assertThat(uri)
                  .hasValue(
                        "scheme:/?uri1=http://query1?k1a=v1a%26k1b=v1b%23f1&uri2=http://query2?k2a=v2a%26k2b=v2b%23f2")
                  .components()
                  .hasQuerySatisfying(q -> Assertions
                        .assertThat(q)
                        .asInstanceOf(type(KeyValueQuery.class))
                        .satisfies(kvq -> {
                           Assertions
                                 .assertThat(kvq.value())
                                 .isEqualTo(
                                       "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2");
                           Assertions
                                 .assertThat(kvq.encodedValue())
                                 .isEqualTo(
                                       "uri1=http://query1?k1a=v1a%26k1b=v1b%23f1&uri2=http://query2?k2a=v2a%26k2b=v2b%23f2");
                           Assertions
                                 .assertThat(kvq.query())
                                 .containsExactly(Pair.of("uri1", list("http://query1?k1a=v1a&k1b=v1b#f1")),
                                                  Pair.of("uri2", list("http://query2?k2a=v2a&k2b=v2b#f2")));
                           Assertions
                                 .assertThat(kvq.encodedQuery())
                                 .containsExactly(Pair.of("uri1",
                                                          list("http://query1?k1a=v1a%26k1b=v1b%23f1")),
                                                  Pair.of("uri2",
                                                          list("http://query2?k2a=v2a%26k2b=v2b%23f2")));
                           Assertions
                                 .assertThat(kvq.query("uri1"))
                                 .containsExactly("http://query1?k1a=v1a&k1b=v1b#f1");
                           Assertions
                                 .assertThat(kvq.query("uri2"))
                                 .containsExactly("http://query2?k2a=v2a&k2b=v2b#f2");
                           Assertions
                                 .assertThat(kvq.encodedQuery("uri1"))
                                 .containsExactly("http://query1?k1a=v1a%26k1b=v1b%23f1");
                           Assertions
                                 .assertThat(kvq.encodedQuery("uri2"))
                                 .containsExactly("http://query2?k2a=v2a%26k2b=v2b%23f2");
                        }));
         });
      }

      @Test
      public void keyValueQueryWhenEncoded() {
         list(testUri(KeyValueQuery.ofEncoded("uri1="
                                              + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                              + "&uri2="
                                              + encode("http://query2?k2a=v2a&k2b=v2b#f2"))),
              testUri(KeyValueQuery.ofEncoded(map(() -> new LinkedHashMap<>(),
                                                  Pair.of("uri1",
                                                          list(encode("http://query1?k1a=v1a&k1b=v1b#f1"))),
                                                  Pair.of("uri2",
                                                          list(encode("http://query2?k2a=v2a&k2b=v2b#f2"))))))).forEach(
               uri -> {

                  assertThat(uri)
                        .hasValue(
                              "scheme:/?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
                        .components()
                        .hasQuerySatisfying(q -> Assertions
                              .assertThat(q)
                              .asInstanceOf(type(KeyValueQuery.class))
                              .satisfies(kvq -> {
                                 Assertions
                                       .assertThat(kvq.value())
                                       .isEqualTo(
                                             "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2");
                                 Assertions
                                       .assertThat(kvq.encodedValue())
                                       .isEqualTo(
                                             "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2");
                                 Assertions
                                       .assertThat(kvq.query())
                                       .containsExactly(Pair.of("uri1",
                                                                list("http://query1?k1a=v1a&k1b=v1b#f1")),
                                                        Pair.of("uri2",
                                                                list("http://query2?k2a=v2a&k2b=v2b#f2")));
                                 Assertions
                                       .assertThat(kvq.encodedQuery())
                                       .containsExactly(Pair.of("uri1",
                                                                list("http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1")),
                                                        Pair.of("uri2",
                                                                list("http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")));
                                 Assertions
                                       .assertThat(kvq.query("uri1"))
                                       .containsExactly("http://query1?k1a=v1a&k1b=v1b#f1");
                                 Assertions
                                       .assertThat(kvq.query("uri2"))
                                       .containsExactly("http://query2?k2a=v2a&k2b=v2b#f2");
                                 Assertions
                                       .assertThat(kvq.encodedQuery("uri1"))
                                       .containsExactly("http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1");
                                 Assertions
                                       .assertThat(kvq.encodedQuery("uri2"))
                                       .containsExactly("http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2");
                              }));
               });
      }

      @Test
      public void setParameterWhenNominal() {
         assertThat(testUri(KeyValueQuery.of("k1=v1&k2=v2").setParameter(false, "k3", list("v3a", "v3b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k3=v3a&k3=v3b");
                        Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k2=v2&k3=v3a&k3=v3b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3a", "v3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3a", "v3b")));
                     }));
      }

      @Test
      public void setParameterWhenEncoding() {
         assertThat(testUri(KeyValueQuery.of("k1=v1&k2=v2").setParameter(false, "k&3", list("v&3a", "v&3b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k&3=v&3a&k&3=v&3b");
                        Assertions
                              .assertThat(kvq.encodedValue())
                              .isEqualTo("k1=v1&k2=v2&k%263=v%263a&k%263=v%263b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k&3", list("v&3a", "v&3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k%263", list("v%263a", "v%263b")));
                     }));
      }

      @Test
      public void setParameterWhenEncoded() {
         assertThat(testUri(KeyValueQuery
                                  .of("k1=v1&k2=v2")
                                  .setParameter(true, "k%263", list("v%263a", "v%263b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k&3=v&3a&k&3=v&3b");
                        Assertions
                              .assertThat(kvq.encodedValue())
                              .isEqualTo("k1=v1&k2=v2&k%263=v%263a&k%263=v%263b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k&3", list("v&3a", "v&3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k%263", list("v%263a", "v%263b")));
                     }));
      }

      @Test
      public void setParameterWhenOverrideValues() {
         assertThat(testUri(KeyValueQuery
                                  .of("k1=v1&k2=v2&k3=v3")
                                  .setParameter(false, "k3", list("v3a", "v3b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k3=v3a&k3=v3b");
                        Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k2=v2&k3=v3a&k3=v3b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3a", "v3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3a", "v3b")));
                     }));
      }

      @Test
      public void setParameterWhenNullOrEmptyValues() {
         StreamUtils
               .<List<String>>stream(null, list())
               .forEach(values -> assertThat(testUri(KeyValueQuery
                                                           .of("k1=v1&k2=v2&k3=v3")
                                                           .setParameter(false, "k3", values)))
                     .components()
                     .hasQuerySatisfying(q -> Assertions
                           .assertThat(q)
                           .asInstanceOf(type(KeyValueQuery.class))
                           .satisfies(kvq -> {
                              Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2");
                              Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k2=v2");
                              Assertions
                                    .assertThat(kvq.query())
                                    .containsExactly(Pair.of("k1", list("v1")),
                                                     Pair.of("k2", list("v2")),
                                                     Pair.of("k3", list()));
                              Assertions
                                    .assertThat(kvq.encodedQuery())
                                    .containsExactly(Pair.of("k1", list("v1")),
                                                     Pair.of("k2", list("v2")),
                                                     Pair.of("k3", list()));
                           })));

      }

      @Test
      public void addParameterWhenNominal() {
         assertThat(testUri(KeyValueQuery.of("k1=v1&k2=v2").addParameter(false, "k3", list("v3a", "v3b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k3=v3a&k3=v3b");
                        Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k2=v2&k3=v3a&k3=v3b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3a", "v3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3a", "v3b")));
                     }));
      }

      @Test
      public void addParameterWhenEncoding() {
         assertThat(testUri(KeyValueQuery.of("k1=v1&k2=v2").addParameter(false, "k&3", list("v&3a", "v&3b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k&3=v&3a&k&3=v&3b");
                        Assertions
                              .assertThat(kvq.encodedValue())
                              .isEqualTo("k1=v1&k2=v2&k%263=v%263a&k%263=v%263b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k&3", list("v&3a", "v&3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k%263", list("v%263a", "v%263b")));
                     }));
      }

      @Test
      public void addParameterWhenEncoded() {
         assertThat(testUri(KeyValueQuery
                                  .of("k1=v1&k2=v2")
                                  .addParameter(true, "k%263", list("v%263a", "v%263b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k&3=v&3a&k&3=v&3b");
                        Assertions
                              .assertThat(kvq.encodedValue())
                              .isEqualTo("k1=v1&k2=v2&k%263=v%263a&k%263=v%263b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k&3", list("v&3a", "v&3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k%263", list("v%263a", "v%263b")));
                     }));
      }

      @Test
      public void addParameterWhenAppendValues() {
         assertThat(testUri(KeyValueQuery
                                  .of("k1=v1&k2=v2&k3=v3")
                                  .addParameter(false, "k3", list("v3a", "v3b"))))
               .components()
               .hasQuerySatisfying(q -> Assertions
                     .assertThat(q)
                     .asInstanceOf(type(KeyValueQuery.class))
                     .satisfies(kvq -> {
                        Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k3=v3&k3=v3a&k3=v3b");
                        Assertions
                              .assertThat(kvq.encodedValue())
                              .isEqualTo("k1=v1&k2=v2&k3=v3&k3=v3a&k3=v3b");
                        Assertions
                              .assertThat(kvq.query())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3", "v3a", "v3b")));
                        Assertions
                              .assertThat(kvq.encodedQuery())
                              .containsExactly(Pair.of("k1", list("v1")),
                                               Pair.of("k2", list("v2")),
                                               Pair.of("k3", list("v3", "v3a", "v3b")));
                     }));
      }

      @Test
      public void addParameterWhenNullOrEmptyValues() {
         StreamUtils
               .<List<String>>stream(null, list())
               .forEach(values -> assertThat(testUri(KeyValueQuery
                                                           .of("k1=v1&k2=v2&k3=v3")
                                                           .addParameter(false, "k3", values)))
                     .components()
                     .hasQuerySatisfying(q -> Assertions
                           .assertThat(q)
                           .asInstanceOf(type(KeyValueQuery.class))
                           .satisfies(kvq -> {
                              Assertions.assertThat(kvq.value()).isEqualTo("k1=v1&k2=v2&k3=v3");
                              Assertions.assertThat(kvq.encodedValue()).isEqualTo("k1=v1&k2=v2&k3=v3");
                              Assertions
                                    .assertThat(kvq.query())
                                    .containsExactly(Pair.of("k1", list("v1")),
                                                     Pair.of("k2", list("v2")),
                                                     Pair.of("k3", list("v3")));
                              Assertions
                                    .assertThat(kvq.encodedQuery())
                                    .containsExactly(Pair.of("k1", list("v1")),
                                                     Pair.of("k2", list("v2")),
                                                     Pair.of("k3", list("v3")));
                           })));

      }

      private DefaultComponentUri testUri(KeyValueQuery query) {
         return hierarchical(SimpleScheme.of("scheme"), SimplePath.rootPath(), query);
      }

   }

}