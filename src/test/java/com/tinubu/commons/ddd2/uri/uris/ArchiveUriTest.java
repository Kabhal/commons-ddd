/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.test.uri.UriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.uris.ArchiveUri.ArchiveUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.uris.ArchiveUri.ArchiveUriRestrictions.ofDefault;
import static com.tinubu.commons.ddd2.uri.uris.ArchiveUri.ArchiveUriRestrictions.ofRestrictions;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.PathUtils.emptyPath;
import static com.tinubu.commons.lang.util.PathUtils.rootPath;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.parts.KeyValueQuery;
import com.tinubu.commons.ddd2.uri.parts.SimpleQuery;

public class ArchiveUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void ofUriWhenBadArguments() {
      assertThatThrownBy(() -> ArchiveUri.ofUri((String) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> ArchiveUri.ofUri((URI) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> ArchiveUri.ofUri((Uri) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Nested
   class OfUri {

      @Test
      public void ofUriWhenNominal() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!/inzip/document")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/inzip/document");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/inzip/document"));
         });
      }

      @Test
      public void ofUriWhenNotRestrictedArchiveEntry() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
         });
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!/inzip/document")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/inzip/document");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/inzip/document"));
         });
      }

      @Test
      public void ofUriWhenRestrictedArchiveEntry() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip",
                                     ofDefault().archiveEntry(true))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
         });
         assertThatThrownBy(() -> ArchiveUri.ofUri("zip:file:/path/file.zip!/inzip/document",
                                                   ofDefault().archiveEntry(true)))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'zip:file:/path/file.zip!/inzip/document' URI: Restricted archive entry");
      }

      @Test
      public void ofUriWhenDocumentIsNormalized() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!/inzip/../document/./")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/inzip/../document/./");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
         });
      }

      @Test
      public void ofUriWhenDocumentHasTraversal() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!/../document")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/../document");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
         });
      }

      @Test
      public void ofUriWhenDocumentIsNotAbsolute() {
         assertThatThrownBy(() -> ArchiveUri.ofUri("zip:file:/path/file.zip!inzip/document"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'zip:file:/path/file.zip!inzip/document' URI: 'archiveEntry=inzip/document' must be absolute path");
      }

      @Test
      public void ofUriWhenEmptyArchiveEntry() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
         });
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!", ofDefault().archiveEntry(true))).satisfies(
               uri -> {
                  Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
               });
      }

      @Test
      public void ofUriWhenArchiveQuery() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!?query", noRestrictions())).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!?query");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("query"));
         });
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip?fileQuery!?query", noRestrictions())).satisfies(
               uri -> {
                  Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip?fileQuery!?query");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("file:/path/file.zip?fileQuery"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("query"));
               });
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip?query!")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip?query!");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip?query"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.archiveQuery()).isEmpty();
         });
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip?query")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip?query");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip?query"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.archiveQuery()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenRestrictedArchiveQuery() {
         assertThatThrownBy(() -> ArchiveUri.ofUri("zip:file:/path/file.zip!?query"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'zip:file:/path/file.zip!?query' URI: Restricted archive query");
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip?query!")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip?query!");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip?query"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.archiveQuery()).isEmpty();
         });
      }

      @Test
      public void ofUriWhenRootArchiveEntry() {
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!/")).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
         });
         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!/", ofDefault().archiveEntry(true))).satisfies(
               uri -> {
                  Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
               });
      }

      @Test
      public void ofUriWhenFragment() {
         assertThatThrownBy(() -> ArchiveUri.ofUri("zip:file:/path/file.zip#fragment"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessageContainingAll("Invalid 'zip:file:/path/file.zip#fragment' URI",
                                        "must not have a fragment");

         assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip%23fragment!/document")).satisfies(uri -> {
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip#fragment"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
         });
      }

      @Test
      public void ofUriWhenIncompatibleUri() {
         assertThatThrownBy(() -> ArchiveUri.ofUri("other:/path"))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'other:/path' URI:", "must be opaque");
         assertThatThrownBy(() -> ArchiveUri.ofUri("zip://host"))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible 'zip://host' URI:", "must be opaque");
         assertThatThrownBy(() -> ArchiveUri.ofUri("//host/path"))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible '//host/path' URI:", "must be opaque");
         assertThatThrownBy(() -> ArchiveUri.ofUri("//host"))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessageContainingAll("Incompatible '//host' URI:", "must be opaque");
         assertThatThrownBy(() -> ArchiveUri.ofUri("path"))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'path' URI: 'ArchiveUri[value=path,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=true],scheme=<null>,schemeSpecific=<null>]' must be opaque");
         assertThatThrownBy(() -> ArchiveUri.ofUri(""))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible '' URI: 'ArchiveUri[value=,restrictions=ArchiveUriRestrictions[schemes=[zip, jar], archiveEntry=false, archiveQuery=true],scheme=<null>,schemeSpecific=<null>]' must be opaque");
      }

      @Test
      public void ofUriWhenInvalidUri() {
         assertThatThrownBy(() -> ArchiveUri.ofUri("zip:"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'zip:' URI: Expected scheme-specific part at index 4: zip:");
         assertThatThrownBy(() -> ArchiveUri.ofUri("zip://"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid 'zip://' URI: Expected authority at index 6: zip://");
         assertThatThrownBy(() -> ArchiveUri.ofUri("//"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid '//' URI: Expected authority at index 2: //");
      }

      @Test
      public void ofUriWhenSchemeRestrictions() {
         assertThat(ArchiveUri.ofUri("jar:file:/path/file.jar!/document")).satisfies(uri -> {
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("jar");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.jar"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
         });
         assertThatThrownBy(() -> ArchiveUri.ofUri("new:file:/path/file.new!/document"))
               .isInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Incompatible 'new:file:/path/file.new!/document' URI: 'scheme=new' must be in [zip,jar]");
         assertThat(ArchiveUri.ofUri("new:file:/path/file.new!/document",
                                     ofDefault().schemes("new"))).satisfies(uri -> {
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("new");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.new"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
         });
         assertThat(ArchiveUri.ofUri("NEW:file:/path/file.new!/document",
                                     ofDefault().schemes("new"))).satisfies(uri -> {
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("NEW");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.new"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
         });
         assertThat(ArchiveUri.ofUri("new:file:/path/file.new!/document",
                                     ofDefault().schemes("NEW"))).satisfies(uri -> {
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("new");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.new"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
         });
      }

      @Nested
      class WhenNestedArchiveUri {

         @Nested
         class WhenEscapedArchiveEntrySeparator {

            @Test
            public void ofUriWhenNominal() {
               assertThat(ArchiveUri.ofUri(
                     "zip:zip:file:/path/file.zip%21/infile/file2.zip!/infile2/file3.zip",
                     noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip%21/infile/file2.zip!/infile2/file3.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile2/file3.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri(
                     "zip:zip:zip:file:/path/file.zip%21/infile/file2.zip%21/infile2/file3.zip!/infile3/file4.zip",
                     noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:zip:file:/path/file.zip%21/infile/file2.zip%21/infile2/file3.zip!/infile3/file4.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile3/file4.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
            }

            @Test
            public void ofUriWhenArchiveQuery() {
               assertThat(ArchiveUri.ofUri(
                     "zip:zip:file:/path/file.zip?query%21/infile/file2.zip?query2!/infile2/file3.zip?query3",
                     noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo(
                              "zip:zip:file:/path/file.zip?query%21/infile/file2.zip?query2!/infile2/file3.zip?query3");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip?query!/infile/file2.zip?query2"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile2/file3.zip"));
                  Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("query3"));
               });
               assertThat(ArchiveUri.ofUri(
                     "zip:zip:zip:file:/path/file.zip?query%21/infile/file2.zip?query2%21/infile2/file3.zip?query3!/infile3/file4.zip?query4",
                     noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo(
                              "zip:zip:zip:file:/path/file.zip?query%21/infile/file2.zip?query2%21/infile2/file3.zip?query3!/infile3/file4.zip?query4");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip?query!/infile/file2.zip?query2!/infile2/file3.zip?query3"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile3/file4.zip"));
                  Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("query4"));
               });
            }

            @Test
            public void ofUriWhenNoOuterArchiveEntry() {
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip%21/infile/file2.zip!/",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip%21/infile/file2.zip!/");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip%21/infile/file2.zip!",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip%21/infile/file2.zip!");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip%21/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip%21/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:zip:file:/path/file.zip%21/infile/file2.zip%21/infile2/file3.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:zip:file:/path/file.zip%21/infile/file2.zip%21/infile2/file3.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
            }

            @Test
            public void ofUriWhenNoInnerArchiveEntry() {
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip%21/!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip%21/!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip%21!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip%21!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:file:/path/file.zip!"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:zip:file:/path/file.zip!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:zip:file:/path/file.zip!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
            }
         }

         @Nested
         class WhenNotEscapedArchiveEntrySeparator {

            @Test
            public void ofUriWhenNominal() {
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile2/file3.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip!/infile3/file4.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip!/infile3/file4.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile3/file4.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
            }

            @Test
            public void ofUriWhenArchiveQuery() {
               assertThat(ArchiveUri.ofUri(
                     "zip:zip:zip:file:/path/file.zip?query!/infile/file2.zip?query2!/infile2/file3.zip?query3!/infile3/file4.zip?query4",
                     noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo(
                              "zip:zip:zip:file:/path/file.zip?query!/infile/file2.zip?query2!/infile2/file3.zip?query3!/infile3/file4.zip?query4");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip?query!/infile/file2.zip?query2!/infile2/file3.zip?query3"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile3/file4.zip"));
                  Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("query4"));
               });
            }

            @Test
            public void ofUriWhenNoOuterArchiveEntry() {
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip!/",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!/infile/file2.zip!/");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip!",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!/infile/file2.zip!");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .as("when ambiguous, archive entry is associated with outer URI")
                        .isEqualTo(Uri.ofUri("zip:file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions
                        .assertThat(uri.targetUri())
                        .as("when ambiguous, archive entry is associated with outer URI")
                        .isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile2/file3.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
            }

            @Test
            public void ofUriWhenNoInnerArchiveEntry() {
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!/!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!/!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:file:/path/file.zip!"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:file:/path/file.zip!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:file:/path/file.zip!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
               assertThat(ArchiveUri.ofUri("zip:zip:zip:file:/path/file.zip!/infile/file2.zip",
                                           noRestrictions())).satisfies(uri -> {
                  Assertions
                        .assertThat(uri.value())
                        .isEqualTo("zip:zip:zip:file:/path/file.zip!/infile/file2.zip");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("zip:zip:file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile/file2.zip"));
                  Assertions.assertThat(uri.archiveQuery()).isEmpty();
               });
            }
         }

      }

      @Nested
      class WhenArchiveQuery {

         @Test
         public void ofUriWhenNominal() {
            assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip?query",
                                        ofDefault().archiveQuery(false))).satisfies(uri -> {
               Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
               Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip?query"));
               Assertions.assertThat(uri.archiveEntry()).isEmpty();
               Assertions.assertThat(uri.archiveQuery()).isEmpty();
            });

            assertThat(ArchiveUri.ofUri("zip:file:/path/file.zip!/document?query",
                                        ofDefault().archiveQuery(false))).satisfies(uri -> {
               Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
               Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
               Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
               Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("query"));
            });

         }

         @Test
         public void ofUriWhenRestrictedQuery() {
            assertThatThrownBy(() -> ArchiveUri.ofUri("zip:file:/path/file.zip!/document?query"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'zip:file:/path/file.zip!/document?query' URI: Restricted archive query");
            assertThat(ArchiveUri.archive("zip", Uri.ofUri("file:/path/file.zip?query"))).satisfies(uri -> {
               Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip?query!/");
               Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
               Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip?query"));
               Assertions.assertThat(uri.archiveEntry()).isEmpty();
               Assertions.assertThat(uri.archiveQuery()).isEmpty();
               Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), true, true));
            });
            assertThat(ArchiveUri.archive("zip",
                                          Uri.ofUri("file:/path/file.zip?query"),
                                          Path.of("/document"))).satisfies(uri -> {
               Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip?query!/document");
               Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
               Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip?query"));
               Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
               Assertions.assertThat(uri.archiveQuery()).isEmpty();
               Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), false, true));
            });
         }

      }
   }

   @Nested
   class ArchiveTest {

      @Test
      public void archiveWhenNominal() {
         assertThat(ArchiveUri.archive("zip", FileUri.ofUri("file:/path/file.zip"))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), true, true));
         });
      }

      @Test
      public void archiveWhenEncodingTargetUri() {
         assertThat(ArchiveUri.archive("zip", Uri.ofUri("file:/path!file.zip#fragment"))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path!file.zip%23fragment!/");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path!file.zip#fragment"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), true, true));
         });
      }

      @Test
      public void archiveWhenArchiveEntryParameter() {
         assertThat(ArchiveUri.archive("zip",
                                       FileUri.ofUri("file:/path/file.zip"),
                                       Path.of("/document"))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/document");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), false, true));
         });
         assertThatThrownBy(() -> ArchiveUri.archive("zip",
                                                     FileUri.ofUri("file:/path/file.zip"),
                                                     Path.of("document")))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'archiveEntry=document' must be absolute path");
      }

      @Test
      public void archiveWhenRootArchiveEntry() {
         assertThat(ArchiveUri.archive("zip",
                                       FileUri.ofUri("file:/path/file.zip"),
                                       rootPath())).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), true, true));
         });
      }

      @Test
      public void archiveWhenEmptyArchiveEntry() {
         assertThat(ArchiveUri.archive("zip", FileUri.ofUri("file:/path/file.zip"), emptyPath())).satisfies(
               uri -> {
                  Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/");
                  Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
                  Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
                  Assertions.assertThat(uri.archiveEntry()).isEmpty();
                  Assertions
                        .assertThat(uri.restrictions())
                        .isEqualTo(ofRestrictions(list("zip"), true, true));
               });
      }

      @Test
      public void archiveWhenNewScheme() {
         assertThat(ArchiveUri.archive("new", FileUri.ofUri("file:/path/file.new"))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("new:file:/path/file.new!/");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("new");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.new"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("new"), true, true));
         });
      }

      @Test
      public void archiveWhenArchiveQuery() {
         assertThat(ArchiveUri.archive("zip",
                                       FileUri.ofUri("file:/path/file.zip"),
                                       Path.of("/document"),
                                       KeyValueQuery.of("k1=v1&k2=v2"))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path/file.zip!/document?k1=v1&k2=v2");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/file.zip"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
            Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("k1=v1&k2=v2"));
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), false, false));
         });
      }

      @Test
      public void archiveWhenEncodingTargetUriWithArchiveQuery() {
         assertThat(ArchiveUri.archive("zip",
                                       Uri.ofUri("file:/path!file.zip?query#fragment"))).satisfies(uri -> {
            Assertions.assertThat(uri.value()).isEqualTo("zip:file:/path!file.zip?query%23fragment!/");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path!file.zip?query#fragment"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), true, true));
         });
         assertThat(ArchiveUri.archive("zip",
                                       Uri.ofUri("file:/path!file.zip?query#fragment"),
                                       SimpleQuery.of("query"))).satisfies(uri -> {
            Assertions
                  .assertThat(uri.value())
                  .isEqualTo("zip:file:/path!file.zip?query%23fragment!/?query");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path!file.zip?query#fragment"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("query"));
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), true, false));
         });
         assertThat(ArchiveUri.archive("zip",
                                       Uri.ofUri("file:/path!file.zip??query#fragment"),
                                       SimpleQuery.of("?query"))).satisfies(uri -> {
            Assertions
                  .assertThat(uri.value())
                  .isEqualTo("zip:file:/path!file.zip??query%23fragment!/??query");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions
                  .assertThat(uri.targetUri())
                  .isEqualTo(Uri.ofUri("file:/path!file.zip??query#fragment"));
            Assertions.assertThat(uri.archiveEntry()).isEmpty();
            Assertions.assertThat(uri.archiveQuery()).hasValue(SimpleQuery.of("?query"));
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), true, false));
         });
      }

      @Test
      public void archiveWhenNestedArchiveUri() {
         assertThat(ArchiveUri.archive("zip",
                                       ArchiveUri.archive("zip",
                                                          FileUri.ofUri("file:/path/file.zip"),
                                                          Path.of("/infile/file2.zip")),
                                       Path.of("/infile2/file3.zip"))).satisfies(uri -> {
            Assertions
                  .assertThat(uri.value())
                  .isEqualTo("zip:zip:file:/path/file.zip!/infile/file2.zip!/infile2/file3.zip");
            Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
            Assertions
                  .assertThat(uri.targetUri())
                  .isEqualTo(Uri.ofUri("zip:file:/path/file.zip!/infile/file2.zip"));
            Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/infile2/file3.zip"));
            Assertions.assertThat(uri.restrictions()).isEqualTo(ofRestrictions(list("zip"), false, true));
         });
      }

   }

}

@Nested
class NormalizeTest {

   @Test
   public void normalizeWhenNominal() {
      assertThat(ArchiveUri.ofUri("zip:file:/path/sub/../.!/document").normalize()).satisfies(uri -> {
         Assertions.assertThat(uri.archiveScheme()).isEqualTo("zip");
         Assertions.assertThat(uri.targetUri()).isEqualTo(Uri.ofUri("file:/path/sub/../."));
         Assertions.assertThat(uri.archiveEntry()).hasValue(Path.of("/document"));
      });
   }

}
