/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.test.uri.UriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.DefaultUriRestrictions.ofRestrictions;
import static java.util.function.Function.identity;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.lang.util.Try.Failure;

public class UriTest {

   @Test
   public void ofUriWhenNominal() {
      assertThat(Uri.ofUri("scheme://user@host/path")).satisfies(uri -> {
         Assertions.assertThat(uri.value()).isEqualTo("scheme://user@host/path");
      });
   }

   @Test
   public void compatibleUriWhenNominal() {
      Assertions.assertThat(Uri.compatibleUri(() -> Uri.ofUri("scheme://user@host/path"))).satisfies(uri -> {
         Assertions.assertThat(uri.orElseThrow(identity()).value()).isEqualTo("scheme://user@host/path");
      });
      Assertions.assertThat(Uri.compatibleUri("scheme://user@host/path", Uri::ofUri)).satisfies(uri -> {
         Assertions.assertThat(uri.orElseThrow(identity()).value()).isEqualTo("scheme://user@host/path");
      });
   }

   @Test
   public void compatibleUriWhenInvalidUri() {
      assertThatThrownBy(() -> Uri.compatibleUri(() -> Uri.ofUri("scheme://")))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'scheme://' URI: Expected authority at index 9: scheme://");
      assertThatThrownBy(() -> Uri.compatibleUri(() -> Uri.ofUri("scheme://host?query",
                                                                 ofRestrictions(true, false))))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'scheme://host?query' URI: 'DefaultUri[value=scheme://host?query,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=false]]' must not have a query");
      assertThatThrownBy(() -> Uri.compatibleUri("scheme://host?query",
                                                 ofRestrictions(true, false),
                                                 Uri::ofUri))
            .isInstanceOf(InvalidUriException.class)
            .hasMessage(
                  "Invalid 'scheme://host?query' URI: 'DefaultUri[value=scheme://host?query,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=false]]' must not have a query");
   }

   @Test
   public void compatibleUriWhenIncompatibleUri() {
      Assertions
            .assertThat(Uri
                              .compatibleUri(() -> FileUri.ofUri("other:/"))
                              .mapFailure(IncompatibleUriException::getMessage))
            .isEqualTo(Failure.of(
                  "Incompatible 'other:/' URI: 'FileUri[value=other:/,restrictions=FileUriRestrictions[relativeFormat=true, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=SimpleScheme[scheme='other'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/', encodedSchemeSpecific='/'],authority=<null>,path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'"));
      Assertions
            .assertThat(Uri
                              .compatibleUri("other:/", FileUri::ofUri)
                              .mapFailure(IncompatibleUriException::getMessage))
            .isEqualTo(Failure.of(
                  "Incompatible 'other:/' URI: 'FileUri[value=other:/,restrictions=FileUriRestrictions[relativeFormat=true, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=SimpleScheme[scheme='other'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/', encodedSchemeSpecific='/'],authority=<null>,path=SimplePath[path=/, encodedPath=/],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'"));
   }
}
