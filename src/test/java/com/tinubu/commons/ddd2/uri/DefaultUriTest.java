/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri;

import static com.tinubu.commons.ddd2.test.uri.UriAssert.assertThat;
import static com.tinubu.commons.ddd2.test.uri.UriAssert.serverAuthority;
import static com.tinubu.commons.ddd2.uri.UriEncoder.encode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.BaseSimpleIdTest;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.test.uri.TestUri;

public class DefaultUriTest extends BaseSimpleIdTest<DefaultUri, String> {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Override
   protected boolean supportsUninitializedValue() {
      return false;
   }

   protected DefaultUri newInstanceForNewObject(String value) {
      return DefaultUri.ofUri(value).asNewObject();
   }

   @Override
   protected DefaultUri newInstanceForNewObject(URI urn) {
      return DefaultUri.ofUrn(urn).asNewObject();
   }

   protected DefaultUri newInstanceForNewObject() {
      throw new UnsupportedOperationException();
   }

   protected DefaultUri newInstance(String value) {
      return DefaultUri.ofUri(value);
   }

   @Override
   protected DefaultUri newInstance(URI urn) {
      return DefaultUri.ofUrn(urn);
   }

   @Override
   protected String neutralValue() {
      return "scheme:/";
   }

   @Override
   protected String urnIdType() {
      return "uri";
   }

   @Nested
   class ofUri {

      @Nested
      class Hierarchical {

         @TestUri("scheme:/path?query#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "/path?query")
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme:/")
         public void ofUriWhenRootPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:/")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "/")
                  .hasPath(false, "/")
                  .hasNoMoreParts();
         }

         @Test
         public void hierarchicalWhenInvalidUri() {
            assertThatThrownBy(() -> DefaultUri.hierarchical(false, "&", "/path", null))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage("Invalid '&:/path' URI: Illegal character in scheme name at index 0: &:/path");
         }

         @Test
         public void hierarchicalWhenNotAbsolute() {
            assertThatThrownBy(() -> DefaultUri.hierarchical(false, null, "/path", "query", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void hierarchicalWhenEmptyScheme() {
            assertThatThrownBy(() -> DefaultUri.hierarchical(false, "", "/path", "query", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void hierarchicalWhenNoPath() {
            assertThatThrownBy(() -> DefaultUri.hierarchical(false, "scheme", null, "query", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void hierarchicalWhenNotAbsolutePath() {
            assertThatThrownBy(() -> DefaultUri.hierarchical(false, "scheme", "path", "query", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path=path' must be absolute");
         }

      }

      @Nested
      class HierarchicalServer {

         @TestUri("scheme://user:pass@host:80/path?query#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user:pass@host:80/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//user:pass@host:80/path?query")
                  .hasAuthority(false, serverAuthority("user:pass", "host", 80))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://user:pass@host/path?query#fragment")
         public void ofUriWhenNoPort(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user:pass@host/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//user:pass@host/path?query")
                  .hasAuthority(false, serverAuthority("user:pass", "host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://user:@host/path?query#fragment")
         public void ofUriWhenUserInfoEmptyPassword(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user:@host/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//user:@host/path?query")
                  .hasAuthority(false, serverAuthority("user:", "host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://user@host/path?query#fragment")
         public void ofUriWhenUserInfoNoPassword(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://user@host/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//user@host/path?query")
                  .hasAuthority(false, serverAuthority("user", "host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://:@host/path?query#fragment")
         public void ofUriWhenUserInfoEmptyUsernamePassword(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://:@host/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//:@host/path?query")
                  .hasAuthority(false, serverAuthority(":", "host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://@host/path?query#fragment")
         public void ofUriWhenUserInfoEmptyUsername(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://@host/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//@host/path?query")
                  .hasAuthority(false, serverAuthority("", "host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://host/path?query#fragment")
         public void ofUriWhenUserInfoNoUsername(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/path?query")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://host/path?query#")
         public void ofUriWhenEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?query#")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/path?query")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://host/path?query")
         public void ofUriWhenNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?query")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/path?query")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://host/path?#fragment")
         public void ofUriWhenEmptyQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path?#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/path?")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://host/path#fragment")
         public void ofUriWhenNoQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/path")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/path")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://host/path")
         public void ofUriWhenNoQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/path")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/path")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/path")
                  .hasNoMoreParts();
         }

         @TestUri({ "scheme://host/", "scheme://host/?query#fragment" })
         public void ofUriWhenRootPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/")
                  .hasNoMoreParts();

            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host/?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host/?query")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri({ "scheme://host", "scheme://host?query#fragment" })
         public void ofUriWhenEmptyPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "")
                  .hasNoMoreParts();

            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host?query")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://host//path")
         public void ofUriWhenDoubleAbsolutePath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme://host//path")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//host//path")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "//path")
                  .hasNoMoreParts();
         }

         @TestUri("scheme:////path")
         public void ofUriWhenEmptyAuthorityAndDoubleAbsolutePath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalServer()
                  .hasValue("scheme:////path")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "////path")
                  .hasAuthority(false, "")
                  .hasPath(false, "//path")
                  .hasNoMoreParts();
         }

         @TestUri({ "scheme:///", "scheme:///path", "scheme://?query" })
         public void ofUriWhenEmptyAuthority(Uri rootPath, Uri path, Uri emptyPath, String description) {
            assertThat(rootPath)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:///")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "///")
                  .hasAuthority(false, "")
                  .hasPath(false, "/")
                  .hasNoMoreParts();

            assertThat(path)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme:///path")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "///path")
                  .hasAuthority(false, "")
                  .hasPath(false, "/path")
                  .hasNoMoreParts();

            assertThat(emptyPath)
                  .as(description)
                  .isAbsolute()
                  .isHierarchical()
                  .hasValue("scheme://?query")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//?query")
                  .hasAuthority(false, "")
                  .hasPath(false, "")
                  .hasQuery(false, "query")
                  .hasNoMoreParts();

         }

         @Test
         public void hierarchicalServerWhenInvalidUri() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalServer(false,
                                                                   "&",
                                                                   serverAuthority("host"),
                                                                   "/path",
                                                                   null))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '&://host/path' URI: Illegal character in scheme name at index 0: &://host/path");
         }

         @Test
         public void hierarchicalServerWhenNotAbsolute() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalServer(false,
                                                                   null,
                                                                   "user:pass",
                                                                   "host",
                                                                   80,
                                                                   "/path",
                                                                   "query",
                                                                   "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void hierarchicalServerWhenEmptyScheme() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalServer(false,
                                                                   "",
                                                                   "user:pass",
                                                                   "host",
                                                                   80,
                                                                   "/path",
                                                                   "query",
                                                                   "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void hierarchicalServerWhenEmptyAuthority() {
            assertThat(DefaultUri.hierarchicalServer(false, "scheme", "", "/", null, null))
                  .isHierarchicalServer()
                  .hasAuthority(false, "");
         }

         @Test
         public void hierarchicalServerWhenNoHost() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalServer(false,
                                                                   "scheme",
                                                                   "user:pass",
                                                                   null,
                                                                   80,
                                                                   "/path",
                                                                   "query",
                                                                   "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'host' must not be null");
         }

         @Test
         public void hierarchicalServerWhenEmptyHost() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalServer(false,
                                                                   "scheme",
                                                                   "user:pass",
                                                                   "",
                                                                   80,
                                                                   "/path",
                                                                   "query",
                                                                   "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'host' must not be blank");
         }

         @Test
         public void hierarchicalUriWhenNoPath() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalServer(false,
                                                                   "scheme",
                                                                   serverAuthority("user:pass@host:80"),
                                                                   null,
                                                                   "query",
                                                                   "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void hierarchicalServerWhenNotAbsoluteOrEmptyPath() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalServer(false,
                                                                   "scheme",
                                                                   serverAuthority("user:pass@host:80"),
                                                                   "path",
                                                                   "query",
                                                                   "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
         }

      }

      @Nested
      class HierarchicalRegistry {

         @TestUri("scheme://r:egistry/path?query#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry/path?query")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://r:egistry/path?query#")
         public void ofUriWhenEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?query#")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry/path?query")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://r:egistry/path?query")
         public void ofUriWhenNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?query")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry/path?query")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://r:egistry/path?#fragment")
         public void ofUriWhenEmptyQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path?#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry/path?")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/path")
                  .hasQuery(false, "")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme://r:egistry/path#fragment")
         public void ofUriWhenNoQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/path#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry/path")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/path")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri({ "scheme://r:egistry/", "scheme://r:egistry/?query#fragment" })
         public void ofUriWhenRootPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry/")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/")
                  .hasNoMoreParts();
            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry/?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry/?query")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri({ "scheme://r:egistry", "scheme://r:egistry?query#fragment" })
         public void ofUriWhenEmptyPath(Uri uri, Uri uriWithQueryFragment, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "")
                  .hasNoMoreParts();
            assertThat(uriWithQueryFragment)
                  .as(description)
                  .isAbsolute()
                  .isHierarchicalRegistry()
                  .hasValue("scheme://r:egistry?query#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "//r:egistry?query")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @Test
         public void hierarchicalRegistryWhenInvalidUri() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalRegistry(false, "&", "r:egistry", "/path"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '&://r:egistry/path' URI: Illegal character in scheme name at index 0: &://r:egistry/path");
         }

         @Test
         public void hierarchicalRegistryWhenNotAbsolute() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalRegistry(false,
                                                                     null,
                                                                     "r:egistry",
                                                                     "/path",
                                                                     "query",
                                                                     "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void hierarchicalRegistryWhenEmptyScheme() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalRegistry(false,
                                                                     "",
                                                                     "r:egistry",
                                                                     "/path",
                                                                     "query",
                                                                     "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void hierarchicalRegistryWhenNoAuthority() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalRegistry(false,
                                                                     "scheme",
                                                                     null,
                                                                     "/path",
                                                                     "query",
                                                                     "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'authority' must not be null");
         }

         @Test
         public void hierarchicalRegistryWhenNoPath() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalRegistry(false,
                                                                     "scheme",
                                                                     "r:egistry",
                                                                     null,
                                                                     "query",
                                                                     "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void hierarchicalRegistryWhenNotAbsoluteOrEmptyPath() {
            assertThatThrownBy(() -> DefaultUri.hierarchicalRegistry(false,
                                                                     "scheme",
                                                                     "r:egistry",
                                                                     "path",
                                                                     "query",
                                                                     "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
         }

      }

      @Nested
      class Opaque {

         @TestUri("scheme:opaque#fragment")
         public void ofUriWhenNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isOpaque()
                  .hasValue("scheme:opaque#fragment")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "opaque")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("scheme:opaque")
         public void ofUriWhenNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isAbsolute()
                  .isOpaque()
                  .hasValue("scheme:opaque")
                  .hasScheme("scheme")
                  .hasSchemeSpecific(false, "opaque")
                  .hasNoMoreParts();
         }

         @Test
         public void opaqueWhenInvalidUri() {
            assertThatThrownBy(() -> DefaultUri.opaque(false, "&", "opaque", "fragment"))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid '&:opaque#fragment' URI: Illegal character in scheme name at index 0: &:opaque#fragment");
         }

         @Test
         public void opaqueWhenNotAbsolute() {
            assertThatThrownBy(() -> DefaultUri.opaque(false, null, "opaque", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be null");
         }

         @Test
         public void opaqueWhenEmptyScheme() {
            assertThatThrownBy(() -> DefaultUri.opaque(false, "", "opaque", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'scheme' must not be blank");
         }

         @Test
         public void opaqueWhenNoSchemeSpecific() {
            assertThatThrownBy(() -> DefaultUri.opaque(false, "scheme", null, "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'schemeSpecific' must not be null");
         }

         @Test
         public void opaqueWhenEmptySchemeSpecific() {
            assertThatThrownBy(() -> DefaultUri.opaque(false, "scheme", "", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'schemeSpecific' must not be blank");
         }

         @Test
         public void opaqueWhenAbsoluteOpaque() {
            assertThatThrownBy(() -> DefaultUri.opaque(false, "scheme", "/opaque", "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'schemeSpecific=/opaque' must not start with '/'");
         }

      }

      @Nested
      class Relative {

         @TestUri("//user:pass@host:80/path?query#fragment")
         public void ofUriWhenServerAuthorityNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//user:pass@host:80/path?query#fragment")
                  .hasAuthority(false, serverAuthority("user:pass", "host", 80))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("//user:pass@host/path?query#")
         public void ofUriWhenServerAuthorityEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//user:pass@host/path?query#")
                  .hasAuthority(false, serverAuthority("user:pass", "host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("//user:pass@host/path?#fragment")
         public void ofUriWhenServerAuthorityEmptyQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//user:pass@host/path?#fragment")
                  .hasAuthority(false, serverAuthority("user:pass", "host"))
                  .hasPath(false, "/path")
                  .hasQuery(false, "")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("//host/")
         public void ofUriWhenServerAuthorityRootPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//host/")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "/")
                  .hasNoMoreParts();
         }

         @TestUri("//host")
         public void ofUriWhenServerAuthorityEmptyPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//host")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("//host//path")
         public void ofUriWhenServerAuthorityDoubleAbsolutePath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalServer()
                  .hasValue("//host//path")
                  .hasAuthority(false, serverAuthority("host"))
                  .hasPath(false, "//path")
                  .hasNoMoreParts();
         }

         @TestUri({ "///", "///path", "//?query" })
         public void ofUriWhenEmptyAuthority(Uri rootPath, Uri path, Uri emptyPath, String description) {
            assertThat(rootPath)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("///")
                  .hasAuthority(false, "")
                  .hasPath(false, "/")
                  .hasNoMoreParts();

            assertThat(path)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("///path")
                  .hasAuthority(false, "")
                  .hasPath(false, "/path")
                  .hasNoMoreParts();

            assertThat(emptyPath)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("//?query")
                  .hasAuthority(false, "")
                  .hasPath(false, "")
                  .hasQuery(false, "query")
                  .hasNoMoreParts();

         }

         @TestUri("////path")
         public void ofUriWhenEmptyAuthorityAndDoubleAbsolutePath(Uri path, String description) {
            assertThat(path)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("////path")
                  .hasAuthority(false, "")
                  .hasPath(false, "//path")
                  .hasNoMoreParts();
         }

         @TestUri("//r:egistry/path?query#fragment")
         public void ofUriWhenRegistryAuthorityNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchicalRegistry()
                  .hasValue("//r:egistry/path?query#fragment")
                  .hasAuthority(false, "r:egistry")
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("/path?query#fragment")
         public void ofUriWhenPathNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/path?query#fragment")
                  .hasNoAuthority()
                  .hasPath(false, "/path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("/path?#")
         public void ofUriWhenPathEmptyQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/path?#")
                  .hasNoAuthority()
                  .hasPath(false, "/path")
                  .hasQuery(false, "")
                  .hasFragment(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("/path")
         public void ofUriWhenPathNoQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/path")
                  .hasPath(false, "/path")
                  .hasNoMoreParts();
         }

         @TestUri("path?query#fragment")
         public void ofUriWhenRelativePathNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path?query#fragment")
                  .hasPath(false, "path")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("path?#")
         public void ofUriWhenRelativePathEmptyQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path?#")
                  .hasPath(false, "path")
                  .hasQuery(false, "")
                  .hasFragment(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("path#fragment")
         public void ofUriWhenRelativePathNoQuery(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path#fragment")
                  .hasPath(false, "path")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("path?query")
         public void ofUriWhenRelativePathNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path?query")
                  .hasPath(false, "path")
                  .hasQuery(false, "query")
                  .hasNoMoreParts();
         }

         @TestUri("path")
         public void ofUriWhenRelativePathNoQueryFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("path")
                  .hasPath(false, "path")
                  .hasNoMoreParts();
         }

         @TestUri("/")
         public void ofUriWhenRootPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("/")
                  .hasPath(false, "/")
                  .hasNoMoreParts();
         }

         @TestUri("")
         public void ofUriWhenEmptyPath(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("")
                  .hasPath(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("?query#fragment")
         public void ofUriWhenQueryNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?query#fragment")
                  .hasPath(false, "")
                  .hasQuery(false, "query")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("?query#")
         public void ofUriWhenQueryEmptyFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?query#")
                  .hasPath(false, "")
                  .hasQuery(false, "query")
                  .hasFragment(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("?query")
         public void ofUriWhenQueryNoFragment(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?query")
                  .hasPath(false, "")
                  .hasQuery(false, "query")
                  .hasNoMoreParts();
         }

         @TestUri("?")
         public void ofUriWhenQueryEmpty(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("?")
                  .hasPath(false, "")
                  .hasQuery(false, "")
                  .hasNoMoreParts();
         }

         @TestUri("#fragment")
         public void ofUriWhenFragmentNominal(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("#fragment")
                  .hasPath(false, "")
                  .hasFragment(false, "fragment")
                  .hasNoMoreParts();
         }

         @TestUri("#")
         public void ofUriWhenFragmentEmpty(Uri uri, String description) {
            assertThat(uri)
                  .as(description)
                  .isRelative()
                  .isHierarchical()
                  .hasValue("#")
                  .hasPath(false, "")
                  .hasFragment(false, "")
                  .hasNoMoreParts();
         }

         @Test
         public void relativeWhenNoParts() {
            assertThatThrownBy(() -> DefaultUri.relative(false, null, null, null, null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage(
                        "Invariant validation error > At least one part must be set | 'path' must not be null");
         }

         @Test
         public void relativeWhenBlankAuthority() {
            assertThat(DefaultUri.relative(false, "", "/", null, null))
                  .isHierarchicalServer()
                  .hasAuthority(false, "");
         }

         @Test
         public void relativeWhenNoPath() {
            assertThatThrownBy(() -> DefaultUri.relative(false, serverAuthority("host"), null, null, null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
            assertThatThrownBy(() -> DefaultUri.relative(false, null, null, "query", null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
            assertThatThrownBy(() -> DefaultUri.relative(false, null, null, null, "fragment"))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path' must not be null");
         }

         @Test
         public void relativeWhenAuthorityAndNotAbsoluteOrEmptyPath() {
            assertThatThrownBy(() -> DefaultUri.relative(false, serverAuthority("host"), "path", null, null))
                  .isInstanceOf(InvariantValidationException.class)
                  .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
         }

      }

      @Nested
      class WhenRestrictions {

         @Test
         public void ofUriWhenQueryRestriction() {
            assertThatThrownBy(() -> DefaultUri.ofUri("scheme:/path?query",
                                                      DefaultUriRestrictions.ofRestrictions(true, true)))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'scheme:/path?query' URI: 'DefaultUri[value=scheme:/path?query,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true]]' must not have a query");
         }

         @Test
         public void ofUriWhenFragmentRestriction() {
            assertThatThrownBy(() -> DefaultUri.ofUri("scheme:/path#fragment",
                                                      DefaultUriRestrictions.ofRestrictions(true, true)))
                  .isInstanceOf(InvalidUriException.class)
                  .hasMessage(
                        "Invalid 'scheme:/path#fragment' URI: 'DefaultUri[value=scheme:/path#fragment,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=true, fragment=true]]' must not have a fragment");
         }

      }
   }

   @Nested
   class Update {

      @Test
      public void updateScheme() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").scheme("s"))
               .isHierarchicalServer()
               .hasValue("s://user:pass@host:80/path?query#fragment")
               .hasScheme("s");
         assertThat(DefaultUri.ofUri("scheme://r:egistry/path?query#fragment").scheme("s"))
               .isHierarchicalRegistry()
               .hasValue("s://r:egistry/path?query#fragment")
               .hasScheme("s");
         assertThat(DefaultUri.ofUri("scheme:opaque#fragment").scheme("s"))
               .isOpaque()
               .hasValue("s:opaque#fragment")
               .hasScheme("s");
      }

      @Test
      public void updateSchemeWhenNotAbsolute() {
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").scheme("s"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be absolute");
      }

      @Test
      public void updateSchemeWhenBlank() {
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme:/path").scheme((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'scheme' must not be null");
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme:/path").scheme(""))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'scheme' must not be blank");
      }

      @Test
      public void updateAuthority() {
         assertThat(DefaultUri
                          .ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .serverAuthority(false, "u:p@h:81"))
               .isHierarchicalServer()
               .hasValue("scheme://u:p@h:81/path?query#fragment")
               .hasServerAuthority(false, "u:p@h:81");
         assertThat(DefaultUri
                          .ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .serverAuthority(false, "h"))
               .isHierarchicalServer()
               .hasValue("scheme://h/path?query#fragment")
               .hasServerAuthority(false, "h");
         assertThat(DefaultUri
                          .ofUri("scheme://r:egistry/path?query#fragment")
                          .registryAuthority(false, "r:e"))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:e/path?query#fragment")
               .hasRegistryAuthority(false, "r:e");
         assertThat(DefaultUri
                          .ofUri("//user:pass@host:80/path?query#fragment")
                          .serverAuthority(false, "u:p@h:81"))
               .isHierarchicalServer()
               .hasValue("//u:p@h:81/path?query#fragment")
               .hasServerAuthority(false, "u:p@h:81");
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").serverAuthority(false, "h"))
               .isHierarchicalServer()
               .hasValue("//h/path?query#fragment")
               .hasServerAuthority(false, "h");
         assertThat(DefaultUri.ofUri("//r:egistry/path?query#fragment").registryAuthority(false, "r:e"))
               .isHierarchicalRegistry()
               .hasValue("//r:e/path?query#fragment")
               .hasRegistryAuthority(false, "r:e");
      }

      @Test
      public void updateServerAuthorityWhenNull() {
         assertThatThrownBy(() -> DefaultUri
               .ofUri("scheme://user:pass@host/path")
               .serverAuthority(false, (String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'serverAuthority' must not be null");
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").serverAuthority(false, (String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updateServerAuthorityWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").serverAuthority(false, "host"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updateRegistryAuthorityWhenNull() {
         assertThatThrownBy(() -> DefaultUri
               .ofUri("scheme://r:egistry/path")
               .registryAuthority(false, (String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'registryAuthority' must not be null");
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").registryAuthority(false, (String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical registry-based");
      }

      @Test
      public void updateServerAuthorityWhenNotHierarchicalRegistry() {
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").registryAuthority(false, "r:egistry"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical registry-based");
      }

      @Test
      public void updateUserInfo() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").userInfo(false, "u:p"))
               .isHierarchicalServer()
               .hasValue("scheme://u:p@host:80/path?query#fragment")
               .hasUserInfo(false, "u:p");
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").userInfo(false, "u:p"))
               .isHierarchicalServer()
               .hasValue("//u:p@host:80/path?query#fragment")
               .hasUserInfo(false, "u:p");
      }

      @Test
      public void updateUserInfoWhenNull() {
         assertThat(DefaultUri
                          .ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .userInfo(false, (String) null))
               .isHierarchicalServer()
               .hasValue("scheme://host:80/path?query#fragment")
               .hasNoUserInfo();
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").userInfo(false, (String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updateUserInfoWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").userInfo(false, "u:p"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updateHost() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").host("h"))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@h:80/path?query#fragment")
               .hasHost("h");
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").host("h"))
               .isHierarchicalServer()
               .hasValue("//user:pass@h:80/path?query#fragment")
               .hasHost("h");
      }

      @Test
      public void updateHostWhenBlank() {
         assertThatThrownBy(() -> DefaultUri
               .ofUri("scheme://user:pass@host:80/path?query#fragment")
               .host((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'host' must not be null");
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").host(""))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'host' must not be blank");
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").host((String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updateHostWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").host("h"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updatePort() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").port(81))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:81/path?query#fragment")
               .hasPort(81);
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").port(81))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:81/path?query#fragment")
               .hasPort(81);
      }

      @Test
      public void updatePortWhenNegOne() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").port(-1))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host/path?query#fragment")
               .hasNoPort();
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").port(-1))
               .isHierarchicalServer()
               .hasValue("//user:pass@host/path?query#fragment")
               .hasNoPort();
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").port(-1))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updatePortWhenNotHierarchicalServer() {
         assertThatThrownBy(() -> DefaultUri.ofUri("/path").port(81))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=/path,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical server-based");
      }

      @Test
      public void updatePath() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").path(false, "/"))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/?query#fragment")
               .hasPath(false, "/");
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").path(false, ""))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80?query#fragment")
               .hasPath(false, "");
         assertThat(DefaultUri.ofUri("scheme://r:egistry/path?query#fragment").path(false, "/"))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/?query#fragment")
               .hasPath(false, "/");
         assertThat(DefaultUri.ofUri("scheme://r:egistry/path?query#fragment").path(false, ""))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry?query#fragment")
               .hasPath(false, "");
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").path(false, "/"))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/?query#fragment")
               .hasPath(false, "/");
         assertThat(DefaultUri.ofUri("//r:egistry/path?query#fragment").path(false, "/"))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/?query#fragment")
               .hasPath(false, "/");
         assertThat(DefaultUri.ofUri("/path?query#fragment").path(false, "/"))
               .isHierarchical()
               .hasValue("/?query#fragment")
               .hasPath(false, "/");
         assertThat(DefaultUri.ofUri("/path?query#fragment").path(false, "path"))
               .isHierarchical()
               .hasValue("path?query#fragment")
               .hasPath(false, "path");
         assertThat(DefaultUri.ofUri("/path?query#fragment").path(false, ""))
               .isHierarchical()
               .hasValue("?query#fragment")
               .hasPath(false, "");
      }

      @Test
      public void updatePathWhenNull() {
         assertThatThrownBy(() -> DefaultUri
               .ofUri("scheme://user:pass@host:80/path?query#fragment")
               .path(false, (String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path' must not be null");
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme:/path?query#fragment").path(false, (String) null))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path' must not be null");
      }

      @Test
      public void updatePathWhenNotAbsolute() {
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme://host/path").path(false, "path"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
         assertThatThrownBy(() -> DefaultUri.ofUri("//host/path").path(false, "path"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path=path' must be absolute or empty");
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme:/path").path(false, "path"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path=path' must be absolute");
      }

      @Test
      public void updatePathWhenNotHierarchical() {
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme:opaque").path(false, "p"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=scheme:opaque,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical");
      }

      @Test
      public void updateQuery() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").query(false, "q"))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(DefaultUri.ofUri("scheme://r:egistry/path?query#fragment").query(false, "q"))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").query(false, "q"))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(DefaultUri.ofUri("//r:egistry/path?query#fragment").query(false, "q"))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(DefaultUri.ofUri("/path?query#fragment").query(false, "q"))
               .isHierarchical()
               .hasValue("/path?q#fragment")
               .hasQuery(false, "q");
         assertThat(DefaultUri.ofUri("/path?query#fragment").query(false, ""))
               .isHierarchical()
               .hasValue("/path?#fragment")
               .hasQuery(false, "");
         assertThat(DefaultUri.ofUri("?query#fragment").query(false, "q"))
               .isHierarchical()
               .hasValue("?q#fragment")
               .hasQuery(false, "q");
         assertThat(DefaultUri.ofUri("?query#fragment").query(false, ""))
               .isHierarchical()
               .hasValue("?#fragment")
               .hasQuery(false, "");
      }

      @Test
      public void updateQueryWhenNull() {
         assertThat(DefaultUri
                          .ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .query(false, (String) null))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path#fragment")
               .hasNoQuery();
         assertThat(DefaultUri.ofUri("scheme://r:egistry/path?query#fragment").query(false, (String) null))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path#fragment")
               .hasNoQuery();
         assertThat(DefaultUri.ofUri("scheme:/path?query#fragment").query(false, (String) null))
               .isHierarchical()
               .hasValue("scheme:/path#fragment")
               .hasNoQuery();
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").query(false, (String) null))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/path#fragment")
               .hasNoQuery();
         assertThat(DefaultUri.ofUri("//r:egistry/path?query#fragment").query(false, (String) null))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/path#fragment")
               .hasNoQuery();
         assertThat(DefaultUri.ofUri("/path?query#fragment").query(false, (String) null))
               .isHierarchical()
               .hasValue("/path#fragment")
               .hasNoQuery();
         assertThat(DefaultUri.ofUri("?query#fragment").query(false, (String) null))
               .isHierarchical()
               .hasValue("#fragment")
               .hasNoQuery();
         assertThat(DefaultUri.ofUri("?query").query(false, (String) null))
               .isHierarchical()
               .hasValue("")
               .hasNoQuery();
      }

      @Test
      public void updateQueryWhenNotHierarchical() {
         assertThatThrownBy(() -> DefaultUri.ofUri("scheme:opaque").query(false, "q"))
               .isInstanceOf(InvariantValidationException.class)
               .hasMessage(
                     "Invariant validation error > 'this=DefaultUri[value=scheme:opaque,restrictions=DefaultUriRestrictions[portRestriction=NONE, query=false, fragment=false]]' must be hierarchical");
      }

      @Test
      public void updateFragment() {
         assertThat(DefaultUri.ofUri("scheme://user:pass@host:80/path?query#fragment").fragment(false, "f"))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query#f")
               .hasFragment(false, "f");
         assertThat(DefaultUri.ofUri("scheme://r:egistry/path?query#fragment").fragment(false, "f"))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path?query#f")
               .hasFragment(false, "f");
         assertThat(DefaultUri.ofUri("scheme:opaque#fragment").fragment(false, "f"))
               .isOpaque()
               .hasValue("scheme:opaque#f")
               .hasFragment(false, "f");
         assertThat(DefaultUri.ofUri("//user:pass@host:80/path?query#fragment").fragment(false, "f"))
               .isHierarchicalServer()
               .hasValue("//user:pass@host:80/path?query#f")
               .hasFragment(false, "f");
         assertThat(DefaultUri.ofUri("//r:egistry/path?query#fragment").fragment(false, "f"))
               .isHierarchicalRegistry()
               .hasValue("//r:egistry/path?query#f")
               .hasFragment(false, "f");
         assertThat(DefaultUri.ofUri("/path?query#fragment").fragment(false, "f"))
               .isHierarchical()
               .hasValue("/path?query#f")
               .hasFragment(false, "f");
         assertThat(DefaultUri.ofUri("/path?query#fragment").fragment(false, ""))
               .isHierarchical()
               .hasValue("/path?query#")
               .hasFragment(false, "");
         assertThat(DefaultUri.ofUri("#fragment").fragment(false, "f"))
               .isHierarchical()
               .hasValue("#f")
               .hasFragment(false, "f");
         assertThat(DefaultUri.ofUri("#fragment").fragment(false, ""))
               .isHierarchical()
               .hasValue("#")
               .hasFragment(false, "");
      }

      @Test
      public void updateFragmentWhenNull() {
         assertThat(DefaultUri
                          .ofUri("scheme://user:pass@host:80/path?query#fragment")
                          .fragment(false, (String) null))
               .isHierarchicalServer()
               .hasValue("scheme://user:pass@host:80/path?query")
               .hasNoFragment();
         assertThat(DefaultUri.ofUri("scheme://r:egistry/path?query#fragment").fragment(false, (String) null))
               .isHierarchicalRegistry()
               .hasValue("scheme://r:egistry/path?query")
               .hasNoFragment();
         assertThat(DefaultUri.ofUri("scheme:/path?query#fragment").fragment(false, (String) null))
               .isHierarchical()
               .hasValue("scheme:/path?query")
               .hasNoFragment();
         assertThat(DefaultUri.ofUri("scheme:opaque#fragment").fragment(false, (String) null))
               .isOpaque()
               .hasValue("scheme:opaque")
               .hasNoFragment();
         assertThat(DefaultUri
                          .ofUri("//user:pass@host:80/path?query#fragment")
                          .fragment(false, (String) null))
               .isHierarchical()
               .hasValue("//user:pass@host:80/path?query")
               .hasNoFragment();
         assertThat(DefaultUri.ofUri("//r:egistry/path?query#fragment").fragment(false, (String) null))
               .isHierarchical()
               .hasValue("//r:egistry/path?query")
               .hasNoFragment();
         assertThat(DefaultUri.ofUri("/path?query#fragment").fragment(false, (String) null))
               .isHierarchical()
               .hasValue("/path?query")
               .hasNoFragment();
         assertThat(DefaultUri.ofUri("?query#fragment").fragment(false, (String) null))
               .isHierarchical()
               .hasValue("?query")
               .hasNoFragment();
         assertThat(DefaultUri.ofUri("#fragment").fragment(false, (String) null))
               .isHierarchical()
               .hasValue("")
               .hasNoFragment();
      }

   }

   @Nested
   class Encoding {

      /* Common encoding : @ %40 | % %25 | # %23 | : %3A | / %2F | ? %3F | & %26 | = %3D */

      @Test
      public void ofUriWhenEncoded() {
         var uri = DefaultUri.ofUri("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment");

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment")
               .hasScheme("scheme")
               .hasSchemeSpecific(true, "//u%23ser:p%23ass@host/p%23ath?q%23uery")
               .hasSchemeSpecific(false, "//u#ser:p#ass@host/p#ath?q#uery")
               .hasAuthority(true, "u%23ser:p%23ass@host")
               .hasAuthority(false, "u#ser:p#ass@host")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true, "q%23uery")
               .hasQuery(false, "q#uery")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment")
               .hasNoMoreParts();
      }

      @Test
      public void hierarchicalServerWhenEncoding() {
         var uri = DefaultUri.hierarchicalServer(false,
                                                 "scheme",
                                                 "u#ser:p#ass",
                                                 "host",
                                                 -1,
                                                 "/p#ath",
                                                 "q#uery",
                                                 "f#ragment");

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true, "q%23uery")
               .hasQuery(false, "q#uery")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void hierarchicalServerUpdateWhenEncoding() {
         var uri = DefaultUri
               .hierarchicalServer(false, "scheme", null, "host", -1, "", null, null)
               .userInfo(false, "u#ser:p#ass")
               .path(false, "/p#ath")
               .query(false, "q#uery")
               .fragment(false, "f#ragment");

         assertThat(uri)
               .hasValue("scheme://u%23ser:p%23ass@host/p%23ath?q%23uery#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true, "q%23uery")
               .hasQuery(false, "q#uery")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void encodingHostWhenContainArobase() {
         var uri = DefaultUri.hierarchicalServer(false, "scheme", "ho@st", "/path", "query", "fragment");

         assertThat(uri)
               .hasValue("scheme://ho@st/path?query#fragment")
               .isHierarchicalServer()
               .hasUserInfo(false, "ho")
               .hasHost("st")
               .hasQuery(false, "query")
               .hasFragment(false, "fragment");

         var uri2 = DefaultUri.hierarchicalServer(false,
                                                  "scheme",
                                                  "user:pass",
                                                  "ho@st",
                                                  -1,
                                                  "/path",
                                                  "query",
                                                  "fragment");

         assertThat(uri2)
               .hasValue("scheme://user:pass@ho@st/path?query#fragment")
               .isHierarchicalRegistry()
               .hasNoUserInfo()
               .hasNoHost()
               .hasRegistryAuthority(true, "user:pass@ho@st")
               .hasQuery(true, "query")
               .hasFragment(true, "fragment");
      }

      @Test
      public void encodingUserInfoWhenContainArobase() {
         var uri = DefaultUri.hierarchicalServer(false,
                                                 "scheme",
                                                 "u@ser",
                                                 "host",
                                                 -1,
                                                 "/path",
                                                 "query",
                                                 "fragment");

         assertThat(uri)
               .hasValue("scheme://u%40ser@host/path?query#fragment")
               .isHierarchicalServer()
               .hasUserInfo(true, "u%40ser")
               .hasServerAuthority(true, "u%40ser@host")
               .hasQuery(true, "query")
               .hasFragment(true, "fragment");
      }

      @Test
      public void encodingQueryUri() {
         var uri = DefaultUri.hierarchicalServer(false,
                                                 "scheme",
                                                 "host",
                                                 "/path",
                                                 "uri1="
                                                 + "http://query1?k1a=v1a&k1b=v1b#f1"
                                                 + "&uri2="
                                                 + "http://query2?k2a=v2a&k2b=v2b#f2",
                                                 "fragment");

         assertThat(uri)
               .hasValue(
                     "scheme://host/path?uri1=http://query1?k1a=v1a&k1b=v1b%23f1&uri2=http://query2?k2a=v2a&k2b=v2b%23f2#fragment")
               .hasQuery(true,
                         "uri1=http://query1?k1a=v1a&k1b=v1b%23f1&uri2=http://query2?k2a=v2a&k2b=v2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "fragment");
      }

      @Test
      public void preserveEncodingOnUpdate() {
         var uri = DefaultUri
               .hierarchicalServer(true,
                                   "scheme",
                                   "u%23ser:p%23ass",
                                   "host",
                                   -1,
                                   "/p%23ath",
                                   "uri1=" + encode("http://query1?k1a=v1a&k1b=v1b#f1") + "&uri2=" + encode(
                                         "http://query2?k2a=v2a&k2b=v2b#f2"),
                                   "f%23ragment")
               .scheme("other");

         assertThat(uri)
               .hasValue(
                     "other://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#f%23ragment")
               .hasScheme("other")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void preserveEncodingOnNormalize() {
         var uri = DefaultUri
               .hierarchicalServer(true,
                                   "scheme",
                                   "u%23ser:p%23ass",
                                   "host",
                                   -1,
                                   "/p%23ath",
                                   "uri1=" + encode("http://query1?k1a=v1a&k1b=v1b#f1") + "&uri2=" + encode(
                                         "http://query2?k2a=v2a&k2b=v2b#f2"),
                                   "f%23ragment")
               .normalize();

         assertThat(uri)
               .hasValue(
                     "scheme://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void preserveEncodingOnResolveFull() {
         var uri = DefaultUri.hierarchicalServer(true,
                                                 "scheme",
                                                 "u%23ser:p%23ass",
                                                 "host",
                                                 -1,
                                                 "/p%23ath",
                                                 "uri1="
                                                 + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                                 + "&uri2="
                                                 + encode("http://query2?k2a=v2a&k2b=v2b#f2"),
                                                 "f%23ragment");

         uri = uri.resolve(uri);

         assertThat(uri)
               .hasValue(
                     "scheme://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#f%23ragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "f%23ragment")
               .hasFragment(false, "f#ragment");
      }

      @Test
      public void preserveEncodingOnResolvePartial() {
         var uri = DefaultUri.hierarchicalServer(true,
                                                 "scheme",
                                                 "u%23ser:p%23ass",
                                                 "host",
                                                 -1,
                                                 "/p%23ath",
                                                 "uri1="
                                                 + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                                 + "&uri2="
                                                 + encode("http://query2?k2a=v2a&k2b=v2b#f2"),
                                                 "f%23ragment");

         uri = uri.resolve(DefaultUri.ofUri("#fragment"));

         assertThat(uri)
               .hasValue(
                     "scheme://u%23ser:p%23ass@host/p%23ath?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#fragment")
               .hasScheme("scheme")
               .hasUserInfo(true, "u%23ser:p%23ass")
               .hasUserInfo(false, "u#ser:p#ass")
               .hasHost("host")
               .hasPath(true, "/p%23ath")
               .hasPath(false, "/p#ath")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "fragment")
               .hasFragment(false, "fragment");
      }

      @Test
      public void ofUriWhenEncodedQueryUri() {
         var uri = DefaultUri.ofUri("scheme://host?uri1="
                                    + encode("http://query1?k1a=v1a&k1b=v1b#f1")
                                    + "&uri2="
                                    + encode("http://query2?k2a=v2a&k2b=v2b#f2")
                                    + "#fragment");

         assertThat(uri)
               .hasValue(
                     "scheme://host?uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2#fragment")
               .hasQuery(true,
                         "uri1=http%3A%2F%2Fquery1%3Fk1a%3Dv1a%26k1b%3Dv1b%23f1&uri2=http%3A%2F%2Fquery2%3Fk2a%3Dv2a%26k2b%3Dv2b%23f2")
               .hasQuery(false, "uri1=http://query1?k1a=v1a&k1b=v1b#f1&uri2=http://query2?k2a=v2a&k2b=v2b#f2")
               .hasFragment(true, "fragment");
      }

   }

}