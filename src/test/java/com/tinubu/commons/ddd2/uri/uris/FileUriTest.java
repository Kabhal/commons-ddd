/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.uri.uris;

import static com.tinubu.commons.ddd2.test.uri.UriAssert.assertThat;
import static com.tinubu.commons.ddd2.uri.Uri.uri;
import static com.tinubu.commons.ddd2.uri.uris.FileUri.FileUriRestrictions.noRestrictions;
import static com.tinubu.commons.ddd2.uri.uris.FileUri.FileUriRestrictions.ofDefault;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.nio.file.Path;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.uri.IncompatibleUriException;
import com.tinubu.commons.ddd2.uri.InvalidUriException;
import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.ddd2.uri.uris.FileUri.FileUriRestrictions;

public class FileUriTest {

   static {
      Assertions.setMaxStackTraceElementsDisplayed(50);
   }

   @Test
   public void fsUriWhenBadArguments() {
      assertThatThrownBy(() -> FileUri.ofUri((String) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> FileUri.ofUri((URI) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
      assertThatThrownBy(() -> FileUri.ofUri((Uri) null))
            .isExactlyInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'uri' must not be null");
   }

   @Nested
   class OfPath {

      @Test
      public void ofPathWhenNominal() {
         assertThat(FileUri.ofPath(Path.of("/path"))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
         });
      }

      @Test
      public void ofPathWhenBadArguments() {
         assertThatThrownBy(() -> FileUri.ofPath(null))
               .isExactlyInstanceOf(InvariantValidationException.class)
               .hasMessage("Invariant validation error > 'path' must not be null");
      }

      @Test
      public void ofPathWhenEmptyPath() {
         assertThat(FileUri.ofPath(Path.of(""))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of(""));
         });
         assertThatThrownBy(() -> FileUri.ofPath(Path.of("")).fileRestrictions(o -> o.emptyPath(true)))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage("Invalid '' URI: 'path' must not be empty");
      }

      @Test
      public void ofPathWhenRelativePath() {
         assertThat(FileUri.ofPath(Path.of("path"))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "path");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("path"));
         });
         assertThatThrownBy(() -> FileUri.ofPath(Path.of("path")).fileRestrictions(o -> o.relativePath(true)))
               .isExactlyInstanceOf(IncompatibleUriException.class)
               .hasMessage(
                     "Incompatible 'path' URI: 'path=SimplePath[path=path, encodedPath=path]' must be absolute");
      }
   }

   @Nested
   class HierarchicalUri {

      @Test
      public void fsUriWhenNominal() {
         assertThat(FileUri.ofUri("file:/path")).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void fsUriWhenRoot() {
         assertThat(FileUri.ofUri("file:/")).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/"));
         });
      }

      @Test
      public void fsUriWhenEmptyHierarchical() {
         assertThat(FileUri.ofUri("file:///path")).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void fsUriWhenFragment() {
         assertThatThrownBy(() -> FileUri.ofUri("file:/path#fragment"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/path#fragment' URI: 'FileUri[value=file:/path#fragment,restrictions=FileUriRestrictions[relativeFormat=true, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path', encodedSchemeSpecific='/path'],authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         assertThat(FileUri.ofUri("file:/path#fragment", noRestrictions())).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasFragment(false, "fragment");
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void fsUriWhenQuery() {
         assertThatThrownBy(() -> FileUri.ofUri("file:/path?query"))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid 'file:/path?query' URI: 'FileUri[value=file:/path?query,restrictions=FileUriRestrictions[relativeFormat=true, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=SimpleScheme[scheme='file'],schemeSpecific=SimpleSchemeSpecific[schemeSpecific='/path?query', encodedSchemeSpecific='/path?query'],authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         assertThat(FileUri.ofUri("file:/path?query", noRestrictions())).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasQuery(false, "query");
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }
   }

   @Nested
   class RelativeUri {

      @Test
      public void fsUriWhenNominal() {
         assertThat(FileUri.ofUri("/path", noRestrictions())).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void fsUriWhenRoot() {
         assertThat(FileUri.ofUri("/", noRestrictions())).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/"));
         });
      }

      @Test
      public void fsUriWhenEmptyHierarchical() {
         assertThat(FileUri.ofUri("///path", noRestrictions())).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void fsUriWhenQuery() {
         assertThatThrownBy(() -> FileUri.ofUri("/path?query", ofDefault().relativeFormat(false)))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '/path?query' URI: 'FileUri[value=/path?query,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=SimpleQuery[query=query, encodedQuery=query],fragment=<null>]' must not have a query");
         assertThat(FileUri.ofUri("/path?query", noRestrictions())).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasQuery(false, "query");
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }

      @Test
      public void fsUriWhenFragment() {
         assertThatThrownBy(() -> FileUri.ofUri("/path#fragment", ofDefault().relativeFormat(false)))
               .isExactlyInstanceOf(InvalidUriException.class)
               .hasMessage(
                     "Invalid '/path#fragment' URI: 'FileUri[value=/path#fragment,restrictions=FileUriRestrictions[relativeFormat=false, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=/path, encodedPath=/path],query=<null>,fragment=SimpleFragment[fragment=fragment, encodedFragment=fragment]]' must not have a fragment");
         assertThat(FileUri.ofUri("/path#fragment", noRestrictions())).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path");
            assertThat(uri).hasFragment(false, "fragment");
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
      }
   }

   @Test
   public void fsUriWhenIncompatibleUri() {
      assertThatThrownBy(() -> FileUri.ofUri("other:/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'other:/path' URI:", "scheme must be equal to 'file'");
      assertThatThrownBy(() -> FileUri.ofUri("file://host/path"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'file://host/path' URI:",
                                     "must be empty hierarchical server-based");
      assertThatThrownBy(() -> FileUri.ofUri("file://host"))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible 'file://host' URI:",
                                     "must be empty hierarchical server-based");
      assertThatThrownBy(() -> FileUri.ofUri("//host/path", noRestrictions()))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible '//host/path' URI:",
                                     "must be empty hierarchical server-based");
      assertThatThrownBy(() -> FileUri.ofUri("//host", noRestrictions()))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessageContainingAll("Incompatible '//host' URI:", "must be empty hierarchical server-based");
   }

   @Test
   public void fsUriWhenInvalidUri() {
      assertThatThrownBy(() -> FileUri.ofUri("file:"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'file:' URI: Expected scheme-specific part at index 5: file:");
      assertThatThrownBy(() -> FileUri.ofUri("file://"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid 'file://' URI: Expected authority at index 7: file://");
      assertThatThrownBy(() -> FileUri.ofUri("//"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '//' URI: Expected authority at index 2: //");

      assertThatThrownBy(() -> FileUri.ofUri("file:/path#fragment"))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessageContainingAll("Invalid 'file:/path#fragment' URI:", "must not have a fragment");
      assertThatThrownBy(() -> FileUri.ofUri("/path#fragment", ofDefault().relativeFormat(false)))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessageContainingAll("Invalid '/path#fragment' URI:", "must not have a fragment");
   }

   @Test
   public void fsUriWhenRelative() {
      assertThatThrownBy(() -> FileUri.ofUri("path", ofDefault().relativeFormat(true).relativePath(true)))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage(
                  "Incompatible 'path' URI: 'FileUri[value=path,restrictions=FileUriRestrictions[relativeFormat=true, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=path, encodedPath=path],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'");
      assertThatThrownBy(() -> FileUri.ofUri("path", ofDefault().relativeFormat(false).relativePath(true)))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage(
                  "Incompatible 'path' URI: 'path=SimplePath[path=path, encodedPath=path]' must be absolute");
      assertThat(FileUri.ofUri("path",
                               ofDefault().relativeFormat(false).relativePath(false))).satisfies(uri -> {
         assertThat(uri).hasPath(false, "path");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("path"));
      });
   }

   @Test
   public void fsUriWhenEmpty() {
      assertThatThrownBy(() -> FileUri.ofUri("",
                                             FileUriRestrictions
                                                   .ofDefault()
                                                   .relativeFormat(true)
                                                   .relativePath(true)
                                                   .emptyPath(true)))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage(
                  "Incompatible '' URI: 'FileUri[value=,restrictions=FileUriRestrictions[relativeFormat=true, relativePath=true, emptyPath=true, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=, encodedPath=],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'");
      assertThatThrownBy(() -> FileUri.ofUri("",
                                             FileUriRestrictions
                                                   .ofDefault()
                                                   .relativeFormat(true)
                                                   .relativePath(false)
                                                   .emptyPath(true)))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage(
                  "Incompatible '' URI: 'FileUri[value=,restrictions=FileUriRestrictions[relativeFormat=true, relativePath=false, emptyPath=true, query=true, fragment=true],scheme=<null>,schemeSpecific=<null>,authority=<null>,path=SimplePath[path=, encodedPath=],query=<null>,fragment=<null>]' must be absolute, and scheme must be equal to 'file'");
      assertThatThrownBy(() -> FileUri.ofUri("",
                                             FileUriRestrictions
                                                   .ofDefault()
                                                   .relativeFormat(false)
                                                   .relativePath(true)
                                                   .emptyPath(true)))
            .isExactlyInstanceOf(IncompatibleUriException.class)
            .hasMessage("Incompatible '' URI: 'path=SimplePath[path=, encodedPath=]' must be absolute");
      assertThatThrownBy(() -> FileUri.ofUri("",
                                             FileUriRestrictions
                                                   .ofDefault()
                                                   .relativeFormat(false)
                                                   .relativePath(false)
                                                   .emptyPath(true)))
            .isExactlyInstanceOf(InvalidUriException.class)
            .hasMessage("Invalid '' URI: 'path' must not be empty");
      assertThat(FileUri.ofUri("",
                               FileUriRestrictions
                                     .ofDefault()
                                     .relativeFormat(false)
                                     .relativePath(true)
                                     .emptyPath(false))).satisfies(uri -> {
         assertThat(uri).hasPath(false, "");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.toPath()).isEqualTo(Path.of(""));
      });
      assertThat(FileUri.ofUri("",
                               FileUriRestrictions
                                     .ofDefault()
                                     .relativeFormat(false)
                                     .relativePath(false)
                                     .emptyPath(false))).satisfies(uri -> {
         assertThat(uri).hasPath(false, "");
         assertThat(uri).hasNoQuery();
         Assertions.assertThat(uri.toPath()).isEqualTo(Path.of(""));
      });
   }

   @Nested
   class NormalizeTest {

      @Test
      public void normalizeWhenNominal() {
         assertThat(FileUri.ofUri("file:/path/sub/../.").normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path/");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
         assertThat(FileUri.ofUri("file:/path/../sub").normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/sub");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/sub"));
         });
      }

      @Test
      public void normalizeWhenNoScheme() {
         assertThat(FileUri.ofUri("/path/sub/../.", noRestrictions()).normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/path/");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/path"));
         });
         assertThat(FileUri.ofUri("/path/../sub", noRestrictions()).normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "/sub");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("/sub"));
         });
      }

      @Test
      public void normalizeWhenRelative() {
         assertThat(FileUri.ofUri("path/sub/../.", noRestrictions()).normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "path/");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("path"));
         });
         assertThat(FileUri.ofUri("path/../sub", noRestrictions()).normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "sub");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("sub"));
         });
      }

      @Test
      public void normalizeWhenEmpty() {
         assertThat(FileUri.ofUri("", noRestrictions()).normalize()).satisfies(uri -> {
            assertThat(uri).hasPath(false, "");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of(""));
         });
      }

   }

   @Nested
   class RelativizeTest {

      @Test
      public void relativizeWhenNominal() {
         assertThat(FileUri
                          .ofUri("file:/path", noRestrictions())
                          .relativize(uri("file:/path/sub"))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "sub");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("sub"));
         });
      }

      @Test
      public void relativizeWhenNoScheme() {
         assertThat(FileUri.ofUri("/path", noRestrictions()).relativize(uri("/path/sub"))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "sub");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("sub"));
         });
      }

      @Test
      public void relativizeWhenRelative() {
         assertThat(FileUri.ofUri("path", noRestrictions()).relativize(uri("path/sub"))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "sub");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of("sub"));
         });
      }

      @Test
      public void relativizeWhenEqual() {
         assertThat(FileUri
                          .ofUri("file:/path", noRestrictions())
                          .relativize(uri("file:/path"))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of(""));
         });
         assertThat(FileUri.ofUri("path", noRestrictions()).relativize(uri("path"))).satisfies(uri -> {
            assertThat(uri).hasPath(false, "");
            assertThat(uri).hasNoQuery();
            Assertions.assertThat(uri.toPath()).isEqualTo(Path.of(""));
         });
      }

   }
}