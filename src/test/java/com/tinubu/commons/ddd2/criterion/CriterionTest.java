package com.tinubu.commons.ddd2.criterion;

import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.BETWEEN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.DEFINED;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.IN;
import static com.tinubu.commons.ddd2.criterion.Criterion.Flag.IGNORE_CASE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.criterion.Criterion.Flags;
import com.tinubu.commons.ddd2.domain.specification.OrSpecification;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

class CriterionTest {

   @Test
   public void testCriterionWhenBadTypeForOperator() {
      Criterion<Integer> c = CriterionBuilder.startWith(32);
   }

   @Test
   public void testCriterionBuilder() {
      Criterion<Integer> criterion1 = CriterionBuilder.equal(33);

      assertThat(criterion1.operator()).isEqualTo(EQUAL);
      assertThat(criterion1.operands()).containsExactly(33);
      assertThat(criterion1.flags().flags()).isEmpty();

      Criterion<Integer> criterion2 = CriterionBuilder.equal(Flags.of(IGNORE_CASE), 33);

      assertThat(criterion2.operator()).isEqualTo(EQUAL);
      assertThat(criterion2.operands()).containsExactly(33);
      assertThat(criterion2.flags().flags()).containsExactly(IGNORE_CASE);
   }

   @Test
   public void testCriterionValidationWhenDefaultOperator() {
      assertThat(new CriterionBuilder<>().operator(null).build()).isEqualTo(new CriterionBuilder<>()
                                                                                  .operator(DEFINED)
                                                                                  .build());
      assertThat(new CriterionBuilder<String>()
                       .operator(null)
                       .operands("value")
                       .build()).isEqualTo(new CriterionBuilder<String>()
                                                 .operator(EQUAL)
                                                 .operands("value")
                                                 .build());
      assertThat(new CriterionBuilder<String>()
                       .operator(null)
                       .operands("value1", "value2")
                       .build()).isEqualTo(new CriterionBuilder<String>()
                                                 .operator(IN)
                                                 .operands("value1", "value2")
                                                 .build());
   }

   @Test
   public void testCriterionValidationWhenUnary() {
      assertThat(new CriterionBuilder<>().operator(DEFINED).build()).isNotNull();
      assertThatThrownBy(() -> new CriterionBuilder<String>().operator(DEFINED).operands("value").build())
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [Criterion[operator=DEFINED,operands=[value],flags=Flags[]]] > 'operator=DEFINED' unary operator must not have extra operands");
   }

   @Test
   public void testCriterionValidationWhenBinary() {
      assertThat(new CriterionBuilder<String>().operator(EQUAL).operands("value").build()).isNotNull();
      assertThatThrownBy(() -> new CriterionBuilder<>().operator(EQUAL).build())
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [Criterion[operator=EQUAL,operands=[],flags=Flags[]]] > 'operator=EQUAL' binary operator must have exactly one extra operand");
   }

   @Test
   public void testCriterionValidationWhenNary() {
      assertThat(new CriterionBuilder<String>()
                       .operator(IN)
                       .operands("value1", "value2")
                       .build()).isNotNull();
   }

   @Test
   public void testCriterionValidationWhenTernary() {
      assertThat(new CriterionBuilder<Integer>().operator(BETWEEN).operands(1, 10).build()).isNotNull();
      assertThatThrownBy(() -> new CriterionBuilder<>().operator(BETWEEN).build())
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [Criterion[operator=BETWEEN,operands=[],flags=Flags[]]] > 'operator=BETWEEN' ternary operator must have exactly two extra operand");
      assertThatThrownBy(() -> new CriterionBuilder<String>().operator(BETWEEN).operands("value").build())
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [Criterion[operator=BETWEEN,operands=[value],flags=Flags[]]] > 'operator=BETWEEN' ternary operator must have exactly two extra operand");
      assertThatThrownBy(() -> new CriterionBuilder<Integer>().operator(BETWEEN).operands(1, 10, 20).build())
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [Criterion[operator=BETWEEN,operands=[1, 10, 20],flags=Flags[]]] > 'operator=BETWEEN' ternary operator must have exactly two extra operand");
   }

   @Test
   public void testCriterionValidationWhenFlags() {
      assertThat(new CriterionBuilder<>().operator(DEFINED).build()).isNotNull();
      assertThat(new CriterionBuilder<>().operator(DEFINED).flags(IGNORE_CASE).build()).isNotNull();
   }

   @Test
   public void testCriterionCompositeSpecification() {
      assertThat(CriterionBuilder.equal(3).or(CriterionBuilder.greater(10))).isEqualTo(new OrSpecification<>(
            Arrays.asList(CriterionBuilder.equal(3), CriterionBuilder.greater(10))));
   }
}