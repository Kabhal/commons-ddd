= commons-ddd
:gitlab-url: https://gitlab.com
:default-branch: master
:project-path: tinubu-public/commons-ddd
:toc:

image:{gitlab-url}/{project-path}/badges/{default-branch}/pipeline.svg[link="{gitlab-url}/{project-path}/-/commits/{default-branch}",title="pipeline status"]
image:{gitlab-url}/{project-path}/badges/{default-branch}/coverage.svg[link="{gitlab-url}/{project-path}/-/commits/{default-branch}",title="coverage report"]

[[domain]]
== Domain objects

[[invariant]]
== Invariant framework

The objective of this framework is to propose a complete validation framework with multiple approaches.

The framework comes with a complete <<invariant-rules-library,library of validation assertions>>.

Framework features :

* Fluent, readable invariant rule DSL
* Powerful invariant rule DSL with compositing (`andRule`/`orRule`), mapping (`map`), conditional evaluation (`ifSatisfies`), lazy parameters
* Supports deferred validation to capture all validation errors at the same time
* Supports notification pattern, so that validation errors are added to `InvariantResults` class instead of throwing exceptions
* Supports specification pattern
* Dedicated exception system with rich information (no more `NullPointerException`, `IllegalArgumentException`)
* Reusable invariant rules that can be applied to any object
* Powerful validation messages with good defaults and value referencing system.
Supports alternative messaging formats (String format, Message format, ...)
* Supports <<invariant-use-naming>>, <<invariant-use-groups>> and <<invariant-context>>

[[invariant-use]]
=== Use cases and usage models

[[invariant-use-inlined]]
==== Classic inlined usage

You can juste validate an object with inlined validation code.

[source,java]
.Validate myObject before assignment or *throw a `InvariantValidationException`*
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

class MyClass {
   void myMethod() {
      this.myObject = validate(myObject, isNotNull()).orThrow();
   }
}
----

Passing parameter name (`"myObject"`) is optional but is advised for more comprehensive validation error messages.

[source,java]
.Validate *optional* myString before assignment, using compositing
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;

class MyClass {
   void myMethod() {
      this.myString = validate(myString, isNull().orValue(isNotBlank())).orThrow();
   }
}
----

[source,java]
.Validate optional myList of type list has size > 3, using *advanced compositing*
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.containsExactly;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThan;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.keys;

class MyClass {
   void myMethod() {
      this.myList = validate(myList, isNull().orValue(size(isGreaterThan(value(3))))).orThrow();
      this.myMap = validate(myMap, keys(containsExactly(value(List.of("index1", "index2"))))).orThrow();
   }
}
----

This invariant notation style is an inlined notation for the equivalent code :

[source,java]
.Validate optional myList not using inlined notation
----
import com.tinubu.commons.ddd2.invariant.Invariant;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThan;

class MyClass {
   void myMethod() {
      this.myList = Invariant.of(() -> myList,
                                 isNull().orValue(size(isGreaterThan(value(3)))))
                             .validate().orThrow();
   }
}
----

[[invariant-use-deferred]]
==== Deferred validation

It's better to evaluate all validations at the same time so that error reporting is more complete.
We have to use some deferring mechanisms.

.Deferred validation using inlined notation
====
[source,java]
----

import com.tinubu.commons.ddd2.invariant.InvariantResults;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.*;

class MyClass {
   void myMethod() {
      InvariantResults results = InvariantResults.empty();
      this.myString = validate(myString, isNull().orValue(isNotBlank())).defer(results);
      this.myList = validate(myList, isNull().orValue(size(isGreaterThan(value(3))))).defer(results);

      results.orThrow();
   }
}
----
====

CAUTION: The validation does *not* stop at first validation error, so that the final result will contain all invariants results, *but* you have to take care about data-correctness assumptions in the meantime.

.Equivalent invariant validation using `Invariants`
====
[source,java]
----
import com.tinubu.commons.ddd2.invariant.Invariants;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.*;

class MyClass {
   void myMethod() {
      Invariants invariants = Invariants.of(
              Invariant.of(() -> myBoolean, isTrue()).objectName("myBoolean"),
              Invariant.of(() -> myList, size(isGreaterThan(3))).objectName("myList"),
              Invariant.of(() -> myArray, list(isNotEmpty())).objectName("myArray"), // <1>
              Invariant.of(() -> myString, isNull().orValue(isNotBlank())).objectName("myString"));

      invariants.validate().orThrow();
   }
}
----

<1> Java arrays are supported, collection rules apply on them after they are converted to Collection using `ArrayRules::collection`.

NOTE: Validating object evaluation is always lazy when not using short notation in `Invariant`.
====

In both cases, the resulting `InvariantResults` is a list of `InvariantResult` that can be analyzed for faulty invariant.
The result is in success if all invariant rules are successful, and can be checked using `InvariantResults::success()`.

It can throw an exception on any validation error using `InvariantResults::orThrow()` :

.Resulting exception message
....
Invariant validation error : 'myBoolean' must be true | 'myList.size=1' must be greater than '3' | {myArray} must not be empty | 'myString' must not be blank
....

[[invariant-use-naming]]
==== Invariant naming

The framework optionally supports naming for various objects :

- invariant
- invariant rule
- parameter
- validating object

These namings are used to generate validation message and are available in result.

An invariant can optionally define a unique name that will be available in result to later identify failed invariants.

[source,java]
.Defines invariants with names
----
import com.tinubu.commons.ddd2.invariant.Invariants;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.*;

class MyClass {
   void myMethod() {
      Invariants invariants = Invariants.of(
              Invariant.of(() -> myBoolean, isTrue()).name("check-myBoolean"), // <1>
              Invariant.of(() -> myList, size(isGreaterThan(value(3)))).name("check-myList")); // <2>

      invariants.validate().orThrow();
   }
}
----

<1> Invariant name is `check-myBoolean`
<2> Invariant name is `check-myList`

Each rule can be contextualized with a _rule name_ and _rule parameters_.
Rule parameters can be back-referenced later in validation message.

[source,java]
.Set a rule name and rule parameters in rule context
----
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.*;
import static com.tinubu.commons.ddd2.invariant.MessageValue.*;

class MyRules {
    public static <T> InvariantRule<T> customEqual(final ParameterValue<Object> value) {
      notNull(value, "value");

      return isNotNull()
              .andValue(satisfiesValue(v -> Objects.equals(v, value.value()),
                                      StringFormat.of("%s must be equal to %s",
                                          validatingObject(),
                                          parameter(0)))
                             .ruleContext("customEqual", value)); // <1>
   }
}
----

<1> Rule most specific function is contextualized with a name and the current rule parameters.

[CAUTION]
====
The call to `InvariantRule::ruleContext` must be done on the most specific invariant rule, i.e. the one that declares the `MessageFormatter`.
====

If the rule takes some parameters and use `ParameterValue`-typed parameters, you can name the parameters.

[source,java]
.Calls previous rule with named parameter and named object
----
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;

class MyClass {
      void myMethod() {
          Invariants invariants = Invariants.of(
                  Invariant.of(() -> myObject,
                               "myObject", // <1>
                               customEqual(value(32, "myParameter")))); // <2>

          invariants.validate().orThrow();
   }
}
----

<1> Named object `myObject` will appear in result and validation message automatically
<2> Named parameter `myParameter` will appear in result and validation message automatically

[[invariant-use-groups]]
==== Invariant groups validation

Each invariant can be associated to [0, n] arbitrary groups.
You can later choose to evaluate all groups or any list of groups.

[source,java]
.Validates invariants using groups
----
import com.tinubu.commons.ddd2.invariant.Invariants;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.*;

class MyClass {
   void myMethod() {
      Invariants invariants = Invariants.of(
              Invariant.of(() -> myBoolean, isTrue()).groups("myBoolean", "group1"), // <1>
              Invariant.of(() -> myList, size(isGreaterThan(value(3)))).groups("myList", "group1"), // <2>
              Invariant.of(() -> myString, isNull().orValue(isNotBlank()))); // <3>

      invariants.validate().orThrow(); // <4>
      invariants.validate("group1", "myList").orThrow(); // <5>
   }
}
----

<1> Invariant is associated to `myBoolean` and `group1` groups
<2> Invariant is associated to `myList` and `group1` groups
<3> Invariant is associated to default group (`<default>`)
<4> Validates all groups
<5> Validates only `group1` and `myList` groups.
If an invariant is in both groups, it is evaluated only one time

[[invariant-context]]
==== Invariant validation context

When validating many objects, it is important to uniquely identify the failing object.
You can provide an arbitrary context to the validation framework so that the context will be available in validation result.

.Add a validation context to invariants
====
[source,java]
----
import com.tinubu.commons.ddd2.invariant.Invariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.*;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.*;

class MyClass {
   void myMethod() {
      Invariants invariants = Invariants.of(
              Invariant.of(() -> myBoolean, isTrue()).objectName("myBoolean"),
              Invariant.of(() -> myList, size(isGreaterThan(3))).objectName("myList"))
         .context(myObjectId, anyContextObject); // <1>

      invariants.validate().orThrow();
   }
}
----

<1> Optional context to help to contextualize the validation error.
Context can contain an arbitrary list of objects

.Resulting exception message
....
Invariant validation error in [StringId[value=00023], AnyContextObject[]] context : {myBoolean} must be true | {myList.size=1} must be greater than '3'
....
====

[[invariant-rules-library]]
=== Rules library

A complete library set is provided :

* `ArrayRules` : array rules
* `BaseRules` : nullability, boolean rules
* `CalendarRules` : `Date` rules (instant comparison : before, after, equal, ...).
* `ClassRules` : class type rules
* `CollectionRules` : `Collection` rules
* `ComparableRules` : `Comparable` rules
* `CriterionRules` : rules satisfying a `Criterion`
* `DateRules` : `Date` rules (instant comparison : before, after, equal, ...).
Use `TemporalRules` with date's instant Use `TemporalRules` with calendar's instant
* `DddRules` : DDD domain object rules
* `DoubleRules` : special `Double` rules (NaN, ...)
* `EqualsRules` : equality rules
* `FileRules` : `File` rules (isFile, isAbsolute, ...)
* `FloatRules `: special `Float` rules (NaN, ...)
* `LocaleRules` : `Locale` rules (country, language, ...)
* `MapRules` : `Map` rules (keys, values, ...)
* `MatchingRules` : pattern matching rules
* `MimeTypeRules` : `MimeType` rules
* `NumberRules`: Number implementations rules (Integer, Long, BigDecimal, ...)
* `OptionalRules` : `Optional` rules (isPresent, optionalValue, ...)
* `PathRules` : `Path` rules (hasRoot, hasParent, fileName, ...)
* `PredicateRules` : rules satisfying a `Predicate`
* `StringRules` : `String` rules (blank, ...)
* `TemporalRules` : `Temporal` (`OffsetDateTime`, `ZoneDateTime`, `LocalDateTime`, `LocalDate`, `Instant`) rules (instant comparison : before, after, equal, ...)
* `ThrowingDelegationRules` : delegates validation to third-party validators throwing exceptions (e.g.: Apache Commons).
* `UriRules` : `URI` rules
* `UuidRules` : `UUID` rules

=== Advanced rules usage

[[invariant-message-formatting]]
=== Message formatting

All <<invariant-rules-library>> and `PredicateInvariantRule` support a default message formatter, but an alternative `MessageFormatter` can be specified.

[source,java]
.Use default message formatter to create a custom validation message with an existing library rule
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

class MyClass {
   void myMethod() {
      validate(myObject, "myObject", isNotNull("myObject must not be null")).orThrow();
    }
}
----

The provided message formatters are :

* `com.tinubu.commons.ddd.invariant.formatter.FixedString` : A fixed string, no positional arguments
* `com.tinubu.commons.ddd.invariant.formatter.StringFormat` : Use `java.util.String::format` with positional arguments
* `com.tinubu.commons.ddd.invariant.formatter.MessageFormat` : Use `java.text.MessageFormat::format` with positional arguments

You can provide your own implementation to extend existing formatters.

The default message formatter is currently `StringFormat` and default locale is `Locale.getDefault(Locale.Category.FORMAT)`.
You can change the defaults using the invariant configuration singleton :

[source,java]
----
import com.tinubu.commons.ddd2.invariant.formatter.InvariantConfiguration;

class MyClass {
    void myInitializationMethod() {
        InvariantConfiguration.messageFormatterDefaultLocale(Locale.FRENCH);
    }
}
----

.Use `MessageFormat` alternative formatter to create a custom validation message with library rules
====
[source,java]
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.MessageValue.*;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormat;

class MyClass {
   void myMethod() {
      validate(myObject, "myObject", isNotNull(MessageFormat.of("{0} must not be null", validatingObjectName()))).orThrow();
   }
}
----
====

When using message formatter supporting optional argument values, each value must implement `MessageValue<T>`.

The following helpers are provided in `MessageValue<T>` :

* `value(Object[, Function])` : A value of any type with an optional mapper
* `validatingObject([Function])` : References actual validating object name and value with an optional mapper
* `validatingObjectName([Function])` : References actual validating object name with an optional mapper
* `validatingObjectValue([Function])` : References actual validating object value with an optional mapper
* `validatingObjectInitialName([Function])` : References initial validating object name, unchanged if any `InvariantRule::map` occurred, with an optional mapper
* `validatingObjectInitialValue([Function])` : References initial validating object value, unchanged if any `InvariantRule::map` occurred, with an optional mapper
* `parameter(ParameterValue, [Function])` : References parameter name and value, with an optional mapper
* `parameter(int, [Function])` : References parameter name and value by index in context, with an optional mapper
* `parameterName(ParameterValue, [Function])` : References parameter name, with an optional mapper
* `parameterName(int, [Function])` : References parameter name by index in context, with an optional mapper
* `parameterValue(ParameterValue, [Function])` : References parameter value, with an optional mapper
* `parameterValue(int, [Function])` : References parameter value by index in context, with an optional mapper

You can use any <<invariant-message-formatting-value-formatter>> as the message value mapping function.

[[invariant-message-formatting-value-formatter]]
==== Message value formatter

You can reuse or create some reusable value formatters to use in message formatters.

You have to create an implementation of `com.tinubu.commons.ddd.invariant.formatter.value.ValueFormatter`.

Available value formatters :

* `ClassValueFormatter` : Uniform formatting for classes
* `IterableValueFormatter` : Value formatter for iterables, the number if items displayed is limited by an optional parameter (100 first items are displayed by default)
* `TemporalInstantValueFormatter` : Uniform formatting for instant in time for all `Temporal`
* `DateInstantValueFormatter` : Same formatting as `TemporalInstantValueFormatter` but for `Date`
* `CalendarInstantValueFormatter` : Same formatting as `TemporalInstantValueFormatter` but for `Calendar`
* `StringValueFormatter` : String formatter (shortener)

[source,java]
.Reference the validating object (myObject) name and value in validation message
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.*;
import com.tinubu.commons.ddd2.valueformatter.IterableValueFormatter;

class MyClass {
   void myMethod() {
      validate(myList, "myList", PredicateInvariantRule.satisfies(Objects::isNull, "%s must be null but is %s", validatingObjectName(), validatingObjectValue(new IterableValueFormatter(50))));
    }
}
----

==== Apply an invariant rule to a mapped value

Sometime, the evaluated object has complex rules on several of its fields.
In this case, you can use local mapping in the composited rules.

[source,java]
.Validate several mapping of the same validating object in rule compositing
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThan;

class MyObject {
  public int value1() { return value1; }
  public int value2() { return value2; }
};

class MyClass {
   void myMethod() {
      validate(myObject, isNotNull() // <1>
                          .andValue(isGreaterThan(3).map(MyObject::value1)) // <2>
                          .andValue(isGreaterThan(4).map(MyObject::value2))) // <3>
       .orThrow();
    }
}
----

<1> `myObject` must not be null
<2> `myObject::value1` must be > 3
<3> `myObject::value2` must be > 3

The validation message automatic generation can be helped by providing a name for the mapping :

[source,java]
----
class MyClass {
   void myMethod() {
      validate(myObject, "myObject", isNotNull()
           .and(isGreaterThan(3).map(MyObject::value1, () -> "myObject.value1"))
           .and(isGreaterThan(4).map(MyObject::value2, () -> "myObject.value2")))
       .orThrow();
    }
}
----

[TIP]
--
You can use `MessageValue::propertyNameMapper` to help generate the mapped value name.

[source,java]
----
import static com.tinubu.commons.ddd2.invariant.MessageValue.propertyNameMapper;

class MyClass {
   void myMethod() {
      validate(myObject, "myObject", isNotNull()
           .and(isGreaterThan(3).map(MyObject::value1, propertyNameMapper("value1")))
           .and(isGreaterThan(4).map(MyObject::value2, propertyNameMapper("value2"))))
       .orThrow();
    }
}
----
--

==== Create your own invariant rules

You reuse and compose existing <<invariant-rules-library>>, or you can create your own invariant rules.

You must provide an implementation of `InvariantRule`, the easiest way is to use `PredicateInvariantRule` helper.

[source,java]
.Creates a rule as a reusable method using `PredicateInvariantRule`
----
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.MessageValue.*;
import com.tinubu.commons.ddd2.invariant.PredicateInvariantRule;

class MyClass {
   public InvariantRule<String> myValidator(MessageFormatter<String> messageFormatter) {
      return isNotNull().andValue( // <1>
                PredicateInvariantRule.notSatisfiesValue(s -> s.isBlank(), messageFormatter)
                                      .ruleContext("ruleName"));
   }

   public <T> InvariantRule<T> myValidator() {
      return myValidator(StringFormat.of("%s must not be blank but is '%s'", validatingObjectName(), validatingObjectValue())); // <2>
   }

   void myMethod() {
      validate(myObject, myValidator());
   }
}
----

<1> You should always ensure the validating object is not null before applying your own validation rule.
<2> When using arguments for messages, you must only use `com.tinubu.commons.ddd.invariant.MessageValue`.
The interface contains helper methods to reference automatically the validating object name or value.

==== Integrates third-party validators

If you want to reuse existing validation rules from third-party validators, you can use the provided helpers from `ThrowingDelegationRules`.
The delegated validators must send an `Exception` on validation errors.
By default, the original exception message will be reused as error message.

[source,java]
.Delegates validation to Apache Commons validators
----
import static com.tinubu.commons.ddd2.invariant.rules.ThrowingDelegationRules.*;

class MyClass {
   void myMethod() {
      validate(myString, satisfiesValue(Validate::notNull));
      validate(myString, satisfies(v -> Validate.notNull(v.mappedValue()), "%s must not be null", v.mappedName()));
   }
}
----

=== Simplified integration with <<domain objects,domain objects>>

The invariant framework has been integrated into domain objects helpers, so that declaring fields invariants is very simple, and it's not required to name again the fields for the validation.

[source,java]
.Integration of invariants in an Entity
----
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.ddd2.invariant.MessageValue.*;

public class User implements Entity<UserId> {
    protected User(UserBuilder builder) {
        this.userId = builder.userId;
        this.userName = builder.userName;
        this.creationDate = builder.creationDate;
        this.updateDate = builder.updateDate;

        checkInvariants(); // <1>
    }

    @Override
    public Fields<User> domainFields() {
        return Fields.<User>builder()
            .field("userId", v -> v.userId, isNotNull()) // <2>
            .field("userName", v -> v.userName, isNull().or(isNotBlank())) // <2>
            .field("creationDate", v -> v.creationDate, isNotNull()) // <2>
            .field("updateDate", v -> v.updateDate, isNotNull()) // <2>
            .invariants(datesInvariantRule()) // <3>
            .build();
    }

    private Invariant<User> datesInvariantRule() { // <4>
        return Invariant.of(() -> this,
                satisfies(user -> !user.updateDate.isBefore(user.creationDate),
                      StringFormat.of("{updateDate=%1$s} must be >= {creationDate=%2$s}",
                                      value(User::updateDate),
                                      value(User::creationDate))))
           .name("date-invariant");
    }
}
----

<1> Validate the User class once in bulk
<2> Declare optional invariant rules on each field.
Invariants are automatically generated with correct object name, grouping and invariant name
<3> If needed, override the `domainInvariants` method to add more complex invariants
<4> Reusable invariant definition comparing several date fields of the `User` domain object

== Build

[[build-git-flow]]
=== Git flow

https://nvie.com/posts/a-successful-git-branching-model/[Git-flow] is a standardized branch model.

We use the following Git flow configuration :

Development branches :

* Development branch : `master` (non default)
* Releases branch : `releases` (non default)

Branches prefixes :

* Feature : `feature/`
* release : `release/`
* hotfix : `hotfix/`
* support : `support/`

Release version tags : `release-` (non default)

[[build-versioning]]
=== Versioning

We use the https://semver.org[Semantic versioning] : `<major>.<minor>.<patch>`, e.g.: `1.0.2`.

When we choose the next version for the project after a release, we always start by incrementing the _<minor>_ number.
The _<patch>_ number is reserved for security/hot-fixes. +
If the development introduces some backwards incompatible changes, we must release the current version, then increment the _<major>_ number and reset the _<minor>_ number to 0 for next version.

[[build-release]]
=== Release procedure

<<build-git-flow,Git flow>> must be configured correctly.

The library release supervisor must follow the procedure :

1. Use `git-flow` to start the release : `git flow release start <release-version>`
2. Bump the current project version and external dependencies that are still in `-SNAPSHOT` on release branch :
* Set release version : `mvn versions:set -DnewVersion=<release-version>`
* External dependencies version must be manually updated
* You *must* run `mvn clean test` successfully
3. Use `git-flow` to finish the release : `git flow release finish <release-version>`
+
CAUTION: Do not push your changes now, before having bumped the development branch version, or library artifacts will be deployed to repositories with release version and will corrupt Maven repository caches on various locations
+
4. Bump the current project version on development branch following the <<build-versioning>> instructions.
+
NOTE: Only the current project version is required to be bumped to next snapshot version, external dependencies should only be bumped when required, and preferably later, in a different commit.
5. Push all your changes, *including the tags*

==== Scripted procedure

A script is available for both release and hotfix procedures, that enforces the release guidelines :

.Release script
[source,bash]
----
release.sh release <release-version> [<resume-step-number>]
----

.Hotfix script
[source,bash]
----
release.sh hotfix <hotfix-version> [<resume-step-number>]
----
